import sbt.Keys.{libraryDependencies, _}
import sbt.Resolver

// set the main class for packaging the main jar
mainClass in (Compile, packageBin) := Some("autoschedule.Runner")

// set the main class for 'sbt run'
mainClass in (Compile, run) := Some("autoschedule.Runner")

ThisBuild / useCoursier := false
lazy val commonSettings = Seq(
	organization := "org.combinators",
	scalaVersion := "2.12.11",
	crossScalaVersions := Seq("2.12.11", scalaVersion.value),

	resolvers ++= Seq(
		Resolver.sonatypeRepo("releases"),
		Resolver.typesafeRepo("releases"),
		Resolver.sonatypeRepo("snapshots"),
		Resolver.typesafeRepo("snapshots")
	),

	headerLicense := Some(HeaderLicense.ALv2("2020", "PG Autoschedule")),

	scalacOptions ++= Seq(
		"-unchecked",
		"-deprecation",
		"-feature",
		"-language:implicitConversions"
	)
)

val osName: SettingKey[String] = SettingKey[String]("osName")

osName := (System.getProperty("os.name") match {
	case name if name.startsWith("Linux") => "linux"
	case name if name.startsWith("Mac") => "mac"
	case name if name.startsWith("Windows") => "win"
	case _ => throw new Exception("Unknown platform!")
})

lazy val root = Project(id = "autoschedule", base = file("."))
	.settings(commonSettings: _*)
	.settings(
		moduleName := "cls-scala-base",
		libraryDependencies ++= Seq(
			"org.combinators" %% "cls-scala" % "2.1.0+7-9e42ea3e",
			"org.scalactic" %% "scalactic" % "3.0.5" % "test",
			"org.scalatest" %% "scalatest" % "3.0.5" % "test",

			"org.projectlombok" % "lombok" % "1.18.12",
			"org.reflections" % "reflections" % "0.9.10",
			"com.novocode" % "junit-interface" % "0.11" % Test,
			"junit" % "junit" % "4.13" % Test,
			"org.xerial" % "sqlite-jdbc" % "3.30.1",

			"com.jfoenix" % "jfoenix" % "9.0.8",
			"org.openjfx" % "javafx-base" % "11-ea+25" classifier osName.value,
			"org.openjfx" % "javafx-controls" % "11-ea+25" classifier osName.value,
			"org.openjfx" % "javafx-fxml" % "11-ea+25" classifier osName.value,
			"org.openjfx" % "javafx-graphics" % "11-ea+25" classifier osName.value,

			"org.uma.jmetal" % "jmetal-core" % "5.9",
			"org.uma.jmetal" % "jmetal-problem" % "5.9",
			"org.uma.jmetal" % "jmetal-algorithm" % "5.9",
			"org.uma.jmetal" % "jmetal-exec" % "5.9",

			"com.fasterxml.jackson.core" % "jackson-databind" % "2.12.1"
		)
	)
