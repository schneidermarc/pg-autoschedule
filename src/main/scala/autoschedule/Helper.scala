package autoschedule

import java.util

import autoschedule.algorithms.Algorithm
import autoschedule.model.MachineEnvironment
import autoschedule.repo.Alpha._
import autoschedule.repo.{AlgorithmListCombinator, IterativeAlgorithmCombinator}
import autoschedule.utils.logging.Logger
import autoschedule.utils.logging.LoggingAnnotation.SkipLogging
import org.combinators.cls.inhabitation.Tree
import org.combinators.cls.interpreter.ReflectedRepository
import org.combinators.cls.types.Type
import org.combinators.cls.types.syntax._

import scala.collection.mutable

object Helper {

  /**
    * Enable/Disable the constraint in the semantic types of the combinators
    */
  var enable_Constraint : Boolean = false

  var algorithmCounter: Int = 0

  /**
   * Creates ProblemClassTyp by a given String
   *
   * @param env of the problem class
   * @return corresponding semanticType for the given string
   */
  def createType(env: MachineEnvironment): Type = {
    env match {
      case MachineEnvironment.JOB_SHOP => JOB_SHOP
      case MachineEnvironment.FLEXIBLE_FLOW_SHOP => FLEXIBLE_FLOW_SHOP
      case MachineEnvironment.FLOW_SHOP => FLOW_SHOP
      case MachineEnvironment.PARALLEL_MACHINES => PARALLEL_MACHINES
      case MachineEnvironment.SINGLE_MACHINE => SINGLE_MACHINE
      case _ => null
    }
  }
  /**
   * Recursively creates and adds combinators to the reflectedRepository
   *
   * incIterCombinator = false
   * If iterCount = 0, only AlgorithmListCombinator('IterationCount0) is added
   * If iterCount > 0, iterCount many IterativeAlgorithmCombinators are added
   * incIterCombinator = true
   * same as incIterCombinator = false for IterativeAlgorithmCombinators, but
   * for each iterCount an AlgorithmListCombinator is added
   *
   * with specific arguments (see IterativeAlgorithmCombinators for an example)
   * The IterativeAlgorithmCombinator-Types are of the form
   * 'IterationCount... (number of the combinator)
   *
   * @param iterCount         indicates, how many IterativeAlgorithmCombinators must
   *                          be added to the reflectedRepository
   * @param incListCombinator enable/disable added AlgorithmListCombinator for all
   *                          iteration counts til iterCount (default: false)
   * @return refreshed reflectedRepository
   */
  def createIterCount(repo : ReflectedRepository[AutoscheduleRepository], iterCount: Int, incListCombinator: Boolean = true): ReflectedRepository[AutoscheduleRepository] = {
    var count = iterCount

    var lastSType = Symbol.apply("IterationCount" + count)

    var newRepo = repo
    newRepo = newRepo.addCombinator(AlgorithmListCombinator(lastSType))

    while (count > 0) {
      newRepo = newRepo.addCombinator(IterativeAlgorithmCombinator(Symbol.apply("IterationCount" + (count - 1)), lastSType))

      count -= 1
      lastSType = Symbol.apply("IterationCount" + count)

      if (incListCombinator) {
        newRepo = newRepo.addCombinator(AlgorithmListCombinator(lastSType))
      }
    }

    newRepo
  }


  /**
   * Analyzes the result tree and sets the corresponding model ids
   *
   * @param tree result tree
   * @param algorithmObjects list of algorithms
   */
  def analyzeTree(tree: Tree, algorithmObjects: util.List[Algorithm], algorithmUIDCounts : mutable.Map[String, Int]): Unit = {
    if (algorithmUIDCounts.contains(tree.name)) {
      algorithmObjects.get(algorithmCounter).modelID = algorithmUIDCounts(tree.name)
      algorithmCounter += 1
    }
    tree.arguments.foreach(elem => analyzeTree(elem, algorithmObjects, algorithmUIDCounts))
  }

  /**
    * Method for logging the exec-time
    * (Copied out of FiniteCombinatoryLogic.scala)
    */
  @SkipLogging def time[R](x: => R) : R = {
    val before = System.currentTimeMillis()
    val forcedResult = x
    val after = System.currentTimeMillis()
    Logger.debug(s" ${after - before}")
    forcedResult
  }
}
