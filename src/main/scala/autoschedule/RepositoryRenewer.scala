package autoschedule

import autoschedule.algorithms.Algorithm
import autoschedule.repo.Alpha._
import autoschedule.repo.Beta._
import org.combinators.cls.inhabitation.{TreeGrammar, prettyPrintTreeGrammar}
import org.combinators.cls.interpreter.ReflectedRepository
import org.combinators.cls.types.Type
import org.combinators.cls.types.syntax._

import java.io.{File, FileOutputStream, ObjectOutputStream}

object RepositoryRenewer {

	var reflectedRepository : ReflectedRepository[AutoscheduleRepository] = _

	def main(args: Array[String]): Unit = {
		renew()
	}

	def renew() : TreeGrammar = {
		println("Renewing Repository")

		reflectedRepository = reflect()

		val alphaList : List[Type] = List(SINGLE_MACHINE, PARALLEL_MACHINES, FLOW_SHOP, FLEXIBLE_FLOW_SHOP, JOB_SHOP)
		val betaSkipList : List[Type] = List(SKIP, SKIP.not)
		val betaPrmuList : List[Type] = List(PRMU, PRMU.not)
		var grammar : TreeGrammar = null

		alphaList.foreach(alpha =>
			betaSkipList.foreach(skip =>
				betaPrmuList.foreach(prmu => {
					val temp = inhabit('Filter(alpha, skip, prmu))

					if (grammar == null)
						grammar = temp
					else
						grammar = grammar ++ temp
				})))

		println(prettyPrintTreeGrammar(grammar))

		write(grammar)
	}

	def reflect() : ReflectedRepository[AutoscheduleRepository] = {
		print("Reflecting Repository")

		val t0 = System.currentTimeMillis()
		val reflectedRepository: ReflectedRepository[AutoscheduleRepository] = new AutoscheduleRepository().reflectedWithConstraints()

		println(", took " + (System.currentTimeMillis() - t0) + "ms")
		reflectedRepository
	}

	def inhabit(ty : Type) : TreeGrammar = {
		print("Starting inhabitation on " + ty.toString)

		val t0 = System.currentTimeMillis()
		val grammar : TreeGrammar  = reflectedRepository.inhabit[List[Algorithm]](ty).grammar

		println(", took " + (System.currentTimeMillis() - t0) + "ms")
		grammar
	}

	def write(grammar : TreeGrammar): TreeGrammar = {
		print("Writing cache")
		val t0 = System.currentTimeMillis()

		val sb = new StringBuilder("src")
		sb += File.separatorChar
		sb ++= "main"
		sb += File.separatorChar
		sb ++= "resources"
		sb += File.separatorChar
		sb ++= "grammarCache"

		val oos = new ObjectOutputStream(new FileOutputStream(sb.mkString))
		oos.writeObject(new TreeGrammarWithHash(grammar, reflectedRepository.combinatorComponents))
		oos.close()

		println(", took " + (System.currentTimeMillis() - t0) + "ms")

		grammar
	}
}
