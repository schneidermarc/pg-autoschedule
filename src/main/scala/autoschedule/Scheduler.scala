package autoschedule

import autoschedule.Helper._
import autoschedule.algorithms.Algorithm
import autoschedule.algorithms.utils.TreeNode
import autoschedule.model.{Constraint, MachineEnvironment}
import autoschedule.repo.Beta._
import autoschedule.utils.logging.Logger
import javafx.beans.property.DoubleProperty
import org.combinators.cls.inhabitation.{TreeGrammar, TreeGrammarEnumeration}
import org.combinators.cls.interpreter.ReflectedRepository.nativeTypeOf
import org.combinators.cls.interpreter.{InhabitationResult, ReflectedRepository}
import org.combinators.cls.types.syntax._
import org.combinators.cls.types.{Intersection, Type}
import shapeless.feat

import java.io.{File, FileInputStream, ObjectInputStream}
import java.util
import scala.collection.JavaConverters._

object Scheduler {

  /**
    * Call this method once in a separate thread at the beginning of everything
    * and you will safe about 3 seconds of your pretty life
    */
  def init(): Unit = {
    Helper.enable_Constraint = true

    // this seems to nothing is happening, but it invokes the repository with all the building stuff of the CLS
    // which can be done separately
    reflectedRepositoryWithConstraints = new AutoscheduleRepository().reflectedWithConstraints()

    val file: File = new File(cachePath)
    if (!file.exists()) {
      cacheGrammar = RepositoryRenewer.renew()
    } else {
      val ois = new ObjectInputStream(new FileInputStream(file))

      try {
        val cache = ois.readObject.asInstanceOf[TreeGrammarWithHash]

        cacheGrammar = if (!cache.compare(reflectedRepositoryWithConstraints.combinatorComponents))
          RepositoryRenewer.renew()
        else
          cache.grammar

      } catch {
        case _: ClassCastException => cacheGrammar = RepositoryRenewer.renew()
      } finally {
        ois.close()
      }
    }
  }

  /** cached tree grammar loaded inside init */
  var cacheGrammar: TreeGrammar = _

  /** path to cached tree grammar */
  val cachePath: String = "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "grammarCache"

  /**
    * ReflectedRepository based on repository
    */
  var reflectedRepositoryWithConstraints: ReflectedRepository[AutoscheduleRepository] = _

  /**
    * Filters repository for applicable algorithms based on machine environment and beta-constraints
    *
    * @param alpha machine environment
    * @return list of algorithms applicable for alpha and betas
    */
  def filter(alpha: MachineEnvironment, beta: util.Set[Constraint]): util.List[Algorithm] = {
    startTime = System.currentTimeMillis()

    //inhabit
    val algorithms: util.List[Algorithm] = new util.ArrayList[Algorithm]()
    val targetTypes: Seq[Type] = Seq(nativeTypeOf[List[Algorithm]],
      'Filter(createType(alpha),
        if (beta.contains(Constraint.SKIP)) SKIP else SKIP.not,
        if (beta.contains(Constraint.PRMU)) PRMU else PRMU.not)
    )

    val targetType: Type = targetTypes.init.foldRight(targetTypes.last) { case (ty, tgt) => Intersection(ty, tgt) }
    val iTerms: feat.Enumeration[List[Algorithm]] = TreeGrammarEnumeration(cacheGrammar, targetType).map(reflectedRepositoryWithConstraints.evalInhabitant2[List[Algorithm]])

    val resultSize: BigInt = iTerms.parts.length

    //read results
    var b = true
    var i = 0
    while (b) {
      try {
        val term: Algorithm = iTerms.index(i).head

        if (algorithms.stream().noneMatch(alg => alg.getClass.equals(term.getClass)))
          algorithms.add(term)


        updateProgress(i, resultSize)
        i += 1
      } catch {
        case _: java.lang.IndexOutOfBoundsException =>
          b = false //break loop

          updateProgress(i, resultSize)
      }
    }
    algorithms
  }

  /**
    * Entrypoint to tell the cls to generate new solutions
    *
    * @param alpha     The problem class as a String
    * @param iterCount Specify how many iterative Algorithm should be used after
    *                  a constructive algorithm has created a solution
    *                  Available options: 0 <= iterCount <= 4
    * @param rootNodes A List of root-TreeNodes
    * @return TreeMap of Strings mapped with a List<Algorithm>
    */
  def run(alpha: MachineEnvironment, iterCount: Int, iterCountLowerOrEqual: Boolean, rootNodes: util.List[TreeNode], progressProperty: DoubleProperty = null): util.TreeMap[String, util.List[Algorithm]] = {
    Helper.enable_Constraint = false
    startTime = System.currentTimeMillis()

    // Initialize result-Map
    val results = new java.util.TreeMap[String, util.List[Algorithm]]()

    // Validate parameters
    if (iterCount < 0)
      return results
    if (rootNodes == null || rootNodes.size() <= 0)
      return results

    // collect dynamicCombinators and renew the repository
    val newRepository = new AutoscheduleRepository()
    val newReflectedRepository = createIterCount(newRepository.reflected(rootNodes.asScala.toList), iterCount, iterCountLowerOrEqual)

    val inhabitationResult: InhabitationResult[List[Algorithm]] = newReflectedRepository.inhabit[List[Algorithm]]('Scheduler(createType(alpha)))

    val resultSize: BigInt = inhabitationResult.size.getOrElse(0)

    var b = true
    var i = 0
    while (b) {
      try {
        val algorithmObjects: util.List[Algorithm] = inhabitationResult.interpretedTerms.index(i).asJava

        // iterate over result tree and set modelID
        algorithmCounter = 0
        inhabitationResult.terms.index(i).arguments.foreach(
          elem => analyzeTree(elem, algorithmObjects, newRepository.algorithmUIDCounts)
        )
        results.put(inhabitationResult.terms.index(i).toString, algorithmObjects)

        updateProgress(i, resultSize, progressProperty)
        i += 1
      } catch {
        case _: java.lang.IndexOutOfBoundsException =>
          b = false //break loop

          updateProgress(i, resultSize, progressProperty)
      }
    }

    results
  }

  val logTimeOffset = 5000 // every 5 seconds log current progress
  var lastTimeLogged: Long = 0
  var startTime: Long = 0

  def updateProgress(i: BigInt, resultSize: BigInt, progressProperty: DoubleProperty = null): Unit = {
    val progress = 1.0 * i.doubleValue() / resultSize.doubleValue()
    if (progressProperty != null) progressProperty.setValue(progress)

    if (i.equals(0)) {
      Logger.system("[··%d%%] CLS started working (%s/%s)".format((progress * 100).toInt, i.toString(), resultSize.toString()))
      lastTimeLogged = System.currentTimeMillis()
    } else if (i.equals(resultSize)) {
      Logger.system("[%3d%%] CLS gave you %s solutions in %sms".format((progress * 100).toInt, i.toString(), (System.currentTimeMillis - startTime).toString))
    } else if (System.currentTimeMillis() - lastTimeLogged > logTimeOffset) {
      val pString = (progress * 100).toInt.toString
      Logger.system("[%s%%] CLS is still generating solutions (%s/%s)".
        format(("···" + pString).substring(pString.length), i.toString(), resultSize.toString()))
      lastTimeLogged = System.currentTimeMillis()
    }
  }
}


