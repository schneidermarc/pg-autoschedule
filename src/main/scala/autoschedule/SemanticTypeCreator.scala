package autoschedule
import org.combinators.cls.types.{Intersection, Type}

abstract class SemanticTypeCreator {

  def createSemanticType(baseType : Type, constraints : Type* ) : Type = {
    if( Helper.enable_Constraint) constraints.foldLeft(baseType) { case (ty, constraint) => Intersection(ty, constraint) } else baseType
  }
}
