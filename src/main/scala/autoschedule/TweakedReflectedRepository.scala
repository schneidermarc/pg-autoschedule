//package autoschedule
//
//import org.combinators.cls.interpreter.{CombinatorInfo, DynamicCombinatorInfo, ReflectedRepository, StaticCombinatorInfo, combinator}
//import org.combinators.cls.inhabitation.Tree
//import org.combinators.cls.types.{Type, _}
//import scala.reflect.NameTransformer
//import scala.reflect.runtime.universe
//import scala.reflect.runtime.universe._
//
//trait TweakedReflectedRepository[R] extends ReflectedRepository[R] {
//
//	/** Interprets `inhabitant` as an instance of type `T`.
//		* Performs reflection based on the reflected repository instance
//		* In case of the dynamic combinators, usage of the saved dynamic combinator instances
//		* inside a DynamicCombinatorInfo
//		* Actually, no reason to reconstructs the shape (empty of no argument list) of the
//		* apply method from `combinatorComponents`.
//		*
//		* @param inhabitant the inhabitant to interpret.
//		* @return the interpreted inhabitant.
//		*/
//	def evalInhabitant2[T](inhabitant: Tree): T = {
//		val tb = this.tb
//		def toTermName(name: String) =
//			TermName(NameTransformer.encode(name))
//		def toCombinatorInstanceTree(info: CombinatorInfo): MethodMirror =
//			info match {
//				case StaticCombinatorInfo(name, _, _, _, _) =>
//					// get the signature of the combinator by its name
//					val typeSig = typeTag.in(tb.mirror).tpe.member(toTermName(name))
//
//					/*----------
//					INFO: If trick casting doesn't work any longer, this code line can be used to get an instance
//					of the combinator:
//
//					val combinatorInstance = repoInstance.reflectModule(typeSig.asModule.asInstanceOf[ModuleSymbol]).instance
//
//					For creating a repoInstance see the following lines of code, or look at the INFO
//					inside of function 'staticCombinatorInfoFor':
//
//					val tb = this.tb
//					val clazzMirror = tb.mirror.reflectClass(typeTag.in(tb.mirror).tpe.typeSymbol.asClass)
//					val constructorSymbol = typeTag.in(tb.mirror).tpe.decl(universe.termNames.CONSTRUCTOR).asMethod
//
//					val repoInstance = tb.mirror.reflect(clazzMirror.reflectConstructor(constructorSymbol)()).asInstanceOf[InstanceMirror]
//					 ----------*/
//
//					// some trick casting (if not being done or no longer working, see INFO above for another solution)
//					val tempRepositoryInstance : Any = instance
//					// get the combinator from the member-info as an instance by calling reflect on the repository instance
//					val combinatorInstance = tb.mirror.reflect(tempRepositoryInstance).reflectModule(typeSig.asModule).instance
//
//					// find the apply method and reflect it on the combinator instance
//					val applyMethod = typeSig.typeSignature.decl(universe.TermName("apply")).asMethod
//					val mirroredApply = tb.mirror.reflect(combinatorInstance).reflectMethod(applyMethod)
//
//					// cast need to be done cause of import trouble
//					mirroredApply.asInstanceOf[MethodMirror]
//
//				case DynamicCombinatorInfo(_, _, _, _, combinatorInstance, combinatorTypeTag, _, _) =>
//					// find the apply method and reflect it on the dynamic combinator instance
//					val applyMethod = combinatorTypeTag.in(tb.mirror).tpe.decl(universe.TermName("apply")).asMethod
//					val mirroredApply = tb.mirror.reflect(combinatorInstance).reflectMethod(applyMethod)
//
//					// cast need to be done cause of import trouble
//					mirroredApply.asInstanceOf[MethodMirror]
//			}
//		def constructTerm(inhabitant: Tree): Any =
//			inhabitant match {
//				// no shape reconstruction for the apply method (if needed, do so)
//				case Tree(name, _) =>
//					toCombinatorInstanceTree(combinatorComponents(name)).apply()
//				case Tree(name, _, arguments@_*) =>
//					toCombinatorInstanceTree(combinatorComponents(name))(arguments.map(constructTerm):_*)
//			}
//		constructTerm(inhabitant).asInstanceOf[T]
//	}
//
//	/** Obtains the [[DynamicCombinatorInfo]] for a single object programmatically added to the repository.
//		* The object must have an apply-Method found by [[applyMethodInfoFor]].
//		* It can include a field `val semanticType: Type`, which will be interpreted as the semantic type of the combinator.
//		* The latter interpretation internally performs  locking, because the Scala runtime compiler used to dynamically
//		* obtain the field content is not thread safe.
//		*
//		* @param combinatorInstance the object to obtain information for.
//		* @param combinatorName the name of the combinator to add; it will be augmented by a random generated UUID
//		*                       avoiding name clashes.
//		* @param position a stack trace of the code position where the combinator is added -- required for debugging.
//		* @return the [[DynamicCombinatorInfo]] for `combinatorInstance`.
//		*/
//	override def dynamicCombinatorInfoFor[C](combinatorName: String, combinatorInstance: C, position: Array[StackTraceElement])
//																					(implicit combinatorTypeTag: WeakTypeTag[C]): DynamicCombinatorInfo[C] =
//		ReflectedRepository.synchronized {
//			val (applyMethodParameters, applyMethodResult) = applyMethodInfoFor(combinatorName, combinatorTypeTag.tpe)
//			val tb = this.tb
//			val semanticType =
//				combinatorTypeTag
//					.tpe
//					.members
//					.collectFirst {
//						//            case m if m.name.toString == "semanticType" =>
//						//              tb.eval(
//						//                q"""${reify(combinatorInstance).in(tb.mirror)}
//						//                      .asInstanceOf[${combinatorTypeTag.in(tb.mirror).tpe}]
//						//                      .semanticType"""
//						//              ).asInstanceOf[Type]
//
//						case m if m.name.toString == "semanticType" =>
//							val fieldID = combinatorTypeTag.in(tb.mirror).tpe.decl(universe.TermName("semanticType")).asTerm
//
//							// some trick casting and then field reflection on the combinatorInstance
//							val tempCombinatorInstance : Any = combinatorInstance.asInstanceOf[C]
//							val value = tb.mirror.reflect(tempCombinatorInstance).reflectField(fieldID).get
//
//							// cast the value to Type
//							value.asInstanceOf[Type]
//					}
//			DynamicCombinatorInfo(combinatorName,
//				applyMethodParameters,
//				applyMethodResult,
//				semanticType,
//				combinatorInstance,
//				combinatorTypeTag,
//				position)
//		}
//
//	/** Obtains the [[StaticCombinatorInfo]] for a single [[combinator]]-annotated object inside of the repository.
//		* The object must have an apply-Method found by [[applyMethodInfoFor]].
//		* It can include a field `val semanticType: Type`, which will be interpreted as the semantic type of the combinator.
//		* The latter interpretation internally performs locking, because the Scala runtime compiler used to dynamically
//		* obtain the field content is not thread safe.
//		*
//		* @param combinatorName the name of the combinator to add.
//		* @param typeSignature reflected type information of the combinator object.
//		* @return the [[StaticCombinatorInfo]] for `typeSignature`.
//		*/
//	override def staticCombinatorInfoFor(combinatorName: String, typeSignature: universe.Type): StaticCombinatorInfo =
//		ReflectedRepository.synchronized {
//			val (applyMethodParameters, applyMethodResult) = applyMethodInfoFor(combinatorName, typeSignature)
//			val tb = this.tb
//			val semanticType =
//				typeSignature
//					.members
//					.collectFirst {
//						//            case m if m.name.toString == "semanticType" =>
//						//              tb.eval(
//						//                q"""import org.combinators.cls.types.Type;
//						//                    import org.combinators.ls14.cls.types.syntax._;
//						//                    identity[Type]({
//						//                      ${reify(instance).in(tb.mirror)}
//						//                        .asInstanceOf[${typeTag.in(tb.mirror).tpe}]
//						//                        .${TermName(NameTransformer.encode(combinatorName))}
//						//                        .semanticType
//						//                      })"""
//						//              ).asInstanceOf[Type]
//
//						case m if m.name.toString == "semanticType" =>
//							println("TEST")
//							// get the signature of the combinator by its name
//							val typeSig = typeTag.in(tb.mirror).tpe.member(TermName(NameTransformer.encode(combinatorName)))
//
//							// some trick casting (if not being done or no longer working, see INFO below for another solution)
//							val tempRepositoryInstance : Any = instance
//							// find field semanticType and reflect this field on a combinatorInstance from repository to get the semanticType
//							val fieldID = typeSig.typeSignature.decl(universe.TermName("semanticType")).asTerm
//							val combinatorInstance = tb.mirror.reflect(tempRepositoryInstance).reflectModule(typeSig.asModule).instance
//							val value = tb.mirror.reflect(combinatorInstance).reflectField(fieldID).get
//							// cast the value to Type
//							value.asInstanceOf[Type]
//
//						/*----------
//            INFO:
//            If no casting to Any, then another solution is to create a instance of
//            the repository with reflection: Code-Example...
//
//            val tb = this.tb
//            val clazzMirror = tb.mirror.reflectClass(typeTag.in(tb.mirror).tpe.typeSymbol.asClass)
//            val constructorSymbol = typeTag.in(tb.mirror).tpe.decl(universe.termNames.CONSTRUCTOR).asMethod
//
//            val repoInstance = tb.mirror.reflect(clazzMirror.reflectConstructor(constructorSymbol)()).asInstanceOf[InstanceMirror]
//
//            // get the combinator from the member-info as an instance by calling reflectModule on the repository instance
//            val ins = repoInstance.reflectModule(typeSig.asModule.asInstanceOf[ModuleSymbol]).instance
//            val value = tb.mirror.reflect(ins).reflectField(fieldID).get
//            value.asInstanceOf[Type]
//             ----------*/
//					}
//			StaticCombinatorInfo(combinatorName, applyMethodParameters, applyMethodResult, semanticType, typeSignature.dealias)
//		}
//}
