package autoschedule

import org.combinators.cls.inhabitation.TreeGrammar
import org.combinators.cls.interpreter.CombinatorInfo

/**
  * Cache object for a TreeGrammar
  *
  * @param gr TreeGrammaar to be hashed and cached
  * @param combinatorComponents Map with the combinators (used to generate the hash)
  */
class TreeGrammarWithHash(gr: TreeGrammar, combinatorComponents : Map[String, CombinatorInfo] ) extends Serializable {
  val hash: Int = genHash(combinatorComponents)

  val grammar: TreeGrammar = gr

  def compare(combinatorComponents : Map[String, CombinatorInfo]): Boolean = {
    genHash(combinatorComponents) == hash
  }

  private def genHash(combinatorComponents : Map[String, CombinatorInfo]) : Int = {
    combinatorComponents.values.map(v => {
      v.name + v.parameters.getOrElse(Seq()).map(a => a.toString).mkString + v.result.toString + v.semanticType.getOrElse('DEFAULT).toString
    }).mkString.hashCode
  }
}
