package autoschedule.repo

import autoschedule.repo.Alpha._
import autoschedule.repo.Beta._
import org.combinators.cls.types.syntax._
import org.combinators.cls.types.{Kinding, Taxonomy, Variable}

trait AutoscheduleKindings {
	lazy val skipConstraint: Variable = Variable("skipConstraint")
	lazy val skipConstraintKinding: Kinding = Kinding(skipConstraint)
		.addOption(SKIP)
		.addOption(SKIP.not)

	lazy val prmuConstraint: Variable = Variable("prmuConstraint")
	lazy val prmuConstraintKinding: Kinding = Kinding(prmuConstraint)
		.addOption(PRMU)
		.addOption(PRMU.not)

	lazy val machineEnvironment: Variable = Variable("machineEnvironment")
	lazy val machineEnvironmentKinding: Kinding = Kinding(machineEnvironment)
		.addOption(SINGLE_MACHINE)
		.addOption(PARALLEL_MACHINES)
		.addOption(FLOW_SHOP)
		.addOption(FLEXIBLE_FLOW_SHOP)
		.addOption(JOB_SHOP)

	val subTypes: Taxonomy =
		Taxonomy(SINGLE_MACHINE.toString()).addSubtype(FLOW_SHOP.toString())
			.merge(Taxonomy(FLOW_SHOP.toString()).addSubtype(JOB_SHOP.toString()))
			.merge(Taxonomy(SINGLE_MACHINE.toString()).addSubtype(PARALLEL_MACHINES.toString()))
			.merge(Taxonomy(FLOW_SHOP.toString()).addSubtype(FLEXIBLE_FLOW_SHOP.toString()))
			.merge(Taxonomy(PARALLEL_MACHINES.toString()).addSubtype(FLEXIBLE_FLOW_SHOP.toString()))

}
