package autoschedule.repo

import autoschedule.repo.AlgorithmType._
import autoschedule.repo.Alpha._
import autoschedule.repo.Beta._
import autoschedule.algorithms.Algorithm
import autoschedule.algorithms.constructive.ga.GA
import autoschedule.algorithms.constructive.ga.crossover.GATwoParentsCrossoverOperator
import autoschedule.algorithms.constructive.ga.mutation.GAMutationOperator
import autoschedule.algorithms.constructive.ga.selection.GASelectionOperator
import autoschedule.algorithms.constructive.{GifflerThompson, NEH, PriorityRuleAlgorithm}
import autoschedule.algorithms.priorityRules.PriorityRule
import autoschedule.{DynamicCombinator, SemanticTypeCreator}
import org.combinators.cls.types.Type
import org.combinators.cls.types.syntax._

trait ConstructiveHeuristicsRepository extends AutoscheduleKindings {

  /**
    * Giffler & Thompson
    */
  @DynamicCombinator(classOf[GifflerThompson]) object GifflerThompsonAlgorithm extends SemanticTypeCreator {
    val semanticType: Type = 'PriorityRule =>: createSemanticType(CONSTRUCTIVE_ALGORITHM :&: JOB_SHOP, SKIP.not, PRMU.not)

    def apply(priorityRule: PriorityRule): Algorithm = {
      new GifflerThompson(priorityRule)
    }
  }

  /**
    * Priority Rule Algorithm
    */
  @DynamicCombinator(classOf[PriorityRuleAlgorithm]) object PriorityRuleAlgorithm extends SemanticTypeCreator {
    val semanticType: Type = 'PriorityRule =>: createSemanticType(CONSTRUCTIVE_ALGORITHM :&: FLEXIBLE_FLOW_SHOP , skipConstraint, prmuConstraint)

    def apply(priorityRule: PriorityRule): Algorithm = {
      new PriorityRuleAlgorithm(priorityRule)
    }
  }

  /**
    * NEH
    */
  @DynamicCombinator(classOf[NEH]) object NEH extends SemanticTypeCreator {
    val semanticType: Type = createSemanticType(CONSTRUCTIVE_ALGORITHM :&: FLOW_SHOP, skipConstraint, PRMU)

    def apply: Algorithm = {
      new autoschedule.algorithms.constructive.NEH()
    }
  }

  /**
    * Genetic Algorithm
    */
  @DynamicCombinator(classOf[GA]) object GA extends SemanticTypeCreator {
    val semanticType: Type = 'GACrossover =>: 'GAMutation =>: 'GASelection =>: createSemanticType(CONSTRUCTIVE_ALGORITHM :&: FLEXIBLE_FLOW_SHOP, skipConstraint, prmuConstraint)

    def apply(crossover: GATwoParentsCrossoverOperator,
              mutation: GAMutationOperator,
              selection: GASelectionOperator): Algorithm = {
      new GA(crossover, mutation, selection)
    }
  }

  /**
    * Genetic Algorithm - Two Parents Crossover Operator
    */
  @DynamicCombinator(classOf[GATwoParentsCrossoverOperator]) object GATwoParentsCrossoverOperator {
    val semanticType: Type = 'GACrossover

    def apply: GATwoParentsCrossoverOperator =
      new GATwoParentsCrossoverOperator()
  }

  /**
    * Genetic Algorithm - Selection Operator
    */
  @DynamicCombinator(classOf[GASelectionOperator]) object GASelectionOperator {
    val semanticType: Type = 'GASelection

    def apply: GASelectionOperator =
      new GASelectionOperator()
  }

  /**
    * Genetic Algorithm - Mutation Operator
    */
  @DynamicCombinator(classOf[GAMutationOperator]) object GAMutationOperator {
    val semanticType: Type = 'GAMutation

    def apply: GAMutationOperator = {
      new GAMutationOperator()
    }
  }

}
