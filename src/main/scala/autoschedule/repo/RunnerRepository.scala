package autoschedule.repo

import autoschedule.repo.AlgorithmType._

import autoschedule.algorithms.Algorithm
import org.combinators.cls.interpreter.combinator
import org.combinators.cls.types.Type
import org.combinators.cls.types.syntax._

trait RunnerRepository extends AutoscheduleKindings {

	/**
		* Filter Constructive Combinator
		*/
	@combinator object FilterConstructiveCombinator {
		val semanticType: Type = CONSTRUCTIVE_ALGORITHM :&: machineEnvironment :&: skipConstraint :&: prmuConstraint =>: 'Filter( machineEnvironment, skipConstraint, prmuConstraint  )

		def apply(algorithm: Algorithm): List[Algorithm] = List(algorithm)
	}

	/**
		* Filter Iterative Combinator
		*/
	@combinator object FilterIterativeCombinator {
		val semanticType: Type = ITERATIVE_ALGORITHM :&: machineEnvironment :&: skipConstraint :&: prmuConstraint =>: 'Filter( machineEnvironment, skipConstraint, prmuConstraint  )

		def apply(algorithm: Algorithm): List[Algorithm] = List(algorithm)
	}

	/**
		* Scheduler
		*/
	@combinator object Scheduler {
		val semanticType: Type = 'AlgorithmList(machineEnvironment) =>: 'Scheduler(machineEnvironment)

		def apply(algorithmList: List[Algorithm]): List[Algorithm] = algorithmList
	}

	/**
	 * Combinator for indicating that there are no more
	 * iterative algorithm combinators needed
	 *
	 * Returns a empty sequence to which the algorithms are added
	 * After adding all iterative algorithms to the sequence, the sequence is
	 * converted into a list (see AlgorithmListCombinator in AutoscheduleRepository)
	 */
	@combinator object EndCombinator {
		var semanticType: Type = 'IterationCount0

		def callbackEmptySeq(): Seq[Algorithm] = Seq()

		def apply(): () => Seq[Algorithm] = {
			callbackEmptySeq
		}
	}

}
