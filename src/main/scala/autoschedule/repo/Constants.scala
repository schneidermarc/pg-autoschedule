package autoschedule.repo

import org.combinators.cls.types.Type
import org.combinators.cls.types.syntax._

object AlgorithmType {
  val CONSTRUCTIVE_ALGORITHM: Type = 'CONSTRUCTIVE_ALGORITHM
  val ITERATIVE_ALGORITHM: Type = 'ITERATIVE_ALGORITHM
}

object Alpha {
  val SINGLE_MACHINE: Type = 'SINGLE_MACHINE
  val PARALLEL_MACHINES: Type = 'PARALLEL_MACHINES
  val FLOW_SHOP: Type = 'FLOW_SHOP
  val FLEXIBLE_FLOW_SHOP: Type = 'FLEXIBLE_FLOW_SHOP
  val JOB_SHOP: Type = 'JOB_SHOP
}

object Beta {
  val SKIP : Type = 'SKIP
  val PRMU : Type = 'PRMU

  implicit class Notter(val ty: Type) {
    def not : Symbol = Symbol("NO_" + ty)
  }
}

