package autoschedule.repo

import autoschedule.repo.AlgorithmType._

import autoschedule.algorithms.Algorithm
import org.combinators.cls.types.Type
import org.combinators.cls.types.syntax._

/**
 * This Combinator is added one time to the repository
 * The Combinator displays that there is exactly one constructive algorithm to be needed
 * and with setting the start-Type there it is possible to add as many iterative algorithms
 * afterwards or directly skip to the EndCombinator
 *
 * See IterativeAlgorithmCombinator for an example
 *
 * @param start Type of the next iterative combinator or 'IterationCount0
 */
sealed case class AlgorithmListCombinator(start: Type) extends AutoscheduleKindings {
  val semanticType: Type = CONSTRUCTIVE_ALGORITHM :&: machineEnvironment =>: start =>: 'AlgorithmList(machineEnvironment)

  def apply(cAlgorithm: Algorithm, callback: () => Seq[Algorithm]): List[Algorithm] = cAlgorithm :: callback.apply().toList
}
