package autoschedule.repo

import autoschedule.repo.AlgorithmType._

import autoschedule.algorithms.Algorithm
import org.combinators.cls.types.Type
import org.combinators.cls.types.syntax._

/**
 * This Combinator is added multiple times to the repository (in total iterCount times)
 * Therefore the types to be resolved must be given as arguments while adding the combinator
 *
 * For example: IterCount = 2 (Then e.g. add the following combinators)
 *
 * addCombinator(IterativeAlgorithmCombinator('IterationCount1, IterationCount2)
 * addCombinator(IterativeAlgorithmCombinator('IterationCount0, 'IterationCount1)
 *
 * Also the combinator AlgorithmListCombinator must be added as followed:
 * addCombinator(AlgorithmListCombinator('IterationCount2))
 *
 * @param next    Type of the next the iterative combinator or 'IterationCount0
 * @param resolve Type which is resolved by this combinator
 */
sealed case class IterativeAlgorithmCombinator(next: Type, resolve: Type) extends AutoscheduleKindings {
  val semanticType: Type = ITERATIVE_ALGORITHM :&: machineEnvironment =>: next =>: resolve
  var savedAlgorithm: Algorithm = _
  var callbackedAlgorithms: Seq[Algorithm] = _

  def callbackAddAlgorithmToSeq(): Seq[Algorithm] = savedAlgorithm +: callbackedAlgorithms

  def apply(algorithm: Algorithm, callback: () => Seq[Algorithm]): () => Seq[Algorithm] = {
    savedAlgorithm = algorithm
    callbackedAlgorithms = callback.apply()
    callbackAddAlgorithmToSeq
  }
}
