package autoschedule.repo

import autoschedule.DynamicCombinator
import autoschedule.algorithms.priorityRules._
import org.combinators.cls.types.Type
import org.combinators.cls.types.syntax._

trait PriorityRuleRepository extends AutoscheduleKindings {

	/**
		* ECT
		*/
	@DynamicCombinator(classOf[ECT]) object ECT {
		val semanticType: Type = 'PriorityRule

		def apply: PriorityRule =
			new ECT()
	}

	/**
		* EDD
		*/
	@DynamicCombinator(classOf[EDD]) object EDD {
		val semanticType: Type = 'PriorityRule

		def apply: PriorityRule =
			new EDD()
	}

	/**
		* FCFS
		*/
	@DynamicCombinator(classOf[FCFS]) object FCFS {
		val semanticType: Type = 'PriorityRule

		def apply: PriorityRule =
			new FCFS()
	}

	/**
		* LPT
		*/
	@DynamicCombinator(classOf[LPT]) object LPT {
		val semanticType: Type = 'PriorityRule

		def apply: PriorityRule =
			new LPT()
	}

	/**
		* SPT
		*/
	@DynamicCombinator(classOf[SPT]) object SPT {
		val semanticType: Type = 'PriorityRule

		def apply: PriorityRule =
			new SPT()
	}

	/**
		* WSPT
		*/
	@DynamicCombinator(classOf[WSPT]) object WSPT {
		val semanticType: Type = 'PriorityRule

		def apply: PriorityRule =
			new WSPT()
	}

	/**
		* WDD
		*/
	@DynamicCombinator(classOf[WDD]) object WDD {
		val semanticType: Type = 'PriorityRule

		def apply: PriorityRule =
			new WDD()
	}

	/**
		* Random
		*/
	@DynamicCombinator(classOf[Random]) object Random {
		val semanticType: Type = 'PriorityRule

		def apply: PriorityRule =
			new Random()
	}

}
