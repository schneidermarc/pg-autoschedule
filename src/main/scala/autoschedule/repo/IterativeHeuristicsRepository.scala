package autoschedule.repo

import autoschedule.algorithms.Algorithm
import autoschedule.algorithms.iterative.localsearch.neighborhoods._
import autoschedule.algorithms.iterative.localsearch.{LocalSearch, SimulatedAnnealing, TabuSearch}
import autoschedule.{DynamicCombinator, SemanticTypeCreator}
import org.combinators.cls.types.Type
import org.combinators.cls.types.syntax._

import autoschedule.repo.AlgorithmType._
import autoschedule.repo.Alpha._

trait IterativeHeuristicsRepository extends AutoscheduleKindings {

  /**
    * Local Search
    */
  @DynamicCombinator(classOf[LocalSearch]) object LocalSearch extends SemanticTypeCreator {
    val semanticType: Type = 'Neighborhood =>: createSemanticType(ITERATIVE_ALGORITHM :&: FLEXIBLE_FLOW_SHOP, skipConstraint, prmuConstraint)
    def apply(neighborhood: Neighborhood): Algorithm = {
      new LocalSearch(neighborhood)
    }
  }

  /**
    * Tabu Search
    */
  @DynamicCombinator(classOf[TabuSearch]) object TabuSearch extends SemanticTypeCreator {
    val semanticType: Type = 'Neighborhood =>: createSemanticType(ITERATIVE_ALGORITHM :&: FLEXIBLE_FLOW_SHOP, skipConstraint, prmuConstraint)

    def apply(neighborhood: Neighborhood): Algorithm = {
      new TabuSearch(neighborhood)
    }
  }

  /**
    * Simulated Annealing
    */
  @DynamicCombinator(classOf[SimulatedAnnealing]) object SimulatedAnnealing extends SemanticTypeCreator {
    val semanticType: Type = 'Neighborhood =>: createSemanticType(ITERATIVE_ALGORITHM :&: FLEXIBLE_FLOW_SHOP, skipConstraint, prmuConstraint)

    def apply(neighborhood: Neighborhood): Algorithm = {
      new SimulatedAnnealing(neighborhood)
    }
  }

  /**
    * Random Descent
    */
  @DynamicCombinator(classOf[RandomDescent]) object RandomDescent {
    val semanticType: Type = 'Move =>: 'Neighborhood

    def apply(move: Move): Neighborhood =
      new RandomDescent(move)
  }

  /**
    * Steepest Descent
    */
  @DynamicCombinator(classOf[SteepestDescent]) object SteepestDescent {
    val semanticType: Type = 'Move =>: 'Neighborhood

    def apply(move: Move): Neighborhood =
      new SteepestDescent(move)
  }

  /**
    * Shift
    */
  @DynamicCombinator(classOf[Shift]) object Shift {
    val semanticType: Type = 'Move

    def apply: Move =
      new Shift()
  }

  /**
    * Swap
    */
  @DynamicCombinator(classOf[Swap]) object Swap {
    val semanticType: Type = 'Move

    def apply: Move =
      new Swap()
  }
}
