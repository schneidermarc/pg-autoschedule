package autoschedule

import scala.annotation.StaticAnnotation

/**
 * Annotation for adding dynamically Combinators to a repository
 *
 * @param clazz java-class of the algorithm
 */
// Needs to be a "case class", cause if only "class",
// calling clazz on an object would not be working
case class DynamicCombinator(clazz: java.lang.Class[_]) extends StaticAnnotation

/*
@Inherited
@Target( ElementType.TYPE )
@Retention( RetentionPolicy.RUNTIME )
public @interface DynamicCombinatorJava {
  Class<?> clazz();
}
*/
