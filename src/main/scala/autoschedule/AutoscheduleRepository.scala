package autoschedule

import autoschedule.algorithms.utils.TreeNode
import autoschedule.repo.{ConstructiveHeuristicsRepository, IterativeHeuristicsRepository, PriorityRuleRepository, RunnerRepository}
import org.combinators.cls.inhabitation.InhabitationAlgorithm
import org.combinators.cls.interpreter.{CombinatorInfo, ReflectedRepository, StaticCombinatorInfo}
import org.combinators.cls.types.syntax._
import org.combinators.cls.types.{FiniteSubstitutionSpace, Taxonomy, Type}

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.reflect.runtime.universe
import scala.reflect.runtime.universe._

class AutoscheduleRepository extends RunnerRepository with ConstructiveHeuristicsRepository with IterativeHeuristicsRepository with PriorityRuleRepository {

  val repository: AutoscheduleRepository = this

  val reflectedRepository: ReflectedRepository[AutoscheduleRepository] = ReflectedRepository(
    repository,
    substitutionSpace = repository.machineEnvironmentKinding.merge(repository.skipConstraintKinding).merge(repository.prmuConstraintKinding),
    semanticTaxonomy = repository.subTypes,
    classLoader = this.getClass.getClassLoader
  )

  /**
    * Map with CombinatorInfo-Names and Java-Algorithm-Clazzes
    * Will be filled by findDynamicCombinators dynamically
    */
  var clazzMapCInfoName: Map[java.lang.Class[_], String] = _

  /**
    * Map with found dynamicCombinators by findDynamicCombinators
    * The CombinatorInfos are used to create new unique StaticCombinatorInfos
    */
  var dynamicCombinatorInfos: Map[String, CombinatorInfo] = _

  /**
    * Map of clazzes with appearances of algorithms
    * Example:
    * Entry: <GA, 2> (means that there exist two GA's in the tree)
    * Entry: <LocalSearch, 1> (means that there exist one LocalSearch in the tree)
    */
  var algorithmUIDClazzCounts: mutable.Map[java.lang.Class[_], Int] = _
  var algorithmUIDCounts: mutable.Map[String, Int] = _

  def reflectedWithConstraints(): ReflectedRepository[AutoscheduleRepository] = {
    renewRepository(addAllDynamicCombinators())
  }

  def reflected(rootNodes: List[TreeNode]): ReflectedRepository[AutoscheduleRepository] = {
    renewRepository(findDynamicCombinators(rootNodes))
  }

  def renewRepository(map: Map[String, CombinatorInfo]): ReflectedRepository[AutoscheduleRepository] = {
    new ReflectedRepository[AutoscheduleRepository] {
      lazy val typeTag: universe.WeakTypeTag[AutoscheduleRepository] = reflectedRepository.typeTag
      lazy val instance: AutoscheduleRepository = reflectedRepository.instance
      lazy val semanticTaxonomy: Taxonomy = reflectedRepository.semanticTaxonomy
      lazy val substitutionSpace: FiniteSubstitutionSpace = reflectedRepository.substitutionSpace
      lazy val algorithm: InhabitationAlgorithm = reflectedRepository.algorithm
      lazy val classLoader: ClassLoader = reflectedRepository.classLoader

      override lazy val combinatorComponents: Map[String, CombinatorInfo] = {
        var temp = reflectedRepository.combinatorComponents
        // merge the two maps (the one from the old repository and the given map)
        for ((k, v) <- map) {
          temp = temp + (k -> v)
        }
        temp
      }
    }
  }

  def addAllDynamicCombinators(): Map[String, CombinatorInfo] = {
    reflectedRepository.typeTag.tpe.members.flatMap(member =>
      member.annotations.foldLeft[Seq[CombinatorInfo]](Seq()) {
        case (Seq(), c) if c.tree.tpe =:= universe.typeOf[DynamicCombinator] =>
          // add combinator
          Seq(reflectedRepository.staticCombinatorInfoFor(member.name.toString, member.typeSignature))
        case (s, _) =>
          s
      }
    ).map(cInfo => cInfo.name -> cInfo).toMap
  }

  /**
    * Find all Combinators with Annotations @dynamicCombinator in the repository
    * Filters the found dynamicCombinators by the set clazzes and only those are
    * added to the returning map, whose clazz (parameter of @dynamicCombinator)
    * is in the given set
    *
    * @param rootNodes list of treeNodes with clazzes of algorithms and their constraints,
    *                  whose combinator should be insert into the map
    * @return Map with Names of Combinators and StaticCombinatorInfos
    */
  protected def findDynamicCombinators(rootNodes: List[TreeNode]): Map[String, CombinatorInfo] = {
    clazzMapCInfoName = Map[java.lang.Class[_], String]()
    algorithmUIDClazzCounts = mutable.Map[java.lang.Class[_], Int]()
    algorithmUIDCounts = mutable.Map[String, Int]()

    val tempMap = universe.typeTag[AutoscheduleRepository].tpe.members.flatMap(member =>
      member.annotations.foldLeft[Seq[CombinatorInfo]](Seq()) {
        case (Seq(), c) if c.tree.tpe =:= universe.typeOf[DynamicCombinator] =>
          /* ----------
            INFO (1/2): This way of getting the class is not as safe as it could be
            If a safer solution is required, use a Java Annotation
            For an example for the Java Annotation equivalent to the Scala Annotation
            see DynamicCombinator.scala and the following code example for getting the
            field-content/the clazz (but it seems that getting the field of the java
            annotation is slower than the way it is done now)

            val tb = reflectedRepository.tb
            val combinatorInstance = reflectedRepository.repoInstance.reflectModule(member.asModule).instance
            val reflectedInstance = tb.mirror.reflect(combinatorInstance)
            val clazz = reflectedInstance.instance.getClass.getAnnotation(classOf[DynamicCombinatorJava]).clazz()
           ---------- */
          val clazz = Class.forName(c.tree.children.tail.collect({ case Literal(Constant(clazz)) => clazz } ).head.toString)
          /*----------
          INFO (2/2): Another possibility is to use runtime compilation of the scala annotation content (cause
          the scala annotations aren't compiled/runtime accessible (this seems to be the slowest way of
          getting the clazz/field of the annotation)

          val tb = reflectedRepository.tb
          val result: DynamicCombinatorJava = tb.eval(tb.untypecheck(c.tree)).asInstanceOf[DynamicCombinator]
          val clazz = result.clazz
           ---------- */

          // check, if parameter of annotation is an element of the set
          if (TreeNode.clazzes.contains(clazz)) {
            // add mapping of class and name for changing types later on
            clazzMapCInfoName += (clazz -> member.name.toString)
            // if so, add combinator to map
            Seq(reflectedRepository.staticCombinatorInfoFor(member.name.toString, member.typeSignature))
          } else {
            // if not, do nothing
            Seq()
          }
        case (s, _) => s
      }
    ).map(cInfo => cInfo.name -> cInfo).toMap

    dynamicCombinatorInfos = tempMap

    val retMap: mutable.Map[String, CombinatorInfo] = mutable.Map[String, CombinatorInfo]()

    /*
     * Unique number which will be added at the end of a semanticType
     * Specific for one subtree
     */
    var uid_semanticType: Int = 1

    // Iterate over tree via postorder and insert combinators
    var modelID: Int = 0
    for (rootNode <- rootNodes) {
      if (algorithmUIDClazzCounts.contains(rootNode.clazz)) {
        modelID = algorithmUIDClazzCounts(rootNode.clazz) + 1
        algorithmUIDClazzCounts.update(rootNode.clazz, modelID)
      } else {
        modelID = 0
        algorithmUIDClazzCounts.put(rootNode.clazz, modelID)
      }

      val mappedName = postorder(mutable.Map(), rootNode, retMap, uid_semanticType.toString)

      // set modelID of the model
      // must be set cause of easier finding the correct model for the algorithm later on
      if (rootNode.model != null)
        rootNode.model.modelID = modelID
      algorithmUIDCounts.put(mappedName, modelID)

      // increment unique number for next subtree
      uid_semanticType += 1
    }

    retMap.toMap
  }

  /**
    * Postorder-Traversal through given tree
    * Iterate over children of a node and change semanticType of combinator
    * given by the clazz inside of a node
    *
    * @param typeChanges Map of old semanticType and new semanticType
    * @param node        Current Node
    * @param retMap      Map with Names of the changed Combinators
    *                    and their (Static)CombinatorInfos with new semanticTypes
    */
  def postorder(typeChanges: mutable.Map[String, String],
                node: TreeNode, retMap:
                mutable.Map[String, CombinatorInfo],
                u_id: String): String = {
    var uu_id: String = u_id
    var siblingFound = false
    if (node.parent != null && node.parent.parent != null) {
      if (node.parent.parent.children.size() > 1) {
        uu_id = u_id + node.parent.parent.children.indexOf(node.parent)
        siblingFound = true
      }
    }

    // do a postorder traversal
    for (child: TreeNode <- node.children.asScala) {
      postorder(typeChanges, child, retMap, uu_id)
    }

    // get raw StaticCombinatorInfo corresponding to the clazz
    val cInfoName: String = clazzMapCInfoName(node.clazz)
    val scInfo: StaticCombinatorInfo = dynamicCombinatorInfos(cInfoName).asInstanceOf[StaticCombinatorInfo]

    // example for "left-" and "right-side"
    // 'PermutationStrategy =>: 'Neighborhood =>: 'IterativeAlgorithm :&: 'FLOW_SHOP :&: 'JOB_SHOP
    // | left-side                              | right-side                                      |
    // everything in front of last =>: contains to left-side and all behind last =>: contains to right-side
    val lr_side = scInfo.semanticType.get.toString.split("->")
    val l_sides = lr_side.slice(0, lr_side.size - 1).map(_.trim)
    val r_side = lr_side(lr_side.size - 1)

    var r_side_type: Type = r_side.split("&").map(_.trim)
      .map(Symbol.apply).map(a => identity[Type](a)).reduce((a, b) => a :&: b)

    // only perform "right-side" changes to none-root node
    // otherwise cls will find no solution
    if (node.parent != null) {
      // right-side needs to be unique
      r_side_type = r_side.split("&").map(_.trim).map(a => {
        typeChanges += (a -> (a + uu_id))
        a + uu_id
        // typeChanges += (a -> (if( siblingFound) (a + uid_semanticType + uu_id) else (a + uid_semanticType)))
        // if( siblingFound) (a + uid_semanticType + uu_id) else (a + uid_semanticType)
      }).map(Symbol.apply).map(a => identity[Type](a)).reduce((a, b) => a :&: b)
    }

    var new_sType = r_side_type

    // left-side change to all nodes
    if (l_sides.length > 0) {
      var tempList = l_sides.map(a => a.split("&").map(_.trim).toList
        .map(a => {
          if (typeChanges.contains(a)) typeChanges(a) else a
        })
        .map(Symbol.apply).map(a => identity[Type](a)).reduce((a, b) => a :&: b)
      ).map(a => identity[Type](a)).toBuffer

      tempList += r_side_type

      new_sType = tempList.reduceRight((a, b) => a =>: b)
    }

    // copy old StaticCombinatorInfo but use new semantic type
    val new_scInfo = scInfo.copy(
      scInfo.name,
      scInfo.parameters,
      scInfo.result,
      // apply new semanticType
      Option.apply(new_sType),
      scInfo.fullSignature)

    /*
    ---------- ---------- ----------
    */
    //    val mirror = universe.runtimeMirror(getClass.getClassLoader)
    //    val tb = mirror.mkToolBox()
    //
    //    println(repository.GifflerThompsonAlgorithm.ID)
    //    repository.GifflerThompsonAlgorithm.ID = 2
    //    println(repository.GifflerThompsonAlgorithm.ID)
    //
    //    val instance = mirror.reflect(repository.GifflerThompsonAlgorithm)
    //
    //    println( instance )
    //    println( instance.symbol )
    //    val fieldID = universe.typeOf[repository.GifflerThompsonAlgorithm.type ].decl(universe.TermName("ID")).asTerm.accessed.asTerm
    //    instance.reflectField(fieldID).set(7)
    //    println(repository.GifflerThompsonAlgorithm.ID)
    /*
    ---------- ---------- ----------
     */

    // Add new StaticCombinatorInfo to retMap
    retMap += (cInfoName + uu_id -> new_scInfo)

    cInfoName + uu_id
  }
}
