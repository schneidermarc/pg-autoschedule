package autoschedule.model;

import autoschedule.control.database.ArticleIdGenerator;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

public class Article {

	@Getter
	private final String articleName;

	@Getter @Setter
	private int scheduledTime;

	/**
	 * the time it takes for the specified amount of this article to be processed on the scheduled machine (productionAmount * processingTime of a singular article on the scheduled Machine)
	 */
	@Getter private int processingTime;

	@Getter @Setter
	private Machine scheduledMachine;

	@Getter @Setter
	private int productionAmount;

	@Getter @Setter
	private int arrival;

	@Getter @Setter
	private Article preArticle;

	@Getter
	private final int id;

	/**
	 * Specifies the machines on which this article can be produced and how long the production on this machine takes on average.
	 */
	@Getter private final LinkedHashMap<Machine, Integer> machineQualification;

	public Article(String pArticleName){
		articleName = pArticleName;
		scheduledTime = -1;
		processingTime = -1;
		productionAmount = -1;
		scheduledMachine = null;
		arrival = -1;
		preArticle = null;
		machineQualification = new LinkedHashMap<>();
		id = ArticleIdGenerator.getNextArticleID();
	}

	/**
	 * Private constructor for cloning.
	 */
	private Article(Article pArticle){
		id = pArticle.id;
		articleName = pArticle.articleName;
		scheduledTime = pArticle.scheduledTime;
		processingTime = pArticle.processingTime;
		scheduledMachine = pArticle.scheduledMachine;
		productionAmount = pArticle.productionAmount;
		arrival = pArticle.arrival;
		preArticle = pArticle.preArticle == null ? null : pArticle.preArticle.copy();
		machineQualification = new LinkedHashMap<>(pArticle.machineQualification);
	}

	/**
	 * Assigns a scheduled machine and a starting time for processing to this article.
	 */
	public void scheduleMachine(Machine pMachine, int pTime) {
		scheduledMachine = pMachine;
		scheduledTime = pTime;
		processingTime = getProcessingTimeOnMachine(scheduledMachine);
	}

	/**
	 * Returns all machines qualified for producing this article.
	 */
	public List<Machine> getQualifiedMachines() {
		return new LinkedList<>(machineQualification.keySet());
	}

	/**
	 * Returns the required time for producing the amount specified in this object of this article on the first qualified machine.
	 */
	public int getProcessingTimeOnMachine() {
		return machineQualification.get(getQualifiedMachines().get(0)) * productionAmount;
	}

	/**
	 * Returns the required time for producing the amount specified in this object of this article on the given qualified machine.
	 * Returns -1 if the given machine is not qualified for producing this article.
	 */
	public int getProcessingTimeOnMachine(Machine pMachine) {
		Integer time = machineQualification.get(pMachine);
		return time == null ? -1 : time * productionAmount;
	}

	/**
	 * Adds 'pMachine' to the set of machines qualified for the production of this article and stores the average production time of this article on this machine 'pProcessingTime' in the corresponding HashMap.
	 */
	public void addQualifiedMachine(Machine pMachine, int pProcessingTime){
		if(this.getArticleName().startsWith("A") && pMachine.getName().startsWith("B")) {
			throw new RuntimeException();
		}

		machineQualification.put(pMachine, pProcessingTime);
	}

	/**
	 * Returns the median of the production time of the amount specified in this object across all qualified machines.
	 */
	public Double getProcessingTimeMedian() {
		List<Integer> processingTimes = new ArrayList<>();
		for(Map.Entry<Machine,Integer> entry : machineQualification.entrySet()){
			processingTimes.add(entry.getValue());
		}
		return median(processingTimes)*productionAmount;
	}

	/**
	 * Returns a copy of this article.
	 * 'scheduledMachine' referenced the same machine as the original object.
	 * 'constraints' is assigned a new list, which is filled with copies of the original object´s constraints.
	 */
	public Article copy(){
		return new Article(this);
	}

	public int getStage() {
		StringBuilder prefix = new StringBuilder();

		String name = articleName;
		while(!Character.isDigit(name.charAt(0))) {
			prefix.append(name.charAt(0));
			name = name.substring(1);
		}

		return prefixToStage(prefix.toString());
	}

	/**
	 * gets time at which a article is finished
	 * @return finishing time of article
	 */
	public int getFinishTime() {
		return scheduledTime + processingTime;
	}

	/**
	 * Converts the prefix of this article´s name to a stage number.
	 * @param prefix prefix
	 * @return Stage
	 */
	private int prefixToStage(String prefix) {
		int value = 0;
		char[] chars = prefix.toCharArray();

		for(int i = 0; i < chars.length; i++) {
			int baseValue = Character.getNumericValue(chars[i]) - 9;

			value += baseValue*Math.pow(26, chars.length-i-1);
		}

		return value;
	}

	/**
	 * Sets 'scheduledMachine' to 'null' and 'scheduledTime' to '-1'.
	 */
	public void clearScheduling() {
		scheduledTime = -1;
		scheduledMachine = null;
	}

	/**
	 * Initializes scheduled machine for non flexible shops. Only one Machine exists for each article
	 */
	public void initForNonFlexible() {
		scheduledMachine = machineQualification.keySet().toArray(Machine[]::new)[0];
		processingTime = machineQualification.get(scheduledMachine) * productionAmount;
	}

	/**
	 * overwritten equals for article based on getClass
	 * @param o object to compare with
	 * @return boolean if equals
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Article article = (Article) o;
		return id == article.id;
	}

	/**
	 * gets hashcode of object
	 * @return hashCode as int
	 */
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	/**
	 * calculate the median from a list of Integers
	 * @param list list of Integer values
	 * @return median of all values contained in 'list'
	 */
	private static double median(List<Integer> list) {
		if(list.size() == 0){
			return 0.0;
		}
		list.sort(Integer::compareTo);
		int middle = list.size()/2;
		if (list.size()%2 == 1) {
			return list.get(middle);
		}
		else {
			return (list.get(middle-1) + list.get(middle)) / 2.0;
		}
	}
}
