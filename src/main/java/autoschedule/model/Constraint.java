package autoschedule.model;

import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * enum for all possible constraints
 */
@RequiredArgsConstructor
public enum Constraint {
    PRMU("Permutation", "prmu"),
    SKIP("Skip","skip"),
    JOB_ARRIVAL("Job Arrival","rj"),
    MACHINE_ARRIVAL("Machine Arrival","rm"),
    MJ("Machine eligibility restrictions","Mj"),
    OTHER("","");

    private final String longName, shortName;

    @Setter
    String name = "";

    /**
     * toString method of all enums (constraints)
     * @return String of constraint
     */
    @Override
    public String toString() {
        return this == OTHER ? name : longName;
    }

    public String shortString() {
       return this == OTHER ? name : shortName;
    }
}
