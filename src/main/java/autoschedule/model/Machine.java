package autoschedule.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

public class Machine {

	@Getter private final String name;

	@Getter @Setter int arrival;

	public Machine(String pName){
		name = pName;
		arrival = 0;
	}

	/**
	 * Private constructor for cloning.
	 */
	private Machine(Machine pMachine){
		this.name = pMachine.name;
		this.arrival = pMachine.arrival;
	}

	/**
	 * overwritten equals method for machines based on getClass
	 * @param o Object to compare with
	 * @return boolean if objects are equal
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Machine machine = (Machine) o;
		return Objects.equals(name, machine.name);
	}

	/**
	 * calculates hashcode of an object
	 * @return hashcode as int
	 */
	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	/**
	 * toString methond for machines
	 * @return String of machien
	 */
	@Override
	public String toString() {
		return name;
	}

	/**
	 * get the stage of the machine
	 * @return Stage as int
	 */
	public int getStage() {
		return name.charAt(0)-64;
    }
}
