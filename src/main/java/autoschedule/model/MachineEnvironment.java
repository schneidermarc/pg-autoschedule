package autoschedule.model;

/**
 * enum for all possible environments
 */
public enum MachineEnvironment {
	
	SINGLE_MACHINE,
	PARALLEL_MACHINES,
	FLOW_SHOP,
	FLEXIBLE_FLOW_SHOP,
	JOB_SHOP,
	FLEXIBLE_JOB_SHOP,
	UNSUPPORTED;

	/**
	 * getter for enivronment: converts the enum to String
	 * @return name of machineenvironment
	 */
    public String getName() {
		switch (this) {
			case SINGLE_MACHINE: return "Single Machine";
			case PARALLEL_MACHINES: return "Parallel Machines";
			case FLOW_SHOP: return "Flow Shop";
			case FLEXIBLE_FLOW_SHOP: return "Flexible Flow Shop";
			case JOB_SHOP: return "Job Shop";
			default: return "Unsupported";
		}
    }

	/**
	 * getter of environment: converts String to enum/environment
	 * @param pName Name of the
	 * @return machineenvironment based on string
	 */
	public static MachineEnvironment getMachineEnvironmentByName(String pName){
		switch(pName){
			case "Single Machine": return SINGLE_MACHINE;
			case "Parallel Machines": return PARALLEL_MACHINES;
			case "Flow Shop": return FLOW_SHOP;
			case "Flexible Flow Shop": return FLEXIBLE_FLOW_SHOP;
			case "Job Shop": return JOB_SHOP;
			default: return UNSUPPORTED;
		}
	}
}
