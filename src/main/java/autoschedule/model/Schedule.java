package autoschedule.model;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.objectives.*;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.control.DatabaseConnector;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.utils.converters.LongTimeConverter;
import autoschedule.control.database.RunIdGenerator;
import autoschedule.control.database.dbConnectorFactory;
import autoschedule.utils.parameteropt.ParameterOptimization;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.stream.Collectors;

public class Schedule {

	@Getter
	private final int year, week, schedulingDuration;
	@Getter
	private int runId = -1;
	@Getter
	private final MachineEnvironment machineEnvironment;
	@Getter
	private final List<Job> jobs;
	@Getter
	private final Collection<Machine> allMachines;
	@Getter
	private final Collection<SchedulingObjective> schedulingObjectives;
	@Getter
	private final Map<SchedulingObjective, Double> evaluatedObjectives;
	@Getter
	private final List<Map.Entry<Algorithm,Integer>> algorithmsHistory;
	@Getter
	private final ObservableList<SchedulingObjective> objectives = FXCollections.observableArrayList();

	private static final String ARROW = " \u27F6 ";

	@Getter @Setter
	private String scheduleName;
	@Setter @Getter
	private String algorithmString;
	@Setter @Getter
	private String simpleAlgorithmString;
	@Getter @Setter
	private List<Algorithm> algorithmsToRun;

	@Setter @Getter
	private Set<Constraint> activeConstraints;
	@Setter @Getter
	private SchedulingObjective activeObjective;
	@Setter @Getter
	private int hashCompareValue;

	public Schedule(int pYear, int pWeek, int pSchedulingDuration, Collection<Machine> pAllMachines, MachineEnvironment pMachineEnvironment, Collection<SchedulingObjective> pSchedulingObjectives ) {
		year = pYear;
		week = pWeek;
		schedulingDuration = pSchedulingDuration;
		allMachines = pAllMachines;
		machineEnvironment = pMachineEnvironment;
		schedulingObjectives = pSchedulingObjectives;
		algorithmsHistory = new ArrayList<>();
		algorithmsToRun = new ArrayList<>();
		jobs = new ArrayList<>();

		evaluatedObjectives = new HashMap<>();
		hashCompareValue = 0;
	}

	/**
	 * Private constructor for cloning.
	 */
	private Schedule(Schedule pSchedule){
		year = pSchedule.year;
		week = pSchedule.week;
		schedulingDuration = pSchedule.schedulingDuration;

		runId = pSchedule.runId;

		machineEnvironment = pSchedule.machineEnvironment;
		algorithmString = pSchedule.algorithmString;
		simpleAlgorithmString = pSchedule.simpleAlgorithmString;
		scheduleName = pSchedule.scheduleName;

		activeConstraints = new TreeSet<>(pSchedule.activeConstraints);
		activeObjective = pSchedule.activeObjective;

		allMachines = new ArrayList<>(pSchedule.allMachines);
		schedulingObjectives = new ArrayList<>(pSchedule.schedulingObjectives);
		algorithmsHistory = new ArrayList<>(pSchedule.algorithmsHistory);
		algorithmsToRun = new ArrayList<>(pSchedule.algorithmsToRun);

		jobs = pSchedule.jobs.stream().map(Job::copy).collect(Collectors.toList());

		evaluatedObjectives = new HashMap<>();
		evaluatedObjectives.putAll(pSchedule.getEvaluatedObjectives());

		hashCompareValue = hashCode();
	}

	/**
	 * Returns all executed algorithms as a string 'Alg1 -> ... -> Alg7'.
	 */
	public String getAlgorithmHistoryString() {
		return algorithmsHistory.stream().map(Map.Entry::getKey).map(Algorithm::toString).collect(Collectors.joining(ARROW));
	}

	public String getAlgorithmHistoryStringWithIterations(){
		return algorithmsHistory.stream().map(entry -> entry.getKey().toString() + (entry.getValue() == 0 ? "" :  " (IT:" + entry.getValue() + ")")).collect(Collectors.joining(ARROW));
	}

	public String getObjectiveString() {
		StringBuilder stringBuilder = new StringBuilder();

		for(Map.Entry<SchedulingObjective, Double> objective : evaluatedObjectives.entrySet()) {
			double value = objective.getValue();
			stringBuilder.append(objective.getKey().getDatabaseColumnName()).append(": ")
					.append(objective.getKey() instanceof UBar ? value : LongTimeConverter.format(value, false, false))
					.append(" ");
		}

		return stringBuilder.toString();
	}

	/**
	 * Generates a algorithmString from algorithmsToRun and saves it in 'algorithmString'
	 */
	public void generateAlgorithmStrings() {
		algorithmString = algorithmsToRun.stream().map(Algorithm::getString).collect(Collectors.joining(ARROW));
		simpleAlgorithmString = algorithmsToRun.stream().map(Algorithm::getSimpleString).collect(Collectors.joining(ARROW));
		scheduleName = algorithmsToRun.stream().map(Algorithm::getSimpleString).collect(Collectors.joining(ARROW));
	}

	/**
	 * Returns the number of stages.
	 */
	public int getStages(){
		return allMachines.stream().mapToInt(Machine::getStage).max().orElse(0);
	}

	/**
	 * Returns all Jobs on specified stage (1-based)
	 */
	public List<Job> getJobsOnStage(int stage) {
		return jobs.stream().filter(job -> job.getArticles().stream().anyMatch(article -> article.getStage() == stage)).collect(Collectors.toList());
	}

	/**
	 * Returns all articles for the given stage.
	 * @param stage Stage
	 * @return all articles
	 */
	public List<Article> getAllArticlesOnStage(int stage) {
		if(stage > getStages())
			throw new IllegalArgumentException("More stages than available");

		List<Article> articles = new LinkedList<>();

		jobs.forEach(job -> job.getArticles().forEach(article -> {
			if(article.getStage() == stage)
				articles.add(article);
		}));

		return articles;
	}

	public List<Article> getAllArticles() {
		List<Article> articles = new LinkedList<>();
		jobs.forEach(job -> articles.addAll(job.getArticles()));
		return articles;
	}

	/**
	 * Returns the total runtime for all executed algorithms.
	 */
	public long getTotalRuntime() {
		return algorithmsHistory.stream().map(Map.Entry::getKey).mapToLong(Algorithm::getRuntimeMillis).sum();
	}

	public void fillObjectives() {
		AlgorithmAnnotations.objectiveFunctions.forEach(clazz -> objectives.add(Algorithm.createInstanceByClass(clazz)));
	}
	/**
	 * Executes all algorithms from the 'algorithmsToRun' list and adds them to 'algorithmsHistory'.
	 * 'algorithmsToRun' is cleared after the execution of all algorithms.
	 */
	public void start() {
		fillObjectives();
		schedulingObjectives.addAll(objectives);
		schedulingObjectives.addAll(Arrays.asList(new CBar(), new CMax(), new FMax(), new LMax(), new UBar()));
		runId = RunIdGenerator.getNextRunID();
		int algorithmId = 1;
		DatabaseConnector dbConnector = dbConnectorFactory.createDatabaseConnector();

		List<Algorithm> algorithmsToRunNoModification = new ArrayList<>(algorithmsToRun);

		for (Algorithm currentAlgorithm : algorithmsToRunNoModification) {
			ParameterOptimization optimization = new ParameterOptimization( currentAlgorithm );
			List<OPT_ObjValuesObject> cachedObjectiveValues = optimization.executeParameterOptimization( this );

			algorithmsHistory.add(new AbstractMap.SimpleEntry<>(currentAlgorithm, cachedObjectiveValues.size()));
			dbConnector.saveCurrentScheduleStateInDatabase(this, currentAlgorithm, cachedObjectiveValues, runId, algorithmId);
			algorithmId++;
		}

		algorithmsToRun.clear();

		for(SchedulingObjective objective : schedulingObjectives) {
			evaluatedObjectives.put(objective, objective.evaluate(this));
		}

		dbConnector.saveScheduleMetadataAndResults(this,runId);
	}

	/**
	 * Returns a copy of this schedule.
	 * 'allMachines' is assigned a new list, which is filled with the original schedule´s machines.
	 * 'schedulingObjectives' is assigned a new list, which is filled with the original schedule´s SchedulingObjectives.
	 * 'allConstraints' is assigned a new list, which is filled with clones of the original schedule´s constraints.
	 * 'algorithmsHistory' is assigned a new list, which is filled with the original schedule´s algorithms.
	 * 'algorithmsToRun' is assigned a new empty list.
	 * 'jobs' is assigned a new list, which is filled with clones of the original schedule´s jobs.
	 */
	public Schedule copy() {
		return new Schedule(this);
	}

	/**
	 * copy a schedule
	 * @param schedule to copy
	 */
	public void copyFrom( Schedule schedule ) {
		// copy
		this.algorithmString = schedule.algorithmString;
		this.simpleAlgorithmString = schedule.simpleAlgorithmString;

		// clear
		this.allMachines.clear();
		this.schedulingObjectives.clear();
		this.algorithmsHistory.clear();
		this.algorithmsToRun.clear();
		this.jobs.clear();

		// add
		this.allMachines.addAll( schedule.allMachines );
		this.schedulingObjectives.addAll( schedule.schedulingObjectives );
		this.algorithmsHistory.addAll( schedule.algorithmsHistory );
		this.algorithmsToRun.addAll( schedule.algorithmsToRun );

		this.jobs.addAll( schedule.jobs );
	}

	public void applyConstraints() {
		if(!activeConstraints.contains(Constraint.JOB_ARRIVAL))
			jobs.forEach(job -> job.setArrival(0));

		if(!activeConstraints.contains(Constraint.MACHINE_ARRIVAL))
			allMachines.forEach(machine -> machine.setArrival(0));

		if(!activeConstraints.contains(Constraint.MJ)) {
			for (Job job : jobs) {
				for (Article article : job.getArticles()) {
					Map<Machine, Integer> qualifications = article.getMachineQualification();
					final int time = (int) qualifications.values().stream().mapToInt(i -> i).average().orElse(0);

					qualifications.clear();

					for (Machine machine : allMachines) {
						if (machine.getStage() == article.getStage())
							article.getMachineQualification().put(machine, time);
					}
				}
			}
		}
	}

	@Override
	public int hashCode() {
		//Sort the jobs and articles in the schedule
		this.jobs.sort((Job j1,Job j2) -> {
			Integer i1 = j1.getId();
			Integer i2 = j2.getId();
			return i1.compareTo(i2);
		});

		//Build a long string, that contains the information of the schedule
		String s = getAlgorithmString();
		for(Job job: this.jobs){
			for(Article article: job.getArticles()){
				s = s + article.getScheduledMachine() + article.getScheduledTime();
			}
		}
		return s.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if( obj instanceof Schedule){
			Schedule schedule = (Schedule) obj;
			return this.hashCompareValue == schedule.hashCompareValue;
		} else {
			return false;
		}
	}
}
