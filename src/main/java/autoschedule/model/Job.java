package autoschedule.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class Job {
	@Getter
	private final String name;
	@Getter
	private final List<Article> articles;
	@Getter @Setter
	private int deadline;
	@Getter @Setter
	private int arrival;
	@Getter @Setter
	private double weight;
	@Getter
	private final int id;

	public Job(String pName, List<Article> articles, int deadline, int arrival, double weight){
		this.name = pName;
		this.articles = articles;
		this.deadline = deadline;
		this.arrival = arrival;
		this.weight = weight;
		this.id = 0;

		for(int i = articles.size() - 1; i > 0; i--) {
			this.articles.get(i).setPreArticle(this.articles.get(i-1));
		}
	}

	public Job(int id, String pName, List<Article> articles, int deadline, int arrival, double weight){
		this.name = pName;
		this.articles = articles;
		this.deadline = deadline;
		this.arrival = arrival;
		this.weight = weight;
		this.id = id;

		for(int i = articles.size() - 1; i > 0; i--) {
			this.articles.get(i).setPreArticle(this.articles.get(i-1));
		}
	}

	/**
	 * Private constructor for cloning.
	 * @param pJob the job to be cloned.
	 */
	private Job(Job pJob){
		this.name = pJob.name;
		this.deadline = pJob.deadline;
		this.arrival = pJob.arrival;
		this.weight = pJob.weight;
		this.id = pJob.id;

		this.articles = new ArrayList<>();
		for(Article article : pJob.articles) {
			Article clone = article.copy();
			articles.stream().filter(a -> a.equals(clone.getPreArticle())).findFirst().ifPresent(clone::setPreArticle);

			articles.stream().filter(a -> clone.equals(a.getPreArticle())).findFirst().ifPresentOrElse(a -> {
				if(!articles.contains(a))
					articles.add(a);
				}, () -> articles.add(clone));
		}
	}

	public int getEndtime(){
		return articles.stream().mapToInt(Article ::getFinishTime).max().orElse(0);
	}

	public List<Integer> getStageOrder(){
		List<Integer> output = new ArrayList<>();
		for(Article article : articles){
			output.add(article.getStage());
		}
		return output;
	}

	/**
	 * Returns a copy of this job.
	 * 'articles' is assigned a new list, which is filled with copies of the original object´s articles.
	 * 'constraints' is assigned a new list, which is filled with copies of the original object`s constraints.
	 * @return A copy of this job.
	 */
	public Job copy(){
		return new Job(this);
	}
}
