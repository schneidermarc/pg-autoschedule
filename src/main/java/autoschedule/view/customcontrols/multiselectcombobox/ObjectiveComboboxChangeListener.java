package autoschedule.view.customcontrols.multiselectcombobox;

import autoschedule.algorithms.objectives.DummyObjective;
import autoschedule.algorithms.objectives.SchedulingObjective;
import com.jfoenix.controls.JFXComboBox;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;

import java.util.Map;
import java.util.stream.Collectors;

public class ObjectiveComboboxChangeListener implements ChangeListener<Map.Entry<SchedulingObjective, Boolean>> {
    private final JFXComboBox<Map.Entry<SchedulingObjective, Boolean>> comboBox;
    private final ObservableList<Map.Entry<SchedulingObjective, Boolean>> entries;

    public ObjectiveComboboxChangeListener(JFXComboBox<Map.Entry<SchedulingObjective, Boolean>> comboBox, ObservableList<Map.Entry<SchedulingObjective, Boolean>> entries) {
        this.comboBox = comboBox;
        this.entries = entries;
    }

    @Override
    public void changed(ObservableValue<? extends Map.Entry<SchedulingObjective, Boolean>> observable, Map.Entry<SchedulingObjective, Boolean> oldValue, Map.Entry<SchedulingObjective, Boolean> newValue) {
        int index = comboBox.getSelectionModel().getSelectedIndex();
        Map.Entry<SchedulingObjective, Boolean> selectedObjective = entries.get(index);

        if(selectedObjective.getKey() instanceof DummyObjective)
            return;

        selectedObjective.setValue(!selectedObjective.getValue());

        entries.add(index,selectedObjective);
        entries.remove(index);

        Map.Entry<SchedulingObjective, Boolean> other = comboBox.getItems().get(comboBox.getItems().size()-1);
        ((DummyObjective)other.getKey()).setName(comboBox.getItems().stream().filter(Map.Entry::getValue).map(entry -> entry.getKey().getDatabaseColumnName()).collect(Collectors.joining(", ")));
        comboBox.getSelectionModel().selectLast();
    }
}

