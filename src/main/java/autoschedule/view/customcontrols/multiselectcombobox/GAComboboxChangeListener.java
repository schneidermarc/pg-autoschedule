package autoschedule.view.customcontrols.multiselectcombobox;

import com.jfoenix.controls.JFXComboBox;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.util.Map;

public class GAComboboxChangeListener<T> implements ChangeListener<Map.Entry<Class<T>, Boolean>> {
    private final JFXComboBox<Map.Entry<Class<T>, Boolean>> comboBox;

    public GAComboboxChangeListener(JFXComboBox<Map.Entry<Class<T>, Boolean>> comboBox) {
        this.comboBox = comboBox;
    }

    @Override
    public void changed(ObservableValue<? extends Map.Entry<Class<T>, Boolean>> observable, Map.Entry<Class<T>, Boolean> oldValue, Map.Entry<Class<T>, Boolean> newValue) {
        Map.Entry<Class<T>, Boolean> selectedAlgorithm = comboBox.getSelectionModel().getSelectedItem();
        selectedAlgorithm.setValue(!selectedAlgorithm.getValue());

        Map.Entry<Class<T>, Boolean> other = comboBox.getItems().get(comboBox.getItems().size()-1);
    }
}

