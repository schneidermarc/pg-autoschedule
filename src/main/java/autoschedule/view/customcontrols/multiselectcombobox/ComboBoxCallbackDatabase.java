package autoschedule.view.customcontrols.multiselectcombobox;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import java.io.File;

public class ComboBoxCallbackDatabase  implements Callback<ListView<File>, ListCell<File>> {
  @Override
    public ListCell<File> call(ListView<File> listView) {
        return new ListCell<>() {
            @Override
            protected void updateItem(File item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setGraphic(null);
                } else {
                    setText(item.getName());
                }
            }
        };
    }
}
