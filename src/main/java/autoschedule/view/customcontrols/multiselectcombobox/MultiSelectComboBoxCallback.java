package autoschedule.view.customcontrols.multiselectcombobox;

import autoschedule.algorithms.objectives.SchedulingObjective;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import java.util.Map;

public class MultiSelectComboBoxCallback<T> implements Callback<ListView<Map.Entry<T, Boolean>>, ListCell<Map.Entry<T, Boolean>>> {

    @Override
    public ListCell<Map.Entry<T, Boolean>> call(ListView<Map.Entry<T, Boolean>> listView) {
        return new ListCell<>() {
            @Override
            protected void updateItem(Map.Entry<T, Boolean> item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty)
                    setGraphic(null);
                else {
                    setText( ( item.getValue() ? "\u2714 " : "     " ) + getName( item.getKey() ) );
                }
            }
        };
    }

    private String getName(T item) {
        if(item instanceof SchedulingObjective)
            return ((SchedulingObjective)item).getDatabaseColumnName();
        else if( item instanceof Class<?>)
            return ( (Class<?>) item ).getSimpleName();
        else
            return item.toString();
    }
}




