package autoschedule.view.customcontrols.multiselectcombobox;

import autoschedule.model.Constraint;
import com.jfoenix.controls.JFXComboBox;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;

import java.util.Map;
import java.util.stream.Collectors;

public class ConstraintComboboxChangeListener implements ChangeListener<Map.Entry<Constraint, Boolean>> {
    private final JFXComboBox<Map.Entry<Constraint, Boolean>> comboBox;
    private final ObservableList<Map.Entry<Constraint, Boolean>> entries;

    public ConstraintComboboxChangeListener(JFXComboBox<Map.Entry<Constraint, Boolean>> comboBox, ObservableList<Map.Entry<Constraint, Boolean>> entries) {
        this.comboBox = comboBox;
        this.entries = entries;
    }

    @Override
    public void changed(ObservableValue<? extends Map.Entry<Constraint, Boolean>> observable, Map.Entry<Constraint, Boolean> oldValue, Map.Entry<Constraint, Boolean> newValue) {
        int index = comboBox.getSelectionModel().getSelectedIndex();
        Map.Entry<Constraint, Boolean> selectedConstraint = entries.get(index);

        if(selectedConstraint.getKey() == Constraint.OTHER)
            return;

        selectedConstraint.setValue(!selectedConstraint.getValue());

        entries.add(index,selectedConstraint);
        entries.remove(index);

        Map.Entry<Constraint, Boolean> other = comboBox.getItems().get(comboBox.getItems().size()-1);
        other.getKey().setName(comboBox.getItems().stream().filter(Map.Entry::getValue).map(entry -> entry.getKey().shortString()).collect(Collectors.joining(", ")));
        comboBox.getSelectionModel().selectLast();
    }
}

