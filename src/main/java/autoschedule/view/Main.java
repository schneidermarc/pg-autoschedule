package autoschedule.view;

import autoschedule.control.DatabaseConnector;
import autoschedule.control.database.dbConnectorFactory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("MainMenu.fxml"));

        Pane root = loader.load();
        MainMenuViewController mainMenuViewController = loader.getController();
        mainMenuViewController.setMenuStage(primaryStage);
        mainMenuViewController.init();

        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getClassLoader().getResource("styles.css").toExternalForm());

        //primaryStage.initStyle(StageStyle.UTILITY);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("AutoSchedule");
        primaryStage.getIcons().add(new Image("icon.png"));
        primaryStage.show();
    }

    public static void main(String[] args) {
        clearSchedulesFolder();
        launch(args);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void clearSchedulesFolder() {
        File[] schedules = new File("Schedules").listFiles();
        if(schedules != null)
            for (File schedule : schedules) {
                schedule.delete();
            }
    }
}
