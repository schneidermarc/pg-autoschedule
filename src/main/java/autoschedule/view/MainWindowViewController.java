package autoschedule.view;

import autoschedule.Scheduler;
import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.model.Schedule;
import autoschedule.utils.logging.CustomMessage;
import autoschedule.utils.logging.Logger;
import autoschedule.utils.threading.DaemonThread;
import autoschedule.view.algorithmtabs.AlgorithmTab;
import autoschedule.view.algorithmtabs.AlgorithmTabFactory;
import autoschedule.view.viewmodel.ViewModel;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainWindowViewController {

    private static final Duration SLIDE_DURATION = Duration.millis(300);
    private static final int AUTO_BUTTON_ITER_COUNT = 1;
    private static final int MAX_SCHEDULES_TO_SHOW_IN_AUTO_MODE = 5;
    private static final boolean ITER_COUNT_LOWER_OR_EQUAL = true;

    @FXML
    private BorderPane borderPane;
    @FXML
    private Button menuButton;
    @FXML
    private Button btnBack, btnNext, btnAutoStart;

    @FXML
    private Pane settingsPageView, chooseAlgorithmsPageView, resultsPageView;
    @FXML
    private SettingsPageViewController settingsPageViewController;
    @FXML
    private ChooseAlgorithmsPageViewController chooseAlgorithmsPageViewController;
    @FXML
    private ResultsPageViewController resultsPageViewController;

    @Getter
    private final ViewModel viewModel = new ViewModel();

    private Stage mainStage;

    void init(Stage mainStage) {

        viewModel.init();
        settingsPageViewController.init(viewModel);
        chooseAlgorithmsPageViewController.init(viewModel);
        //solutionsPageViewController.init(viewModel, mainStage);
        resultsPageViewController.init(viewModel, mainStage);
        this.mainStage = mainStage;
        menuButton.setTextAlignment(TextAlignment.CENTER);
        btnAutoStart.visibleProperty().bind(viewModel.getOptimizationRunningProperty().not().and(viewModel.getCurrentPaneProperty().isEqualTo(0)));

        new Thread(() -> {
            long startTime = System.currentTimeMillis();
            Scheduler.init();
            Logger.system("You woke up the sleeping \uD83D\uDC09-CLS in " + (System.currentTimeMillis() - startTime) + "ms. CLS no longer acts like a \uD83D\uDC22 !!!");
            Platform.runLater( () -> {btnBack.setDisable(false); btnNext.setDisable(false);});
        }).start();
    }

    @FXML
    public void onButtonBackAction(ActionEvent actionEvent) {

        Pane[] centerPanes = new Pane[]{settingsPageView, chooseAlgorithmsPageView, resultsPageView };

        if(viewModel.getCurrentPaneProperty().get()==2 && getViewModel().isAutoButtonPushed()){
            TranslateTransition fadeIn = new TranslateTransition(SLIDE_DURATION, centerPanes[1]);
            TranslateTransition fadeOut = new TranslateTransition(SLIDE_DURATION, centerPanes[viewModel.getCurrentPaneProperty().get()]);
            fadeIn.setFromX(-borderPane.getPrefWidth());
            fadeIn.setToX(0);
            fadeOut.setFromX(0);
            fadeOut.setToX(borderPane.getPrefWidth());
            fadeIn.setOnFinished(event -> { btnBack.setDisable(false); btnNext.setDisable(false); });
            //chooseAlgorithmsPageView.setLayoutX( chooseAlgorithmsPageView.getLayoutX()+2*borderPane.getPrefWidth());

            btnBack.setDisable(true);
            btnNext.setDisable(true);
            fadeOut.play();
            fadeIn.play();

            viewModel.getCurrentPaneProperty().set(1);
            viewModel.setAutoButtonPushed(false);
            viewModel.getOptimizationRunningProperty().set(false);
            settingsPageViewController.clearDB();
        }

        if(viewModel.getCurrentPaneProperty().get()==0) {
            mainStage.close();
            return;
        }


        TranslateTransition fadeIn = new TranslateTransition(SLIDE_DURATION, centerPanes[viewModel.getCurrentPaneProperty().get()-1]);
        TranslateTransition fadeOut = new TranslateTransition(SLIDE_DURATION, centerPanes[viewModel.getCurrentPaneProperty().get()]);
        fadeIn.setFromX(-borderPane.getPrefWidth());
        fadeIn.setToX(0);
        fadeOut.setFromX(0);
        fadeOut.setToX(borderPane.getPrefWidth());
        fadeIn.setOnFinished(event -> { btnBack.setDisable(false); btnNext.setDisable(false); });

        btnBack.setDisable(true);
        btnNext.setDisable(true);
        fadeOut.play();
        fadeIn.play();

        viewModel.getCurrentPaneProperty().set(viewModel.getCurrentPaneProperty().get() -1);
    }

    @FXML
    public void onButtonNextAction(ActionEvent actionEvent) {
        Pane[] centerPanes = new Pane[]{settingsPageView, chooseAlgorithmsPageView, resultsPageView };

        TranslateTransition fadeIn = new TranslateTransition(SLIDE_DURATION, centerPanes[(viewModel.getCurrentPaneProperty().get()+1)%3]);
        TranslateTransition fadeOut = new TranslateTransition(SLIDE_DURATION, centerPanes[viewModel.getCurrentPaneProperty().get()]);
        fadeIn.setFromX(borderPane.getPrefWidth());
        fadeIn.setToX(0);
        fadeOut.setFromX(0);
        fadeOut.setToX(-borderPane.getPrefWidth());
        fadeIn.setOnFinished(event -> { btnBack.setDisable(false); btnNext.setDisable(false); });
        if(viewModel.getCurrentPaneProperty().get()==2) {
            settingsPageView.setTranslateX(borderPane.getPrefWidth());
            chooseAlgorithmsPageView.setTranslateX(borderPane.getPrefWidth());
            fadeOut.setOnFinished(event -> resultsPageView.setTranslateX(borderPane.getPrefWidth()));
        }

        //calculate the actual viewController
        boolean goToNextPane = true;
        switch(viewModel.getCurrentPaneProperty().get()) {
            case 0:
                if(settingsPageViewController.databaseInputValid()){
                    viewModel.setAutoButtonPushed(false);
                    settingsPageViewController.prepareNext();
                    viewModel.getSelectedObjectiveToShowProperty().set(viewModel.getSelectedObjectiveProperty().get());
                    fadeOut.play();
                    fadeIn.play();
                } else {
                    String[] errors = {"Select a database"};
                    CustomMessage.showCustomErrors("Warning", errors);
                    goToNextPane = false;
                }
                chooseAlgorithmsPageViewController.resetViews();
                break;
            case 1:
                if( chooseAlgorithmsPageViewController.inputValid()) {
                    new Thread(()->{
                        chooseAlgorithmsPageViewController.prepareNext();

                        viewModel.setChosenAlgorithms(generateTree());

                        List<Schedule> schedules =  viewModel.getSchedulingController().getOptimizationRunner().generateSchedulesFromCLS(
                                viewModel.getSelectedMachineEnvironment().getValue(),
                                viewModel.getSelectedIterationCount().getValue(),
                                viewModel.getIterCountLowerOrEqual().get(),
                                viewModel.getChosenAlgorithms());

                        Platform.runLater(()->{
                            viewModel.getSelectedSolutionsList().clear();
                            viewModel.getGeneratedSolutionsList().clear();
                            viewModel.getGeneratedSolutionsList().addAll(schedules);


                            for( Schedule schedule : schedules ) {
                                for( Algorithm alg : schedule.getAlgorithmsToRun()){
                                    updateCorrespondingViewModel(alg);
                                }
                            }

                            resultsPageViewController.prepareNext();
                            fadeOut.play();
                            fadeIn.play();
                        });

                    }).start();
                } else {
                    goToNextPane = false;
                }
                break;
            case 2:
                fadeOut.play();
                fadeIn.play();
                //settingsPageViewController.clearDB();
                viewModel.getOptimizationRunningProperty().set(false);
                break;
        }
        if(goToNextPane) {
            btnBack.setDisable(true);
            btnNext.setDisable(true);
            viewModel.getCurrentPaneProperty().set((viewModel.getCurrentPaneProperty().get()+1) % 3);
        }
    }

    void setToStart() {
        settingsPageView.setTranslateX(0);
        chooseAlgorithmsPageView.setTranslateX(borderPane.getPrefWidth());
        resultsPageView.setTranslateX(2*borderPane.getPrefWidth());
    }
    /**
     * Manages the switch of viewModels from the second to the third pane;
     *
     * Example: Pane2 has 2 (differently parametrized) Local Searches (LS1,LS2).
     * CLS then generates n solutions from which approximately n/2 are of LS1 and the other half of LS2.
     * Each algorithm in the solution now needs to be mapped to the correct parameters, that were entered in the second pane.
     * This is done here via the model-Id
     *
     * @param alg Depending on the algorithm's modelId the 'right' view model from the second pane gets selected
     */
    private void updateCorrespondingViewModel(Algorithm alg) {
        List<AlgorithmViewModel> algorithmViewModels = viewModel.getAlgorithmViewModels().get( alg.getClass());
        if( algorithmViewModels != null  )
            alg.setModel( algorithmViewModels.get( alg.modelID ).clone() );
    }

    /**
     * Generate Tree with TreeNodes
     *
     * @return List of TreeNodes as root-Nodes
     */
    private List<TreeNode> generateTree() {
        List<TreeNode> roots = new ArrayList<>();

        chooseAlgorithmsPageViewController.getTabPaneConstructiveAlgorithms().getTabs().stream().map(
                tab -> ( (AlgorithmTab) tab )
                        .getSelectedCombinators() )
                        .forEach(item -> {
                            if( item != null) roots.add( item );
                        });

        chooseAlgorithmsPageViewController.getTabPaneIterativeAlgorithms().getTabs().stream().map(
                tab -> ( (AlgorithmTab) tab )
                        .getSelectedCombinators() )
                .forEach( roots::add );

        return roots;
    }


    /**
     * Closes the main stage and resets the view model
     */
    public void onButtonMenuAction(ActionEvent actionEvent) {
        setToStart();
        mainStage.close();
        viewModel.getOptimizationRunningProperty().set(false);
        viewModel.getCurrentPaneProperty().set(0);
        viewModel.setLoadedDB(null);
        settingsPageViewController.clearDB();
    }

    public void onAutoStartAction(ActionEvent actionEvent) {
        //change View and disbale button
        viewModel.setAutoButtonPushed(true);

       if(settingsPageViewController.databaseInputValid()) {
//           resultsPageViewController.setObjective();
           TranslateTransition fadeIn = new TranslateTransition(SLIDE_DURATION, resultsPageView);
           TranslateTransition fadeOut = new TranslateTransition(SLIDE_DURATION, settingsPageView);
           fadeIn.setFromX(borderPane.getPrefWidth());
           fadeIn.setToX(0);
           fadeOut.setFromX(0);
           fadeOut.setToX(-borderPane.getPrefWidth());

           settingsPageViewController.prepareNext();
           fadeOut.play();
           fadeIn.play();
           viewModel.getCurrentPaneProperty().set(2);

           viewModel.getOptimizationRunningProperty().set(false);
           viewModel.getConstructiveAlgorithms().forEach(algorithm -> chooseAlgorithmsPageViewController.getTabPaneConstructiveAlgorithms().getTabs().add(new AlgorithmTabFactory().generate(algorithm, viewModel)));
           viewModel.getIterativeAlgorithms().forEach(algorithm -> chooseAlgorithmsPageViewController.getTabPaneIterativeAlgorithms().getTabs().add(new AlgorithmTabFactory().generate(algorithm, viewModel)));
           viewModel.setChosenAlgorithms(generateTree());
           viewModel.getOptimizationProgressProperty().set(0.0);
           viewModel.getSelectedObjectiveToShowProperty().set(viewModel.getSelectedObjectiveProperty().get());

           Thread thread = new DaemonThread(() -> {
               List<Schedule> schedules = viewModel.getSchedulingController().getOptimizationRunner().generateSchedulesFromCLS(
                       viewModel.getSelectedMachineEnvironment().getValue(),
                       AUTO_BUTTON_ITER_COUNT,
                       ITER_COUNT_LOWER_OR_EQUAL,
                       viewModel.getChosenAlgorithms());

               Collections.shuffle(schedules);

               Platform.runLater(() -> {
                   viewModel.getGeneratedSolutionsList().clear();
                   viewModel.getSelectedSolutionsList().clear();
                   viewModel.getSelectedSolutionsList().addAll(schedules);
                   resultsPageViewController.initPreferencesList();

                   for (Schedule schedule : schedules) {
                       for (Algorithm alg : schedule.getAlgorithmsToRun()) {
                           updateCorrespondingViewModel(alg);
                       }
                   }

                   //Optimization Button Action
                  viewModel.getSchedulingController().getOptimizationRunner().startOptimization(MAX_SCHEDULES_TO_SHOW_IN_AUTO_MODE);
               });
           });

           fadeIn.setOnFinished((e) -> thread.start());
       } else {
            if(!settingsPageViewController.databaseInputValid()) {
               String[] errors = {"Select a database"};
               CustomMessage.showCustomErrors("Warning", errors);
           }
       }
    }
}