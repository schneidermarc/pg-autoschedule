package autoschedule.view;

import autoschedule.Scheduler;
import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.control.ClassificationController;
import autoschedule.control.database.RouteManager;
import autoschedule.model.Constraint;
import autoschedule.model.MachineEnvironment;
import autoschedule.model.Schedule;
import autoschedule.control.database.DemandData;
import autoschedule.utils.RouteData;
import autoschedule.utils.converters.IntegerStringConverter;
import autoschedule.utils.ScheduleFactory;
import autoschedule.utils.converters.LongTimeConverter;
import autoschedule.utils.logging.CustomMessage;
import autoschedule.view.customcontrols.multiselectcombobox.ComboBoxCallbackDatabase;
import autoschedule.view.customcontrols.multiselectcombobox.ConstraintComboboxChangeListener;
import autoschedule.view.customcontrols.multiselectcombobox.MultiSelectComboBoxCallback;
import autoschedule.view.viewmodel.ViewModel;
import com.jfoenix.controls.JFXComboBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import lombok.Getter;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class SettingsPageViewController {

    @FXML
    private JFXComboBox<File> comboBoxDatabase;
    @FXML
    private JFXComboBox<Map.Entry<Constraint, Boolean>> comboBoxConstraint;
    @FXML
    private JFXComboBox<SchedulingObjective> comboBoxObjectives;
    @FXML
    private JFXComboBox<Integer> comboBoxYears;
    @FXML
    private JFXComboBox<Integer> comboBoxWeeks;
    @FXML
    private NumberTextField textFieldDuration;
    @FXML
    private TableView<DemandData> tableViewDemands;
    @FXML
    private TableView<RouteData> tableViewRoute;
    @FXML
    private Label numberOfArticles;
    @FXML
    private Label numberOfJobs;
    @FXML
    private TableColumn<DemandData,Integer> columnProdTime;
    @FXML
    private TableColumn<RouteData,Integer> columnStepProdTime;

    @Getter
    private ViewModel viewModel;

    private Map<Integer,List<RouteData>> routeTableData;
    private boolean updatingAllowed;


    private final ObservableList<File> observableListDatabaseCombobox = FXCollections.observableArrayList();
    private final Callback<ListView<Map.Entry<Constraint, Boolean>>, ListCell<Map.Entry<Constraint, Boolean>>> cellFactoryConstraints = new MultiSelectComboBoxCallback<>();
    private final Callback<ListView<File>, ListCell<File>> cellFactoryDatabase = new ComboBoxCallbackDatabase();

    /**
     * initializes the view content
     */
    void init(ViewModel viewModel) {
        this.updatingAllowed = true;
        this.viewModel = viewModel;
        this.comboBoxYears.valueProperty().bindBidirectional(viewModel.getSelectedYear());
        this.comboBoxWeeks.valueProperty().bindBidirectional(viewModel.getSelectedWeek());
        this.textFieldDuration.textProperty().bindBidirectional(viewModel.getSchedulingDuration(), new IntegerStringConverter());
        this.routeTableData = new HashMap<>();

        setDatabaseComboBox();
        setDemandTableSelectionListener();

        setColumnWidth(0,0.1);  //Job ID
        setColumnWidth(1,0.1);  //Weight
        setColumnWidth(2,0.12);  //Article
        setColumnWidth(3,0.09);  //Year
        setColumnWidth(4,0.09);  //Week
        setColumnWidth(5,0.1);  //Demand
        setColumnWidth(6,0.2);  //Demand with Scrap
        setColumnWidth(7,0.2);  //AVG complete prod time

        setColumnWidthRoute(0,0.2); //Step
        setColumnWidthRoute(1,0.4); //Article
        setColumnWidthRoute(2,0.4); //AVG prod time

        columnProdTime.setCellFactory(column ->
            new TableCell<>(){
                @Override
                protected void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                    setText(empty ? null : LongTimeConverter.format(item,false,false));
                }
            }
        );

        columnStepProdTime.setCellFactory(column ->
            new TableCell<>(){
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : LongTimeConverter.format(item,false,false));
                }
            }
        );

        //populate beta dropdown
        this.comboBoxConstraint.setButtonCell(cellFactoryConstraints.call(null));
        this.comboBoxConstraint.setCellFactory(cellFactoryConstraints);
        this.comboBoxConstraint.getSelectionModel().selectedItemProperty().addListener(new ConstraintComboboxChangeListener(comboBoxConstraint, viewModel.getConstraints()));
        this.comboBoxConstraint.setItems(viewModel.getConstraints());

        //populate target function dropdown
        this.comboBoxObjectives.setItems(viewModel.getObjectives());
        this.comboBoxObjectives.getSelectionModel().select(1);
        this.comboBoxObjectives.valueProperty().bindBidirectional(viewModel.getSelectedObjectiveProperty());
    }

    private void setColumnWidth(int column, double pColumnWidth){
        tableViewDemands.getColumns().get(column).prefWidthProperty().bind(tableViewDemands.widthProperty().multiply(pColumnWidth));
        tableViewDemands.getColumns().get(column).maxWidthProperty().bind(tableViewDemands.widthProperty().multiply(pColumnWidth));
        tableViewDemands.getColumns().get(column).minWidthProperty().bind(tableViewDemands.widthProperty().multiply(pColumnWidth));
    }

    private void setColumnWidthRoute(int column, double pColumnWidth){
        tableViewRoute.getColumns().get(column).prefWidthProperty().bind(tableViewRoute.widthProperty().multiply(pColumnWidth));
        tableViewRoute.getColumns().get(column).maxWidthProperty().bind(tableViewRoute.widthProperty().multiply(pColumnWidth));
        tableViewRoute.getColumns().get(column).minWidthProperty().bind(tableViewRoute.widthProperty().multiply(pColumnWidth));
    }

    /**
     * refresh the combobox for the weeks
     */
    public void refreshWeeks() {
        if(comboBoxYears.getItems().size() > 0) {
            comboBoxWeeks.getItems().clear();
            comboBoxWeeks.getItems().addAll(viewModel.getSchedulingController().getDatabaseConnector().getWeeksFromDemand(viewModel.getSelectedYear().getValue()));
            comboBoxWeeks.getSelectionModel().selectFirst();
            comboBoxWeeks.setDisable(comboBoxWeeks.getItems().size() == 0);
        }
    }

    /**
     * reImports the new demand
     */
    private void reimportDemand() {
        if(updatingAllowed) {
            viewModel.getDemands().clear();
            viewModel.getRouteData().clear();
            List<DemandData> demands = viewModel.getSchedulingController().getDatabaseConnector().getData(
                    viewModel.getSelectedYear().getValue(),
                    viewModel.getSelectedWeek().getValue(),
                    viewModel.getSchedulingDuration().get(),
                    false);

            reloadRouteData(demands);

            viewModel.getDemands().addAll(demands);
            int numberJobs = viewModel.getDemands().size();
            int numberArticles = (int) viewModel.getDemands().stream().map(DemandData::getArticle).distinct().count();
            numberOfJobs.setText("" + numberJobs);
            numberOfArticles.setText("" + numberArticles);
        }
    }

    private void reloadRouteData(List<DemandData> demands){
        routeTableData.clear();

        Map<String,Double> processingTimes = viewModel.getSchedulingController().getDatabaseConnector().getProcessingMedianByArticle();
        RouteManager routeManager = viewModel.getSchedulingController().getDatabaseConnector().getArticleRoutes(viewModel.getSelectedMachineEnvironment().get());
        Map<Character,Double> processingTimeMedianByStage = viewModel.getSchedulingController().getDatabaseConnector().getProcessingMedianByStage();

        for(DemandData demandData : demands){
            int jobId = demandData.getJobId();
            double demand = demandData.getDemandScrap();
            String article = demandData.getArticle();
            List<String> route = routeManager.getRoute(article,jobId);
            List<RouteData> routeDataList = new LinkedList<>();
            for(int i = 0; i < route.size(); i++){
                int step = i+1;
                String currentArticle = route.get(i);
                Double processingTime = processingTimes.get(currentArticle);
                if(processingTime == null){
                    Character stage = article.charAt(0);
                    processingTime = processingTimeMedianByStage.get(stage);
                }
                int completeProcessingTime = (int)Math.round(processingTime * demand);
                RouteData data = new RouteData(step,currentArticle,completeProcessingTime);
                routeDataList.add(data);
            }
            routeTableData.put(jobId,routeDataList);
            int jobTime = routeDataList.stream().mapToInt(RouteData::getProcessingTime).sum();
            demandData.setAvgCompleteProdTime(jobTime);
        }
    }

    private void setDemandTableSelectionListener(){
        tableViewDemands.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            viewModel.getRouteData().clear();
            if (newSelection != null) {
                int jobId = newSelection.getJobId();
                viewModel.getRouteData().addAll(routeTableData.get(jobId));
            }
        });
    }

    /**
     * decides whether the database input is valid or not
     *
     * @return if the input is valid
     */
    public boolean databaseInputValid() {
        return comboBoxDatabase.getSelectionModel().getSelectedItem() != null;
    }

    /**
     * set the properties of the schedulingController
     */
    public void prepareNext() {
        viewModel.getSchedulingController().setTemplateSchedule(ScheduleFactory.getScheduleFromDatabase(
                viewModel.getSelectedYear().getValue(),
                viewModel.getSelectedWeek().getValue(),
                viewModel.getSchedulingDuration().getValue(),
                viewModel.getSelectedMachineEnvironment().getValue(),
                viewModel.getSelectedConstraints(),
                viewModel.getSchedulingController().getDatabaseConnector())
        );

        viewModel.getSchedulingController().getTemplateSchedule().setActiveConstraints(viewModel.getSelectedConstraints());
        //Add Scheduling Objectives
        viewModel.getSchedulingController().getTemplateSchedule().setActiveObjective(comboBoxObjectives.getSelectionModel().getSelectedItem());

        viewModel.getSchedulingController().getTemplateSchedule().applyConstraints();

        TreeNode.cleanTree();

        List<Algorithm> filteredAlgorithms = Scheduler.filter( this.viewModel.getSelectedMachineEnvironment().getValue(), this.viewModel.getSelectedConstraints());

        viewModel.getConstructiveAlgorithms().clear();
        viewModel.getIterativeAlgorithms().clear();

        viewModel.getConstructiveAlgorithms().addAll(AlgorithmAnnotations.constructiveAlgorithms.stream().filter(clazz -> filteredAlgorithms.stream().anyMatch(alg -> alg.getClass().equals(clazz))).collect(Collectors.toList()));
        viewModel.getIterativeAlgorithms().addAll(AlgorithmAnnotations.iterativeAlgorithms.stream().filter(clazz -> filteredAlgorithms.stream().anyMatch(alg -> alg.getClass().equals(clazz))).collect(Collectors.toList()));
    }

    @FXML
    public void weekComboboxChanged(){
        if(viewModel.getSelectedWeek().getValue() != null && viewModel.getSelectedMachineEnvironment().get() != null) {
            if (comboBoxWeeks.getItems().size() == 0) {
                return;
            }
            classifyDatabase();
            reimportDemand();
        }
    }

    /**
     * key event change listener
     */
    public void onKeyTyped(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            if(textFieldDuration.getText().length() > 0) {
                Integer value = Integer.parseInt(textFieldDuration.getText());
                if(value > 0 ) {
                    viewModel.getSchedulingDuration().setValue(value);
                    reimportDemand();
                } else {
                    CustomMessage.showWarning( "Warning", "enter a number greater than zero" );
                }
            } else {
                CustomMessage.showWarning( "Warning", "enter a number greater than zero" );
            }

        }
    }

    /**
     * load the db-files from the databases directory in the resource directory
     */
    private ArrayList<File> loadDBFiles() {
        String fileName = "Databases";
        ClassLoader classLoader = getClass().getClassLoader();

        File dir = new File(classLoader.getResource(fileName).getFile());
        final String[] extensions = new String[]{
                "db"
        };
        ArrayList<File> databaseFiles = new ArrayList<>();
        if(dir.isDirectory()) {
            for (final File f : dir.listFiles()) {
                for(String ext : extensions) {
                    if(f.getName().endsWith(ext)) {
                        databaseFiles.add(f);
                    }
                }
            }
        }
        return databaseFiles;
    }

    /**
     * reloads the data with the data in the given database
     */
    private void reloadData(File database) {
        if(database == null){
            return;
        }
        // viewModel.getSchedulingController().getDatabaseConnector().getConnectionForDatabase(databaseName);
        this.updatingAllowed = false;
        viewModel.getSchedulingController().getDatabaseConnector().establishConnectionWithDatabase(database);
        viewModel.setLoadedDB(database);

        //populate scheduling time dropdowns
        //load years from db - automatically refreshes weeks
        this.viewModel.getSelectedYear().addListener((obs, oldV, newV) -> refreshWeeks());
        this.comboBoxYears.getItems().clear();
        this.comboBoxYears.getItems().addAll(viewModel.getSchedulingController().getDatabaseConnector().getYearsFromDemand());
        this.comboBoxYears.getSelectionModel().selectFirst();
        //init tableView
        this.updatingAllowed = true;
        classifyDatabase();
        this.reimportDemand();

        this.tableViewDemands.setItems(viewModel.getDemands());
        this.tableViewRoute.setItems(viewModel.getRouteData());
    }

    public void clearDB(){
        this.comboBoxDatabase.getSelectionModel().select(null);
        this.tableViewDemands.getItems().clear();
    }

    private void classifyDatabase() {
        if(updatingAllowed) {
            Schedule schedule = ScheduleFactory.getScheduleFromDatabase(
                    this.viewModel.getSelectedYear().getValue(),
                    this.viewModel.getSelectedWeek().getValue(),
                    this.viewModel.getSchedulingDuration().getValue(),
                    MachineEnvironment.SINGLE_MACHINE,
                    new HashSet<>(),
                    viewModel.getSchedulingController().getDatabaseConnector());
            ClassificationController classificationController = new ClassificationController();
            MachineEnvironment env = classificationController.classify(schedule);
            viewModel.getSelectedMachineEnvironment().setValue(env);
        }
    }

    /**
     * fill the database combobox with the db-files and "use extern database"
     */
    private void setDatabaseComboBox() {
        ArrayList<File> databaseFiles = loadDBFiles();
        observableListDatabaseCombobox.addAll(databaseFiles);
        comboBoxDatabase.setItems(observableListDatabaseCombobox);
        comboBoxDatabase.setButtonCell(cellFactoryDatabase.call(null));
        comboBoxDatabase.setCellFactory(cellFactoryDatabase);
        comboBoxDatabase.getSelectionModel().selectedItemProperty().addListener((observableValue, s, t1) -> reloadData(t1));
    }
}

