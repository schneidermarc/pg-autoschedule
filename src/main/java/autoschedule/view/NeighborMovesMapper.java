package autoschedule.view;

import autoschedule.algorithms.iterative.localsearch.neighborhoods.Move;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import com.jfoenix.controls.JFXCheckBox;
import javafx.beans.property.BooleanProperty;
import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import lombok.Getter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Mapper for Moves to Neighborhood
 */
public class NeighborMovesMapper {
	public final Class<? extends Neighborhood> neighborhoodClazz;

	@Getter
	private final Map<JFXCheckBox, Class<Move>> cbxMoves = new LinkedHashMap<>();

	private int selectedMoves = 0;

	@Getter
	private boolean childAction = false;

	public NeighborMovesMapper( VBox parentVBox, Class<? extends Neighborhood> neighborhood, BooleanProperty parentCheckBoxProp ) {
		this.neighborhoodClazz = neighborhood;

		VBox vBox = new VBox();
		vBox.setSpacing( 20 );
		vBox.setPadding( new Insets( 0, 10, 0, 10 ) );

		for( Class<Move> clazz : AlgorithmAnnotations.moveStrategies ) {
			JFXCheckBox checkBox = new JFXCheckBox( AlgorithmAnnotations.getNameOfAlgorithm( clazz ) );
			vBox.getChildren().add( checkBox );
			cbxMoves.put( checkBox, clazz );

			checkBox.selectedProperty().addListener( ( obsValue, oldValue, newValue ) -> {
				selectedMoves = newValue ? selectedMoves + 1 : selectedMoves - 1;
				childAction = true;
				if( newValue )
					parentCheckBoxProp.setValue( true );
				else {
					if( selectedMoves == 0 )
						parentCheckBoxProp.setValue( false );
				}
				childAction = false;

			} );

			checkBox.setSelected( true );
		}

		parentVBox.getChildren().add( vBox );
	}

	public void changeSelected( boolean isSelected ) {
		cbxMoves.keySet().forEach( cbx -> cbx.setSelected( isSelected ) );
	}
}
