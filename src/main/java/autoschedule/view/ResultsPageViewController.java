package autoschedule.view;

import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.control.DatabaseConnector;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import autoschedule.utils.*;
import autoschedule.utils.converters.AlgorithmStringConverter;
import autoschedule.utils.converters.LongTimeConverter;
import autoschedule.control.database.TableRowResultsOptimization;
import autoschedule.view.solutionviews.HTMLScheduleExporter;
import autoschedule.view.solutionviews.SolutionProgressViewController;
import autoschedule.view.viewmodel.ViewModel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXProgressBar;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;
import lombok.SneakyThrows;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ResultsPageViewController {

    @FXML
    private JFXProgressBar progressBar;
    @FXML
    private JFXComboBox<SchedulingObjective> comboBoxObjectives;
    @FXML
    private TableView<TableRowResultsOptimization> tableViewResults;
    @FXML
    private TableColumn<TableRowResultsOptimization, Integer> colId;
    @FXML
    private TableColumn<TableRowResultsOptimization, Long> colTime;
    @FXML
    private TableColumn<TableRowResultsOptimization, String> colHeuristic;
    @FXML
    private TableColumn<TableRowResultsOptimization, Double> colObjectiveValue;
    @FXML
    private JFXButton buttonAddAll, buttonAddSelected, buttonRemoveSelected, buttonRemoveAll, buttonStartOptimization;
    @FXML
    private JFXListView<Schedule> listViewGenerated, listViewQueue;
    @FXML
    private JFXListView<String> listViewProperties;

    // Attribute
    private ViewModel viewModel;
    private Stage stage;

    /**
     * initializes the view content
     */
    void init(ViewModel viewModel, Stage stage) {
        this.viewModel = viewModel;
        this.stage = stage;

        this.comboBoxObjectives.valueProperty().bindBidirectional(this.viewModel.getSelectedObjectiveToShowProperty());
        this.comboBoxObjectives.setItems(FXCollections.observableList(viewModel.getObjectives()));

        this.listViewGenerated.setCellFactory(new AlgorithmStringConverter());
        this.listViewGenerated.setItems(viewModel.getGeneratedSolutionsList());

        this.listViewQueue.setCellFactory(new AlgorithmStringConverter());
        this.listViewQueue.setItems(viewModel.getSelectedSolutionsList());

        this.buttonAddAll.disableProperty().bind(viewModel.getOptimizationRunningProperty());
        this.buttonAddSelected.disableProperty().bind(viewModel.getOptimizationRunningProperty());
        this.buttonRemoveAll.disableProperty().bind(viewModel.getOptimizationRunningProperty());
        this.buttonRemoveSelected.disableProperty().bind(viewModel.getOptimizationRunningProperty());
        this.buttonStartOptimization.disableProperty().bind(Bindings.size(listViewQueue.getItems()).isEqualTo(0));
        this.buttonStartOptimization.textProperty().bindBidirectional(viewModel.getOptimizationRunningProperty(), new StringConverter<>() {
            @Override
            public String toString(Boolean value) {
                return value ? "Stop optimization" : "Start optimization";
            }

            @Override
            public Boolean fromString(String s) {
                return s.equals("Stop optimization");
            }
        });


        this.viewModel.getOptimizationRunningProperty().addListener((observableValue, aBoolean, t1) -> {
            if(t1) buttonStartOptimization.getStyleClass().add("runBtn");
            else buttonStartOptimization.getStyleClass().remove("runBtn");
        });

        // Init result TableView
        this.colId.setCellValueFactory(new PropertyValueFactory<>("runId"));
        this.colHeuristic.setCellValueFactory(new PropertyValueFactory<>("heuristic"));
        this.colTime.setCellValueFactory(new PropertyValueFactory<>("time"));
        this.colObjectiveValue.setCellValueFactory(param -> Bindings.createObjectBinding(() -> param.getValue().getEvaluatedObjectives().get(this.viewModel.getSelectedObjectiveToShowProperty().get())));

        this.colTime.setCellFactory(new LongTimeConverter<>(true, this.viewModel.getSelectedObjectiveToShowProperty()));
        this.colObjectiveValue.setCellFactory(new LongTimeConverter<>(false, this.viewModel.getSelectedObjectiveToShowProperty()));

        MenuItem showScheduleItem = new MenuItem("Show Schedule");
        MenuItem showProgressItem = new MenuItem("Show Progress");
        MenuItem deleteSelectedItem = new MenuItem("Delete Selected Item");
        MenuItem deleteAllItems = new MenuItem("Delete All");

        showScheduleItem.setOnAction(event -> showSchedule(tableViewResults.getSelectionModel().getSelectedItem()));
        showProgressItem.setOnAction(event -> showProgress(tableViewResults.getSelectionModel().getSelectedItem()));
        deleteSelectedItem.setOnAction(event -> tableViewResults.getItems().remove(tableViewResults.getSelectionModel().getSelectedItem()));
        deleteAllItems.setOnAction(event -> tableViewResults.getItems().clear());

        this.tableViewResults.setItems(viewModel.getOptimizationFinishedSchedules());
        this.tableViewResults.setContextMenu(new ContextMenu(showScheduleItem, showProgressItem, deleteSelectedItem, deleteAllItems));
        this.tableViewResults.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2){
                showSchedule(tableViewResults.getSelectionModel().getSelectedItem());
            }
        });

        this.progressBar.progressProperty().bind(viewModel.getOptimizationProgressProperty());
    }

    @FXML
    private void handleComboBoxAction() {
        tableViewResults.refresh();
    }
    /**
     * Opens a Popup which shows the non-CLS parameters of the selected Schedule
     * @param mouseEvent the event, bound with the fxml-file
     */
    public void onDoubleMouseClickOnRightList(MouseEvent mouseEvent){
        if (mouseEvent.getClickCount() == 2) {
            openAndChangeSchedule(listViewQueue);
        }
    }
    /**
     * Opens a Popup which shows the non-CLS parameters of the selected Schedule
     * @param mouseEvent the event, bound with the fxml-file
     */
    public void onDoubleMouseClickOnLeftList(MouseEvent mouseEvent){
        if (mouseEvent.getClickCount() == 2) {
            openAndChangeSchedule(listViewGenerated);
        }
    }

    private void openAndChangeSchedule(JFXListView<Schedule> listView) {
        Schedule currentSchedule = listView.getSelectionModel().getSelectedItem();
        Schedule copySchedule = currentSchedule.copy();

        //Load an fxml file which contains the popup-data
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("EditParameterPopup.fxml"));
        // initializing the controller
        EditParameterPopupController popupController = new EditParameterPopupController();
        loader.setController(popupController);
        Parent layout;
        try {
            //Build a new Stage, which is dependant on the mainStage and embeds the fxml-file
            layout = loader.load();
            Scene scene = new Scene(layout);
            Stage popupStage = new Stage();
            popupController.setStage(popupStage);
            popupStage.initOwner(stage);
            popupStage.initModality(Modality.WINDOW_MODAL);
            popupStage.setScene(scene);
            popupStage.setResizable(false);
            popupStage.setTitle("Parameters of " + currentSchedule.getScheduleName());
            popupController.init(copySchedule);

            //Actions after showAndWait are executed once the popup has been closed
            popupStage.showAndWait();

            ObservableList<Schedule> schedulesInList = listView.getItems();
            int indexOfChangedItem = listView.getSelectionModel().getSelectedIndex();
            schedulesInList.set(indexOfChangedItem,copySchedule);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * move all solutions from the generated listView to the selected listView
     */
    public void onAddAllButtonAction(@SuppressWarnings("unused") ActionEvent event) {
        viewModel.getSelectedSolutionsList().addAll(viewModel.getGeneratedSolutionsList());
        viewModel.getGeneratedSolutionsList().clear();
    }

    /**
     * move all selected solutions from the generated listView to the selected listView
     */
    public void onAddSelectedButtonAction(@SuppressWarnings("unused") ActionEvent event) {
        Collection<Schedule> selected = new LinkedList<>(listViewGenerated.getSelectionModel().getSelectedItems());
        for (Schedule schedule : selected) {
            if (viewModel.getGeneratedSolutionsList().contains(schedule)){
                viewModel.getSelectedSolutionsList().add(schedule);
                viewModel.getGeneratedSolutionsList().remove(listViewGenerated.getSelectionModel().getSelectedIndex());

            }
        }
        listViewGenerated.getSelectionModel().clearSelection();
        listViewQueue.getSelectionModel().clearSelection();

    }

    /**
     * move all selected solutions from the selected listView to the genereated listView
     */
    public void onRemoveSelectedButtonAction(@SuppressWarnings("unused") ActionEvent event) {
        Collection<Schedule> selected = new LinkedList<>(listViewQueue.getSelectionModel().getSelectedItems());

        for (Schedule schedule : selected) {
            if (viewModel.getSelectedSolutionsList().contains(schedule)){
                viewModel.getGeneratedSolutionsList().add(schedule);
                viewModel.getSelectedSolutionsList().remove(listViewQueue.getSelectionModel().getSelectedIndex());
            }
        }
        listViewGenerated.getSelectionModel().clearSelection();
        listViewQueue.getSelectionModel().clearSelection();
    }

    /**
     * move all solutions from the selected listView to the genereated listView
     */
    public void onRemoveAllButtonAction(@SuppressWarnings("unused") ActionEvent event) {
        viewModel.getGeneratedSolutionsList().addAll(viewModel.getSelectedSolutionsList());
        viewModel.getSelectedSolutionsList().clear();
    }

    /**
     * loads the schedule which is clicked and shows the GANTT chart
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SneakyThrows
    private void showSchedule(TableRowResultsOptimization row){
        if (Files.notExists(Path.of("Schedules")))
            new File("Schedules").mkdirs();

        File file = new File("Schedules" + File.separator + "Schedule-" + row.getHeuristic().replaceAll(" ", "_").replaceAll(":", "").replaceAll(">", "") + System.currentTimeMillis() + ".html");
        HTMLScheduleExporter ex = new HTMLScheduleExporter(viewModel.getSchedulingController().getDatabaseConnector());
        ex.setFileName("Schedule");
        ex.setFile(file);
        ex.writeSchedule(viewModel.getSchedulingController().getDatabaseConnector().getComputedSchedule(row.getRunId()));

        if (OSValidator.isMac())
            Runtime.getRuntime().exec("open " + file.getAbsolutePath());
        else if (OSValidator.isWindows())
            Desktop.getDesktop().open(file);
    }

    @SneakyThrows
    private void showProgress(TableRowResultsOptimization row) {
        List<OPT_ObjValuesObject> solutionProgress = new DatabaseConnector().getSolutionProgress(row.getRunId());

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("SolutionProgressView.fxml"));
        Stage diagramStage = new Stage();
        diagramStage.initStyle(StageStyle.UTILITY);
        diagramStage.setScene(new Scene(fxmlLoader.load()));
        diagramStage.setResizable(false);
        diagramStage.setTitle("AutoSchedule");
        diagramStage.show();

        SolutionProgressViewController controller = fxmlLoader.getController();
        controller.updateData(solutionProgress);
    }


    /**
     * Starts the calculation
     */
    public void onStartStopOptimizationAction(ActionEvent event) {
        if(viewModel.getOptimizationRunningProperty().get())
            viewModel.getOptimizationRunningProperty().set(false);
        else
            viewModel.getSchedulingController().getOptimizationRunner().startOptimization();
        event.consume();
    }

    /**
     * Load settings in Preferences List
     */
    public void initPreferencesList() {
        ObservableList<String> list = FXCollections.observableList(new LinkedList<>());
        list.add("Database: " + viewModel.getLoadedDB().getName());
        list.add("Starting Year: " + viewModel.getSelectedYear().getValue());
        list.add("Starting Week: " + viewModel.getSelectedWeek().getValue());
        list.add("Duration: " + viewModel.getSchedulingDuration().getValue());
        list.add("Machine Environment: " + viewModel.getSelectedMachineEnvironment().get().getName());
        list.add("Constraints: ");
        viewModel.getConstraints().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).forEach(e -> list.add("      - " + e));
        list.add("Default Objective: " + viewModel.getSelectedObjectiveProperty().get());

        listViewProperties.setItems(list);
    }

    public void prepareNext() {
        viewModel.getClsResultProgress().setValue( 0 );
        initPreferencesList();
    }
}
