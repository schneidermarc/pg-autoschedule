package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.constructive.GifflerThompson;
import autoschedule.algorithms.constructive.NEH;
import autoschedule.algorithms.constructive.PriorityRuleAlgorithm;
import autoschedule.algorithms.constructive.ga.GA;
import autoschedule.algorithms.iterative.localsearch.LocalSearch;
import autoschedule.algorithms.iterative.localsearch.SimulatedAnnealing;
import autoschedule.algorithms.iterative.localsearch.TabuSearch;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.view.viewmodel.ViewModel;

public class AlgorithmTabFactory {

  public AlgorithmTab generate(Class<?> item, ViewModel model) {
    AlgorithmTab tab;

    if( item == PriorityRuleAlgorithm.class ){
      tab = new PriorityRuleTab( model );
    }
    else if(item == GifflerThompson.class) {
      tab = new GifflerThompsonTab( model );
    }
    else if(item == GA.class) {
      tab = new GATab( model );
      model.addNewAlgorithmViewModel( GA.class, tab.getViewModel() );
	}
    else if(item == LocalSearch.class) {
      tab = new LocalSearchTab(model);
      model.addNewAlgorithmViewModel( LocalSearch.class, tab.getViewModel() );
    }
    else if(item == TabuSearch.class) {
      tab = new TabuSearchTab(model);
      model.addNewAlgorithmViewModel( TabuSearch.class, tab.getViewModel() );
    }
    else if(item == NEH.class) {
      tab = new NEHTab(model);
      model.addNewAlgorithmViewModel( NEH.class, tab.getViewModel() );
    }
    else if(item == SimulatedAnnealing.class) {
      tab = new SimulatedAnnealingTab( model );
      model.addNewAlgorithmViewModel( SimulatedAnnealing.class, tab.getViewModel() );
    }
    else {
      tab = new GenericAlgorithmTab();
    }

    tab.setClazz(item);
    tab.setText(AlgorithmAnnotations.getNameOfAlgorithm(item));
    return tab;
  }

}
