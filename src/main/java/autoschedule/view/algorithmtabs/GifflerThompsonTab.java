package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.view.viewmodel.ViewModel;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import autoschedule.view.viewmodel.algorithmModels.GifflerThompsonViewModel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.LinkedHashMap;
import java.util.Map;

public class GifflerThompsonTab extends AlgorithmTab{

    private final Map<JFXCheckBox, Class<?>> checkBoxClassMap = new LinkedHashMap<>();

    private final GifflerThompsonViewModel model;

    public GifflerThompsonTab(ViewModel viewModel) {
        super();
        this.model = new GifflerThompsonViewModel(viewModel);
        HBox hBox = new HBox();
        VBox vBoxCLSParams = new VBox();
        vBoxCLSParams.setSpacing(20);vBoxCLSParams.setPrefWidth(475.0);
        vBoxCLSParams.setPadding(new Insets(15,20, 10,10));

        JFXButton buttonSelectAll = new JFXButton("Toggle all");
        vBoxCLSParams.getChildren().add(buttonSelectAll);

        for(Class<?> clazz : AlgorithmAnnotations.priorityRules) {
            JFXCheckBox checkBox = new JFXCheckBox(AlgorithmAnnotations.getNameOfAlgorithm(clazz));
            checkBox.setSelected(true);
            vBoxCLSParams.getChildren().add(checkBox);
            checkBoxClassMap.put(checkBox, clazz);
        }

        buttonSelectAll.setOnAction(e -> checkBoxClassMap.keySet().forEach(checkBox -> checkBox.setSelected(!checkBox.isSelected())));

        hBox.getChildren().addAll(intoScrollPane(vBoxCLSParams), intoScrollPane(generateVBoxNonCLSParams(model, false)));
        this.setContent(hBox);
    }

    @Override
    public AlgorithmViewModel getViewModel() {
        return model;
    }

    @Override
    public TreeNode getSelectedCombinators() {
        TreeNode root = new TreeNode( clazz, null );
        checkBoxClassMap.keySet().stream()
                .filter(CheckBox::isSelected)
                .map(checkBoxClassMap::get)
                .forEach( a -> root.registerChild(new TreeNode(a, root)));
        return root;
    }

    @Override
    public String validInput() {
        for (Map.Entry<JFXCheckBox, Class<?>> jfxCheckBoxClassEntry : checkBoxClassMap.entrySet()) {
            JFXCheckBox checkBox = jfxCheckBoxClassEntry.getKey();
            if (checkBox.isSelected()) {
                return null;
            }
        }
        return "Select at least one priority rule for " + AlgorithmAnnotations.getNameOfAlgorithm( clazz );
    }
}
