package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.utils.Tuple;
import autoschedule.utils.logging.Logger;
import autoschedule.utils.parameteropt.JSONOptimizationConfig;
import autoschedule.utils.parameteropt.Optimizer;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel.NonCLSParam;
import autoschedule.view.viewmodel.algorithmModels.NodeType;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.*;

public abstract class AlgorithmTab extends Tab {

	/**
	 * Must be overwritten by specific tab
	 * if one AlgorithmViewModel exists
	 *
	 * @return null or specific AlgorithmViewModel
	 */
	public AlgorithmViewModel getViewModel() {
		return null;
	}

	/**
	 * Clazz of the algorithm corresponding to the AlgorithmTab
	 */
	@Setter @Getter
	protected Class<?> clazz;

	/**
	 * Create tree for CLS
	 * The root is the Algorithm itself and the
	 * leaves are specific to the parameters of the constructors
	 *
	 * @return root of the tree
	 */
	public abstract TreeNode getSelectedCombinators();

	/**
	 * Check, whether the input is valid or not
	 * @return if the input is valid, the returned string is null, otherwise a error message is returned
	 */
	public abstract String validInput();

	public static VBox generateVBoxParameterOptimization( Algorithm algorithm ) {

		if( algorithm == null || algorithm.getModel() == null) {
			VBox vBox = new VBox();
			HBox hbox = createSpacedHBox( 5, 10, 10 );
			hbox.getChildren().add( new Label( "Nothing to change here!"));

			vBox.getChildren().add( hbox );
			return vBox;
		}

		/* ---------- ---------- */
		Class<?> algorithmClazz = algorithm.getClass();

		// Reverse the order of the clazz hierarchy
		// Reason: Displaying fields of sub-classes are the first fields
		// and the specific fields are lower
		List<Class<?>> algorithmClazzes = new LinkedList<>();
		while( algorithmClazz != Object.class ) {
			algorithmClazzes.add( 0, algorithmClazz );
			// get next level class
			algorithmClazz = algorithmClazz.getSuperclass();
		}

		List<Field> boundedFields2Display = new ArrayList<>();
		for( Class<?> c : algorithmClazzes ) {
			for( Field field : c.getDeclaredFields() ) {
				try {
					if( Modifier.isStatic(field.getModifiers() ))
						continue;

					boolean canAccess = field.canAccess( algorithm );
					if( !canAccess )
						field.setAccessible( true );

					Object obj = field.get( algorithm );
					if( obj != null && algorithm.getModel().getBoundClassField( obj.getClass() ) != null ) {
						boundedFields2Display.add( algorithm.getModel().getBoundFields().get( obj.getClass() ));
					}
					// change back the accessibility
					field.setAccessible( canAccess );
				} catch( IllegalAccessException e ) {
					e.printStackTrace();
				}
			}
		}
		/* ---------- ---------- */

		// clear the Map that contains the nodes which are generated
		algorithm.getModel().getNodes().clear();

		VBox vBoxNonCLSParams = new VBox();
		vBoxNonCLSParams.setSpacing( 20 );
		vBoxNonCLSParams.setPrefWidth( 475.0 );
		vBoxNonCLSParams.setPadding( new Insets( 15, 20, 10, 10 ) );

		Class<?> clazz = algorithm.getModel().getClass();

		// Reverse the order of the clazz hierarchy
		// Reason: Displaying fields of sub-classes are the first fields
		// and the specific fields are lower
		List<Class<?>> clazzes = new LinkedList<>();
		while( clazz != Object.class ) {
			clazzes.add( 0, clazz );
			// get next level class
			clazz = clazz.getSuperclass();
		}

		for( Class<?> c : clazzes ) {
			for( Field field : c.getDeclaredFields() ) {
				if( field.isAnnotationPresent( NonCLSParam.class ) ){
					boolean canAccess = field.canAccess( algorithm.getModel() );
					if( !canAccess )
						field.setAccessible( true );

					NonCLSParam annotation = field.getAnnotation( NonCLSParam.class );

					boolean displayField = false;

					if( annotation.boundClass() != Object.class ) {
						if( boundedFields2Display.contains( field ) ){
							displayField = true;
						}
					} else {
						displayField = true;
					}

					if( displayField ) {
						if( annotation.applicableAlgorithms().length > 0 && Arrays.stream( annotation.applicableAlgorithms() ).noneMatch( a -> a == algorithm.getModel().getClass() ) )
							continue;

						generateHBoxByAnnotationAndField( algorithm.getModel(), vBoxNonCLSParams, field, annotation, true );
					}
					// change back the accessibility
					field.setAccessible( canAccess );
				}
			}
		}
		return vBoxNonCLSParams;
	}

	/**
	 * Generate a VBox (ScrollPane) with the Non-CLS-Parameter of a Algorithm
	 * The Non-CLS-Parameters are located inside a model
	 * and annotated with @NonCLSParam
	 *
	 * @return VBox (ScrollPane) containing JavaFX-Nods for changing the Non-CLS-Parameter or null,
	 * if no ViewModel is set (getViewModel must be overwritten by the tab and return the specific ViewModel)
	 */
	public static VBox generateVBoxNonCLSParams( AlgorithmViewModel avModel, boolean generatePOBoxes ) {
		if( avModel == null ) {
			return new VBox();
		}

		// clear the Map that contains the nodes which are generated
		avModel.getNodes().clear();

		VBox vBoxNonCLSParams = new VBox();
		vBoxNonCLSParams.setSpacing( 20 );
		vBoxNonCLSParams.setPrefWidth( 475.0 );
		vBoxNonCLSParams.setPadding( new Insets( 15, 20, 10, 10 ) );

		Class<?> clazz = avModel.getClass();

		// Reverse the order of the clazz hierarchy
		// Reason: Displaying fields of sub-classes are the first fields
		// and the specific fields are lower
		List<Class<?>> clazzes = new LinkedList<>();
		while( clazz != Object.class ) {
			clazzes.add( 0, clazz );
			// get next level class
			clazz = clazz.getSuperclass();
		}

		for( Class<?> c : clazzes ) {
			for( Field field : c.getDeclaredFields() ) {
				if( field.isAnnotationPresent( NonCLSParam.class ) )
					performFieldAnalyze( avModel, field, vBoxNonCLSParams, generatePOBoxes );
			}
		}
		return vBoxNonCLSParams;
	}

	protected static Parent intoScrollPane( VBox vbox ) {
		ScrollPane scrollPane = new ScrollPane( vbox );
		scrollPane.setFitToHeight( true );
		return scrollPane;
	}

	/**
	 * @param field            to analyze
	 * @param vBoxNonCLSParams vbox for Non-CLS-Parameter
	 */
	private static void performFieldAnalyze( AlgorithmViewModel avModel, Field field, VBox vBoxNonCLSParams, boolean generatePOBoxes ) {
		// set field accessible if not accessible
		boolean canAccess = field.canAccess( avModel );
		if( !canAccess )
			field.setAccessible( true );

		NonCLSParam annotation = field.getAnnotation( NonCLSParam.class );

		if( annotation.applicableAlgorithms().length > 0 && Arrays.stream( annotation.applicableAlgorithms() ).noneMatch( a -> a == avModel.getClass() ) )
			return;

		generateHBoxByAnnotationAndField( avModel, vBoxNonCLSParams, field, annotation, generatePOBoxes );

		// change back the accessibility
		field.setAccessible( canAccess );
	}

	/**
	 * Generates a new hBox and inserts a label and a specific JavaFX-Node to this hBox.
	 * Then adds the hBox to the given vbox
	 *
	 * @param vBoxNonCLSParams vbox to which the combination of label and specific JavaFX-Node will be added to
	 * @param field            annotated field
	 * @param annotation       annotation-object corresponding to the field
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	private static void generateHBoxByAnnotationAndField(AlgorithmViewModel avModel, VBox vBoxNonCLSParams, Field field, NonCLSParam annotation, boolean generatePOBoxes ) {
		try {
			HBox hbox = createSpacedHBox( 5, 0, 0 );
			Label label = new Label( annotation.paramName() );
			label.setPrefWidth( 220.0 );

			// instantiate StringConverter if one is set
			StringConverter<?> converter = null;
			if( !( annotation.converter() == AlgorithmViewModel.DEFAULT.class ) ) {
				Constructor<? extends StringConverter<?>> converterConstructor = annotation.converter().getDeclaredConstructor();
				converter = converterConstructor.newInstance();
			}

			/* BEGIN :: Switch-case to identify the JavaFX-Node-Type */
			switch( annotation.nodeType() ) {
				case JFXComboBox:
					JFXComboBox<Class<?>> objectiveFunctionCombobox = NodeType.instantiate( annotation.nodeType() );

					// if 'allItems' is set, the corresponding field must be found by its name
					try {
						objectiveFunctionCombobox.setItems( findAllItemsField( avModel, annotation.allItems() ) );
						//noinspection unchecked
						objectiveFunctionCombobox.valueProperty().bindBidirectional( (Property<Class<?>>) field.get( avModel ) );
						objectiveFunctionCombobox.valueProperty().addListener(
								(obsValue, oV, nV)
										-> JSONOptimizationConfig.loadConfiguration( avModel )
						);

						// objectiveFunctionCombobox.getSelectionModel().select( CMax.class );
						setComboBoxConverter( objectiveFunctionCombobox );
						hbox.getChildren().addAll( label, objectiveFunctionCombobox );
						vBoxNonCLSParams.getChildren().add( hbox );


					} catch( NoSuchFieldException e ) {
						StringBuilder builder = new StringBuilder();
						builder.append( "Could not find field for all items. " );
						builder.append( annotation.allItems().equals( "" )
								? "Need to set attribute 'allItems' of @NonCLSParams for field " + field.getName()
								: "Maybe wrong name of field (current name: " + annotation.allItems() + ")" );
						Logger.system( builder );
						Logger.error( e );
					}
					break;
				case TextField:
					if( List.class.isAssignableFrom( field.getType() ) ) {
						VBox tempVBox = new VBox();
						JFXCheckBox poCheckBox = new JFXCheckBox();
						for( Map.Entry<Class<?>, Tuple<Property<Boolean>, Property>> entry : (List<Map.Entry<Class<?>, Tuple<Property<Boolean>, Property>>>) field.get( avModel ) ) {
							HBox tempHBox = createSpacedHBox( 0, 0, 0 );

							JFXCheckBox checkBox = new JFXCheckBox( AlgorithmAnnotations.getNameOfAlgorithm( entry.getKey() ) );
							checkBox.disableProperty().bindBidirectional( poCheckBox.selectedProperty() );
							checkBox.selectedProperty().bindBidirectional( entry.getValue().getFirst() );
							checkBox.setPrefWidth( 220.0 );
							TextField tempTextField = NodeType.instantiate( poCheckBox.selectedProperty(), annotation.nodeType() );
							tempTextField.textProperty().bindBidirectional( entry.getValue().getSecond(), converter );

							tempHBox.getChildren().addAll( checkBox, tempTextField );
							tempVBox.getChildren().add( tempHBox );
						}
						if( generatePOBoxes ) {
							HBox poHBox = createSpacedHBox( 0, 0, 0 );
							Label poLabel = new Label( "Optimize:" );
							//If there is no map-entry for this field, create one here
							if( avModel.getParameterOptimizationMap().get( field ) == null ) {
								Property<Boolean> poActive = new SimpleObjectProperty<>( false );
								avModel.getParameterOptimizationMap().put( field, poActive );
							}
							poCheckBox.selectedProperty().bindBidirectional( avModel.getParameterOptimizationMap().get( field ) );
							poHBox.getChildren().addAll( poLabel, poCheckBox );
							hbox.getChildren().addAll( new TitledPane( annotation.paramName(), tempVBox ) );

							boolean isOptimizable = false;
							for( Optimizer opt : Optimizer.values() ) {
								if( field.isAnnotationPresent( opt.clazz ) ) {
									isOptimizable = true;
									break;
								}
							}

							if( isOptimizable )
								vBoxNonCLSParams.getChildren().addAll( hbox, poHBox );
							else
								vBoxNonCLSParams.getChildren().addAll( hbox );

						} else {
							hbox.getChildren().addAll( new TitledPane( annotation.paramName(), tempVBox ) );
							vBoxNonCLSParams.getChildren().add( hbox );
						}
					} else {
						//If Parameter-optimization checkboxes should be displayed
						if( generatePOBoxes ) {
							Label poLabel = new Label( "Optimize:" );
							JFXCheckBox poCheckBox = new JFXCheckBox();
							//If there is no map-entry for this field, create one here
							if( avModel.getParameterOptimizationMap().get( field ) == null ) {
								Property<Boolean> poActive = new SimpleObjectProperty<>( false );
								avModel.getParameterOptimizationMap().put( field, poActive );
							}
							poCheckBox.selectedProperty().bindBidirectional( avModel.getParameterOptimizationMap().get( field ) );
							TextField textField = NodeType.instantiate( poCheckBox.selectedProperty(), annotation.nodeType() );
							textField.textProperty().bindBidirectional( (Property) field.get( avModel ), converter );

							boolean isOptimizable = false;
							for( Optimizer opt : Optimizer.values() ) {
								if( field.isAnnotationPresent( opt.clazz ) ) {
									isOptimizable = true;
									break;
								}
							}

							if( isOptimizable ) {

								hbox.getChildren().addAll( label, textField, poLabel, poCheckBox );
							} else {
								hbox.getChildren().addAll( label, textField, poLabel );
							}
						} else {
							TextField textField = NodeType.instantiate( annotation.nodeType() );
							textField.textProperty().bindBidirectional( (Property) field.get( avModel ), converter );
							hbox.getChildren().addAll( label, textField );
						}
						vBoxNonCLSParams.getChildren().add( hbox );
					}
					break;
			}
			if( annotation.boundClass() != Object.class ) {
				List<Node> nodes = avModel.getNodes().computeIfAbsent( annotation.boundClass(), k -> new ArrayList<>() );
				nodes.add( hbox );
			}

			/* END :: Switch-case */
		} catch( NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e ) {
			e.printStackTrace();
		}
	}

	private static void setComboBoxConverter( JFXComboBox<Class<?>> objectiveFunctionCombobox ) {
		objectiveFunctionCombobox.setConverter( new StringConverter<>() {
			private final Map<String, Class<?>> map = new HashMap<>();

			@Override
			public String toString( Class<?> o ) {
				String str = "";
				if( o != null ) {
					str = AlgorithmAnnotations.getNameOfAlgorithm( o );
					map.put( str, o );
				}
				return str;
			}

			@Override
			public Class<?> fromString( String str ) {
				if( !map.containsKey( str ) ) {
					objectiveFunctionCombobox.setValue( null );
					objectiveFunctionCombobox.getEditor().clear();
					return null;
				}
				return map.get( str );
			}
		} );
	}

	protected static HBox createSpacedHBox( int spacing, int top, int bottom ) {
		HBox hbox = new HBox();
		hbox.setSpacing( spacing );
		hbox.setPrefWidth( 475.0 );
		hbox.setPadding( new Insets( top, 20, bottom, 10 ) );
		return hbox;
	}

	/**
	 * Find and return field by its name
	 * Iterates over all classes down to Object.class
	 * @param viewModel ViewModel corresponding to the field
	 * @param fieldName name of the field
	 * @param <T> will be automatically estimate by java
	 * @return specific field casted to type T or null, if no field with fieldName was found
	 * @throws NoSuchFieldException Thrown, if no field were found
	 */
	@SuppressWarnings( "unchecked" )
	private static <T> T findAllItemsField( AlgorithmViewModel viewModel, String fieldName )
			throws NoSuchFieldException {
		Class<?> clazz = viewModel.getClass();

		while( clazz != Object.class ) {
			try {
				Field field = clazz.getDeclaredField( fieldName );
				boolean canAccess = field.canAccess( viewModel );
				if( !canAccess )
					field.setAccessible( true );

				T retVal = (T) field.get( viewModel );

				// change back the accessibility
				field.setAccessible( canAccess );
				return retVal;
			} catch( NoSuchFieldException | IllegalAccessException e ) {
				//Logger.system( e.getMessage() );
				clazz = clazz.getSuperclass();
			}
		}
		throw new NoSuchFieldException( fieldName );
	}
}
