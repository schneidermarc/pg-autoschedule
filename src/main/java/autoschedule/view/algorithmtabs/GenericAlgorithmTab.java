package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.utils.TreeNode;
import autoschedule.view.viewmodel.ViewModel;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GenericAlgorithmTab extends AlgorithmTab {
    @Override
    public TreeNode getSelectedCombinators() {
        return new TreeNode( clazz, null );
    }

    @Override
    public String validInput() {
        return null;
    }
}
