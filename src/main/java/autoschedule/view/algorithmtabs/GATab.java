package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.constructive.ga.GA;
import autoschedule.algorithms.constructive.ga.mutation.GAMutationOperator;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.view.customcontrols.multiselectcombobox.GAComboboxChangeListener;
import autoschedule.view.customcontrols.multiselectcombobox.MultiSelectComboBoxCallback;
import autoschedule.view.viewmodel.ViewModel;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import autoschedule.view.viewmodel.algorithmModels.GAViewModel;
import com.jfoenix.controls.JFXComboBox;
import javafx.beans.property.Property;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.util.Map;

public class GATab extends AlgorithmTab {

    /**
     * Corresponding model
     */
    private final GAViewModel model;

    public GATab(ViewModel viewModel) {
        super();
        model = new GAViewModel(viewModel);
        HBox hBox = new HBox();
        VBox vBoxCLSParams = new VBox();
        VBox vBoxNonCLSParams = new VBox();
        vBoxCLSParams.setSpacing(20);vBoxCLSParams.setPrefWidth(475.0);
        vBoxCLSParams.setPadding(new Insets(15,20, 10,10));
        vBoxNonCLSParams.setSpacing(20);vBoxNonCLSParams.setPrefWidth(475.0);
        vBoxNonCLSParams.setPadding(new Insets(15,20, 10,10));

        // START CLS Parameters
        vBoxCLSParams.getChildren().add( addGACLSOperator("Crossover Operator", model.getSelectedCrossoverOperators()));
        //vBoxCLSParams.getChildren().add( addGACLSOperator("Mutation Operators", model.getSelectedMutationOperators()) );
        vBoxCLSParams.getChildren().add( addGACLSOperator("Selection Operator", model.getSelectedSelectionOperators()));
        // END CLS Parameters

        hBox.getChildren().addAll(vBoxCLSParams,intoScrollPane(generateVBoxNonCLSParams(model, false)));
        this.setContent(hBox);
    }

    /**
     * Get corresponding model
     *
     * @return model
     */
    @Override
    public AlgorithmViewModel getViewModel() {
        return model;
    }

    /**
     * Create a HBox with a label and a combobox
     *
     * @param labelText text of the label
     * @param choosableOperators items for the combobox
     * @return hbox (label: combobox)
     */
    private static <T> HBox addGACLSOperator(String labelText,  ObservableList<Map.Entry<Class<T>, Boolean>> choosableOperators) {
        HBox hbox = createSpacedHBox(20, 15,10);

        Label gaOperatorLabel = new Label(labelText);
        gaOperatorLabel.setPrefWidth(170.0);

        Callback<ListView<Map.Entry<Class<T>, Boolean>>, ListCell<Map.Entry<Class<T>, Boolean>>> cellFactory = new MultiSelectComboBoxCallback<>();

        JFXComboBox<Map.Entry<Class<T>, Boolean>> gaComboBoxOperator = new JFXComboBox<>();
        gaComboBoxOperator.setPrefWidth( 270.0 );
        gaComboBoxOperator.setButtonCell(cellFactory.call(null));
        gaComboBoxOperator.setCellFactory(cellFactory);
        gaComboBoxOperator.getSelectionModel().selectedItemProperty().addListener(new GAComboboxChangeListener<>(gaComboBoxOperator));
        gaComboBoxOperator.setItems(choosableOperators);
        gaComboBoxOperator.getSelectionModel().selectFirst();
        gaComboBoxOperator.getSelectionModel().getSelectedItem().setValue( true );

        hbox.getChildren().addAll( gaOperatorLabel, gaComboBoxOperator );
        return hbox;
    }

    /**
     * Create tree for CLS
     *
     * @return root of the tree
     */
    @Override
    public TreeNode getSelectedCombinators() {
        TreeNode root = new TreeNode( GA.class, null, model );

        model.getSelectedCrossoverOperators().forEach(operator -> root.registerChild(new TreeNode( operator.getKey(), root)));
        model.getSelectedSelectionOperators().forEach(operator -> root.registerChild(new TreeNode(operator.getKey(), root )));
        root.registerChild(new TreeNode( GAMutationOperator.class, root));

        return root;
    }

    @Override
    public String validInput() {
        return null;
    }
}
