package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.iterative.localsearch.TabuSearch;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.SteepestDescent;
import autoschedule.view.viewmodel.ViewModel;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import autoschedule.view.viewmodel.algorithmModels.TabuSearchViewModel;

import java.util.Collections;
import java.util.List;

public class TabuSearchTab extends AbstractLocalSearchTab<TabuSearchViewModel> {

	public TabuSearchTab( ViewModel viewModel ) {
		super( viewModel );
	}

	@Override
	public TabuSearchViewModel createAlgorithmViewModel( ViewModel model ) {
		return new TabuSearchViewModel( model );
	}

	@Override
	protected List<Class<? extends Neighborhood>> applicableNeighborhood() {
		return Collections.singletonList( SteepestDescent.class );
	}

	@Override
	protected Class<? extends Algorithm> getCorrespondingAlgorithmClazz() {
		return TabuSearch.class;
	}

	/**
	 * Get corresponding model
	 *
	 * @return model
	 */
	@Override
	public AlgorithmViewModel getViewModel() {
		return model;
	}
}
