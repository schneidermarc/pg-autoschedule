package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.iterative.localsearch.SimulatedAnnealing;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.view.viewmodel.ViewModel;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import autoschedule.view.viewmodel.algorithmModels.SimulatedAnnealingViewModel;

import java.util.ArrayList;
import java.util.List;

public class SimulatedAnnealingTab
        extends AbstractLocalSearchTab<SimulatedAnnealingViewModel> {

    public SimulatedAnnealingTab( ViewModel viewModel ) {
        super( viewModel );
    }

    @Override
    public SimulatedAnnealingViewModel createAlgorithmViewModel( ViewModel model ) {
        return new SimulatedAnnealingViewModel( model );
    }

    @Override
    protected List<Class<? extends Neighborhood>> applicableNeighborhood() {
        return new ArrayList<>( AlgorithmAnnotations.neighborhoods );
    }

    @Override
    protected Class<? extends Algorithm> getCorrespondingAlgorithmClazz() {
        return SimulatedAnnealing.class;
    }


    /**
     * Get corresponding model
     *
     * @return model
     */
    @Override
    public AlgorithmViewModel getViewModel() {
        return model;
    }
}
