package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.iterative.localsearch.LocalSearch;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.view.viewmodel.ViewModel;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import autoschedule.view.viewmodel.algorithmModels.LocalSearchViewModel;

import java.util.ArrayList;
import java.util.List;

public class LocalSearchTab
		extends AbstractLocalSearchTab<LocalSearchViewModel> {

	public LocalSearchTab( ViewModel viewModel ) {
		super( viewModel );
	}

	@Override
	public LocalSearchViewModel createAlgorithmViewModel( ViewModel model ) {
		return new LocalSearchViewModel( model );
	}

	@Override
	protected List<Class<? extends Neighborhood>> applicableNeighborhood() {
		return new ArrayList<>( AlgorithmAnnotations.neighborhoods );
	}

	@Override
	protected Class<? extends Algorithm> getCorrespondingAlgorithmClazz() {
		return LocalSearch.class;
	}

	/**
	 * Get corresponding model
	 *
	 * @return model
	 */
	@Override
	public AlgorithmViewModel getViewModel() {
		return model;
	}
}
