package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.view.NeighborMovesMapper;
import autoschedule.view.viewmodel.ViewModel;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import com.jfoenix.controls.JFXCheckBox;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractLocalSearchTab<T extends AlgorithmViewModel>
		extends AlgorithmTab {
	/**
	 * Corresponding model
	 */
	protected final T model;

	// >>> ---------- ---------- ---------- <<<
	// >>> CLS Parameters
	// >>> ---------- ---------- ---------- <<<
	protected final Map<JFXCheckBox, NeighborMovesMapper> cbxNeighborhoods = new LinkedHashMap<>();

	public AbstractLocalSearchTab( ViewModel viewModel ) {
		super();
		this.model = createAlgorithmViewModel( viewModel );

		HBox hBox = new HBox();
		VBox vBoxCLSParams = new VBox();
		vBoxCLSParams.setSpacing( 20 );
		vBoxCLSParams.setPrefWidth( 475.0 );
		vBoxCLSParams.setPadding( new Insets( 15, 20, 10, 10 ) );

		// loop through the annotated neighborhoods
		VBox generateVBoxNonCLSParams = generateVBoxNonCLSParams( model, false );
		for( Class<? extends Neighborhood> clazz : applicableNeighborhood() ) {
			addVBoxForNeighborhood( vBoxCLSParams, clazz );
		}

		hBox.getChildren().addAll( intoScrollPane( vBoxCLSParams ), intoScrollPane( generateVBoxNonCLSParams ) );
		this.setContent( hBox );
	}

	private void addVBoxForNeighborhood( VBox parentVBox, Class<? extends Neighborhood> neighborhoodClazz ) {
		VBox vBox = new VBox();
		vBox.setSpacing( 20 );
		vBox.setPadding( new Insets( 0, 10, 0, 10 ) );

		JFXCheckBox cbxNeighborhood = new JFXCheckBox( AlgorithmAnnotations.getNameOfAlgorithm( neighborhoodClazz ) );

		if( model.getNodes().get( neighborhoodClazz ) != null )
			model.getNodes().get( neighborhoodClazz ).forEach( node -> node.disableProperty().bind( cbxNeighborhood.selectedProperty().not() ) );

		cbxNeighborhood.setSelected( true );
		vBox.getChildren().add( cbxNeighborhood );

		this.cbxNeighborhoods.put( cbxNeighborhood, new NeighborMovesMapper( vBox, neighborhoodClazz, cbxNeighborhood.selectedProperty() ) );

		for( Map.Entry<JFXCheckBox, NeighborMovesMapper> entry : cbxNeighborhoods.entrySet() ) {
			entry.getKey().selectedProperty().addListener( ( obsValue, oldValue, newValue ) -> {
				if( !entry.getValue().isChildAction() )
					entry.getValue().changeSelected( newValue );
			} );
		}

		parentVBox.getChildren().add( vBox );
	}

	protected abstract List<Class<? extends Neighborhood>> applicableNeighborhood();

	protected abstract Class<? extends Algorithm> getCorrespondingAlgorithmClazz();

	@Override
	public TreeNode getSelectedCombinators() {
		TreeNode root = new TreeNode( getCorrespondingAlgorithmClazz(), null, model );

		cbxNeighborhoods.keySet().stream()
				.filter( CheckBox::isSelected )
				.map( cbxNeighborhoods::get )
				.forEach( mapper -> {
					TreeNode node = root.registerChild( new TreeNode( mapper.neighborhoodClazz, root ) );
					mapper.getCbxMoves().keySet().stream()
							.filter( CheckBox::isSelected )
							.map( mapper.getCbxMoves()::get )
							.forEach( move -> node.registerChild( new TreeNode( move, node ) ) );
				} );

		return root;
	}

	public abstract T createAlgorithmViewModel( ViewModel model );

	@Override
	public String validInput() {
		if( cbxNeighborhoods.keySet().stream().anyMatch( CheckBox::isSelected ) )
			return null;
		else
			return "Select at least one neighborhood for " + AlgorithmAnnotations.getNameOfAlgorithm( clazz );
	}
}
