package autoschedule.view.algorithmtabs;

import autoschedule.algorithms.constructive.NEH;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.view.viewmodel.ViewModel;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import autoschedule.view.viewmodel.algorithmModels.NEHViewModel;
import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;


public class NEHTab extends AlgorithmTab {

    /**
     * Corresponding model
     */
    private final NEHViewModel model;

    public NEHTab(ViewModel viewModel) {
        super();
        this.model = new NEHViewModel(viewModel);
        HBox hBox = new HBox();
        VBox vBoxCLSParams = new VBox();
        VBox vBoxNonCLSParams = new VBox();
        vBoxCLSParams.setSpacing(20);
        vBoxCLSParams.setPrefWidth(475.0);
        vBoxCLSParams.setPadding(new Insets(15, 20, 10, 10));
        vBoxNonCLSParams.setSpacing(20);
        vBoxNonCLSParams.setPrefWidth(475.0);
        vBoxNonCLSParams.setPadding(new Insets(15, 20, 10, 10));

        //Add non-cls parameters
        hBox.getChildren().addAll(vBoxCLSParams, intoScrollPane(generateVBoxNonCLSParams(model, false)));
        this.setContent(hBox);
    }

    /**
     * Get corresponding model
     * @return model
     */
    @Override
    public AlgorithmViewModel getViewModel() {
        return model;
    }

    public  TreeNode getSelectedCombinators() {
        return new TreeNode( NEH.class, null, model );
    }

    @Override
    public String validInput() {
        return null;
    }
}

