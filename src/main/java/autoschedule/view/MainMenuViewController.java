package autoschedule.view;

import autoschedule.utils.logging.Logger;
import autoschedule.view.solutionviews.ShowScheduleController;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import lombok.Setter;
import lombok.SneakyThrows;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("ConstantConditions")
public class MainMenuViewController {

    @FXML
    JFXButton editDatabase, showSchedule, generateSchedule;

    @Setter
    private Stage menuStage;

    private Stage mainStage, scheduleStage;

    public void init() throws IOException {
        loadMainStage();
        loadSchedules();

        ImageView settingsIcon = new ImageView("settingsIcon.png");
        ImageView showScheduleIcon = new ImageView("Show Schedule.png");
        ImageView ganttChartIcon = new ImageView("GanttChart.png");
        settingsIcon.setFitWidth(90);
        settingsIcon.setFitHeight(70);
        editDatabase.setGraphic(settingsIcon);
        showScheduleIcon.setFitWidth(90);
        showScheduleIcon.setFitHeight(70);
        showSchedule.setGraphic(showScheduleIcon);
        ganttChartIcon.setFitWidth(90);
        ganttChartIcon.setFitHeight(70);
        generateSchedule.setGraphic(ganttChartIcon);
    }

    private void loadMainStage() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("MainWindow.fxml"));
        Pane root = fxmlLoader.load();

        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getClassLoader().getResource("styles.css").toExternalForm());


        mainStage = new Stage();
        //mainStage.initStyle(StageStyle.UNIFIED);
        mainStage.setScene(scene);
        mainStage.getIcons().add(new Image("icon.png"));
        mainStage.setResizable(false);
        mainStage.setTitle("AutoSchedule");

        MainWindowViewController mainWindowViewController = fxmlLoader.getController();
        mainWindowViewController.init(mainStage);
    }

    public void loadSchedules() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ShowSchedule.fxml"));
        try {
            Pane root = fxmlLoader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getClassLoader().getResource("styles.css").toExternalForm());

            scheduleStage = new Stage();
            scheduleStage.setScene(scene);
            scheduleStage.getIcons().add(new Image("icon.png"));
            scheduleStage.setResizable(false);
            scheduleStage.setTitle("Show Schedule");

            ShowScheduleController showScheduleController = fxmlLoader.getController();
            showScheduleController.init(scheduleStage);
        }catch(Exception e){
            Logger.error(e);
        }
    }

    /**
     * Opens SQLite DBBrowser on Button click
     * @param actionEvent actionEvent
     */
    public void onEditDatabaseButtonAction(ActionEvent actionEvent) {
        try {
            String osName = System.getProperty("os.name");
            File fileToDB = new File("src/main/resources/Databases/db.db");
            if(osName.contains("Windows")) {
                File fileToExecutable = new File("SQLiteDatabaseBrowser/SQLiteDatabaseBrowserPortable.exe");
                new ProcessBuilder(fileToExecutable.getAbsolutePath(), fileToDB.getAbsolutePath()).start();
            } else if(osName.contains("Mac")) {
                File fileToExecutable = new File("SQLiteDatabaseBrowser/DB Browser for SQLite 2.app/Contents/MacOS/DB Browser for SQLite");
                new ProcessBuilder(fileToExecutable.getAbsolutePath(), fileToDB.getAbsolutePath()).start();
            }
        }catch (Exception e) {
            Logger.error(e);
        }
        actionEvent.consume();
    }

    @SneakyThrows
    @FXML
    public void onLoadDatabaseButtonAction(ActionEvent actionEvent) {
        mainStage.show();
        mainStage.setOnHidden(event -> menuStage.show());
        menuStage.hide();
        actionEvent.consume();
    }

    @FXML
    public void onShowScheduleButtonAction(ActionEvent actionEvent) {
        scheduleStage.show();
        scheduleStage.setOnHidden(event -> menuStage.show());
        menuStage.hide();
        actionEvent.consume();
    }
}
