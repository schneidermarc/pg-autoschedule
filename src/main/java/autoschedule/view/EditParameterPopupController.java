package autoschedule.view;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Schedule;
import autoschedule.view.algorithmtabs.AlgorithmTab;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;


public class EditParameterPopupController {

	@Getter @Setter
	private Stage stage;
	@FXML
	private TextField textFieldName;
	@FXML
	private TextField textFieldAlgString;

	@FXML
	private VBox algorithmBox;


	private Schedule schedule;

	void init(Schedule schedule) {
		this.schedule = schedule;

		//Initialize the textfield
		this.initTextFields();
		stage.setOnShowing( e -> initTextFields());

        this.addParametersFields();

	}
	/**
	 * Sets the default name of the algorithmString and adds the onAction-Event to the control
	 */
	private void initTextFields() {
		textFieldName.setText(schedule.getScheduleName());
		textFieldName.setOnAction(evt -> {
			schedule.setScheduleName(textFieldName.getText());
			stage.setTitle("Parameters of "+schedule.getScheduleName());
		});
		textFieldAlgString.setText(schedule.getSimpleAlgorithmString());
	}

	/**
	 * Scans the schedule (to be edited) and creates a parameter window for each algorithm (which has editable parameters)
	 */
	private void addParametersFields() {
		for(Algorithm alg : schedule.getAlgorithmsToRun()){
			setAlgorithmVBoxVisible(alg);
		}
	}

	/**
	 * For the input-algorithm, set the corresponding vbox visible and managed. So it shows up
	 * @param alg the algorithm, which needs to be shown
	 */
	private void setAlgorithmVBoxVisible(Algorithm alg){
		algorithmBox.getChildren().add( createVBox( AlgorithmAnnotations.getNameOfAlgorithm( alg.getClass() ), alg ));
	}

    /**
     * Creates a V-Box, that shows all parameters of the given model and has a fitting label
     * @param labelName fitting label
     * @param alg the algorithm
     * @return the right vbox
     */
	private VBox createVBox(String labelName, Algorithm alg){
		VBox retBox = new VBox();
		Label label = new Label(labelName);
		label.setStyle("-fx-font-weight: bold;-fx-font-size: 13");
		Separator sep = new Separator();
		VBox parameterVBox = AlgorithmTab.generateVBoxParameterOptimization(alg);
		retBox.getChildren().addAll(label, sep, parameterVBox);
		return retBox;
	}
}
