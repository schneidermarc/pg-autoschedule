package autoschedule.view.solutionviews;

import autoschedule.algorithms.objectives.*;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.utils.logging.CustomMessage;
import autoschedule.view.customcontrols.multiselectcombobox.MultiSelectComboBoxCallback;
import autoschedule.view.customcontrols.multiselectcombobox.ObjectiveComboboxChangeListener;
import com.jfoenix.controls.JFXComboBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ToggleButton;
import javafx.util.Callback;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class SolutionProgressViewController {

	@FXML
	private LineChart<Number, Number> chartLin, chartLog;
	@FXML
	private NumberAxis xAxisLin, yAxisLin, yAxisLog;
	@FXML
	private LogarithmicAxis xAxisLog;
	@FXML
	private ToggleButton toggleButton;
	@FXML
	private JFXComboBox<Map.Entry<SchedulingObjective, Boolean>> lineSelector;

	private static final CMax C_MAX = new CMax();
	private static final CBar C_BAR = new CBar();
	private static final FMax F_MAX = new FMax();
	private static final LMax L_MAX = new LMax();
	private static final UBar U_BAR = new UBar();
	private static final DummyObjective DUMMY = new DummyObjective("Cmax");
	private static final Callback<ListView<Map.Entry<SchedulingObjective, Boolean>>, ListCell<Map.Entry<SchedulingObjective, Boolean>>> cellFactory = new MultiSelectComboBoxCallback<>();
	private static final ObservableList<Map.Entry<SchedulingObjective, Boolean>> objectives = FXCollections.observableArrayList(
			new AbstractMap.SimpleEntry<>(C_MAX, true),
			new AbstractMap.SimpleEntry<>(C_BAR, false),
			new AbstractMap.SimpleEntry<>(F_MAX, false),
			new AbstractMap.SimpleEntry<>(L_MAX, false),
			new AbstractMap.SimpleEntry<>(U_BAR, false),
			new AbstractMap.SimpleEntry<>(DUMMY, false));

	private XYChart.Series<Number, Number> cMax = new XYChart.Series<>();
	private XYChart.Series<Number, Number> cBar = new XYChart.Series<>();
	private XYChart.Series<Number, Number> fMax = new XYChart.Series<>();
	private XYChart.Series<Number, Number> lMax = new XYChart.Series<>();
	private XYChart.Series<Number, Number> uBar = new XYChart.Series<>();

	private List<OPT_ObjValuesObject> values;

	@FXML
	void initialize() {
		this.xAxisLog.setAutoRanging(false);
		this.xAxisLin.setAutoRanging(false);
		this.xAxisLin.setTickUnit(1000);

		this.chartLin.setAnimated(false);
		this.chartLog.setAnimated(false);

		this.chartLin.visibleProperty().bind(toggleButton.selectedProperty().not());
		this.chartLog.visibleProperty().bind(toggleButton.selectedProperty());
		this.toggleButton.setOnAction(e -> updateVisibility());

		this.lineSelector.setButtonCell(cellFactory.call(null));
		this.lineSelector.setCellFactory(cellFactory);
		this.lineSelector.getSelectionModel().selectedItemProperty().addListener(new ObjectiveComboboxChangeListener(lineSelector, objectives));
		this.lineSelector.setItems(objectives);
		this.lineSelector.getSelectionModel().selectLast();

		this.cMax.setName("Cmax");
		this.cBar.setName("Cbar");
		this.fMax.setName("Fmax");
		this.lMax.setName("Lmax");
		this.uBar.setName("Ubar");
	}

	public void updateData(List<OPT_ObjValuesObject> values) {
		if(values.size() == 0) {
			CustomMessage.showInformation("Not enough data", "Dataset is empty. Maybe you selected a run without iterations?");
			return;
		}

		this.values = values;

		this.cMax = new XYChart.Series<>();
		this.cBar = new XYChart.Series<>();
		this.fMax = new XYChart.Series<>();
		this.lMax = new XYChart.Series<>();
		this.uBar = new XYChart.Series<>();

		this.cMax.setName("Cmax");
		this.cBar.setName("Cbar");
		this.fMax.setName("Fmax");
		this.lMax.setName("Lmax");
		this.uBar.setName("Ubar");

		//populating the series with data
		double[] lastValues = new double[]{-1,-1,-1,-1,-1};
		for(int iteration = 1; iteration < values.size()+1; iteration++) {
			OPT_ObjValuesObject obj = values.get(iteration-1);

			if(obj.getCMax() != lastValues[0]) {
				this.cMax.getData().add(new XYChart.Data<>(iteration, obj.getCMax()));
				lastValues[0] = obj.getCMax();
			}
			if(obj.getCBar() != lastValues[1]) {
				this.cBar.getData().add(new XYChart.Data<>(iteration, obj.getCBar()));
				lastValues[1] = obj.getCBar();
			}
			if(obj.getFMax() != lastValues[2]) {
				this.fMax.getData().add(new XYChart.Data<>(iteration, obj.getFMax()));
				lastValues[2] = obj.getFMax();
			}
			if(obj.getLMax() != lastValues[3]) {
				this.lMax.getData().add(new XYChart.Data<>(iteration, obj.getLMax()));
				lastValues[3] = obj.getLMax();
			}
			if(obj.getUBar() != lastValues[4]) {
				this.uBar.getData().add(new XYChart.Data<>(iteration, obj.getUBar()));
				lastValues[4] = obj.getUBar();
			}
		}

		this.cMax.getData().add(new XYChart.Data<>(values.size()-1, values.get(values.size()-1).getCMax()));
		this.cBar.getData().add(new XYChart.Data<>(values.size()-1, values.get(values.size()-1).getCBar()));
		this.fMax.getData().add(new XYChart.Data<>(values.size()-1, values.get(values.size()-1).getFMax()));
		this.lMax.getData().add(new XYChart.Data<>(values.size()-1, values.get(values.size()-1).getLMax()));
		this.uBar.getData().add(new XYChart.Data<>(values.size()-1, values.get(values.size()-1).getUBar()));

		updateVisibility();
	}

	private void updateVisibility() {
		if(this.values == null)
			return;

		chartLin.getData().removeAll(cMax, cBar, fMax, lMax, uBar);
		chartLog.getData().clear();

		Map<SchedulingObjective, Boolean> visibility = objectives.stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		double maxY = 0;
		double maxX = values.size();
		if(visibility.get(C_MAX))
			maxY = Math.max(maxY, addLine(cMax));

		if(visibility.get(C_BAR))
			maxY = Math.max(maxY, addLine(cBar));

		if(visibility.get(F_MAX))
			maxY = Math.max(maxY, addLine(fMax));

		if(visibility.get(L_MAX))
			maxY = Math.max(maxY, addLine(lMax));

		if(visibility.get(U_BAR))
			maxY = Math.max(maxY, addLine(uBar));

		xAxisLin.setUpperBound(maxX);
		xAxisLog.setUpperBound(maxX);

		yAxisLin.setUpperBound(maxY);
		yAxisLog.setUpperBound(maxY);
	}

	/**
	 * @param series Series to be added
	 * @return max Y value of series
	 */
	private double addLine(XYChart.Series<Number, Number> series) {
		(toggleButton.isSelected() ? chartLog : chartLin).getData().add(series);
		return series.getData().stream().mapToDouble(t -> t.getYValue().doubleValue()).max().orElse(0);
	}

	public void onSelectionChanged(@SuppressWarnings("unused") Event event) {
		updateVisibility();
	}
}
