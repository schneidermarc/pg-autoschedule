package autoschedule.view.solutionviews;

import lombok.Getter;


public class ScheduleData {
    // Attribute
    @Getter
    private final long start_time, end_time, deadline, arrival;
    @Getter
    private final String article, jobName, qMachines;
    @Getter
    private final double weight;

    public ScheduleData(long start_time, long end_time, long deadline, long arrival, String article, String jobName, double weight, String qMachines) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.deadline = deadline;
        this.arrival = arrival;
        this.article = article;
        this.jobName = jobName;
        this.weight = weight;
        this.qMachines = qMachines;
    }
}
