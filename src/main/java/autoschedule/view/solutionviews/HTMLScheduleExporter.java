package autoschedule.view.solutionviews;

import autoschedule.control.DatabaseConnector;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


public class HTMLScheduleExporter {

    @Getter @Setter
    private String fileName = "defaultProject";

    @Getter @Setter
    private File file = null;

    @Getter
    private final DatabaseConnector dc;

    public HTMLScheduleExporter(DatabaseConnector dc) {
        this.dc = dc;
    }

    /**
     * Creates HTML Gantt-Chart
     */
    @SneakyThrows
    public void writeSchedule(CachedRowSet rows) {
        String data = "";
        long date = 0;
        LinkedHashMap<String, List<ScheduleData>> map = new LinkedHashMap<>();
        while(rows.next()){
            Calendar cld = Calendar.getInstance();
            cld.set(Calendar.YEAR, rows.getInt("year"));
            cld.set(Calendar.WEEK_OF_YEAR, rows.getInt("week"));
            date = date == 0 ? cld.getTimeInMillis() : date;

            String article = rows.getString("article");
            String jobName = "Job " + rows.getInt("job_id");
            String machine = rows.getString("machine");
            long start_date = date + rows.getLong("start_time") * 1000;
            long end_date = date + rows.getLong("end_time") * 1000;
            long arrival = date + rows.getLong("arrival") * 1000;
            long deadline = date + rows.getLong("deadline") * 1000;
            double weight = rows.getDouble("weight");
            int runId = rows.getInt("run_id");
            String qualMachines = dc.getMachineQualifications(runId, article);
            map.putIfAbsent(machine, new LinkedList<>());
            map.get(machine).add(new ScheduleData(start_date, end_date, deadline, arrival, article, jobName, weight, qualMachines));
        }

        for (String s : map.keySet().stream().sorted((o1, o2) -> {
            if (o2 == null || o1 == null)
                return 0;

            int lengthFirstStr = o1.length();
            int lengthSecondStr = o2.length();

            int index1 = 0;
            int index2 = 0;

            while (index1 < lengthFirstStr && index2 < lengthSecondStr) {
                char ch1 = o1.charAt(index1);
                char ch2 = o2.charAt(index2);

                char[] space1 = new char[lengthFirstStr];
                char[] space2 = new char[lengthSecondStr];

                int loc1 = 0;
                int loc2 = 0;

                do {
                    space1[loc1++] = ch1;
                    index1++;

                    if (index1 < lengthFirstStr) {
                        ch1 = o1.charAt(index1);
                    } else {
                        break;
                    }
                } while (Character.isDigit(ch1) == Character.isDigit(space1[0]));

                do {
                    space2[loc2++] = ch2;
                    index2++;

                    if (index2 < lengthSecondStr) {
                        ch2 = o2.charAt(index2);
                    } else {
                        break;
                    }
                } while (Character.isDigit(ch2) == Character.isDigit(space2[0]));

                String str1 = new String(space1);
                String str2 = new String(space2);

                int result;

                if (Character.isDigit(space1[0]) && Character.isDigit(space2[0])) {
                    Integer firstNumberToCompare = Integer.parseInt(str1.trim());
                    Integer secondNumberToCompare = Integer.parseInt(str2.trim());
                    result = firstNumberToCompare.compareTo(secondNumberToCompare);
                } else {
                    result = str1.compareTo(str2);
                }

                if (result != 0) {
                    return result;
                }
            }
            return lengthFirstStr - lengthSecondStr;
            }).collect(Collectors.toList())){

            for (ScheduleData i : map.get(s)){
                data = data.concat(getDataToString(s, i.getArticle(),i.getJobName(), i.getDeadline(), i.getArrival(), i.getWeight(), i.getStart_time(), i.getEnd_time(), i.getQMachines()));
            }
        }
        String schedule = generateHtmlSchedule(data);

        if (file != null){
            FileWriter writer = new FileWriter(file);
            writer.write(schedule);
            writer.close();
        }

    }




    /**
     * Convert Data to String. This String can use in Google Timeline
     * @param machine Machine name
     * @param article Article name
     * @param startDate Start time to Schedule
     * @param endDate End time to Schedule
     * @return Data as String for HTML File
     */
    private String getDataToString(String machine, String article, String jobName, long deadline, long arrival, double weight, long startDate, long endDate, String qMachines){
        return String.format("[ '%s', '%s', createCustomHTMLContent('%s', '%s', new Date(%d), new Date(%d), new Date(%d), new Date(%d), %s, '%s'), new Date(%d), new Date(%d)],\n",
                machine, article, article, jobName,startDate, endDate, deadline, arrival, weight, qMachines, startDate, endDate);
    }

    @SneakyThrows
    private String generateHtmlSchedule(String data){
        String schedule = "";
        File file = new File("src/main/resources/index.html");
        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null) {
            if (st.contains("__data__")){
                // Replace Datatag with data
                schedule = schedule.concat(st.replace("__data__", data));
            } else {
                schedule = schedule.concat(st + "\n");
            }
        }
        return schedule;
    }
}
