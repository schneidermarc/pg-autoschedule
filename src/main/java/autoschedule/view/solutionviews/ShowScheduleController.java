package autoschedule.view.solutionviews;

import autoschedule.control.DatabaseConnector;
import autoschedule.control.database.*;
import autoschedule.utils.OSValidator;
import autoschedule.utils.logging.Logger;
import autoschedule.view.viewmodel.ScheduleViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.SneakyThrows;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ShowScheduleController {

    private Stage stage;

    @FXML
    private TableView<OPT_ResultsObject> mainTableView;

    @FXML
    private TableColumn<OPT_ResultsObject, Date> dateColumn;

    @FXML
    private TableColumn<OPT_ResultsObject,String> columnAlgorithm;

    @FXML
    private TableColumn<OPT_ResultsObject,Integer> columnIterations;

    @FXML
    private TableColumn<OPT_ResultsObject,Integer> columnDuration;

    @FXML
    private TableColumn<ExecutedAlgorithmData,Integer> columnRuntime;

    @FXML
    private TableView<ExecutedAlgorithmData> executedAlgorithmTable;

    @FXML
    private TableView<ObjectiveValueWrapper> tableObjectiveValues;

    @FXML
    private TableColumn<ObjectiveValueWrapper,String> columnObjective;

    @FXML
    private TableColumn<ObjectiveValueWrapper,Integer> columnObjectiveValue;



    @FXML
    private Label chosenDatabaseLabel;

    private ScheduleViewModel scheduleViewModel;
    //The Result-Object, that should be shown in the right hand side
    private OPT_ResultsObject highlightedResult;

    private DatabaseConnector dbConnector;

    /**
     * Initializes this view:
     * Fills the Viewmodel with data and binds the items to the table. Also adds a mouse-click Listener
     * @param mainStage The current Stage.
     */
    public void init(Stage mainStage) {
        stage = mainStage;

        setColumnWidth(0,0.1);  //run_id
        setColumnWidth(1,0.25);  //run_name
        setColumnWidth(2,0.2);  //calc_date
        setColumnWidth(3,0.2);  //machine_environment
        setColumnWidth(4,0.05);  //week
        setColumnWidth(5,0.05);  //year
        setColumnWidth(6,0.15);  //duration
        //setColumnWidth(7,0.05);  //steps
        setColumnWidthExecutedAlgorithms(0,0.6); //algorithm
        setColumnWidthExecutedAlgorithms(1,0.2); //iterations
        setColumnWidthExecutedAlgorithms(2,0.192); //runtime
        setColumnWidthObjectiveValues(0,0.5);
        setColumnWidthObjectiveValues(1,0.492);


        dateColumn.setCellFactory(column ->
            new TableCell<>(){
                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : new SimpleDateFormat("dd.MM.yyyy").format(item));
                }
            }
        );

        columnDuration.setCellFactory(column ->
            new TableCell<>(){
                @Override
                protected void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                if(item == null){
                    setText(empty ? null : "0 weeks");
                }
                else{
                    setText(empty ? null : (item == 1 ? "1 week" : item + " weeks"));
                }
                }
            }
        );

        columnRuntime.setCellFactory(column ->
                new TableCell<>(){
                    @Override
                    protected void updateItem(Integer item, boolean empty) {
                        super.updateItem(item, empty);
                        if(item != null){
                            setText(item + "ms");
                        }
                    }
                }
        );

        columnObjectiveValue.setCellFactory(column ->
                new TableCell<>(){
                    @Override
                    protected void updateItem(Integer item, boolean empty) {
                        super.updateItem(item, empty);
                        if(item != null){
                            int value = item;
                            setText(value == -1 ? "n/a" : ""+value);
                        }
                    }
                }
        );

        columnAlgorithm.setCellValueFactory((new PropertyValueFactory<>("algorithm")));
        columnIterations.setCellValueFactory((new PropertyValueFactory<>("iterations")));
        columnRuntime.setCellValueFactory((new PropertyValueFactory<>("runtime")));
        columnObjective.setCellValueFactory(new PropertyValueFactory<>("objective"));
        columnObjectiveValue.setCellValueFactory(new PropertyValueFactory<>("value"));

        dbConnector = dbConnectorFactory.createDatabaseConnector();

        //Create new Viewmodel for this Stage
        scheduleViewModel = new ScheduleViewModel();
        //Load all result-data from the database
        scheduleViewModel.init(chosenDatabaseLabel);
        File defaultResultDatabase = new File(DatabaseConnector.DEFAULT_RESULT_DATABASE_PATH);
        scheduleViewModel.updateData(loadResultDatabase(defaultResultDatabase),DatabaseConnector.DEFAULT_RESULT_DATABASE_PATH);
        //Bind the result-items to the table
        mainTableView.setItems(scheduleViewModel.getResultObjects());

        //Set Selection Event
        mainTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                onClickSchedule(mainTableView.getSelectionModel().getSelectedItem());
            }
        });

        // Set selection event to save the schedule
        executedAlgorithmTable.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                onClickAlgorithm(executedAlgorithmTable.getSelectionModel().getSelectedItem());
            }
        });
    }

    private void setColumnWidth(int column, double pColumnWidth){
        mainTableView.getColumns().get(column).prefWidthProperty().bind(mainTableView.widthProperty().multiply(pColumnWidth));
        mainTableView.getColumns().get(column).maxWidthProperty().bind(mainTableView.widthProperty().multiply(pColumnWidth));
        mainTableView.getColumns().get(column).minWidthProperty().bind(mainTableView.widthProperty().multiply(pColumnWidth));
    }

    private void setColumnWidthExecutedAlgorithms(int column, double pColumnWidth){
        executedAlgorithmTable.getColumns().get(column).prefWidthProperty().bind(executedAlgorithmTable.widthProperty().multiply(pColumnWidth));
        executedAlgorithmTable.getColumns().get(column).maxWidthProperty().bind(executedAlgorithmTable.widthProperty().multiply(pColumnWidth));
        executedAlgorithmTable.getColumns().get(column).minWidthProperty().bind(executedAlgorithmTable.widthProperty().multiply(pColumnWidth));
    }

    private void setColumnWidthObjectiveValues(int column, double pColumnWidth){
        tableObjectiveValues.getColumns().get(column).prefWidthProperty().bind(tableObjectiveValues.widthProperty().multiply(pColumnWidth));
        tableObjectiveValues.getColumns().get(column).maxWidthProperty().bind(tableObjectiveValues.widthProperty().multiply(pColumnWidth));
        tableObjectiveValues.getColumns().get(column).minWidthProperty().bind(tableObjectiveValues.widthProperty().multiply(pColumnWidth));
    }

    public List<OPT_ResultsObject> loadResultDatabase(File database){
        //dbConnector.closeConnection(database);
        return dbConnector.getOPTResults(database);
    }

    /**
     * We load the list of algorithms in the right list, by extracting the info out of the alg-string
     * @param clickedObject object is the item, that was clicked by the user in the main table
     */
    public void onClickSchedule(OPT_ResultsObject clickedObject) {
//        Split the alg-string into single algorithms
        ObservableList<ExecutedAlgorithmData> data = FXCollections.observableArrayList();
        data.addAll(dbConnector.getExecutedAlgorithmData(clickedObject.getRunId()));
        executedAlgorithmTable.setItems(data);
        executedAlgorithmTable.refresh();

        ObservableList<ObjectiveValueWrapper> objectiveValueList = FXCollections.observableArrayList();
        objectiveValueList.addAll(dbConnector.getObjectiveValuesForRun(clickedObject.getRunId()));
        tableObjectiveValues.setItems(objectiveValueList);
        tableObjectiveValues.refresh();

        //Set the highligthed-object
        highlightedResult = clickedObject;
    }

    /**
     * Extract the algorithm ID and save the schedule.
     * @param data ExecutedAlgorithmData data
     */
    public void onClickAlgorithm(ExecutedAlgorithmData data){
        if (data != null && data.getAlgorithm() != null){
            Logger.message("RunID: " + highlightedResult.getRunId() + " -- Selection: " + data.getAlgorithm());
            int algId = Character.getNumericValue(data.getAlgorithm().charAt(0));
            saveAndOpenGanttChart(algId);
        }
    }

    public void onClickChangeDatabase(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select result database");
        File resourceFolder = new File(DatabaseConnector.RESOURCE_FOLDER);
        fileChooser.setInitialDirectory(resourceFolder);
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            String fileName = file.getName();
            String[] fileNameSplit = fileName.split("\\.");
            if(!"db".equals(fileNameSplit[fileNameSplit.length-1])){
                displayError("The chosen database has to be a '.db' file");
                return;
            }
            try {
                scheduleViewModel.updateData(loadResultDatabase(file),file.getName());
                executedAlgorithmTable.setItems(FXCollections.observableList(Collections.emptyList()));
            }
            catch(Exception e){
                displayError("An error occurred while reading the requested data from the database");
            }
        }
    }

    public void onClickClearDatabase(@SuppressWarnings("unused") ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Warning");
        alert.setHeaderText("");
        alert.setContentText("Are you sure you want to clear the whole database?");
        ButtonType okButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);
        alert.getButtonTypes().setAll(okButton, noButton);
        alert.showAndWait().ifPresent(type -> {
            if (type == okButton) {
                if(mainTableView.getItems().size() > 0) {
                    mainTableView.getItems().clear();
                    dbConnector.clearResultsDB();
                }
            }
        });
    }

    private void displayError(String contentText){
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setTitle("Error");
        errorAlert.setHeaderText("Input not valid");
        errorAlert.setContentText(contentText);
        errorAlert.showAndWait();
    }

    /**
     * Method after user clicked the button on the bottom-right corner
     * Call the method for showing the gantt-chart here
     */
    public void showGanttChart() {
        saveAndOpenGanttChart(0);
    }

    /**
     * Save Schedule in Tempfolder as HTML an open it.
     * @param algId If Algorithm Id is zero it saves just the newest Schedule.
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SneakyThrows
    private void saveAndOpenGanttChart(int algId){
        if(highlightedResult != null) {
            if (Files.notExists(Path.of("Schedules"))) {
                new File("Schedules").mkdirs();
            }
            File file = new File("Schedules" + File.separator + "Schedule" + System.currentTimeMillis() + ".html");

            HTMLScheduleExporter ex = new HTMLScheduleExporter(dbConnector);
            ex.setFile(file);
            ex.writeSchedule(dbConnectorFactory.createDatabaseConnector().getComputedSchedule(highlightedResult.getRunId(), algId));

            if (OSValidator.isMac())
                Runtime.getRuntime().exec("open " + file.getAbsolutePath());
            else if (OSValidator.isWindows()) {
                Desktop.getDesktop().open(file);
            }
        }
    }

    public void onClickMenu(@SuppressWarnings("unused") ActionEvent actionEvent) {
        stage.close();
        scheduleViewModel = new ScheduleViewModel();
    }

    public void showProgressView(@SuppressWarnings("unused") MouseEvent mouseEvent) throws IOException {
        List<OPT_ObjValuesObject> solutionProgress = new DatabaseConnector().getSolutionProgress(mainTableView.getSelectionModel().getSelectedItem().getRunId());

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("SolutionProgressView.fxml"));
        Stage diagramStage = new Stage();
        diagramStage.initStyle(StageStyle.UTILITY);
        diagramStage.setScene(new Scene(fxmlLoader.load()));
        diagramStage.setResizable(false);
        diagramStage.setTitle("AutoSchedule");
        diagramStage.show();

        SolutionProgressViewController controller = fxmlLoader.getController();
        controller.updateData(solutionProgress);
    }
}
