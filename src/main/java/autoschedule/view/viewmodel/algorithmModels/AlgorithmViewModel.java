package autoschedule.view.viewmodel.algorithmModels;

import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.view.viewmodel.ViewModel;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.util.StringConverter;
import lombok.Getter;
import lombok.Setter;

import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class AlgorithmViewModel {

	/**
	 * ModelID
	 * (Will be set by the cls)
	 */
	@Setter
	@Getter
	public int modelID;

	@Getter
	private ViewModel viewModel;

	// >>> ---------- ---------- ---------- <<<
	// >>> START :: Non CLS Parameters
	// >>> ---------- ---------- ---------- <<<
	/**
	 * To display the combobox for objectiveFunctions,
	 * add the corresponding AlgorithmViewModel to applicableAlgorithms
	 * (see Annotation @NonCLSParam)
	 */
	@Getter
	@NonCLSParam( paramName = "Objective Function",
			nodeType = NodeType.JFXComboBox,
			applicableAlgorithms = {NEHViewModel.class, GAViewModel.class, LocalSearchViewModel.class, TabuSearchViewModel.class, SimulatedAnnealingViewModel.class},
			allItems = "allObjectiveFunctions" )
	private final Property<Class<? extends SchedulingObjective>> selectedObjectiveFunction = new SimpleObjectProperty<>();

	@Getter
	private final ObservableList<Class<SchedulingObjective>> allObjectiveFunctions = FXCollections.observableArrayList( AlgorithmAnnotations.objectiveFunctions );

	@Getter
	private final Property<Boolean> parameterOptimize = new SimpleObjectProperty<>(false);

	/**
	 * Indicates which Fields aka. parameters should be optimized later
	 *
	 * See Method ParameterOptimization.scanParametersToOptimize for scan
	 */
	@Getter
	private Map<Field, Property<Boolean>> parameterOptimizationMap = new HashMap<>();

	/**
	 * Map of Fields mapped by their class
	 * This Map is automatically filled with an Entry, if a Field is annotated with
	 * NonCLSParam-Annotation and the boundClass is not equals Object.class
	 */
	@Getter
	private Map<Class<?>, Field> boundFields = new HashMap<>();

	@SuppressWarnings("unchecked")
	public <T> T getBoundClassField(Class<?> clazz ) {
		if( boundFields.get( clazz ) == null )
			return null;
		try {
			boolean canAccess = boundFields.get( clazz ).canAccess( this );
			if( !canAccess )
				boundFields.get( clazz ).setAccessible( true );
			T retVal = ((Property<T>) boundFields.get( clazz ).get( this )).getValue();
			boundFields.get( clazz ).setAccessible( canAccess);

			return retVal;
		} catch( IllegalAccessException | ClassCastException e ) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Automatically filled while generating the View from this AlgorithmViewModel
	 * Will be cleared each time, the Generate-Process is called
	 */
	@Getter
	private final Map<Class<?>, List<Node>> nodes = new HashMap<>();
	// >>> ---------- ---------- ---------- <<<
	// >>> END :: Non CLS Parameters
	// >>> ---------- ---------- ---------- <<<

	/**
	 * Annotation for declaring non cls parameters
	 *
	 * AlgorithmTab - generateVBoxNonCLSParams iterates over all fields of a model
	 * and checks, whether the Annotation @NonCLSParam is present or not.
	 * If the Annotation is present, the fields of the annotation are analyzed.
	 * A label with a label-text of paramName and a javafx-node (see NodeType) will
	 * be instantiated. Afterwards a hBox with the label and the javafx-node is created
	 */
	@Inherited
	@Target( ElementType.FIELD )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface NonCLSParam {
		/**
		 * @return Name of the label
		 */
		String paramName();

		/**
		 * Type of the Node
		 *
		 * @return NodeType (e.g. NodeType.TextField)
		 */
		NodeType nodeType();

		/**
		 * Used for TextFields
		 * Specify the StringConverter
		 *
		 * @return Class of the StringConverter (e.g. IntegerStringConverter.class)
		 */
		Class<? extends StringConverter<?>> converter() default DEFAULT.class;

		/**
		 * List of AlgorithmViewModels (Algorithms) for which the field should be displayed/generated
		 * (e.g. {GAViewModel.class, LocalSearchViewModel.class} which means,
		 * that the field is generated and displayed for the models (algorithms) GAViewModel (GA) and LocalSearchViewModel (LocalSearch))
		 *
		 * @return List of AlgorithmViewModels
		 */
		Class<? extends AlgorithmViewModel>[] applicableAlgorithms() default {};

		/**
		 * Name of the attribute which contains all items (for example for a combobox)
		 * Usage (e.g. JFXComboBox):
		 * Declare a Property for which contains the selected Item.
		 * Annotate this property with @NonCLSParam.
		 * Then create a property for all items and don't annotate this property with @NonCLSParam
		 * Instead of annotating this property set the allItems-field to the name of the property
		 *
		 * Concrete Example:
		 * @NonCLSParam( paramName = "Name of the Combobox", nodeType = NodeType.JFXComboBox, allItems = "allItems" )
		 * private final Property<Class<?>> selectedItem = new SimpleObjectProperty<>();
		 * @Getter
		 * private final ObservableList<Class<?>> allItems = FXCollections.observableArrayList( AlgorithmAnnotations.objectiveFunctions );
		 *
		 * @return name of the attribute
		 */
		String allItems() default "";

		/**
		 * Adds a checkbox in front of the label to indicate whether the option is selectable
		 */
		boolean optional() default false;
		
		Class<?> boundClass() default Object.class;
	}

	/** Copy-Constructor */
	public AlgorithmViewModel(AlgorithmViewModel other ) {
		this.parameterOptimizationMap = new HashMap<>(other.getParameterOptimizationMap());
		this.selectedObjectiveFunction.setValue(other.getSelectedObjectiveFunction().getValue());
		this.boundFields = new HashMap<>(other.boundFields);
		this.viewModel = other.viewModel;
		this.modelID = other.modelID;
	}

	public AlgorithmViewModel(ViewModel viewModel){
		this();
		this.viewModel = viewModel;
		this.selectedObjectiveFunction.setValue(viewModel.getSelectedObjectiveProperty().get().getClass());
	}

	public AlgorithmViewModel(){
		// field scan for binding
		Class<?> clazz = this.getClass();

		// Reverse the order of the clazz hierarchy
		// Reason: Displaying fields of sub-classes are the first fields
		// and the specific fields are lower
		List<Class<?>> clazzes = new LinkedList<>();
		while( clazz != Object.class ) {
			clazzes.add(0, clazz );
			// get next level class
			clazz = clazz.getSuperclass();
		}

		for( Class<?> c : clazzes ) {
			for( Field field : c.getDeclaredFields() ) {
				if( field.isAnnotationPresent( NonCLSParam.class ) ) {
					if( !field.getAnnotation( NonCLSParam.class ).boundClass().equals( Object.class )) {
						boundFields.put( field.getAnnotation( NonCLSParam.class ).boundClass(), field );
					}
				}
			}
		}
	}

	public abstract AlgorithmViewModel clone();

	/**
	 * Filler to indicate, that there is no specific StringConverter
	 */
	public static class DEFAULT extends StringConverter<Object> {
		@Override
		public String toString( Object o ) {
			return o.toString();
		}

		@Override
		public Object fromString( String s ) {
			return s;
		}
	}

}

