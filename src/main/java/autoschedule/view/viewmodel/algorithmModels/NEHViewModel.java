package autoschedule.view.viewmodel.algorithmModels;

import autoschedule.utils.parameteropt.JSONOptimizationConfig;
import autoschedule.view.viewmodel.ViewModel;

public class NEHViewModel extends AlgorithmViewModel {
    // >>> ---------- ---------- ---------- <<<
    // >>> CLS Parameters
    // >>> ---------- ---------- ---------- <<<

    // >>> ---------- ---------- ---------- <<<
    // >>> Non CLS Parameters
    // >>> ---------- ---------- ---------- <<<

    // >>> ---------- ---------- ---------- <<<
    // >>> Methods
    // >>> ---------- ---------- ---------- <<<

    public NEHViewModel(ViewModel viewModel) {
        super(viewModel);

        // need to be called at this point, cause otherwise it will cause in an error
        JSONOptimizationConfig.loadConfiguration( this );
    }

    /** Copy-Constructor */
    public NEHViewModel(NEHViewModel other){
        super(other);
    }

    @Override
    public NEHViewModel clone(){
        return new NEHViewModel(this);
    }

}
