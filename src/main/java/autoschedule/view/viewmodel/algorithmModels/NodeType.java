package autoschedule.view.viewmodel.algorithmModels;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import javafx.beans.property.Property;
import javafx.scene.Node;
import javafx.scene.control.TextField;

import java.lang.reflect.InvocationTargetException;

/**
 * Enumeration of supported dynamic types for generating a view out of a model
 *
 * To add a new JavaFX-Node fulfill the following steps:
 * 1. Add a new NodeType with any specific name and the corresponding class of the JavaFX-Node
 * 2. Add a case in AlgorithmTab - generateHBoxByAnnotationAndField
 */
public enum NodeType {
	JFXComboBox( JFXComboBox.class ),
	TextField( TextField.class ),
	JFXCheckBox( JFXCheckBox.class );

	/**
	 * JavaFX node class corresponding to the enumeration type
	 */
	private final Class<? extends Node> nodeClazz;

	/**
	 * Constructor
	 * @param clazz Node-Clazz
	 */
	NodeType( Class<? extends Node> clazz ) {
		this.nodeClazz = clazz;
	}

	/**
	 * Create an instance of the class of nodeClazz given by a NodeType
	 *
	 * @param type NodeType
	 * @param <T> will be figured out automatically by java
	 * @return instance of the connected JavaFX node class
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Node> T instantiate( NodeType type ) {
		try {
			return (T) type.nodeClazz.getDeclaredConstructor().newInstance();
		} catch( NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | ClassCastException e ) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Create an instance of the class of nodeClazz given by a NodeType
	 *
	 * @param prop Boolean-Property that is bind bidirectional with the node's Disable-Property
	 * @param type NodeType
	 * @param <T> will be figured out automatically by java
	 * @return instance of the connected JavaFX node class
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Node> T instantiate(Property<Boolean> prop, NodeType type ) {
		try {
			T node = (T) type.nodeClazz.getDeclaredConstructor().newInstance();
			node.disableProperty().bindBidirectional(prop);
			return node;
		} catch( NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | ClassCastException e ) {
			e.printStackTrace();
		}
		return null;
	}
}
