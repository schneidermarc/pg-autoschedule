package autoschedule.view.viewmodel.algorithmModels;

import autoschedule.algorithms.iterative.localsearch.neighborhoods.RandomDescent;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.SteepestDescent;
import autoschedule.utils.converters.DoubleStringConverter;
import autoschedule.utils.converters.IntegerStringConverter;
import autoschedule.utils.parameteropt.JSONOptimizationConfig;
import autoschedule.utils.parameteropt.OptimizationAnnotations.DoubleIntervalParameter;
import autoschedule.utils.parameteropt.OptimizationAnnotations.IntIntervalParameter;
import autoschedule.view.viewmodel.ViewModel;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import lombok.Getter;

public class LocalSearchViewModel extends AlgorithmViewModel {

	// >>> ---------- ---------- ---------- <<<
	// >>> CLS Parameters
	// >>> ---------- ---------- ---------- <<<

	// >>> ---------- ---------- ---------- <<<
	// >>> Non CLS Parameters
	// >>> ---------- ---------- ---------- <<<

	@Getter
	@NonCLSParam( paramName = "Max Iterations (Random Descent)", boundClass = RandomDescent.class, nodeType = NodeType.TextField, converter = IntegerStringConverter.class )
	@IntIntervalParameter( startValue = 1000, endValue = 5000, stepSize = 1000 )
	private final Property<Integer> maxIterationsRandomDescent = new SimpleObjectProperty<>(5000);

	@Getter
	@NonCLSParam( paramName = "Abort Threshold", nodeType = NodeType.TextField, converter = DoubleStringConverter.class )
	@DoubleIntervalParameter( startValue = 0.0005, endValue = 0.001, stepSize = 0.0001)
	private final Property<Double> abortThreshold = new SimpleObjectProperty<>(0.0001);

	@Getter
	@NonCLSParam( paramName = " Max Iterations (Steepest Descent)", boundClass = SteepestDescent.class, nodeType = NodeType.TextField, converter = IntegerStringConverter.class )
	@IntIntervalParameter( startValue = 1, endValue = 10)
	private final Property<Integer> maxIterationsSteepestDescent = new SimpleObjectProperty<>(5);

	// >>> ---------- ---------- ---------- <<<
	// >>> Methods
	// >>> ---------- ---------- ---------- <<<
	public LocalSearchViewModel(ViewModel viewModel) {
		super(viewModel);

		// need to be called at this point, cause otherwise it will cause in an error
		JSONOptimizationConfig.loadConfiguration( this );
	}

	/** Copy-Constructor */
	public LocalSearchViewModel(LocalSearchViewModel other) {
		super(other);
		this.maxIterationsRandomDescent.setValue( other.getMaxIterationsRandomDescent().getValue() );
		this.maxIterationsSteepestDescent.setValue( other.getMaxIterationsSteepestDescent().getValue() );
	}

	@Override
	public LocalSearchViewModel clone() {
		return new LocalSearchViewModel( this );
	}

}
