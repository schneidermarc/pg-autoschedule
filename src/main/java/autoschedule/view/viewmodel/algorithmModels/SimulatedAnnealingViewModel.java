package autoschedule.view.viewmodel.algorithmModels;

import autoschedule.algorithms.iterative.localsearch.neighborhoods.RandomDescent;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.SteepestDescent;
import autoschedule.utils.parameteropt.JSONOptimizationConfig;
import autoschedule.utils.parameteropt.OptimizationAnnotations.IntIntervalParameter;
import autoschedule.utils.converters.IntegerStringConverter;
import autoschedule.view.viewmodel.ViewModel;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import lombok.Getter;

public class SimulatedAnnealingViewModel extends AlgorithmViewModel {
    // >>> ---------- ---------- ---------- <<<
    // >>> CLS Parameters
    // >>> ---------- ---------- ---------- <<<

    // >>> ---------- ---------- ---------- <<<
    // >>> Non CLS Parameters
    // >>> ---------- ---------- ---------- <<<
    @Getter
    @NonCLSParam( paramName = "Max Iterations (Random Descent)", boundClass = RandomDescent.class, nodeType = NodeType.TextField, converter = IntegerStringConverter.class )
    @IntIntervalParameter( startValue = 500, endValue = 5000, stepSize = 500 )
    private final Property<Integer> maxIterationsRandomDescent = new SimpleObjectProperty<>(1000);

    @Getter
    @NonCLSParam( paramName = "Max Iterations (Steepest Descent)", boundClass = SteepestDescent.class, nodeType = NodeType.TextField, converter = IntegerStringConverter.class )
    @IntIntervalParameter( startValue = 1, endValue = 10 )
    private final Property<Integer> maxIterationsSteepestDescent = new SimpleObjectProperty<>(5);

    @Getter
    @NonCLSParam( paramName = "Temperature", nodeType = NodeType.TextField, converter = IntegerStringConverter.class )
    @IntIntervalParameter( startValue = 30, endValue = 100, stepSize = 10 )
    private final Property<Integer> temperature = new SimpleObjectProperty<>(100);

    // >>> ---------- ---------- ---------- <<<
    // >>> Methods
    // >>> ---------- ---------- ---------- <<<
    public SimulatedAnnealingViewModel(ViewModel viewModel) {
        super(viewModel);

        // need to be called at this point, cause otherwise it will cause in an error
        JSONOptimizationConfig.loadConfiguration( this );
    }

    /** Copy-Constructor */
    public SimulatedAnnealingViewModel(SimulatedAnnealingViewModel other) {
        super(other);
        this.maxIterationsRandomDescent.setValue(other.getMaxIterationsRandomDescent().getValue());
        this.maxIterationsSteepestDescent.setValue(other.getMaxIterationsSteepestDescent().getValue());
        this.temperature.setValue(other.getTemperature().getValue());
    }

    @Override
    public SimulatedAnnealingViewModel clone(){
        return new SimulatedAnnealingViewModel(this);
    }
}
