package autoschedule.view.viewmodel.algorithmModels;

import autoschedule.algorithms.constructive.ga.GASolution;
import autoschedule.algorithms.constructive.ga.mutation.Mutation;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.utils.converters.IntegerStringConverter;
import autoschedule.utils.Tuple;
import autoschedule.utils.parameteropt.JSONOptimizationConfig;
import autoschedule.utils.parameteropt.OptimizationAnnotations.IntIntervalParameter;
import autoschedule.utils.parameteropt.OptimizationAnnotations.DoubleIntervalParameter;
import autoschedule.view.viewmodel.ViewModel;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.converter.DoubleStringConverter;
import lombok.Getter;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

public class GAViewModel extends AlgorithmViewModel implements Cloneable{

	// >>> ---------- ---------- ---------- <<<
	// >>> CLS Parameters
	// >>> ---------- ---------- ---------- <<<
	@Getter
	private final ObservableList<Map.Entry<Class<CrossoverOperator<GASolution>>, Boolean>> selectedCrossoverOperators = FXCollections.observableArrayList();
	@Getter
	private final ObservableList<Map.Entry<Class<MutationOperator<GASolution>>, Boolean>> selectedMutationOperators = FXCollections.observableArrayList();
	@Getter
	private final ObservableList<Map.Entry<Class<SelectionOperator<List<GASolution>, GASolution>>, Boolean>> selectedSelectionOperators = FXCollections.observableArrayList();

	// >>> ---------- ---------- ---------- <<<
	// >>> Non CLS Parameters
	// >>> ---------- ---------- ---------- <<<
	@Getter
	@NonCLSParam( paramName = "Mutations", nodeType = NodeType.TextField, converter = DoubleStringConverter.class, optional = true )
	private final ObservableList<Map.Entry<Class<Mutation>, Tuple<Property<Boolean>, Property<Double>>>> mutations = FXCollections.observableArrayList();

	@Getter
	@NonCLSParam( paramName = "Population Size", nodeType = NodeType.TextField, converter = IntegerStringConverter.class )
	@IntIntervalParameter( startValue = 150, endValue = 250, stepSize = 10)
	private final Property<Integer> populationSize = new SimpleObjectProperty<>(200);

	@Getter
	@NonCLSParam( paramName = "Generation Count", nodeType = NodeType.TextField, converter = IntegerStringConverter.class )
	@IntIntervalParameter( startValue = 1, endValue = 10 )
	private final Property<Integer> generationCount = new SimpleObjectProperty<>(5);

	@Getter
	@NonCLSParam( paramName = "Selection Probability", nodeType = NodeType.TextField, converter = DoubleStringConverter.class )
	@DoubleIntervalParameter( startValue = 0.01, endValue = 0.09, stepSize = 0.01 )
	private final Property<Double> selectionProbability = new SimpleObjectProperty<>(0.09);

	@Getter
	@NonCLSParam( paramName = "Crossover Probability of Permutations", nodeType = NodeType.TextField, converter = DoubleStringConverter.class )
	@DoubleIntervalParameter( startValue = 0.01, endValue = 0.09, stepSize = 0.01 )
	private final Property<Double> crossoverProbabilityPermutations = new SimpleObjectProperty<>(0.05);

	@Getter
	@NonCLSParam( paramName = "Crossover Probability of Machines", nodeType = NodeType.TextField, converter = DoubleStringConverter.class )
	@DoubleIntervalParameter( startValue = 0.1, endValue = 0.2, stepSize = 0.01 )
	private final Property<Double> crossoverProbabilityMachines = new SimpleObjectProperty<>(0.15);

	@Getter
	@NonCLSParam( paramName = "Mutation Probability of Permutations", nodeType = NodeType.TextField, converter = DoubleStringConverter.class )
	@DoubleIntervalParameter( startValue = 0.1, endValue = 0.5, stepSize = 0.1 )
	private final Property<Double> mutationProbabilityPermutations = new SimpleObjectProperty<>(0.2);

	@Getter
	@NonCLSParam( paramName = "Mutation Probability of Machines", nodeType = NodeType.TextField, converter = DoubleStringConverter.class )
	@DoubleIntervalParameter( startValue = 0.5, endValue = 1, stepSize = 0.1 )
	private final Property<Double> mutationProbabilityMachines = new SimpleObjectProperty<>(0.8);

	public GAViewModel(ViewModel viewModel) {
		super(viewModel);
		AlgorithmAnnotations.gaCrossoverOperators.forEach(clazz -> selectedCrossoverOperators.add(new AbstractMap.SimpleEntry<>(clazz, false)));
		AlgorithmAnnotations.gaMutationOperators.forEach(clazz -> mutations.add(new AbstractMap.SimpleEntry<>(clazz, new Tuple<>( new SimpleObjectProperty<>( true ), new SimpleObjectProperty<>(1.0 / AlgorithmAnnotations.gaMutationOperators.size())))));
		AlgorithmAnnotations.gaSelectionOperators.forEach(clazz -> selectedSelectionOperators.add(new AbstractMap.SimpleEntry<>(clazz, false)));

		// need to be called at this point, cause otherwise it will cause in an error
		JSONOptimizationConfig.loadConfiguration( this );
	}

	/** Copy-Constructor */
	public GAViewModel(GAViewModel other) {
		super(other);
		this.selectedCrossoverOperators.setAll(other.getSelectedCrossoverOperators());
		this.selectedMutationOperators.setAll(other.getSelectedMutationOperators());
		this.selectedSelectionOperators.setAll(other.getSelectedSelectionOperators());
		//Clone the items of mutations separately
		ObservableList<Map.Entry<Class<Mutation>, Tuple<Property<Boolean>, Property<Double>>>> clonedMutations = FXCollections.observableArrayList(other.getMutations());
		for(int i = 0; i < clonedMutations.size(); i++ ){
			Map.Entry<Class<Mutation>, Tuple<Property<Boolean>, Property<Double>>> entry = clonedMutations.get(i);
			Property<Boolean> copiedBooleanProperty = new SimpleObjectProperty<>(entry.getValue().getFirst().getValue());
			Property<Double> copiedDoubleProperty = new SimpleObjectProperty<>(entry.getValue().getSecond().getValue());
			entry = new AbstractMap.SimpleEntry<>(entry.getKey(), new Tuple<>(copiedBooleanProperty, copiedDoubleProperty));
			clonedMutations.set(i, entry);
		}
		this.mutations.setAll(clonedMutations);
		this.populationSize.setValue(other.getPopulationSize().getValue());
		this.generationCount.setValue(other.getGenerationCount().getValue());
		this.selectionProbability.setValue(other.getSelectionProbability().getValue());
		this.crossoverProbabilityPermutations.setValue(other.getCrossoverProbabilityPermutations().getValue());
		this.crossoverProbabilityMachines.setValue(other.getCrossoverProbabilityMachines().getValue());
		this.mutationProbabilityPermutations.setValue(other.getMutationProbabilityPermutations().getValue());
		this.mutationProbabilityMachines.setValue(other.getMutationProbabilityMachines().getValue());
	}

	@Override
	public GAViewModel clone(){
		return new GAViewModel(this);
	}
}
