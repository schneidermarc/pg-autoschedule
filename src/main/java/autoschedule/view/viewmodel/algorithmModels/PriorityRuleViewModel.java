package autoschedule.view.viewmodel.algorithmModels;

import autoschedule.utils.parameteropt.JSONOptimizationConfig;
import autoschedule.view.viewmodel.ViewModel;

public class PriorityRuleViewModel extends AlgorithmViewModel {
    // >>> ---------- ---------- ---------- <<<
    // >>> CLS Parameters
    // >>> ---------- ---------- ---------- <<<

    // >>> ---------- ---------- ---------- <<<
    // >>> Non CLS Parameters
    // >>> ---------- ---------- ---------- <<<

    // >>> ---------- ---------- ---------- <<<
    // >>> Methods
    // >>> ---------- ---------- ---------- <<<

    public PriorityRuleViewModel(ViewModel viewModel) {
        super(viewModel);

        // need to be called at this point, cause otherwise it will cause in an error
        JSONOptimizationConfig.loadConfiguration( this );
    }

    /** Copy-Constructor */
    public PriorityRuleViewModel(PriorityRuleViewModel other){
        super(other);
    }

    @Override
    public PriorityRuleViewModel clone(){
        return new PriorityRuleViewModel(this);
    }

}
