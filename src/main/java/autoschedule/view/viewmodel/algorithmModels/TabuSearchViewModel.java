package autoschedule.view.viewmodel.algorithmModels;

import autoschedule.algorithms.iterative.localsearch.neighborhoods.SteepestDescent;
import autoschedule.utils.parameteropt.JSONOptimizationConfig;
import autoschedule.utils.parameteropt.OptimizationAnnotations.IntIntervalParameter;
import autoschedule.utils.converters.IntegerStringConverter;
import autoschedule.view.viewmodel.ViewModel;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import lombok.Getter;

public class TabuSearchViewModel extends AlgorithmViewModel {

    // >>> ---------- ---------- ---------- <<<
    // >>> CLS Parameters
    // >>> ---------- ---------- ---------- <<<

    // >>> ---------- ---------- ---------- <<<
    // >>> Non CLS Parameters
    // >>> ---------- ---------- ---------- <<<
    @Getter
    @NonCLSParam( paramName = "Max Iterations (Steepest Descent)", boundClass = SteepestDescent.class, nodeType = NodeType.TextField, converter = IntegerStringConverter.class )
    @IntIntervalParameter( startValue = 2, endValue = 20, stepSize = 2 )
    private final Property<Integer> maxIterationsSteepestDescent = new SimpleObjectProperty<>(10);

    @Getter
    @NonCLSParam( paramName = "Tabu-List Size", nodeType = NodeType.TextField, converter = IntegerStringConverter.class )
    @IntIntervalParameter( startValue = 1, endValue = 10)
    private final Property<Integer> tabuListSize = new SimpleObjectProperty<>(5);

    // >>> ---------- ---------- ---------- <<<
    // >>> Methods
    // >>> ---------- ---------- ---------- <<<
    public TabuSearchViewModel(ViewModel viewModel) {
        super(viewModel);

        // need to be called at this point, cause otherwise it will cause in an error
        JSONOptimizationConfig.loadConfiguration( this);
    }

    /** Copy-Constructor */
    public TabuSearchViewModel(TabuSearchViewModel other){
        super(other);
        this.maxIterationsSteepestDescent.setValue(other.getMaxIterationsSteepestDescent().getValue());
        this.tabuListSize.setValue(other.getTabuListSize().getValue());
    }

    @Override
    public TabuSearchViewModel clone() {
        return new TabuSearchViewModel(this);
    }
}
