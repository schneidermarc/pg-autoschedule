package autoschedule.view.viewmodel.algorithmModels;

import autoschedule.utils.parameteropt.JSONOptimizationConfig;
import autoschedule.view.viewmodel.ViewModel;

public class GifflerThompsonViewModel extends AlgorithmViewModel {

	// >>> ---------- ---------- ---------- <<<
	// >>> CLS Parameters
	// >>> ---------- ---------- ---------- <<<

	// >>> ---------- ---------- ---------- <<<
	// >>> Non CLS Parameters
	// >>> ---------- ---------- ---------- <<<

	// >>> ---------- ---------- ---------- <<<
	// >>> Methods
	// >>> ---------- ---------- ---------- <<<

	public GifflerThompsonViewModel( ViewModel viewModel ) {
		super( viewModel );

		// need to be called at this point, cause otherwise it will cause in an error
		JSONOptimizationConfig.loadConfiguration( this );
	}

	/** Copy-Constructor */
	public GifflerThompsonViewModel( GifflerThompsonViewModel other ) {
	    super(other);
	}

	@Override
	public GifflerThompsonViewModel clone() {
		return new GifflerThompsonViewModel( this );
	}

}
