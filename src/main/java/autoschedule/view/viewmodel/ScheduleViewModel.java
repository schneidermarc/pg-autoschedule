package autoschedule.view.viewmodel;

import autoschedule.control.database.OPT_ResultsObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import lombok.Getter;

import java.util.List;

public class ScheduleViewModel {

    @Getter
    private final ObservableList<OPT_ResultsObject> resultObjects = FXCollections.observableArrayList();

    @Getter
    private final SimpleStringProperty currentDatabaseName = new SimpleStringProperty("");

    public void init(Label pLabel){
        this.resultObjects.clear();
        pLabel.textProperty().bind(currentDatabaseName);
    }

    public void updateData(List<OPT_ResultsObject> pResultsObjects, String currentDatabase){
        resultObjects.clear();
        resultObjects.addAll(pResultsObjects);
        currentDatabaseName.setValue(currentDatabase);
    }
}
