package autoschedule.view.viewmodel;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.objectives.CMax;
import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.algorithms.priorityRules.PriorityRule;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.control.SchedulingController;
import autoschedule.model.Constraint;
import autoschedule.model.MachineEnvironment;
import autoschedule.model.Schedule;
import autoschedule.utils.RouteData;
import autoschedule.utils.algorithmsorting.AlgorithmSorter;
import autoschedule.control.database.DemandData;
import autoschedule.control.database.TableRowResultsOptimization;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class ViewModel {

    @Getter
    private final SchedulingController schedulingController = new SchedulingController(this);

    @Getter @Setter
    private File loadedDB;

    @Getter
    private final IntegerProperty currentPaneProperty = new SimpleIntegerProperty(0);

    //region SettingsPageView
    @Getter
    private final ObservableList<MachineEnvironment> machineEnvironments = FXCollections.observableArrayList();
    @Getter
    private final ObjectProperty<MachineEnvironment> selectedMachineEnvironment = new SimpleObjectProperty<>();
    @Getter
    private final BooleanProperty optimizationRunningProperty = new SimpleBooleanProperty(false);
    @Getter
    private final ObservableList<Map.Entry<Constraint, Boolean>> constraints = FXCollections.observableArrayList();
    @Getter
    private final ObservableList<SchedulingObjective> objectives = FXCollections.observableArrayList();
    @Getter
    private final ObjectProperty<SchedulingObjective> selectedObjectiveProperty = new SimpleObjectProperty<>(new CMax());

    @Getter
    private final Property<Integer> selectedWeek = new SimpleObjectProperty<>();
    @Getter
    private final Property<Integer> selectedYear = new SimpleObjectProperty<>();
    @Getter
    private final IntegerProperty schedulingDuration = new SimpleIntegerProperty(1);

    @Getter
    private final ObservableList<DemandData> demands = FXCollections.observableArrayList();

    @Getter
    private final ObservableList<RouteData> routeData = FXCollections.observableArrayList();
    //endregion

    //region AlgorithmPageView
    @Getter
    private final ObservableList<Class<Algorithm>> constructiveAlgorithms = FXCollections.observableArrayList();
    @Getter
    private final ObservableList<Class<Algorithm>> iterativeAlgorithms = FXCollections.observableArrayList();
    @Getter
    private final ObservableList<Class<PriorityRule>> priorityRules = FXCollections.observableArrayList();
    @Getter
    private final ObservableList<Integer> iterationCount = FXCollections.observableArrayList();
    @Getter
    private final BooleanProperty iterCountLowerOrEqual = new SimpleBooleanProperty(false);
    @Getter
    private final Property<Integer> selectedIterationCount = new SimpleObjectProperty<>(0);

    @Getter
    private final DoubleProperty clsResultProgress = new SimpleDoubleProperty(0);

    @Getter @Setter
    private List<TreeNode> chosenAlgorithms;

    //endregion

    //region OptimizationPageView
    @Getter
    private final DoubleProperty optimizationProgressProperty = new SimpleDoubleProperty(0.0);
    @Getter
    private final ObjectProperty<SchedulingObjective> selectedObjectiveToShowProperty = new SimpleObjectProperty<>(new CMax());
    @Getter
    private final ObservableList<TableRowResultsOptimization> optimizationFinishedSchedules = FXCollections.observableArrayList();
    @Getter
    private final ObservableList<Schedule> generatedSolutionsList = FXCollections.observableArrayList();
    @Getter
    private final ObservableList<Schedule> selectedSolutionsList = FXCollections.observableArrayList();
    //endregion

    @Getter @Setter
    private boolean autoButtonPushed;

    //region Algorithm ViewModels
    /**
     * Maps a Algorithm-Class and a List of AlgorithmViewModels (see AlgorithmTabFactory for add)
     */
    @Getter
    private final Map<Class<? extends Algorithm>, List<AlgorithmViewModel>> algorithmViewModels = new HashMap<>();
    //endregion

    public void init() {
        AlgorithmAnnotations.objectiveFunctions.forEach(clazz -> objectives.add(Algorithm.createInstanceByClass(clazz)));

        EnumSet.allOf(Constraint.class).forEach(constraint -> constraints.add(new AbstractMap.SimpleEntry<>(constraint, false)));

        generatedSolutionsList.addListener((ListChangeListener<Schedule>) change -> {
            change.next();
            if(change.wasAdded())
                generatedSolutionsList.sort(new AlgorithmSorter(selectedMachineEnvironment));
        });
    }

    public Set<Constraint> getSelectedConstraints() {
        return constraints.stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).collect(Collectors.toSet());
    }

    /**
     * Adds a given AlgorithmViewModel to the algorithmViewModels-List corresponding to the given clazz
     * If no AlgorithmViewModels exists for the given Class, then a new List for the AlgorithmViewModels is created
     *
     * @param algorithmClazz Class of the algorithm to which the AlgorithmViewModel corresponds
     * @param algorithmViewModel The AlgorithmViewModel to be added
     */
    public void addNewAlgorithmViewModel( Class<? extends Algorithm> algorithmClazz, AlgorithmViewModel algorithmViewModel ) {
        List<AlgorithmViewModel> models = algorithmViewModels.computeIfAbsent( algorithmClazz, k -> new ArrayList<>() );
        models.add( algorithmViewModel );
    }
}
