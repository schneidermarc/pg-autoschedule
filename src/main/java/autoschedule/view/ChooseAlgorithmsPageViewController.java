package autoschedule.view;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.MachineEnvironment;
import autoschedule.utils.logging.CustomMessage;
import autoschedule.view.algorithmtabs.AlgorithmTab;
import autoschedule.view.algorithmtabs.AlgorithmTabFactory;
import autoschedule.view.viewmodel.ViewModel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTabPane;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import lombok.Getter;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ChooseAlgorithmsPageViewController implements Initializable {

	private static final int MAX_ITERATIVE_COUNT = 10;

	@FXML
	private JFXComboBox<Class<Algorithm>> comboBoxConstructiveAlgorithms, comboBoxIterativeAlgorithms;
	@FXML
	private JFXComboBox<Integer> comboBoxIterativeCount;
	@FXML
	public JFXButton buttonIterativeCountLowerOrEqual;
	@FXML
	@Getter
	private JFXTabPane tabPaneConstructiveAlgorithms, tabPaneIterativeAlgorithms;
	@FXML
	@Getter
	private JFXProgressBar progressBar;

	@FXML
	private JFXComboBox<String> comboBoxSecondStageStrategy;
	@FXML
	private Label labelSecondStage;

	private ViewModel viewModel;

	/**
	 * Method, that is called after all @FXML-Annotations are processed
	 */
	@Override
	public void initialize( URL location, ResourceBundle resources ) {
		// Prepare ComboBoxes
		prepareComboBox( comboBoxConstructiveAlgorithms, tabPaneConstructiveAlgorithms );
		prepareComboBox( comboBoxIterativeAlgorithms, tabPaneIterativeAlgorithms );

		tabPaneIterativeAlgorithms.getTabs().addListener( (ListChangeListener<Tab>) change -> {
			change.next();
			if( change.wasAdded() && change.getList().size() == 1 ) {
				comboBoxIterativeCount.getSelectionModel().selectNext();
				comboBoxIterativeCount.setDisable( false );
			} else if( change.wasRemoved() && change.getList().isEmpty() ) {
				comboBoxIterativeCount.getSelectionModel().selectFirst();
				comboBoxIterativeCount.setDisable( true );
			}
		} );
	}

	/**
	 * initializes the view content
	 */
	void init( ViewModel viewModel ) {
		this.viewModel = viewModel;
		this.comboBoxConstructiveAlgorithms.setPromptText( "Choose Algorithms to add" );
		this.comboBoxIterativeAlgorithms.setPromptText( "Choose Algorithms to add" );
		this.comboBoxIterativeCount.valueProperty().bindBidirectional( viewModel.getSelectedIterationCount() );
		this.comboBoxIterativeCount.getSelectionModel().selectFirst();

		// fill dropdown-menus with the names of the algorithms
		// constructive and priorityRule-based algorithm
		this.viewModel.getPriorityRules().addAll( AlgorithmAnnotations.priorityRules );
		this.viewModel.getIterationCount().addAll( IntStream.range( 0, MAX_ITERATIVE_COUNT + 1 ).boxed().collect( Collectors.toList() ) );

		this.comboBoxConstructiveAlgorithms.setItems( viewModel.getConstructiveAlgorithms() );
		this.comboBoxIterativeAlgorithms.setItems( viewModel.getIterativeAlgorithms() );
		this.comboBoxIterativeCount.setItems( viewModel.getIterationCount() );

		this.viewModel.getIterCountLowerOrEqual().addListener( ( o, oV, nV ) -> buttonIterativeCountLowerOrEqual.setText( nV ? "\u2264" : "\u003D" ) );
		this.buttonIterativeCountLowerOrEqual.setOnAction( e -> viewModel.getIterCountLowerOrEqual().set( !viewModel.getIterCountLowerOrEqual().get() ) );

		this.progressBar.progressProperty().bind( viewModel.getClsResultProgress() );
		this.progressBar.visibleProperty().bind( viewModel.getClsResultProgress().greaterThan( 0 ) );

		this.comboBoxSecondStageStrategy.visibleProperty().bind( this.viewModel.getSelectedMachineEnvironment().isEqualTo( MachineEnvironment.FLOW_SHOP ).or( this.viewModel.getSelectedMachineEnvironment().isEqualTo( MachineEnvironment.FLEXIBLE_FLOW_SHOP ) ) );
		this.labelSecondStage.visibleProperty().bind( comboBoxSecondStageStrategy.visibleProperty() );
	}

	/**
	 * Prepare ComboBox
	 * Extract names of Algorithms and display in Combobox
	 */
	private void prepareComboBox( JFXComboBox<Class<Algorithm>> comboBox, JFXTabPane tabPane ) {
		comboBox.setButtonCell( new ListCell<>() {
			@Override
			protected void updateItem( Class<Algorithm> item, boolean empty ) {
				super.updateItem( item, empty );
				this.setVisible( item != null || !empty );
				if( empty || item == null ) {
					setText( null );
				} else {
					setText( AlgorithmAnnotations.getNameOfAlgorithm( item ) );
				}
			}
		} );

		comboBox.setCellFactory( new Callback<>() {
			@Override
			public ListCell<Class<Algorithm>> call( ListView<Class<Algorithm>> param ) {
				return new ListCell<>() {
					@Override
					protected void updateItem( Class<Algorithm> item, boolean empty ) {
						super.updateItem( item, empty );
						if( empty || item == null ) {
							setText( "" );
						} else {
							setText( AlgorithmAnnotations.getNameOfAlgorithm( item ) );
						}
					}
				};
			}
		} );

		comboBox.setConverter( new StringConverter<>() {
			private final Map<String, Class<Algorithm>> map = new HashMap<>();

			@Override
			public String toString( Class<Algorithm> o ) {
				String str = "";
				if( o != null ) {
					str = AlgorithmAnnotations.getNameOfAlgorithm( o );
					map.put( str, o );
				}
				return str;
			}

			@Override
			public Class<Algorithm> fromString( String str ) {
				if( !map.containsKey( str ) ) {
					comboBox.setValue( null );
					comboBox.getEditor().clear();
					return null;
				}
				return map.get( str );
			}
		} );

		comboBox.getSelectionModel().selectedItemProperty().addListener( ( observable, oldValue, newValue ) -> {
			if( newValue != null ) {
				Tab tab = new AlgorithmTabFactory().generate( newValue, viewModel );
				tabPane.getTabs().add( tab );
				tabPane.getSelectionModel().select( tab );
				// to clear the ComboBox the method must be run later, cause if direct run, threads will compete
				if( newValue.isAnnotationPresent( AlgorithmAnnotations.Constructive.class )
						&& newValue.getAnnotation( AlgorithmAnnotations.Constructive.class ).singleUse() ) {
					tab.setOnClosed( ( a ) -> comboBoxConstructiveAlgorithms.getItems().add( newValue ) );
				}

				Platform.runLater( () -> {
					comboBox.getSelectionModel().clearSelection();

					if( newValue.isAnnotationPresent( AlgorithmAnnotations.Constructive.class )
							&& newValue.getAnnotation( AlgorithmAnnotations.Constructive.class ).singleUse() ) {
						comboBoxConstructiveAlgorithms.getItems().remove( newValue );
					}
				} );
			}
		} );
	}

	/**
	 * sets the visibility of the progress bar
	 */
	public void prepareNext() {
		viewModel.getClsResultProgress().setValue( 0 );
	}

	/**
	 * decides whether the input is valid oder not
	 *
	 * @return if the input is valid
	 */
	public boolean inputValid() {
		String[] selectError = tabPaneConstructiveAlgorithms.getTabs().size() == 0 ? new String[]{ "Select at least one constructive algorithm" } :  new String[]{};

		String[] constructiveErrors = tabPaneConstructiveAlgorithms.getTabs().stream().map( tab -> ( (AlgorithmTab) tab ).validInput() ).filter( Objects::nonNull ).toArray(String[]::new);
		String[] iterativeErrors = tabPaneIterativeAlgorithms.getTabs().stream().map( tab -> ( (AlgorithmTab) tab ).validInput() ).filter( Objects::nonNull ).toArray(String[]::new);

		String[] errors = new String[selectError.length + constructiveErrors.length + iterativeErrors.length];

		System.arraycopy(selectError, 0, errors, 0, selectError.length);
		System.arraycopy(constructiveErrors, 0, errors, selectError.length, constructiveErrors.length);
		System.arraycopy(iterativeErrors, 0, errors,  selectError.length + constructiveErrors.length, iterativeErrors.length);

		if( errors.length > 0 ) {
			CustomMessage.showCustomErrors( "Warning", errors );
			return false;
		} else {
			return true;
		}
	}

	public void resetViews() {
		tabPaneConstructiveAlgorithms.getTabs().clear();
		tabPaneIterativeAlgorithms.getTabs().clear();
	}
}
