package autoschedule.algorithms;

import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public abstract class Algorithm {

	/**
	 * Indicates the corresponding model
	 * Will be set by CLS
	 */
	public int modelID;
	@Getter
	private long runtimeMillis;
	@Setter
	protected Schedule schedule;
	@Setter
	@Getter
	protected SchedulingObjective objective;

	@Setter
	@Getter
	protected AlgorithmViewModel model;

	/**
	 * Writes all the parameters in the viewModel into the algorithm
	 * e.g. this.setObjective( viewModel.getObjective() )
	 * This method is implemented individually in each algorithm
	 */
	protected abstract void prepareParameters();

	/**
	 * Executes scheduling algorithm
	 */
	protected abstract List<OPT_ObjValuesObject> execute( Schedule schedule );

	/**
	 * Checks for constraints and executes algorithm
	 */
	public final List<OPT_ObjValuesObject> run( Schedule schedule ) {
		// if(!checkConstraints(schedule.getActiveConstraints()))
		// 	return Collections.singletonList(new OPT_ObjValuesObject());

		// Set the Parameters, that are set in the view model
		prepareParameters();

		long startTime = System.currentTimeMillis();
		List<OPT_ObjValuesObject> result = execute(schedule);
		this.runtimeMillis = System.currentTimeMillis() - startTime;

		return result;
	}

	/**
	 * returns an object containing configuration information about the algorithm.
	 * Used for OPT_algorithm_config table to save algorithm configuration.
	 * Returns null if no configuration exists.
	 */
	@SuppressWarnings("SameReturnValue")
	public abstract AlgorithmConfig getAlgorithmConfig();

	/**
	 * Creates a new Instance by a Class. The return type will be set automatically
	 *
	 * @param clazz to be instantiated
	 * @param args  constructor parameters as a array of object
	 * @param <T>   will be set automatically
	 * @return new instance of the given class
	 */
	public static <T> T createInstanceByClass( Class<T> clazz, Object[] args ) {
		Constructor<?>[] constructors = clazz.getDeclaredConstructors();

		for( Constructor<?> constructor : constructors ) {
			if( constructor.getParameterCount() != args.length )
				continue;
			try {
				return clazz.getDeclaredConstructor( constructor.getParameterTypes() ).newInstance( args );
			} catch( InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e ) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Creates a new Instance by a Class. The return type will be set automatically
	 *
	 * @param clazz to be instantiated
	 * @param <T>   will be set automatically
	 * @return new instance of the given class
	 */
	public static <T> T createInstanceByClass( Class<T> clazz ) {
		try {
			return clazz.getDeclaredConstructor().newInstance();
		} catch( InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e ) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String toString() {
		return getString();
	}

	public abstract String getString();

	public abstract String getSimpleString();
}
