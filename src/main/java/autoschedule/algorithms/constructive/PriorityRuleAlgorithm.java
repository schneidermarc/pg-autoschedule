//autoschedule.algorithms.constructive.PriorityRuleAlgorithm
package autoschedule.algorithms.constructive;

import autoschedule.algorithms.priorityRules.PriorityRule;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.*;
import lombok.Getter;

import java.util.*;

@AlgorithmAnnotations.Constructive(name = "PriorityRuleAlgorithm", singleUse = true)
public class PriorityRuleAlgorithm extends ConstructiveAlgorithm {

    @Getter
    private final PriorityRule priorityRule;

    /**
     * Public constructor
     * @param priorityRule Prio rule to be used
     */
    public PriorityRuleAlgorithm(PriorityRule priorityRule) {
        this.priorityRule = priorityRule;
    }

    /**
     * schedules all jobs based on rank(priority) on machine which would finish first
     * @param schedule the jobs/schedule to rearrange
     * @return schedule/list of jobs ordered based on rank
     */
    @Override
    protected List<OPT_ObjValuesObject> execute(Schedule schedule) {
        List<Job> ranking = priorityRule.rank(schedule.getJobs());

        /* Initialize Hashmap to store working times of machines; Save arrivalTimes of machines */
        Map<Machine, Integer> machineWorkingUntil = new HashMap<>();
        for (Machine machine : schedule.getAllMachines()) {
            machineWorkingUntil.put(machine, machine.getArrival());
        }

        /* Schedule first stage */
        for (Job job : ranking) {
            Article firstArticleOfJob = job.getArticles().get(0);

            //find all qualified machines
            List<Machine> qualifiedMachinesOnStageOne = new ArrayList<>(firstArticleOfJob.getMachineQualification().keySet());

            //find machine that would finish first
            //noinspection OptionalGetWithoutIsPresent
            Machine selectedMachine = qualifiedMachinesOnStageOne.stream().min(Comparator.comparingInt(machine -> Math.max(machineWorkingUntil.get(machine), job.getArrival()) + firstArticleOfJob.getProcessingTimeOnMachine(machine))).get();

            //schedule article
            int jobStartTime = Math.max(machineWorkingUntil.get(selectedMachine), job.getArrival());
            int processingTime = firstArticleOfJob.getProcessingTimeOnMachine(selectedMachine);
            firstArticleOfJob.scheduleMachine(selectedMachine, jobStartTime); //schedule
            machineWorkingUntil.put(selectedMachine, jobStartTime + processingTime); //update
        }

        //schedule next stages with ECT
        return new LinkedList<>(new ECT().run(schedule));
    }

    @Override
    public AlgorithmConfig getAlgorithmConfig() {
        return null;
    }

    @Override
    public String getString() {
        return "Priority rule algorithm (Rule: " + priorityRule.toString() + ")";
    }

    @Override
    public String getSimpleString() {
        return "Prio (Rule: " + priorityRule.toString() + ")";
    }

    @Override
    protected void prepareParameters() {}
}
