package autoschedule.algorithms.constructive;

import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.*;

import java.util.*;

public class ECT extends ConstructiveAlgorithm {

    /**
     * ECT: earliest scheduling time
     * schedules (mostly the second stage) based on scheduling time
     * @param schedule schedule to rearrange
     * @return new schedule as list
     */
    @Override
    protected List<OPT_ObjValuesObject> execute(Schedule schedule) {
        List<OPT_ObjValuesObject> objectiveValuesList = new LinkedList<>();

        //analyse stage one schedule regarding machine completion time
        Map<Machine, Integer> machineWorkingUntil = new HashMap<>();

        List<Article> articlesOnFirstStage = schedule.getAllArticlesOnStage(1);
        schedule.getAllMachines().forEach(machine -> articlesOnFirstStage.stream()
                .filter(article -> machine.equals(article.getScheduledMachine()))
                .mapToInt(Article::getFinishTime).max()
                .ifPresentOrElse(value -> machineWorkingUntil.put(machine, value), () -> machineWorkingUntil.put(machine, machine.getArrival())));

        for (int stage = 1; stage < schedule.getStages(); stage++) {
            Collection<Article> waitingArticles = new ArrayList<>();
            for (Job job : schedule.getJobs()) { //Skipping last stage
                final int stageId = stage + 1;
                job.getArticles().stream().filter(article -> article.getStage() == stageId).findFirst().ifPresent(waitingArticles::add);
            }

            while (waitingArticles.size() > 0) {
                Article selectedArticle = null;
                Machine selectedMachine = null;
                int leastFinishTime = Integer.MAX_VALUE;

                for (Article article : waitingArticles) {
                    List<Machine> qualifiedMachinesOnArticle = new ArrayList<>(article.getMachineQualification().keySet());

                    //find machine that would finish first
                    Machine machine = qualifiedMachinesOnArticle.stream().min(Comparator.comparingInt(m -> Math.max(machineWorkingUntil.get(m), article.getPreArticle() == null ? 0 : article.getPreArticle().getFinishTime()) + article.getProcessingTimeOnMachine(m))).orElse(null);

                    int finishLastStage = article.getPreArticle() == null ? 0 : article.getPreArticle().getFinishTime();
                    int time = Math.max(machineWorkingUntil.get(machine), finishLastStage) + article.getProcessingTimeOnMachine(machine);

                    if (leastFinishTime > time) {
                        selectedArticle = article;
                        selectedMachine = machine;
                        leastFinishTime = time;
                    }
                }

                //schedule article
                if(selectedMachine != null) {
                    selectedArticle.scheduleMachine(selectedMachine, leastFinishTime - selectedArticle.getProcessingTimeOnMachine(selectedMachine)); //schedule
                    machineWorkingUntil.put(selectedMachine, leastFinishTime); //update
                    waitingArticles.remove(selectedArticle);
                }
            }
        }
        return objectiveValuesList;
    }

    @Override
    protected void prepareParameters() {}

    @Override
    public AlgorithmConfig getAlgorithmConfig() {
        return null;
    }

    @Override
    public String getString() {
        return "ECT";
    }

    @Override
    public String getSimpleString() {
        return "ECT";
    }
}
