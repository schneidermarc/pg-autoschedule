//autoschedule.algorithms.iterative.ga.GA
package autoschedule.algorithms.constructive.ga;

import autoschedule.algorithms.constructive.ConstructiveAlgorithm;
import autoschedule.algorithms.constructive.ga.crossover.GATwoParentsCrossoverOperator;
import autoschedule.algorithms.constructive.ga.mutation.FirstChromeMutation;
import autoschedule.algorithms.constructive.ga.mutation.GAMutationOperator;
import autoschedule.algorithms.constructive.ga.mutation.Mutation;
import autoschedule.algorithms.constructive.ga.mutation.SecondChromeMutation;
import autoschedule.algorithms.constructive.ga.selection.GASelectionOperator;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import autoschedule.utils.Tuple;
import autoschedule.view.viewmodel.algorithmModels.GAViewModel;
import javafx.beans.property.Property;
import lombok.Getter;
import lombok.Setter;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.singleobjective.geneticalgorithm.GeneticAlgorithmBuilder;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

import java.util.*;

@AlgorithmAnnotations.Constructive(name = "Genetic Algorithm")
public class GA extends ConstructiveAlgorithm {

    public static final JMetalRandom randomGenerator = JMetalRandom.getInstance();

    @Setter
    private GAParameters GAParameters;

    private final GATwoParentsCrossoverOperator crossover;

    @Getter
    private final GAMutationOperator mutation;

    private final GASelectionOperator selection;

    public GA(GATwoParentsCrossoverOperator crossover, GAMutationOperator mutation, GASelectionOperator selection) {
        this.crossover = crossover;
        this.mutation = mutation;
        this.selection = selection;
    }

    @Override
    protected List<OPT_ObjValuesObject> execute(Schedule schedule) {
        List<OPT_ObjValuesObject> objectiveValuesList = new LinkedList<>();
        selection.setCachedObjectiveValues(objectiveValuesList);

        GeneticAlgorithmBuilder<GASolution> builder = importParametersToStructures(schedule);

        Algorithm<GASolution> algorithm = builder.build();
        AlgorithmRunner algoRunner = new AlgorithmRunner.Executor(algorithm).execute();

        builder.getEvaluator().shutdown();

        GASolution sol = algorithm.getResult();

        //long computingTime = algoRunner.getComputingTime();

        schedule.copyFrom(sol.toSchedule());
        return objectiveValuesList;
    }

    /**
     * Set GA parameters based on given schedule
     */
    private GeneticAlgorithmBuilder<GASolution> importParametersToStructures(Schedule schedule) {
        if(selection.getCachedObjectiveValues() == null) {
            selection.setCachedObjectiveValues(new LinkedList<>());
        }
        GAProblem problem = new GAProblem(schedule, objective);
        crossover.setProblem(problem);
        crossover.setProbabilityFirstChrome(GAParameters.getCrossoverProbabilityPermutations());
        crossover.setProbabilitySecondChrome(GAParameters.getCrossoverProbabilityMachines());

        mutation.setProblem(problem);
        mutation.setProbabilityFirst(GAParameters.getMutationProbabilityPermutations());
        mutation.setProbabilitySecond(GAParameters.getMutationProbabilityMachines());

        selection.setSelectionProbability(GAParameters.getSelectionProbability());
        selection.setSolutionSize(GAParameters.getPopulationSize());

        return new GeneticAlgorithmBuilder<>(problem, crossover, mutation)
                .setSelectionOperator(selection)
                .setPopulationSize(GAParameters.getPopulationSize())
                .setMaxEvaluations(GAParameters.getPopulationSize() * GAParameters.getGenerationCount())
                .setSolutionListEvaluator(new SequentialSolutionListEvaluator<>());
    }

    /**
     * returns an object which holds information about the confiugrations for this algorithm.
     * This object is used in order to save the algorithms' configurations in the OPT_algorithm table.
     * If an algorithm has no configurations that have to be saved null will be returned
     */
    @Override
    public AlgorithmConfig getAlgorithmConfig() {
        return null;
    }

    @Override
    public String getString() {
        return "Genetic Algorithm";
    }

    @Override
    public String getSimpleString() {
        return "GA";
    }

    /**
     * Sets all the parameters from the viewmodel before executing the algorithm
     */
    @Override
    protected void prepareParameters(){
        GAViewModel gaViewModel = (GAViewModel) model;
        GAParameters = new GAParameters(
                gaViewModel.getPopulationSize().getValue(),
                gaViewModel.getGenerationCount().getValue(),
                gaViewModel.getSelectionProbability().getValue(),
                gaViewModel.getCrossoverProbabilityPermutations().getValue(),
                gaViewModel.getCrossoverProbabilityMachines().getValue(),
                gaViewModel.getMutationProbabilityPermutations().getValue(),
                gaViewModel.getMutationProbabilityMachines().getValue());
        setObjective( createInstanceByClass( gaViewModel.getSelectedObjectiveFunction().getValue() ));
        //first iteration: sum up all values
        double sumFirstChrome = 0, sumSecondChrome = 0; // sum = 0
        for( Map.Entry<Class<Mutation>, Tuple<Property<Boolean>, Property<Double>>> entry : gaViewModel.getMutations() ) {
            if( entry.getValue().getFirst().getValue() ) {
                if( FirstChromeMutation.class.isAssignableFrom( entry.getKey()))
                    sumFirstChrome += entry.getValue().getSecond().getValue();
                else if( SecondChromeMutation.class.isAssignableFrom( entry.getKey()))
                    sumSecondChrome += entry.getValue().getSecond().getValue();

            }
        }
        double densityFirstChrome = 0, densitySecondChrome = 0; // density = 0
        //second iteration: update the values with the density propabilities
        for( Map.Entry<Class<Mutation>, Tuple<Property<Boolean>, Property<Double>>> entry : gaViewModel.getMutations() ) {
            if( entry.getValue().getFirst().getValue() ) {
                Mutation tempMutation;
                if( FirstChromeMutation.class.isAssignableFrom( entry.getKey())) {
                    densityFirstChrome += entry.getValue().getSecond().getValue()/sumFirstChrome;
                    tempMutation = createInstanceByClass( entry.getKey(), new Object[]{ densityFirstChrome } );
                    this.mutation.getFirstChromeMutations().add( (FirstChromeMutation) tempMutation );
                } else if( SecondChromeMutation.class.isAssignableFrom( entry.getKey())) {
                    densitySecondChrome += entry.getValue().getSecond().getValue()/sumSecondChrome;
                    tempMutation = createInstanceByClass( entry.getKey(), new Object[]{ densitySecondChrome } );
                    this.mutation.getSecondChromeMutations().add( (SecondChromeMutation) tempMutation );
                }
            }
        }

        this.mutation.getFirstChromeMutations().sort( Comparator.comparing( Mutation::getProbability  ));
        this.mutation.getSecondChromeMutations().sort( Comparator.comparing( Mutation::getProbability ) );
    }
}


