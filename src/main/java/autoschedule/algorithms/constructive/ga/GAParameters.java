package autoschedule.algorithms.constructive.ga;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class GAParameters {
    @Getter @Setter
    private int populationSize = 200;
    @Getter @Setter
    private int generationCount = 5;

    @Getter @Setter
    private double selectionProbability = 0.09;
    @Getter @Setter
    private double crossoverProbabilityPermutations = 0.05, crossoverProbabilityMachines = 0.15;
    @Getter @Setter
    private double mutationProbabilityPermutations = 0.2, mutationProbabilityMachines = 0.8;

}
