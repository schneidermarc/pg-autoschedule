package autoschedule.algorithms.constructive.ga.selection;

import autoschedule.algorithms.constructive.ga.GA;
import autoschedule.algorithms.constructive.ga.GASolution;
import autoschedule.algorithms.objectives.*;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import lombok.Getter;
import lombok.Setter;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.util.JMetalException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Selection according to Wu.2003
 */
@AlgorithmAnnotations.GASelection( name = "Selection" )
public class GASelectionOperator implements SelectionOperator<List<GASolution>, GASolution> {

    /**
     * Probabilities of selecting a chromosome
     * The chromosome will be chosen dependent to its rank k with probability
     * selectionProbability * (1-selectionProbability)^k
     *
     * Will be set in GA:prepareParameters
     */
    @Setter
    private double selectionProbability;

    /**
     * The size of a generation
     */
    @Setter
    private int solutionSize;

    /**
     * Every iterationCounter % solutionSize = 0 steps the best solution is saved/cached
     */
    private int iterationCounter = 0;

    /**
     * The cached objective values that will be written to the database
     */
    @Setter @Getter
    private List<OPT_ObjValuesObject> cachedObjectiveValues;

    /**
     * Constructor, called by CLS
     */
    public GASelectionOperator() {}

    @Override
    public GASolution execute(List<GASolution> solutions) throws JMetalException {
        if(solutions == null)
            throw new JMetalException("The solution list is null");

        if(solutions.isEmpty())
            throw new JMetalException("The solution list is empty");

        if (solutions.size() < solutionSize)
            throw new JMetalException("The population size (" + solutions.size() + ") is smaller than the solutions to selected ("+solutions+")");

        List<GASolution> copyList = new ArrayList<>(solutions);

        copyList.removeIf(sol -> sol.getObjective(0) == Double.MAX_VALUE);
        copyList.sort(Comparator.comparingDouble(o -> o.getObjective(0)));

        iterationCounter++;
        if(iterationCounter%solutionSize == 0 && !copyList.isEmpty()){
            GASolution solution = copyList.get(0);
            Schedule schedule = solution.toSchedule();
            OPT_ObjValuesObject objectiveValues = new OPT_ObjValuesObject(
                    -1,
                    (int)Math.round(new CMax().evaluate(schedule)), //CMax
                    (int)Math.round(new CBar().evaluate(schedule)), //Cbar
                    (int)Math.round(new LMax().evaluate(schedule)), //Lmax
                    (int)Math.round(new FMax().evaluate(schedule)), //Fmax
                    (int)Math.round(new UBar().evaluate(schedule)) //Ubar
            );
            cachedObjectiveValues.add(objectiveValues);
        }

        ArrayList<Double> probs = new ArrayList<>();
        for(int i = 1; i <= copyList.size(); i++) {
            double prob = selectionProbability*Math.pow((1-selectionProbability), i-1);
            probs.add(prob);
        }

        Collections.reverse(probs);
        Collections.reverse(copyList);

        GASolution erg = null;
        double randProb = GA.randomGenerator.nextDouble(0,1);
        for(int j = 0; j < probs.size(); j++) {
            double d = probs.get(j);
            if((j == probs.size()-1) || (d < randProb && randProb < probs.get(j+1))) {
                erg = copyList.get(j);
                break;
            }
        }
        return erg;
    }
}
