package autoschedule.algorithms.constructive.ga;

import autoschedule.algorithms.constructive.PriorityRuleAlgorithm;
import autoschedule.algorithms.objectives.CMax;
import autoschedule.algorithms.priorityRules.Random;
import autoschedule.model.Article;
import autoschedule.model.Job;
import autoschedule.model.Machine;
import autoschedule.model.Schedule;
import org.uma.jmetal.solution.impl.AbstractGenericSolution;

import java.util.*;

public class GASolution extends AbstractGenericSolution<DNA, GAProblem> {

    /**
     * Public Constructor
     * @param gaProblem GAProblem instance
     */
    protected GASolution(GAProblem gaProblem ) {
        super(gaProblem);

        //create random schedule

        PriorityRuleAlgorithm pra = new PriorityRuleAlgorithm(new Random());
        pra.setObjective( new CMax() );
        pra.run(gaProblem.getSchedule());

        // list of articles grouped by stage
        List<List<Article>> firstChrome = new ArrayList<>();
        // matrix of machines
        List<Map<Article, Machine>> secondChrome = new ArrayList<>();

        for( int jobIndex = 0; jobIndex < gaProblem.getSchedule().getJobs().size(); jobIndex++ ) {
            Job job = gaProblem.getSchedule().getJobs().get(jobIndex);
            job.getArticles().sort( Comparator.comparingInt( Article::getStage ) );
        }

        // create random schedule
        for (int stage = 1; stage <= gaProblem.getSchedule().getStages(); stage++) {
            ArrayList<Article> articlesOnStage = new ArrayList<>(gaProblem.getSchedule().getAllArticlesOnStage(stage));
            Collections.shuffle(articlesOnStage);
            firstChrome.add(articlesOnStage);
        }

        // choose one qualified machine for each article and save in second chrome
        // as for reasons of consistence to wu.2003 usage of k and i
        // k means stage-Index
        for (List<Article> articles : firstChrome) {
            // i means Article-Index
            Map<Article, Machine> machinesOnStage = new HashMap<>();
            for (Article article : articles) {
                List<Machine> tmp = article.getQualifiedMachines();

                machinesOnStage.put(article, tmp.get(GA.randomGenerator.nextInt(0, tmp.size() - 1)));
            }
            secondChrome.add(machinesOnStage);
        }

        setVariableValue(0, new DNA(firstChrome, secondChrome));
    }

    /**
     * Copy constructor
     */
    private GASolution(GASolution other) {
        super(other.problem);

        this.setVariableValue(0, other.getVariableValue(0).copy());

        this.attributes.putAll(other.attributes);
    }

    @Override
    public String getVariableValueString(int index) {
        return getVariableValue(index).toString();
    }

    /**
     * Copy method
     * @return new GASolution based on this
     */
    @Override
    public GASolution copy() {
        return new GASolution(this);
    }

    @Override
    public Map<Object, Object> getAttributes() {
        return attributes;
    }

    /**
     * Convert GASolution into schedule
     * @return Corresponding schedule
     */
    public Schedule toSchedule() {
        Schedule schedule = this.problem.getSchedule().copy();
        List<Job> oldJobs = new ArrayList<>(schedule.getJobs());
        schedule.getJobs().clear();

        Map<Machine, Integer> machineWorkingUntil = new HashMap<>();

        List<List<Article>> firstChrome = new ArrayList<>();
        for(List<Article> articleList : this.getVariables().get(0).getFirstChrome()) {
            firstChrome.add(new ArrayList<>(articleList));
        }

        List<Map<Article, Machine>> tempSecondChrome = new ArrayList<>();
        for(Map<Article, Machine> machineList : this.getVariables().get(0).getSecondChrome()) {
            tempSecondChrome.add(new HashMap<>(machineList));
            machineList.values().forEach(machine -> machineWorkingUntil.put(machine, machine.getArrival()));
        }

        List<Article> allArticles = new ArrayList<>();

        //schedule stage by stage
        //for (int stageIndex = firstChrome.size() - 1; stageIndex >= 0; stageIndex--) {
        for (int stageIndex = 0; stageIndex < firstChrome.size(); stageIndex++) {
            for (int articleIndex = 0; articleIndex < firstChrome.get(stageIndex).size(); articleIndex++) {



                Article article = firstChrome.get(stageIndex).get(articleIndex);
                Machine machine = tempSecondChrome.get(stageIndex).get(article);//get(articleIndex);

                final int index = stageIndex;
                int articleArrival = oldJobs.stream().filter(job -> job.getArticles().size() > index && job.getArticles().get(index).getId() == article.getId()).map(Job::getArrival).findFirst().orElse(0);
                int time = Math.max(Math.max(machineWorkingUntil.get(machine), articleArrival), article.getPreArticle() == null ? 0 : article.getPreArticle().getFinishTime());

                article.scheduleMachine(machine, time); //schedule
                //machineWorkingUntil.put(machine, time + article.getFinishTime()); //update
                machineWorkingUntil.put(machine, article.getFinishTime()); //update

                allArticles.add(article);
            }
        }

        allArticles.sort(Comparator.comparingInt(Article::getStage));
        Collections.reverse(allArticles);

        int newJobID = 0;

        while(!allArticles.isEmpty()) {
            LinkedList<Article> articlesOfJob = new LinkedList<>();
            Article article = allArticles.get(0);

            do {
                articlesOfJob.add(article);
                allArticles.remove(article);
                article = article.getPreArticle();
            } while (article != null);

            Collections.reverse(articlesOfJob);

            Job job = oldJobs.stream().filter(j -> j.getArticles().get(0).getId() == articlesOfJob.get(0).getId()).findFirst().orElseThrow();
            schedule.getJobs().add(new Job(newJobID++, "", articlesOfJob, job.getDeadline(), job.getArrival(), job.getWeight()));
        }

        return schedule;
    }

}
