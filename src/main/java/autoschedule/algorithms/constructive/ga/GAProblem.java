package autoschedule.algorithms.constructive.ga;

import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.model.Schedule;
import lombok.Getter;
import org.uma.jmetal.problem.impl.AbstractGenericProblem;

public class GAProblem extends AbstractGenericProblem<GASolution> {

    /**
     * Corresponding schedule
     *
     * Will be filled with content in GASolution:toSolution
     */
    @Getter
    private final Schedule schedule;

    /**
     * Objective value to be optimized
     */
    private final SchedulingObjective objective;

    /**
     * Public constructor
     * @param schedule Schedule
     * @param objective Scheduling objective for algorithm
     */
    public GAProblem(Schedule schedule, SchedulingObjective objective) {
        this.schedule = schedule;
        this.objective = objective;

        setNumberOfVariables(1);
        setNumberOfObjectives(1);
        setName("GA Optimization");
    }

    @Override
    public GASolution createSolution() {
        return new GASolution(this);
    }

    @Override
    public void evaluate(GASolution solution) {
        solution.setObjective(0, objective.evaluate(solution.toSchedule()));
    }
}
