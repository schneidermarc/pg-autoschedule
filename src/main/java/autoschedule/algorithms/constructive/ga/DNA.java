package autoschedule.algorithms.constructive.ga;

import autoschedule.model.Article;
import autoschedule.model.Machine;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DNA {

    /**
     * List of Articles grouped by stage
     * Articles are not index-matching with machines of secondChrome
     */
    @Getter @Setter
    private List<List<Article>> firstChrome;

    /**
     * Matrix of machines grouped by stage
     * Machines are not index-matching with articles of firstChrome
     */
    @Getter	@Setter
    private List<Map<Article,Machine>> secondChrome;

    /**
     * Public constructor
     * @param firstChrome Article chrome
     * @param secondChrome Job chrome
     */
    public DNA(List<List<Article>> firstChrome, List<Map<Article, Machine>> secondChrome) {
        this.firstChrome = firstChrome;
        this.secondChrome = secondChrome;
    }

    /**
     * Copy constructor
     *
     * copies the first and second chrome
     */
    public DNA(DNA old) {
        List<List<Article>> newFirstChrome = new ArrayList<>();
        for (int i = 0; i < old.getFirstChrome().size(); i++) {
            List<Article> temp = new ArrayList<>(old.getFirstChrome().get(i));
            newFirstChrome.add(temp);
        }

        List<Map<Article, Machine>> newSecondChrome = new ArrayList<>();
        for (int i = 0; i < old.getSecondChrome().size(); i++) {
            Map<Article, Machine> temp = new HashMap<>(old.getSecondChrome().get(i));
            newSecondChrome.add(temp);
        }

        this.firstChrome = newFirstChrome;
        this.secondChrome = newSecondChrome;

        if(this.firstChrome.size() != old.getFirstChrome().size() || this.secondChrome.size() != old.getSecondChrome().size())
            throw new RuntimeException("Error in DMA copy");
    }

    /**
     * Calls the copy-constructor and return a new instance of a DNA
     * @return New DNA with same values like this
     */
    public DNA copy() {
        return new DNA(this);
    }

    @Override
    public String toString() {
        return firstChrome + "/" + secondChrome;
    }
}
