package autoschedule.algorithms.constructive.ga.mutation;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Article;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unused")
@AlgorithmAnnotations.GAMutation( name = "Reverse (Articles)" )
public class ReverseArticleMutation extends FirstChromeMutation {

	public ReverseArticleMutation( double probability ) {
		super( probability );
	}

	/**
	 * Shuffles jobs between two points and shifts them to the end
	 *
	 * @param articlesOnStage List of article, which are on the same stage
	 */
	@Override
	public void mutate( List<Article> articlesOnStage ) {
		// select two different points in DNA
		int[] points = getPoints( 2, 0, articlesOnStage.size() - 1 );

		// Reverse Mutation
		ArrayList<Article> newRev = new ArrayList<>();
		for( int i = points[ 0 ]; i < points[ 1 ]; i++ ) {
			newRev.add( articlesOnStage.get( i ) );
		}

		Collections.reverse( newRev );
		int runner = 0;
		for( int i = points[ 0 ]; i < points[ 1 ]; i++ ) {
			articlesOnStage.set( i, newRev.get( runner ) );
			runner++;
		}
	}
}
