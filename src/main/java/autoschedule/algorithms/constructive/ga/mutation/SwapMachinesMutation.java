package autoschedule.algorithms.constructive.ga.mutation;

import autoschedule.algorithms.constructive.ga.GA;
import autoschedule.algorithms.constructive.ga.GAProblem;
import autoschedule.algorithms.constructive.ga.GASolution;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Article;
import autoschedule.model.Machine;

import java.util.List;
import java.util.Map;

@AlgorithmAnnotations.GAMutation( name = "Swap (Machines)" )
public class SwapMachinesMutation extends SecondChromeMutation {

	public SwapMachinesMutation( double probability ) {
		super( probability );
	}

	/**
	 * Swaps machines on a random stage
	 *
	 * @param solution the GASolution, which holds a information about the chromosome
	 * @param problem  the GAProblem, which holds a information about the schedule
	 */
	@Override
	public void mutate( GASolution solution, GAProblem problem ) {
		List<Map<Article, Machine>> secondChrome = solution.getVariables().get(0).getSecondChrome();

		for(int stage = 1; stage <= problem.getSchedule().getStages(); stage++) {
			if(secondChrome.get(stage - 1).size() != problem.getSchedule().getJobsOnStage(stage).size()) {
				throw new RuntimeException( "Error at stage " + stage + " in mutation" );
			}
		}

		int stage = GA.randomGenerator.nextInt(1, problem.getSchedule().getStages());
		int job = GA.randomGenerator.nextInt(0, problem.getSchedule().getJobsOnStage(stage).size() - 1);

		Article article = solution.getVariables().get(0).getFirstChrome().get(stage - 1).get(job);

		// it only makes sense, if there is more than one qualified machine
		if( article.getQualifiedMachines().size() > 1) {
			Machine oldMachine = secondChrome.get(stage - 1).get(article);

			Machine newMachine;
			do {
				newMachine = article.getQualifiedMachines().get( GA.randomGenerator.nextInt(0, article.getQualifiedMachines().size() - 1));
			}
			while( oldMachine == newMachine );

			secondChrome.get(stage - 1).put(article, newMachine);
		}
	}
}
