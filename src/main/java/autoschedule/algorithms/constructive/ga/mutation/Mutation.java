package autoschedule.algorithms.constructive.ga.mutation;

import autoschedule.algorithms.constructive.ga.GA;
import lombok.Getter;

import java.util.Arrays;

/**
 * Abstract class for mutations
 */
public abstract class Mutation {

	/**
	 * Probability that indicates whether a mutation is chosen or not
	 */
	@Getter
	private final double probability;

	public Mutation( Double probability ) {
		this.probability = probability;
	}

	//public abstract void mutate( GASolution solution, GAProblem problem );

	/**
	 * Generate n different random points in between from and to if possible
	 * If n is greater than range of from - to, there might be duplicates
	 *
	 * @param n amount of random points
	 * @param from lower bound (e.g. zero)
	 * @param to upper bound (e.g. articlesOnStage.size() - 1)
	 * @return sorted array (ascending) of ints interpretable as indices
	 */
	@SuppressWarnings("SameParameterValue")
	protected int[] getPoints(int n, int from, int to ) {
		assert from < to;
		assert n > 0;

		// select two different points in DNA
		int[] points = new int[ n ];

		boolean allowDuplicates = (from - to) < n;
		int a = 0;

		// Generate points
		while( a < n  ) {
			int point = GA.randomGenerator.nextInt( from, to );

			// Allow duplicates
			if( !allowDuplicates ) {
				boolean isDuplicate = true;
				while( isDuplicate ) {
					isDuplicate = false;
					// Check if generated point already generated
					// If already generated, generate a new point
					for( int z = 0; z < a && !isDuplicate ; z++ ) {
						if( points[z] == point) {
							point = GA.randomGenerator.nextInt( from, to );
							isDuplicate = true;
						}
					}
				}
			}

			points[a++] = point;
		}

		// sort points
		Arrays.sort(points);

		return points;
	}

}
