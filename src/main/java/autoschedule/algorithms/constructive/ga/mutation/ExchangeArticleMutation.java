package autoschedule.algorithms.constructive.ga.mutation;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Article;

import java.util.List;

@SuppressWarnings("unused")
@AlgorithmAnnotations.GAMutation( name = "Exchange (Articles)" )
public class ExchangeArticleMutation extends FirstChromeMutation {

	public ExchangeArticleMutation( double probability ) {
		super( probability );
	}

	/**
	 * Exchanges jobs on two points
	 *
	 * @param articlesOnStage List of article, which are on the same stage
	 */
	@Override
	public void mutate( List<Article> articlesOnStage ) {
		// select two different points in DNA
		int[] points = getPoints( 2, 0, articlesOnStage.size() - 1 );

		// Exchange Mutation
		Article tmp = articlesOnStage.get( points[ 0 ] );
		articlesOnStage.set( points[ 0 ], articlesOnStage.get( points[ 1 ] ) );
		articlesOnStage.set( points[ 1 ], tmp );
	}

}
