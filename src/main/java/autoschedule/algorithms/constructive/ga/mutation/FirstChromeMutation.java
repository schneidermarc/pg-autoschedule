package autoschedule.algorithms.constructive.ga.mutation;

import autoschedule.model.Article;

import java.util.List;

public abstract class FirstChromeMutation extends Mutation {

	public FirstChromeMutation( Double probability ) {
		super( probability );
	}

	public abstract void mutate( List<Article> articlesOnStage );
}
