package autoschedule.algorithms.constructive.ga.mutation;

import autoschedule.algorithms.constructive.ga.GA;
import autoschedule.algorithms.constructive.ga.GAProblem;
import autoschedule.algorithms.constructive.ga.GASolution;
import autoschedule.model.Article;
import lombok.Getter;
import lombok.Setter;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.util.JMetalException;

import java.util.ArrayList;
import java.util.List;

/**
 * Mutations according to Wu.2003
 */
public class GAMutationOperator implements MutationOperator<GASolution> {

	/**
	 * List of Mutations, that can be performed
	 * on the first chromosome (list of articles grouped by stage)
	 *
	 * Filled dynamically by GA itself (see method GA:prepareParameters)
	 * The execute-Method will choose exactly one mutation out of this list
	 *
	 * Sum of probabilities of all mutations in the list will be 1
	 */
	@Getter
	private final List<FirstChromeMutation> firstChromeMutations = new ArrayList<>();

	/**
	 * List of Mutations, that can be performed
	 * on the second chromosome (mapped machine-articles grouped by stage)
	 *
	 * Filled dynamically by GA itself (see method GA:prepareParameters)
	 * The execute-Method will choose exactly one mutation out of this list
	 *
	 * Sum of probabilities of all mutations in the list will be 1
	 */
	@Getter
	private final List<SecondChromeMutation> secondChromeMutations = new ArrayList<>();

	@Setter
	private GAProblem problem;

	/**
	 * Probabilities that indicates whether a mutation should be made or not on the first or second chromosome
	 * It is possible, that a mutation is performed on the first and second chromosome during one execute-call
	 *
	 * Will be set in GA:prepareParameters
	 */
	@Setter
	private double probabilityFirst, probabilitySecond;

	/**
	 * Constructor, called by CLS
	 */
	public GAMutationOperator() {}

	@Override
	public GASolution execute( GASolution solution ) throws JMetalException {
		if( solution == null )
			throw new JMetalException( "Parent is null" );

		// Mutation on first chromosome (with selecting exactly one mutation)
		if( probabilityFirst < GA.randomGenerator.nextDouble( 0, 1 ) ) {
			List<List<Article>> articles = solution.getVariables().get( 0 ).getFirstChrome();

			for( int stage = 1; stage <= articles.size(); stage++ ) {
				List<Article> articlesOnStage = articles.get( stage - 1 );

				if( articlesOnStage.size() != problem.getSchedule().getJobsOnStage( stage ).size() ) {
					throw new RuntimeException( "Error at stage " + stage + " in mutation. Found " + problem.getSchedule().getJobsOnStage( stage ).size() + " Jobs on stage, but " + articlesOnStage.size() + " articles." );
				}

				double rng = GA.randomGenerator.nextDouble( 0, 1 );
				int i = firstChromeMutations.size() - 1;
				while( i >= 0 && firstChromeMutations.get( i ).getProbability() >= rng ) {
					i--;
				}
				firstChromeMutations.get( i+1 ).mutate( articlesOnStage );
			}
		}

		// Mutation on second chromosome (with selecting exactly one mutation)
		if( probabilitySecond < GA.randomGenerator.nextDouble( 0, 1 ) ) {
			double rng = GA.randomGenerator.nextDouble( 0, 1 );
			int i = secondChromeMutations.size() - 1;
			while( i >= 0 && secondChromeMutations.get( i ).getProbability() >= rng ) {
				i--;
			}
			secondChromeMutations.get( i+1 ).mutate( solution, problem );
		}

		return solution;
	}
}
