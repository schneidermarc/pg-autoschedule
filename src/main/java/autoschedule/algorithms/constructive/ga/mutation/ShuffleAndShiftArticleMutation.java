package autoschedule.algorithms.constructive.ga.mutation;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Article;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AlgorithmAnnotations.GAMutation( name = "Shuffle and Shift (Articles)" )
public class ShuffleAndShiftArticleMutation extends FirstChromeMutation {

	public ShuffleAndShiftArticleMutation( double probability ) {
		super( probability );
	}

	/**
	 * Reverses order of jobs between two points
	 *
	 * @param articlesOnStage  List of article, which are on the same stage
	 */
	@Override
	public void mutate( List<Article> articlesOnStage ) {
		// select two different points in DNA
		int[] points = getPoints( 2, 0, articlesOnStage.size() - 1 );

		//shuffle between two points
		List<Article> range = new ArrayList<>();

		for( int i = 0; i <= ( points[ 1 ] - points[ 0 ] ); i++ ) {
			range.add( articlesOnStage.remove( points[ 0 ] ) );
		}

		Collections.shuffle( range );
		articlesOnStage.addAll( range );
	}
}
