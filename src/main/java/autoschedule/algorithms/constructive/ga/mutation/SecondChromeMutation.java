package autoschedule.algorithms.constructive.ga.mutation;

import autoschedule.algorithms.constructive.ga.GAProblem;
import autoschedule.algorithms.constructive.ga.GASolution;

public abstract class SecondChromeMutation extends Mutation {

	public SecondChromeMutation( Double probability ) {
		super( probability );
	}

	public abstract void mutate( GASolution solution, GAProblem problem );
}
