package autoschedule.algorithms.constructive.ga.crossover;

import autoschedule.algorithms.constructive.ga.GA;
import autoschedule.algorithms.constructive.ga.GAProblem;
import autoschedule.algorithms.constructive.ga.GASolution;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Article;
import autoschedule.model.Machine;
import lombok.Setter;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.util.JMetalException;

import java.util.*;

/**
 * Crossovers according to Wu.2003
 */
@AlgorithmAnnotations.GACrossover( name = "Two Parent Crossover" )
public class GATwoParentsCrossoverOperator implements CrossoverOperator<GASolution> {

    /**
     * Probability to perform a crossover (2nd Chrome) on a stage
     */
    private static final float PROBABILITY_CROSSOVER_ON_EACH_STAGE = 0.8F;

    @Setter
    private GAProblem problem;

    /**
     * Probabilities that indicates whether a crossover should be made or not on the first or second chromosome
     * It is possible, that a crossover is performed on the first and second chromosome during one execute-call
     *
     * Will be set in GA:prepareParameters
     */
    @Setter
    private double probabilityFirstChrome, probabilitySecondChrome;

    /**
     * Constructor, called by CLS
     */
    public GATwoParentsCrossoverOperator() {}

    @Override
    public List<GASolution> execute(List<GASolution> parents) throws JMetalException{
        if(parents == null)
            throw new JMetalException("Null parameter");

        if(parents.size() != 2)
            throw new JMetalException("There must be two parents instead of " + parents.size());

        GASolution firstSolution = parents.get(0).copy();
        GASolution secondSolution = parents.get(1).copy();

        if(GA.randomGenerator.nextDouble(0,1) <= probabilityFirstChrome)
            doCrossoverFirstChrome(firstSolution, secondSolution);

        if(GA.randomGenerator.nextDouble(0,1) <= probabilitySecondChrome)
            doCrossoverSecondChrome(firstSolution, secondSolution);

        return Arrays.asList(firstSolution, secondSolution);
    }

    /**
     * Executes crossover on Article chrome
     * @param firstSolution First solution
     * @param secondSolution Second solution
     */
    private void doCrossoverFirstChrome(GASolution firstSolution, GASolution secondSolution) {
        List<List<Article>> firstSolutionChrome = firstSolution.getVariables().get(0).getFirstChrome();
        List<List<Article>> secondSolutionChrome = secondSolution.getVariables().get(0).getFirstChrome();

        if(firstSolutionChrome.size() != secondSolutionChrome.size())
            throw new RuntimeException("Error in operations");

        for(int stage = 1; stage <= firstSolutionChrome.size(); stage++) {
            List<Article> firstSolutionChromeOnStage = firstSolutionChrome.get(stage-1);
            List<Article> secondSolutionChromeOnStage = secondSolutionChrome.get(stage-1);

            if(firstSolutionChromeOnStage.size() != problem.getSchedule().getJobsOnStage(stage).size() ||
                    secondSolutionChromeOnStage.size() != problem.getSchedule().getJobsOnStage(stage).size()) {

                throw new RuntimeException("Error at stage " + stage + " in first chrome crossover. "
                        + problem.getSchedule().getJobsOnStage(stage).size() + " jobs on stage, but "
                        + firstSolutionChromeOnStage.size() + " articles in first solution and "
                        + secondSolutionChromeOnStage.size() + " articles in second solution.");
            }

            //generate 4 different points sorted ascending
            ArrayList<Integer> points = new ArrayList<>();
            while(points.size() < 4) {
                int point = GA.randomGenerator.nextInt(0, firstSolutionChromeOnStage.size()-1);
                if(!points.contains(point))
                    points.add(point);
            }
            Collections.sort(points);

            //get articles from chromes
            ArrayList<Article> insertionList1 = new ArrayList<>(), insertionList2 = new ArrayList<>();
            for(int i = 0; i < 4; i++) {
                insertionList1.add(secondSolutionChromeOnStage.get(points.get(i)));
                insertionList2.add(firstSolutionChromeOnStage.get(points.get(i)));
            }

            //do mutation
            mutate(insertionList1, firstSolutionChromeOnStage);
            mutate(insertionList2, secondSolutionChromeOnStage);
        }
    }

    /**
     * Executes mutation on Article chrome
     * @param insertionList Insertion list
     * @param chrome Article chrome
     */
    private void mutate(List<Article> insertionList, List<Article> chrome) {
        for (Article article : insertionList) {
            for (int j = 0; j < chrome.size(); j++) {
                if (chrome.get(j) == article) {
                    chrome.set(j, null);
                    break;
                }
            }
        }

        int runner = 0;
        for(int i = 0; i < chrome.size(); i++) {
            if(chrome.get(i) == null) {
                chrome.set(i, insertionList.get(runner));
                runner++;
            }
        }
    }

    /**
     * Swaps second chrome on each stage between two random points
     * @param firstSolution firstSolution
     * @param secondSolution secondSolution
     */
    private void doCrossoverSecondChrome(GASolution firstSolution, GASolution secondSolution) {
        List<Map<Article, Machine>> firstSolutionChrome = firstSolution.getVariables().get(0).getSecondChrome();
        List<Map<Article, Machine>> secondSolutionChrome = secondSolution.getVariables().get(0).getSecondChrome();

        //for each stage
        for(int stage = 1; stage <= problem.getSchedule().getStages(); stage++) {
            if(GA.randomGenerator.nextInt(0, 1) > PROBABILITY_CROSSOVER_ON_EACH_STAGE)
                continue;

            if(firstSolutionChrome.get(stage-1).size() != problem.getSchedule().getJobsOnStage(stage).size() || secondSolutionChrome.get(stage-1).size() != problem.getSchedule().getJobsOnStage(stage).size())
                throw new RuntimeException("Error in operations");

            for(int i = 0; i < firstSolutionChrome.size(); i++) {
                //select two different points in DNA
                int point1 = GA.randomGenerator.nextInt(0, firstSolutionChrome.get(i).size()), point2;
                do {
                    point2 = GA.randomGenerator.nextInt(0, firstSolutionChrome.get(i).size());
                } while(point1 == point2);

                //make point 1 the smaller point
                if(point1 > point2) {
                    int change = point1;
                    point1 = point2;
                    point2 = change;
                }

                List<Article> fscArticles = List.copyOf( firstSolutionChrome.get( i ).keySet());
                List<Article> sscArticles = List.copyOf( secondSolutionChrome.get( i ).keySet());

                //swap between points
                for(int range = point1; range < point2; range++) {
                    Machine temp = firstSolutionChrome.get( i ).get( fscArticles.get( range ) );
                    firstSolutionChrome.get( i ).put( fscArticles.get( range ), secondSolutionChrome.get( i ).get( sscArticles.get( range ) ) );
                    secondSolutionChrome.get( i ).put( sscArticles.get( range ), temp );
                }

            }
        }
    }

    @Override
    public int getNumberOfGeneratedChildren() {
        return 2;
    }

    @Override
    public int getNumberOfRequiredParents() {
        return 2;
    }
}
