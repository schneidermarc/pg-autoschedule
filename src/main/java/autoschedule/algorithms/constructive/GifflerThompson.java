package autoschedule.algorithms.constructive;

import autoschedule.algorithms.priorityRules.PriorityRule;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.*;
import lombok.Getter;

import java.util.*;

//Job Shop priority algorithm for non-skipping job shop
@AlgorithmAnnotations.Constructive(name="Giffler & Thompson",  singleUse = true)
public class GifflerThompson extends ConstructiveAlgorithm {

    @Getter
    private final PriorityRule priorityRule;

    /**
     * Public constructor
     * @param priorityRule Priority rule to be used
     */
    public GifflerThompson( PriorityRule priorityRule ) {
        this.priorityRule = priorityRule;
    }

    @Override
    protected List<OPT_ObjValuesObject> execute(Schedule schedule) {
        List<OPT_ObjValuesObject> objectiveValuesList = new LinkedList<>();
        final Map<Machine, Integer> machineWorkingUntil = new HashMap<>(); //Zi
        final Map<Job, Integer> jobWorkingUntil = new HashMap<>(); //Rj
        final Map<Job, Integer> stepOfJob = new HashMap<>();

        //init articles
        schedule.getJobs().forEach(job -> job.getArticles().forEach(Article::initForNonFlexible));

        //initialize machine start times
        schedule.getAllMachines().forEach(machine -> machineWorkingUntil.put(machine, machine.getArrival()));
        schedule.getJobs().forEach(job -> {
            jobWorkingUntil.put(job, job.getArrival());
            stepOfJob.put(job, 0);
        });
        System.out.println("Anzahl Jobs: " + schedule.getJobs().size());

        //Iterate #jobs x #machines times
        for (int i = 0; i < schedule.getJobs().stream().mapToInt(job -> job.getArticles().size()).sum(); i++) {
            System.out.println("Iteration " + i + " von " + (schedule.getAllMachines().size() * schedule.getJobs().size()));
            Article articleToSchedule = null;
            int finishTime = Integer.MAX_VALUE;

            //Find machine that may finish first
            for (Job j : schedule.getJobs()) {
                if (stepOfJob.get(j) >= j.getArticles().size()) //skip finished jobs
                    continue;

                Article nextArticle = j.getArticles().get(stepOfJob.get(j));
                int time = nextArticle.getProcessingTimeOnMachine() + Math.max(machineWorkingUntil.get(nextArticle.getScheduledMachine()), jobWorkingUntil.get(j));
                if (time < finishTime) {
                    articleToSchedule = nextArticle;
                    finishTime = time;
                }
            }

            //Find all jobs waiting for machine
            LinkedList<Job> waitingJobsOnMachine = new LinkedList<>();
            for(Job j : schedule.getJobs()) {
                if (stepOfJob.get(j) >= j.getArticles().size()) //skip finished jobs
                    continue;

                //noinspection ConstantConditions
                if(j.getArticles().get(stepOfJob.get(j)).getScheduledMachine().equals(articleToSchedule.getScheduledMachine())) //find all jobs which wait for the same machine as articleToSchedule
                    waitingJobsOnMachine.add(j);
            }

            //apply priority rule
            Job jobToSchedule = priorityRule.rank(waitingJobsOnMachine).get(0);

            articleToSchedule = jobToSchedule.getArticles().get(stepOfJob.get(jobToSchedule)); //find new articleToSchedule
            int processingTime = articleToSchedule.getProcessingTimeOnMachine();
            int startTime = Math.max(machineWorkingUntil.get(articleToSchedule.getScheduledMachine()), jobWorkingUntil.get(jobToSchedule)); //max(machine is ready, job is ready)
            finishTime = processingTime + startTime;

            //schedule article
            jobToSchedule.getArticles().get(stepOfJob.get(jobToSchedule)).setScheduledTime(startTime);
            System.out.println(finishTime);

            //Update
            stepOfJob.put(jobToSchedule, stepOfJob.get(jobToSchedule) + 1);
            jobWorkingUntil.put(jobToSchedule, finishTime);
            machineWorkingUntil.put(articleToSchedule.getScheduledMachine(), finishTime);
        }
        return objectiveValuesList;
    }

    @Override
    public AlgorithmConfig getAlgorithmConfig() {
        return null;
    }

    @Override
    public String getString() {
        return "Giffler & Thompson algorithm (Rule: " + priorityRule.toString() + ")";
    }

    @Override
    public String getSimpleString() {
        return "G&T (Rule: " + priorityRule.toString() + ")";
    }

    @Override
    protected void prepareParameters(){}
}
