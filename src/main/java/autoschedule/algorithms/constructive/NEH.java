//autoschedule.algorithms.constructive.NEH
package autoschedule.algorithms.constructive;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.*;

import java.util.*;

@AlgorithmAnnotations.Constructive( name = "NEH", singleUse = true )
public class NEH extends ConstructiveAlgorithm {

	/**
	 * NEH
	 * orders all jobs in descending order of processing time
	 * insert every job at any position and take the solution with lowest makespan
	 * @param schedule schedule to rearrange
	 * @return new schedule
	 */
	@Override
	protected List<OPT_ObjValuesObject> execute( Schedule schedule ) {
		List<OPT_ObjValuesObject> objectiveValuesList = new LinkedList<>();
		final List<Job> jobs = new LinkedList<>(schedule.getJobs());

		//init articles
		jobs.forEach(job -> job.getArticles().forEach(Article::initForNonFlexible));

		//sort jobs in descending order of processing time + arrival
		jobs.sort(Comparator.comparingInt(job -> -(job.getArticles().stream().mapToInt(Article::getProcessingTimeOnMachine).sum() + job.getArrival())));

		//initialize local schedule with first job (the job with the longest makespan)
		List<Job> permutation = new LinkedList<>();
		permutation.add(jobs.remove(0));

		//schedule each job in the List

		for(Job job : jobs) {
			int currentCmax = Integer.MAX_VALUE;
			List<Job> currentPermutation = new LinkedList<>(permutation);

			//add next job at each position
			for (int i = 0; i < permutation.size(); i++) {
				//copy local best
				List<Job> clonedPermutation = new LinkedList<>(permutation);
				clonedPermutation.add(i, job);

				//Set times
				Map<Machine, Integer> machineWorkingUntil = new HashMap<>();
				clonedPermutation.forEach(j -> j.getArticles().forEach(a -> machineWorkingUntil.put(a.getScheduledMachine(), a.getScheduledMachine().getArrival())));

				for (Job jobToSchedule : clonedPermutation) {
					//for(Job jobToSchedule : clonedPermutation) {
					for (int articleIndex = 0; articleIndex < jobToSchedule.getArticles().size(); articleIndex++) {
						Article articleToSchedule = jobToSchedule.getArticles().get(articleIndex);
						Machine machine = articleToSchedule.getScheduledMachine();

						//finish time of previous job
						int t1 = articleIndex == 0 ? jobToSchedule.getArrival() : jobToSchedule.getArticles().get(articleIndex - 1).getFinishTime();

						//finish time of machine
						int t2 = machineWorkingUntil.get(machine);
						int scheduleTime = Math.max(t1, t2);

						articleToSchedule.scheduleMachine(machine, scheduleTime);
						machineWorkingUntil.put(machine, articleToSchedule.getFinishTime());
					}
				}

				//update best
				int localCmax = clonedPermutation.get(clonedPermutation.size()-1).getEndtime();
				if (localCmax < currentCmax) {
					currentCmax = localCmax;
					currentPermutation = clonedPermutation;
				}
			}

			permutation = currentPermutation;
		}
		return objectiveValuesList;
	}

	@Override
	public AlgorithmConfig getAlgorithmConfig() {
		return null;
	}

	@Override
	public String getString(){
		return "NEH";
	}

	@Override
	public String getSimpleString(){
		return "NEH";
	}

	@Override
	protected void prepareParameters(){
		setObjective(  createInstanceByClass( model.getSelectedObjectiveFunction().getValue() ) );
	}
}

