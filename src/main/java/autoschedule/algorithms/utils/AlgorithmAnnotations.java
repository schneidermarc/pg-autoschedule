package autoschedule.algorithms.utils;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.constructive.ga.GASolution;
import autoschedule.algorithms.constructive.ga.mutation.Mutation;
import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.algorithms.priorityRules.PriorityRule;
import org.reflections.Reflections;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.SelectionOperator;
import scala.tools.jline_embedded.internal.Log;

import java.lang.annotation.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * @author cstockhoff
 */
public class AlgorithmAnnotations {

	/**
	 * Name of the package for entry point of searching for classes with annotations
	 * Search is recursive through all sub-packages
	 */
	private static final String packageName = "autoschedule.algorithms";

	/**
	 * List containing all found constructive algorithms annotated with @Constructive
	 * inside specified packageName
	 */
	public static final ArrayList<Class<Algorithm>> constructiveAlgorithms = new ArrayList<>();

	/**
	 * List containing all found iterative algorithms annotated with @Iterative
	 * inside specified packageName
	 */
	public static final ArrayList<Class<Algorithm>> iterativeAlgorithms = new ArrayList<>();

	/**
	 * List containing all found priority rules annotated with @Priority
	 * inside specified packageName
	 */
	public static final ArrayList<Class<PriorityRule>> priorityRules = new ArrayList<>();

	/**
	 * List containing all found objective functions annotated with @ObjectiveFunction
	 * inside specified packageName
	 */
	public static final ArrayList<Class<SchedulingObjective>> objectiveFunctions = new ArrayList<>();

	/**
	 * List containing all found neighborhood functions annotated with @Neighborhood
	 * inside specified packageName
	 */
	public static final ArrayList<Class<autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood>> neighborhoods = new ArrayList<>();

	/**
	 * List containing all found move strategies annotated with @Move
	 * inside specified packageName
	 */
	public static final ArrayList<Class<autoschedule.algorithms.iterative.localsearch.neighborhoods.Move>> moveStrategies = new ArrayList<>();

	/**
	 * List containing all found genetic algorithms crossover operators annotated with @GACrossover
	 * inside specified packageName
	 */
	public static final ArrayList<Class<CrossoverOperator<GASolution>>> gaCrossoverOperators = new ArrayList<>();

	/**
	 * List containing all found genetic algorithms mutation operators annotated with @GAMutation
	 * inside specified packageName
	 */
	public static final ArrayList<Class<Mutation>> gaMutationOperators = new ArrayList<>();

	/**
	 * List containing all found genetic algorithms selection operators annotated with @GASelection
	 * inside specified packageName
	 */
	public static final ArrayList<Class<SelectionOperator<List<GASolution>, GASolution>>> gaSelectionOperators = new ArrayList<>();

	/**
	 * List containing all found genetic algorithms selection operators annotated with @GASelection
	 * inside specified packageName
	 */
	public static final ArrayList<Class<autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood>> saNeighborhood = new ArrayList<>();

	/*
	 * Static call for collecting annotated classes
	 */
	static {
		Reflections reflections = new Reflections( packageName );

		// collect constructive algorithms
		constructiveAlgorithms.addAll(autoCast(reflections.getTypesAnnotatedWith( Constructive.class )));
		constructiveAlgorithms.sort( Comparator.comparing( Class::toString ) );

		// collect iterative algorithms
		iterativeAlgorithms.addAll( autoCast(reflections.getTypesAnnotatedWith( Iterative.class )));
		iterativeAlgorithms.sort( Comparator.comparing( Class::toString ) );

		// collect priority rules
		priorityRules.addAll( autoCast(reflections.getTypesAnnotatedWith( Priority.class )));
		priorityRules.sort( Comparator.comparing( Class::toString ) );

		// collect objective functions
		objectiveFunctions.addAll(autoCast(reflections.getTypesAnnotatedWith( ObjectiveFunction.class )));
		objectiveFunctions.sort( Comparator.comparing( Class::toString ) );

		// collect neighborhood functions
		neighborhoods.addAll( autoCast(reflections.getTypesAnnotatedWith( Neighborhood.class )));
		neighborhoods.sort( Comparator.comparing( Class::toString ) );

		// collect permutation strategies
		moveStrategies.addAll( autoCast(reflections.getTypesAnnotatedWith( Move.class )));
		moveStrategies.sort( Comparator.comparing( Class::toString ) );

		// collect permutation strategies
		gaCrossoverOperators.addAll( autoCast(reflections.getTypesAnnotatedWith( GACrossover.class )));
		gaCrossoverOperators.sort( Comparator.comparing( Class::toString ) );

		// collect permutation strategies
		gaMutationOperators.addAll( autoCast(reflections.getTypesAnnotatedWith( GAMutation.class )));
		gaMutationOperators.sort( Comparator.comparing( Class::toString ) );

		// collect permutation strategies
		gaSelectionOperators.addAll( autoCast(reflections.getTypesAnnotatedWith( GASelection.class )));
		gaSelectionOperators.sort( Comparator.comparing( Class::toString ) );

		// collect permutation strategies
		saNeighborhood.addAll( autoCast(reflections.getTypesAnnotatedWith( SANeighborhood.class )));
		saNeighborhood.sort( Comparator.comparing( Class::toString ) );
	}

	/**
	 * Get name of algorithms by class
	 * Check if Priority-, Constructive- or Iterative-Annotation is present
	 * if no one is present, return empty string
	 */
	public static String getNameOfAlgorithm(Class<?> clazz) {
		if( clazz.isAnnotationPresent( AlgorithmAnnotations.Constructive.class ) )
			return clazz.getAnnotation( AlgorithmAnnotations.Constructive.class ).name();
		else if( clazz.isAnnotationPresent( AlgorithmAnnotations.Iterative.class ) )
			return clazz.getAnnotation( AlgorithmAnnotations.Iterative.class ).name();
		else if( clazz.isAnnotationPresent( AlgorithmAnnotations.Priority.class ) )
			return clazz.getAnnotation( AlgorithmAnnotations.Priority.class ).name();
		else if( clazz.isAnnotationPresent( AlgorithmAnnotations.ObjectiveFunction.class ) )
			return clazz.getAnnotation( AlgorithmAnnotations.ObjectiveFunction.class ).name();
		else if( clazz.isAnnotationPresent( AlgorithmAnnotations.Neighborhood.class ) )
			return clazz.getAnnotation( AlgorithmAnnotations.Neighborhood.class ).name();
		else if( clazz.isAnnotationPresent( Move.class ) )
			return clazz.getAnnotation( Move.class ).name();
		else if( clazz.isAnnotationPresent( AlgorithmAnnotations.GACrossover.class ) )
			return clazz.getAnnotation( AlgorithmAnnotations.GACrossover.class ).name();
		else if( clazz.isAnnotationPresent( AlgorithmAnnotations.GAMutation.class ) )
			return clazz.getAnnotation( AlgorithmAnnotations.GAMutation.class ).name();
		else if( clazz.isAnnotationPresent( AlgorithmAnnotations.GASelection.class ) )
			return clazz.getAnnotation( AlgorithmAnnotations.GASelection.class ).name();
		else if( clazz.isAnnotationPresent( AlgorithmAnnotations.SANeighborhood.class ) )
			return clazz.getAnnotation( AlgorithmAnnotations.SANeighborhood.class ).name();
		else
			return "";
	}

	@SuppressWarnings("unchecked")
	private static <T> Collection<T> autoCast(Collection<Class<?>> objects) {
		Collection<T> result = new ArrayList<>();

		for (Class<?> object : objects) {
			try {
				result.add((T) object);
			}catch (ClassCastException e) {
				Log.error(e);
			}
		}

		return result;
	}

	/**
	 * Annotation for adding dynamically constructive algorithms
	 * Attribute name: Name of the algorithm
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface Constructive {
		String name();
		boolean singleUse() default false;
	}

	/**
	 * Annotation for adding dynamically iterative algorithms
	 * Attribute name: Name of the algorithm
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface Iterative {
		String name();
	}

	/**
	 * Annotation for adding dynamically priority rules
	 * Attribute name: Name of the priority rule
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface Priority {
		String name();
	}

	/**
	 * Annotation for adding dynamically objective functions
	 * Attribute name: Name of the objective function
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface ObjectiveFunction {
		String name();
	}

	/**
	 * Annotation for adding dynamically neighborhood functions
	 * Attribute name: Name of the neighborhood function
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface Neighborhood {
		String name();
	}

	/**
	 * Annotation for adding dynamically move strategies
	 * Attribute name: Name of the move strategy
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface Move {
		String name();
	}

	/**
	 * Annotation for adding dynamically genetic algorithm crossover operators
	 * Attribute name: Name of the genetic algorithm crossover operator
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface GACrossover {
		String name();
	}

	/**
	 * Annotation for adding dynamically genetic algorithm mutation operators
	 * Attribute name: Name of the genetic algorithm mutation operator
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface GAMutation {
		String name();
	}

	/**
	 * Annotation for adding dynamically genetic algorithm selection operators
	 * Attribute name: Name of the genetic algorithm selection operator
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface GASelection {
		String name();
	}

	/**
	 * Annotation for adding dynamically genetic algorithm selection operators
	 * Attribute name: Name of the genetic algorithm selection operator
	 */
	@Inherited
	@Target( ElementType.TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface SANeighborhood {
		String name();
	}
}
