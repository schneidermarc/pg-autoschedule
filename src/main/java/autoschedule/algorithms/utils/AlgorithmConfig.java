package autoschedule.algorithms.utils;

import lombok.Getter;

public class AlgorithmConfig {

    @Getter
    private final Object configData;

    public AlgorithmConfig(Object pConfigData){
        configData = pConfigData;
    }
}
