package autoschedule.algorithms.utils;

import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author cstockhoff
 */
public class TreeNode {
	/**
	 * Classes of Algorithms
	 * Used by CLS to look up and filter needed combinators
	 * Is (and must be) cleaned every time, CLS is started
	 *
	 * Is filled automatically by creating a new TreeNode (See Constructor of TreeNode)
	 */
	public static final Set<Class<?>> clazzes = new HashSet<>();

	/**
	 * Clazz of a Algorithm
	 */
	public final Class<?> clazz;

	/**
	 * List of Children-TreeNodes
	 *
	 * Filled automatically by calling the constructor
	 * If a parent-node is not null, the new TreeNode is added as a child
	 * to the children-list of the parent-node
	 */
	public final List<TreeNode> children;

	/**
	 * Parent-TreeNode
	 * If null, this TreeNode is a root-Node
	 */
	public final TreeNode parent;

	/**
	 * Corresponding model of the Algorithm/Node
	 * (must be set before cls-call)
	 */
	public final AlgorithmViewModel model;

	/**
	 * If parent is not null, this object is added to the children of the parent node
	 */
	public TreeNode( @NonNull Class<?> clazz, TreeNode parent ) {
		this( clazz, parent, null );
	}

	/**
	 * If parent is not null, this object is added to the children of the parent node
	 */
	public TreeNode( @NonNull Class<?> clazz, TreeNode parent, AlgorithmViewModel model ) {
		this.clazz = clazz;
		this.parent = parent;
		this.model = model;
		this.children = new ArrayList<>();

		clazzes.add( clazz );
	}

	public TreeNode registerChild(TreeNode child) {
		this.children.add(child);
		return child;
	}

	/**
	 * Clean up method
	 * Is called by CLS
	 */
	public static void cleanTree() {
		clazzes.clear();
	}
}
