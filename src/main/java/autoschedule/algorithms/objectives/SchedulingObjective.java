package autoschedule.algorithms.objectives;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Schedule;

public abstract class SchedulingObjective implements Comparable<SchedulingObjective> {

	/**
	 * @param schedule the schedule to calculate for
	 * @return the calculated objective
	 */
	public abstract double evaluate(Schedule schedule);

	/**
	 * @return the searched databaseColumn
	 */
	public abstract String getDatabaseColumnName();

	@Override
	public String toString() {
		return AlgorithmAnnotations.getNameOfAlgorithm(this.getClass());
	}


	public int compareTo(SchedulingObjective o) {
		return toString().compareTo(o.toString());
	}

	@Override
	public int hashCode() {
		return getClass().getSimpleName().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;

		return getClass().getSimpleName().equals(obj.getClass().getSimpleName());
	}
}
