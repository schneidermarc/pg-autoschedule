//autoschedule.algorithms.objectives.UBar
package autoschedule.algorithms.objectives;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Schedule;

@AlgorithmAnnotations.ObjectiveFunction( name = "UBar" )
public class UBar extends SchedulingObjective {

    /**
     * calculates UBar: Number of late jobs
     * @param schedule schedule to calculate UBar
     * @return UBar
     */
    @Override
    public double evaluate(Schedule schedule) {
        return schedule.getJobs().stream().filter(job -> job.getDeadline() < job.getArticles().get(job.getArticles().size()-1).getFinishTime()).count();
    }

    @Override
    public String getDatabaseColumnName() {
        return "Ubar";
    }

}
