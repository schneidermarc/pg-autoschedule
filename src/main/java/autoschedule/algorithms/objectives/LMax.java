//autoschedule.algorithms.objectives.LMax
package autoschedule.algorithms.objectives;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Schedule;

@AlgorithmAnnotations.ObjectiveFunction( name = "LMax" )
public class LMax extends SchedulingObjective {

    /**
     * calculates LMax: Maximum lateness
     * @param schedule schedule to calculate LMax
     * @return LMax
     */
    @Override
    public double evaluate(Schedule schedule) {
        return schedule.getJobs().stream().mapToInt(job -> Math.max(job.getArticles().get(job.getArticles().size()-1).getFinishTime() - job.getDeadline(), 0)).max().orElse(-1);
    }

    @Override
    public String getDatabaseColumnName() {
        return "Lmax";
    }

}
