//autoschedule.algorithms.objectives.CMax
package autoschedule.algorithms.objectives;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Schedule;


@AlgorithmAnnotations.ObjectiveFunction( name = "CMax" )
public class CMax extends SchedulingObjective {

    /**
     * calculates CMax: Maximum completion time
     * @param schedule schedule to calculate CMar
     * @return CMax
     */
    @Override
    public double evaluate(Schedule schedule) {
         return schedule.getJobs().stream().mapToInt(job -> job.getArticles().get(job.getArticles().size()-1).getFinishTime()).max().orElse(-1);
    }

    @Override
    public String getDatabaseColumnName() {
        return "Cmax";
    }
}
