//autoschedule.algorithms.objectives.FMax
package autoschedule.algorithms.objectives;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Schedule;

@AlgorithmAnnotations.ObjectiveFunction( name = "FMax" )
public class FMax extends SchedulingObjective {

    /**
     * calculates FMax: Maximum flow time
     * @param schedule schedule to calculate FMax
     * @return FMax
     */
    @Override
    public double evaluate(Schedule schedule) {
        return schedule.getJobs().stream().mapToInt(job -> job.getArticles().get(job.getArticles().size()-1).getFinishTime() - job.getArticles().get(0).getScheduledTime()).max().orElse(-1);
    }

    @Override
    public String getDatabaseColumnName() {
        return "Fmax";
    }

}
