package autoschedule.algorithms.objectives;

import autoschedule.model.Schedule;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class DummyObjective extends SchedulingObjective {

	@Getter @Setter
	private String name;

	@Override
	public double evaluate(Schedule schedule) {
		return 0;
	}

	@Override
	public String getDatabaseColumnName() {
		return name.isEmpty() ? "NONE" : name;
	}
}