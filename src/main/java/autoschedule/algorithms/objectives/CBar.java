//autoschedule.algorithms.objectives.CBar
package autoschedule.algorithms.objectives;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Schedule;


@AlgorithmAnnotations.ObjectiveFunction( name = "CBar" )
public class CBar extends SchedulingObjective {

    /**
     * calculates CBar: Total/Average completion time
     * @param schedule schedule to calculate CBar
     * @return CBar
     */
    @Override
    public double evaluate(Schedule schedule) {
        return schedule.getJobs().stream().mapToInt(job -> job.getArticles().get(job.getArticles().size()-1).getFinishTime()).average().orElse(-1);
    }

    @Override
    public String getDatabaseColumnName() {
        return "CBar";
    }

}
