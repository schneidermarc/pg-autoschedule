package autoschedule.algorithms.iterative;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;
import lombok.Getter;

public abstract class IterativeAlgorithm extends Algorithm {
	@Getter
	protected final Neighborhood neighborhood;

	protected IterativeAlgorithm(Neighborhood neighborhood) {
		this.neighborhood = neighborhood;
	}
}