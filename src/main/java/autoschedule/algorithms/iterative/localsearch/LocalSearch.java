package autoschedule.algorithms.iterative.localsearch;

import autoschedule.algorithms.iterative.IterativeAlgorithm;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import autoschedule.view.viewmodel.algorithmModels.LocalSearchViewModel;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

@AlgorithmAnnotations.Iterative( name = "Local Search" )
public class LocalSearch extends IterativeAlgorithm {

	@Setter
	private int maxIterations = 5000;

	@Setter
	protected double abortThreshold;

	public LocalSearch(Neighborhood neighborhood) {
		super(neighborhood);
	}

	@Override
	protected List<OPT_ObjValuesObject> execute(Schedule schedule) {
		List<OPT_ObjValuesObject> objectiveValuesList = new LinkedList<>();
		LSProblem problem = new LSProblem(schedule, objective);

		LSSolutionComparator comparator = new LSSolutionComparator(objective);
		comparator.setCachedObjectiveValues(objectiveValuesList);

		this.neighborhood.setSolutionComparator(comparator);
		this.neighborhood.setAbortThreshold(abortThreshold);
		LSSolution solution = this.neighborhood.run(problem, comparator, maxIterations);

		schedule.copyFrom(solution.getSchedule());
		return objectiveValuesList;
	}

	/**
	 * Gibt ein Objekt zurueck, dass Konfigurationsinformationen fuer diesen Algorithmus enthaelt.
	 * Das Objekt wird verwendet, um in der OPT_algorithm_config Tabelle die Konfigurationen von ausgefuerhten Algorithmen zu hinterlegen.
	 * Hat ein Algorithmus keine Einstellungen, die zur Nachvollziehbarkeit gespeichert werden müssen, wird null zurueckgegeben.
	 */
	@Override
	public AlgorithmConfig getAlgorithmConfig() {
		return null;
	}

	@Override
	public String getString() {
		return "LocalSearch " + neighborhood.getString();
	}

	@Override
	public String getSimpleString() {
		return "LS " + neighborhood.getSimpleString();
	}

	/**
	 * Sets all the parameters from the viewmodel before executing the algorithm
	 */
	@Override
	protected void prepareParameters(){
		setObjective( createInstanceByClass( model.getSelectedObjectiveFunction().getValue() ) );
		setMaxIterations( model.getBoundClassField( this.neighborhood.getClass() ) );
		setAbortThreshold(((LocalSearchViewModel) model).getAbortThreshold().getValue() );
	}
}
