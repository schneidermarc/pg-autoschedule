package autoschedule.algorithms.iterative.localsearch;

import autoschedule.algorithms.objectives.*;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

public class LSSolutionComparator implements Comparator<LSSolution> {

    @Getter
    private final SchedulingObjective objective;
    @Setter private List<OPT_ObjValuesObject> cachedObjectiveValues;

    LSSolutionComparator(SchedulingObjective objective) {
        this.objective = objective;
    }

    @Override
    public int compare(LSSolution o1, LSSolution o2){
        return compare(o1, o2, true);
    }

    public int compare(LSSolution o1, LSSolution o2, boolean cacheValue) {
        int compareValue = Double.compare(
                objective.evaluate(o1.getSchedule()),
                objective.evaluate(o2.getSchedule()));
        Schedule schedule;
        if(compareValue >= 0){  // o1 >= o2
            schedule = o2.getSchedule();
        }
        else{   // o1 < o2
            schedule = o1.getSchedule();
        }

        OPT_ObjValuesObject objectiveValues = new OPT_ObjValuesObject(
                -1,
                (int)Math.round(new CMax().evaluate(schedule)), //CMax
                (int)Math.round(new CBar().evaluate(schedule)), //Cbar
                (int)Math.round(new LMax().evaluate(schedule)), //Lmax
                (int)Math.round(new FMax().evaluate(schedule)), // Fmax
                (int)Math.round(new UBar().evaluate(schedule)) //Ubar
        );

        if (cacheValue) cachedObjectiveValues.add(objectiveValues);

        return compareValue;
    }


    public void addObjectiveValueFor(LSSolution lsSolution){
        OPT_ObjValuesObject objectiveValues = new OPT_ObjValuesObject(
                -1,
                (int)Math.round(new CMax().evaluate(lsSolution.getSchedule())), //CMax
                (int)Math.round(new CBar().evaluate(lsSolution.getSchedule())), //Cbar
                (int)Math.round(new LMax().evaluate(lsSolution.getSchedule())), //Lmax
                (int)Math.round(new FMax().evaluate(lsSolution.getSchedule())), // Fmax
                (int)Math.round(new UBar().evaluate(lsSolution.getSchedule())) //Ubar
        );

        cachedObjectiveValues.add(objectiveValues);
    }

    public double getObjectiveDifference(LSSolution oldSolution, LSSolution newSolution){
        double oldObjectiveValue = objective.evaluate(oldSolution.getSchedule());
        double newObjectiveValue = objective.evaluate(newSolution.getSchedule());
        return 1-(newObjectiveValue / oldObjectiveValue);
    }

    @Override
    public Comparator<LSSolution> reversed() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Comparator<LSSolution> thenComparing(Comparator<? super LSSolution> other) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <U> Comparator<LSSolution> thenComparing(Function<? super LSSolution, ? extends U> keyExtractor, Comparator<? super U> keyComparator) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <U extends Comparable<? super U>> Comparator<LSSolution> thenComparing(Function<? super LSSolution, ? extends U> keyExtractor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Comparator<LSSolution> thenComparingInt(ToIntFunction<? super LSSolution> keyExtractor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Comparator<LSSolution> thenComparingLong(ToLongFunction<? super LSSolution> keyExtractor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Comparator<LSSolution> thenComparingDouble(ToDoubleFunction<? super LSSolution> keyExtractor) {
        throw new UnsupportedOperationException();
    }
}
