package autoschedule.algorithms.iterative.localsearch;

import autoschedule.algorithms.iterative.IterativeAlgorithm;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import autoschedule.view.viewmodel.algorithmModels.TabuSearchViewModel;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

@AlgorithmAnnotations.Iterative( name = "Tabu Search" )
public class TabuSearch extends IterativeAlgorithm {

	@Setter
	private int maxIterations = 5000;

	@Setter
	private int tabuListSize = 5;

	public TabuSearch(Neighborhood neighborhood) {
		super(neighborhood);
	}

	@Override
	protected List<OPT_ObjValuesObject> execute( Schedule schedule ) {
		List<OPT_ObjValuesObject> objectiveValuesList = new LinkedList<>();
		LSProblem problem = new LSProblem(schedule, objective);

		LSSolutionComparator comparator = new LSSolutionComparator(objective);
		comparator.setCachedObjectiveValues(objectiveValuesList);

		TabuMemory<LSSolution> tabuMemory = new TabuMemory<>(tabuListSize);
		this.neighborhood.setSolutionComparator(comparator);
		// this.neighborhood.setAbortThreshold(abortThreshold);

		LSSolution solution = this.neighborhood.run(problem, comparator, maxIterations, tabuMemory);

		schedule.copyFrom(solution.getSchedule());
		return objectiveValuesList;
	}

	/**
	 * returns an object which holds information about the configurations for this algorithm.
	 * This object is used in order to save the algorithms' configurations in the OPT_algorithm table.
	 * If an algorithm has no configurations that have to be saved null will be returned
	 *
	 */
	@Override
	public AlgorithmConfig getAlgorithmConfig() {
		return null;
	}

	@Override
	public String getString() {
		return "TabuSearch " + neighborhood.getString();
	}

	@Override
	public String getSimpleString() {
		return "TS " + neighborhood.getSimpleString();
	}

	/**
	 * Sets all the parameters from the viewmodel before executing the algorithm
	 */
	@Override
	protected void prepareParameters(){
		TabuSearchViewModel tsViewModel = (TabuSearchViewModel) model;

		setObjective( createInstanceByClass( model.getSelectedObjectiveFunction().getValue() ) );
		setMaxIterations( model.getBoundClassField( this.neighborhood.getClass() ) );
		setTabuListSize( tsViewModel.getTabuListSize().getValue() );
	}
}
