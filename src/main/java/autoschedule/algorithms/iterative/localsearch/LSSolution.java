package autoschedule.algorithms.iterative.localsearch;


import autoschedule.model.Schedule;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

public class LSSolution {

    @Getter
    private final LSProblem lsProblem;
    @Getter
    private final Schedule schedule;
    @Getter @Setter
    private double objective;

    /**
     * Constructor
     */
    protected LSSolution(LSProblem lsProblem) {
        this.lsProblem = lsProblem;
        this.schedule = lsProblem.getSchedule().copy();
    }

    /**
     * Copy Constructor
     */
    private LSSolution(LSSolution solution) {
        this.lsProblem = solution.getLsProblem();
        this.schedule = solution.getSchedule().copy();
        this.objective = solution.getObjective();
    }

    public LSSolution copy() {
        return new LSSolution(this);
    }

    public boolean equals(Object o) {
        if (o instanceof LSSolution){
            LSSolution solution  = (LSSolution) o;
            return this.schedule.equals(solution.getSchedule());
        } else {
            return false;
        }
    }
}

