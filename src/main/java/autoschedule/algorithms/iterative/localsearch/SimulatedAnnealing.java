package autoschedule.algorithms.iterative.localsearch;

import autoschedule.algorithms.iterative.IterativeAlgorithm;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import autoschedule.view.viewmodel.algorithmModels.SimulatedAnnealingViewModel;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;


/**
 * Simulated Annealing is an extension to Local Search
 *
 * It is inspired by the process of annealing in metal work
 * Annealing involves heating and cooling a material in order to alter its physical properties
 * As the material cools down, its new structure becomes fixed (it retains its new properties)
 * This heating process is simulated by keeping a temperature variable
 *
 * It starts high and decreases over time. As long as the temperature is high the algorithm tends to accept solution which are worse than the current one
 * in order to escape from local minimums.
 * As the temperature is reduced so is the chance of accepting worse solutions,
 * therefore allowing the algorithm to gradually focus in on a area of the search space in which hopefully,
 * a close to optimum solution can be found.
 */
@AlgorithmAnnotations.Iterative(name = "Simulated Annealing")
public class SimulatedAnnealing extends IterativeAlgorithm {

    /*
        Amount of iterations before the algorithm terminates.
        Will also terminate if the temperature is fully cooled down
     */
    @Setter
    private int iterations = 1000;
    /**
     * Start-Temperature
     */
    @Setter
    private int temperature = 200;

    /**
     * @param neighborhood - Will be a SingleMoveNeighborhood
     */
    public SimulatedAnnealing(Neighborhood neighborhood){
        super(neighborhood);
    }


    @Override
    protected List<OPT_ObjValuesObject> execute(Schedule schedule) {
        //Starting temerature
        double temperature = this.temperature;

        //Amount of iterations
        double iterations = this.iterations;

        // Cooling rate for the temperature -  Decreases the temperature in every iteration
        double coolingRate = Math.pow( (Math.log(0.99)/Math.log(0.01)) , (1.0/ (iterations-1.0)) );

        //Initializes neigborshood and all other necessary structures
        List<OPT_ObjValuesObject> objectiveValuesList = new LinkedList<>();
        LSSolutionComparator comparator = new LSSolutionComparator(objective);
        comparator.setCachedObjectiveValues(objectiveValuesList);
        this.neighborhood.setSolutionComparator(comparator);

        // Initialize Objective Values and set a neutral Solution as the current best solution
        LSSolution currentBest = new LSSolution( new LSProblem(schedule, this.objective));

        // Main Loop of the algorithm -  Terminates when the temperature is cooled down enough
        while(iterations-- > 0) {
            LSSolution newSolution = neighborhood.execute(currentBest.copy(), new TabuMemory<>(0));
            comparator.addObjectiveValueFor(newSolution);
            /*
                ((C_current - C_other)/temperature) > random (0,1)
                Decides whether to take a new Solution as the current best one or not
             */
            if(acceptanceProbability(this.objective.evaluate(currentBest.getSchedule()), this.objective.evaluate(newSolution.getSchedule()), temperature) > Math.random()){
                currentBest = newSolution;
            }
            //Decrease the temperature in each iteration by the cooling Rate
            temperature *= coolingRate;
        }

        // Copy schedule from best solution and return the objecive values
        schedule.copyFrom(currentBest.getSchedule());
        return objectiveValuesList;
    }

    /**
     * Calculate the acceptanceProbability
     * If the current solution is better than the other solution then the probability is 1
     * If the current solution is worse than the other solution return a value specified by a formula
     * @param energy - objective Value of current solution
     * @param newEnergy - objective Value of other solution
     * @param temperature - current temperature
     * @return probability for taking a new solution as the current best solution
     */
    private double acceptanceProbability(double energy, double newEnergy, double temperature){
        if(newEnergy<energy){
            return 1;
        } else {
            return Math.exp((energy-newEnergy)/temperature);
        }
    }

    @Override
    public String getString() {
        return "SimulatedAnnealing " + neighborhood.getString();
    }

    @Override
    public String getSimpleString() {
        return "SA " + neighborhood.getSimpleString();
    }

    @Override
    public AlgorithmConfig getAlgorithmConfig() {
        return null;
    }

    @Override
    protected void prepareParameters(){
        setObjective( createInstanceByClass( model.getSelectedObjectiveFunction().getValue() ) );
        setIterations( model.getBoundClassField( this.neighborhood.getClass() ) );
        setTemperature(((SimulatedAnnealingViewModel) model).getTemperature().getValue());
    }

}
