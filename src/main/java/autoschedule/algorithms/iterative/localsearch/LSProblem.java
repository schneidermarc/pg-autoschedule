package autoschedule.algorithms.iterative.localsearch;

import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.model.Schedule;
import lombok.Getter;

public class LSProblem {

    @Getter
    private final Schedule schedule;
    private final SchedulingObjective objective;

    public LSProblem(Schedule schedule, SchedulingObjective objective) {
        this.schedule = schedule;
        this.objective = objective;
    }

    public LSSolution createSolution() {
        return new LSSolution(this);
    }

    public void evaluate(LSSolution solution) {
        solution.setObjective(objective.evaluate(solution.getSchedule()));
    }
}
