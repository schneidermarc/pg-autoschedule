package autoschedule.algorithms.iterative.localsearch.neighborhoods;

import autoschedule.algorithms.iterative.localsearch.LSSolution;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Article;
import autoschedule.model.Machine;
import autoschedule.model.Schedule;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Shifts an article to another qualified machine at a random position
 */
@AlgorithmAnnotations.Move( name = "Shift" )
@AllArgsConstructor
@NoArgsConstructor
public class Shift extends Move {

    private Article article;
    private Machine machine;
    private int position;

    /**
     * Take one article and place it at another index, shift all affected articles
     * @param solution used solution
     */
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @Override
    LSSolution doMove(LSSolution solution) {
        final Schedule schedule = solution.getSchedule();

        final int firstArticleId = article.getId();
        final Article firstArticle = schedule.getAllArticles().stream().filter(a->a.getId() == firstArticleId).findFirst().get();

        final Machine oldMachine = firstArticle.getScheduledMachine();
        final Machine newMachine = schedule.getAllMachines().stream().filter(m -> m.getName().equals(machine.getName())).findFirst().get();//firstArticle.getQualifiedMachines().get(random.nextInt(firstArticle.getQualifiedMachines().size())); // Find the real cloned machine

        final List<Article> articlesOnOldMachine = schedule.getAllArticlesOnStage(1).stream().filter(article -> article.getScheduledMachine().equals(oldMachine)).sorted(Comparator.comparingInt(Article::getFinishTime)).collect(Collectors.toList());
        final List<Article> articlesOnNewMachine = schedule.getAllArticlesOnStage(1).stream().filter(article -> article.getScheduledMachine().equals(newMachine)).sorted(Comparator.comparingInt(Article::getFinishTime)).collect(Collectors.toList());

        articlesOnOldMachine.removeIf(a -> a.getId() == firstArticleId);
        articlesOnNewMachine.add(Math.min(position,articlesOnNewMachine.size()),firstArticle);
        firstArticle.scheduleMachine(newMachine,0); // Updates processingTime based on machine qualification times

        if(oldMachine.equals(newMachine)) {
            articlesOnOldMachine.add(Math.min(position,articlesOnOldMachine.size()),firstArticle);
        }

        if(articlesOnOldMachine.size() > 0){
            //get first article on old machine in chain e.g. can start at t=0
            Article lastArticle = articlesOnOldMachine.get(0);

            //t = max {machine arrived, article arrived}
            lastArticle.setScheduledTime(Math.max(oldMachine.getArrival(), lastArticle.getArrival()));

            //for each remaining article in  correct scheduling order
            for(int i = 1; i < articlesOnOldMachine.size(); i++){
                Article currentArticle = articlesOnOldMachine.get(i);

                //t = max {endTime last article, article arrived}
                currentArticle.setScheduledTime(Math.max(lastArticle.getFinishTime(), currentArticle.getArrival()));
                lastArticle = currentArticle;
            }
        }

        if(articlesOnNewMachine.size() > 0 && !oldMachine.equals(newMachine)){
            //get first article on new machine in chain e.g. can start at t=0
            Article lastArticle = articlesOnNewMachine.get(0);

            //t = max {machine arrived, article arrived}
            lastArticle.setScheduledTime(Math.max(newMachine.getArrival(), lastArticle.getArrival()));

            //for each remaining article in  correct scheduling order
            for(int i = 1; i < articlesOnNewMachine.size(); i++){
                Article currentArticle = articlesOnNewMachine.get(i);

                //t = max {endTime last article, article arrived}
                currentArticle.setScheduledTime(Math.max(lastArticle.getFinishTime(), currentArticle.getArrival()));
                lastArticle = currentArticle;
            }
        }

        //schedule higher stages
        scheduleHigherStages(schedule);
        return solution;
    }

    @Override
    List<Move> getAllNeighbors(LSSolution solution) {
        List<Article> articles = solution.getSchedule().getAllArticlesOnStage(1);
        List<Move> neighbors = new ArrayList<>();

        for(Article article: articles){
            for(Machine machine: article.getQualifiedMachines()){
                IntStream.rangeClosed(0, (int) (articles.stream().filter(a -> a.getScheduledMachine().equals(machine)).count())).forEach(position -> neighbors.add(new Shift(article,machine,position)));
            }
        }
        return neighbors;
    }

    @Override
    List<Move> getRandomNeighbor(LSSolution solution) {
        List<Article> articles = solution.getSchedule().getAllArticles();
        Article article = articles.get(random.nextInt(articles.size()));

        List<Machine> qualifiedMachines = article.getQualifiedMachines();
        Machine machine = qualifiedMachines.get(random.nextInt(qualifiedMachines.size()));

        int maxPos = (int) articles.stream().filter(a -> a.getScheduledMachine().equals(machine)).count();
        int position = maxPos > 0 ? random.nextInt(maxPos) : 0;

        return Collections.singletonList(new Shift(article,machine,position));
    }

    @Override
    public String toString() {
        return "Shift";
    }
}






