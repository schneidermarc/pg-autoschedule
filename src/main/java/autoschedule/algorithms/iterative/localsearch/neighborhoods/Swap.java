package autoschedule.algorithms.iterative.localsearch.neighborhoods;

import autoschedule.algorithms.iterative.localsearch.LSSolution;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Article;
import autoschedule.model.Machine;
import autoschedule.model.Schedule;
import autoschedule.utils.MathUtils;
import autoschedule.utils.logging.Logger;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Swaps two articles on the same machine or different machines
 * and shifts all jobs in between to match machine start times.
 */
@AlgorithmAnnotations.Move( name = "Swap" )
@AllArgsConstructor
@NoArgsConstructor
public class Swap extends Move {

    private Article firstArticleCopy;
    private Article secondArticleCopy;

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    LSSolution doMove(LSSolution solution) {
        if(!isValid()){
            Logger.error(new IllegalStateException("Illegal Swap"));
            return solution;
        }

        final int firstArticleId = firstArticleCopy.getId();
        final int secondArticleId = secondArticleCopy.getId();


        Article firstArticle = solution.getSchedule().getAllArticles().stream().filter(a->a.getId() == firstArticleId).findFirst().get();
        Article secondArticle = solution.getSchedule().getAllArticles().stream().filter(a->a.getId() == secondArticleId).findFirst().get();

        Schedule schedule = solution.getSchedule();

        final Machine firstArticleMachine = firstArticle.getScheduledMachine();
        final Machine secondArticleMachine = secondArticle.getScheduledMachine();

        final List<Article> articlesOnFirstMachine = schedule.getAllArticlesOnStage(1).stream().filter(article -> article.getScheduledMachine().equals(firstArticleMachine)).sorted(Comparator.comparingInt(Article::getFinishTime)).collect(Collectors.toList());
        final List<Article> articlesOnSecondMachine = schedule.getAllArticlesOnStage(1).stream().filter(article -> article.getScheduledMachine().equals(secondArticleMachine)).sorted(Comparator.comparingInt(Article::getFinishTime)).collect(Collectors.toList());

        final int firstArticlePosition = articlesOnFirstMachine.indexOf(firstArticle);
//                IntStream.range(0,articlesOnFirstMachine.size()).filter(i -> articlesOnFirstMachine.get(i).getId() == firstArticleId).findFirst().orElse(-1);
        final int secondArticlePosition = articlesOnSecondMachine.indexOf(secondArticle);
//                IntStream.range(0,articlesOnSecondMachine.size()).filter(i -> articlesOnSecondMachine.get(i).getId() == secondArticleId).findFirst().orElse(-1);


        firstArticle.scheduleMachine(secondArticleMachine, 0);
        secondArticle.scheduleMachine(firstArticleMachine, 0);

        articlesOnFirstMachine.removeIf(a -> a.getId() == firstArticleId || a.getId() == secondArticleId);
        articlesOnSecondMachine.removeIf(a -> a.getId() == firstArticleId || a.getId() == secondArticleId) ;

        articlesOnFirstMachine.add(Math.min(firstArticlePosition, articlesOnFirstMachine.size()),secondArticle);
        if(firstArticleMachine.equals(secondArticleMachine)){
            articlesOnFirstMachine.add(Math.min(secondArticlePosition, articlesOnFirstMachine.size()),firstArticle);
        }

        Article lastArticle = articlesOnFirstMachine.get(0);
        lastArticle.setScheduledTime(Math.max(firstArticleMachine.getArrival(), lastArticle.getArrival()));
        for(int i = 1; i < articlesOnFirstMachine.size(); i++){
            Article currentArticle = articlesOnFirstMachine.get(i);
            currentArticle.setScheduledTime(MathUtils.max(lastArticle.getFinishTime(),firstArticleMachine.getArrival(),currentArticle.getArrival()));
            lastArticle = currentArticle;
        }

        if(!firstArticleMachine.equals(secondArticleMachine)){
            articlesOnSecondMachine.add(Math.min(secondArticlePosition, articlesOnSecondMachine.size()),firstArticle);

            lastArticle = articlesOnSecondMachine.get(0);
            lastArticle.setScheduledTime(Math.max(secondArticleMachine.getArrival(), lastArticle.getArrival()));
            for(int i = 1; i < articlesOnSecondMachine.size(); i++){
                Article currentArticle = articlesOnSecondMachine.get(i);
                currentArticle.setScheduledTime(MathUtils.max(lastArticle.getFinishTime(), secondArticleMachine.getArrival(), currentArticle.getArrival()));
                lastArticle = currentArticle;
            }
        }

        scheduleHigherStages(schedule);
        return solution;
    }

    private boolean isValid() {
        return firstArticleCopy != secondArticleCopy && firstArticleCopy.getQualifiedMachines().contains(secondArticleCopy.getScheduledMachine()) && secondArticleCopy.getQualifiedMachines().contains(firstArticleCopy.getScheduledMachine());
    }

    @Override
    List<Move> getAllNeighbors(LSSolution solution) {
        List<Article> articles = solution.getSchedule().getAllArticlesOnStage(1);
        List<Move> neighbors = new ArrayList<>();
        for (int i = 0; i < articles.size()-1; i++) {
            for (int j = i+1; j < articles.size(); j++) {
                Swap swap = new Swap(articles.get(i), articles.get(j));
                if(swap.isValid())
                    neighbors.add(swap);
            }
        }
        return neighbors;
    }

    @Override
    List<Move> getRandomNeighbor(LSSolution solution) {
        List<Article> articles = solution.getSchedule().getAllArticlesOnStage(1);
        Swap swap;
        do {
            swap = new Swap(articles.get(random.nextInt(articles.size())),articles.get(random.nextInt(articles.size())));
        } while(swap.firstArticleCopy == swap.secondArticleCopy || !swap.isValid());

        return Collections.singletonList(swap);
    }

    @Override
    public String toString() {
        return "Swap";
    }
}
