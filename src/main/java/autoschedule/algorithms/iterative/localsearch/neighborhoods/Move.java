package autoschedule.algorithms.iterative.localsearch.neighborhoods;

import autoschedule.algorithms.constructive.ECT;
import autoschedule.algorithms.iterative.localsearch.LSSolution;
import autoschedule.model.Article;
import autoschedule.model.Schedule;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public abstract class Move {

    protected final Random random = new Random();

    abstract LSSolution doMove(LSSolution solution);

    /**
     * Resets scheduling for stages 2+ and reschedules them with ECT
     * @param schedule schedule to be re-scheduled
     */
    protected void scheduleHigherStages(Schedule schedule) {
        List<Article> allArticlesOnHigherStage = new LinkedList<>();
        schedule.getJobs().forEach(job -> allArticlesOnHigherStage.addAll(job.getArticles()));
        allArticlesOnHigherStage.removeAll(schedule.getAllArticlesOnStage(1));
        allArticlesOnHigherStage.forEach(Article::clearScheduling);

        new ECT().run(schedule);
    }

    abstract List<Move> getAllNeighbors(LSSolution solution);

    abstract List<Move> getRandomNeighbor(LSSolution solution);

}
