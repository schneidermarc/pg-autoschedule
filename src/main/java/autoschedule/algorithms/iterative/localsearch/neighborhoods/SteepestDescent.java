package autoschedule.algorithms.iterative.localsearch.neighborhoods;

import autoschedule.algorithms.iterative.localsearch.LSSolution;
import autoschedule.algorithms.utils.AlgorithmAnnotations;

import java.util.List;

@AlgorithmAnnotations.Neighborhood( name = "SteepestDescent" )
public class SteepestDescent extends Neighborhood {

    public SteepestDescent(Move move) {
        super(move);
    }

    @Override
    protected List<Move> create(LSSolution solution) {
        return move.getAllNeighbors(solution);
    }

    @Override
    protected boolean checkThreshold(double improvement) {
        return improvement < abortThreshold;
    }

    @Override
    public String getString() {
        return "Steepest Descent " + move.toString();
    }

    @Override
    public String getSimpleString() {
        return "SD " + move.toString();
    }
}
