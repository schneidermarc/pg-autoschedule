package autoschedule.algorithms.iterative.localsearch.neighborhoods;

import autoschedule.algorithms.iterative.localsearch.LSSolution;
import autoschedule.algorithms.utils.AlgorithmAnnotations;

import java.util.List;

@AlgorithmAnnotations.Neighborhood( name = "RandomDescent" )
public class RandomDescent extends Neighborhood {

    public RandomDescent(Move move) {
        super(move);
    }

    @Override
    protected List<Move> create(LSSolution solution) {
        return move.getRandomNeighbor(solution);
    }

    @Override
    protected boolean checkThreshold(double improvement) {
        return true;
    }

    @Override
    public String getString() {
        return "Random Descent " + move.toString();
    }

    @Override
    public String getSimpleString() {
        return "RD " + move.toString();
    }
}
