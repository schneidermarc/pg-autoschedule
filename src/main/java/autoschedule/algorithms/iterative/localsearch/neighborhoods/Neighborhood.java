package autoschedule.algorithms.iterative.localsearch.neighborhoods;

import autoschedule.algorithms.iterative.localsearch.LSProblem;
import autoschedule.algorithms.iterative.localsearch.LSSolution;
import autoschedule.algorithms.iterative.localsearch.LSSolutionComparator;
import autoschedule.algorithms.iterative.localsearch.TabuMemory;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

public abstract class Neighborhood {
    @Getter
    protected final Move move;

    @Setter
    private LSSolutionComparator solutionComparator;

    @Setter
    protected double abortThreshold;

    public Neighborhood(Move move) {
        this.move = move;
    }

    public LSSolution execute(LSSolution solution, TabuMemory<LSSolution> tabuMemory){
        List<Move> neighborhood = this.create(solution).stream().filter(move -> !tabuMemory.contains(move.doMove(solution.copy()))).collect(Collectors.toList());

        if(neighborhood.isEmpty()){
            return solution;
        }

        LSSolution currentBest = solution.copy();
        neighborhood.get(0).doMove(currentBest);
        neighborhood.remove(0);

        for(Move move: neighborhood){
            LSSolution clone = solution.copy();
            move.doMove(clone);

            if(solutionComparator.compare(currentBest, clone, false) > 0){
                currentBest = clone;
            }

        }
        //solutionComparator.addObjectiveValueFor(currentBest);

        if(tabuMemory.getTabuListSize() > 0)
            tabuMemory.add(currentBest);

        //return best
        return currentBest;
    }

    public LSSolution run(LSProblem problem, LSSolutionComparator comparator, int maxIterations ){
        return run(problem,comparator,maxIterations,new TabuMemory<>(0));
    }

    public LSSolution run(LSProblem problem, LSSolutionComparator comparator, int maxIterations, TabuMemory<LSSolution> tabuMemory){
        LSSolution oldSolution = problem.createSolution().copy();
        LSSolution currentBest = problem.createSolution().copy();
        problem.evaluate(oldSolution);
        int best;

        int i = 0;
        boolean threshold = true;
        while (i++ < maxIterations && threshold ) {
            LSSolution mutatedSolution = this.execute(currentBest.copy(),tabuMemory);
            problem.evaluate(mutatedSolution);
            best = comparator.compare(currentBest, mutatedSolution);

            if (best < 0) {
                continue;
            }

            if (best > 0) {
                currentBest = mutatedSolution;
            } else {
                if (Math.random() < 0.5) {
                    currentBest = mutatedSolution;
                }
            }

            threshold = checkThreshold(comparator.getObjectiveDifference(oldSolution, currentBest));
            oldSolution = currentBest;
        }
        return currentBest.copy();
    }

    protected abstract boolean checkThreshold(double improvement);

    protected abstract List<Move> create(LSSolution solution);

    public abstract String getString();

    public abstract String getSimpleString();
}
