package autoschedule.algorithms.iterative.localsearch;

import lombok.Getter;

import java.util.LinkedList;

public class TabuMemory<S> {

    private final LinkedList<S> tabuList = new LinkedList<>();

    @Getter
    private final int tabuListSize;

    public TabuMemory(int tabuListSize) {
        this.tabuListSize = tabuListSize;
    }

    /**
     * adds the solution to the tabu-list. Edits the tabu-list depending on what solutions gets added (no duplicates)
     * @param solution the solution to be added
     */
    public void add(S solution) {
        if(tabuList.stream().anyMatch( s -> s.equals(solution))){
            tabuList.remove(solution);
        }

        tabuList.add(solution);

        if(tabuList.size() > tabuListSize){
            tabuList.removeFirst();
        }
    }
    public boolean contains(S solution){
        return tabuList.stream().anyMatch(s -> s.equals(solution));
    }

}
