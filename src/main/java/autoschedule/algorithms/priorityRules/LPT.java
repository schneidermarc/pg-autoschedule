package autoschedule.algorithms.priorityRules;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Article;
import autoschedule.model.Job;

import java.util.*;

@AlgorithmAnnotations.Priority( name = "LPT" )
public class LPT extends PriorityRule {

    /**
     * LPT: longest processing time first
     * ranks jobs based on processing time
     * @param jobs a list of jobs to rearrange
     * @return the new ordered Job-List
     */
    @Override
    public List<Job> rank(Collection<Job> jobs) {
        ArrayList<Job> result = new ArrayList<>(jobs);
        result.sort(Comparator.comparingDouble(job -> job.getArticles().stream().mapToDouble(Article::getProcessingTimeMedian).sum()));
        Collections.reverse(result);
        return result;
    }
}
