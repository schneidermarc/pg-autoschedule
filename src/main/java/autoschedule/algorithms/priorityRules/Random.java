package autoschedule.algorithms.priorityRules;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@AlgorithmAnnotations.Priority( name = "Random" )
public class Random extends PriorityRule {

    /**
     * Random
     * ranks jobs in random order
     * @param jobs  a list of jobs to rearrange
     * @return the new ordered Job-List
     */
    @Override
    public List<Job> rank( Collection<Job> jobs) {
        ArrayList<Job> result = new ArrayList<>(jobs);
        Collections.shuffle(result);
        return result;
    }
}
