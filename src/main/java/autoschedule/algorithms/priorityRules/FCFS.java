package autoschedule.algorithms.priorityRules;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@AlgorithmAnnotations.Priority( name = "FCFS" )
public class FCFS extends PriorityRule {

    /**
     * FCFS: first come first served
     * ranks jobs based on arrival time
     * @param jobs a list of jobs to rearrange
     * @return the new ordered Job-List
     */
    @Override
    public List<Job> rank(Collection<Job> jobs) {
        ArrayList<Job> result = new ArrayList<>(jobs);
        result.sort(Comparator.comparingDouble(Job::getArrival));
        return result;
    }
}
