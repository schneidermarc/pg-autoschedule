package autoschedule.algorithms.priorityRules;

import autoschedule.model.Job;

import java.util.Collection;
import java.util.List;

public abstract class PriorityRule {
    public abstract List<Job> rank(Collection<Job> jobs);

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}