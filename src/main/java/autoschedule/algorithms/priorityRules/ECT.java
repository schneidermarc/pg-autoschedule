package autoschedule.algorithms.priorityRules;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Article;
import autoschedule.model.Job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@AlgorithmAnnotations.Priority( name = "ECT" )
public class ECT extends PriorityRule {

    /**
     * ECT: earliest completion time first
     * ranks jobs based on processing time + arrival
     * @param jobs a list of jobs to rearrange
     * @return the new ordered Job-List
     */
    @Override
    public List<Job> rank(Collection<Job> jobs) {
        ArrayList<Job> result = new ArrayList<>(jobs);
        result.sort(Comparator.comparingDouble(job -> job.getArrival() + job.getArticles().stream().mapToDouble(Article::getProcessingTimeMedian).sum()));
        return result;
    }
}
