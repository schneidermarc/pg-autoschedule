package autoschedule.algorithms.priorityRules;

import autoschedule.algorithms.utils.AlgorithmAnnotations;
import autoschedule.model.Job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@AlgorithmAnnotations.Priority( name = "EDD" )
public class EDD extends PriorityRule {

    /**
     * EDD: earliest due date first
     * ranks jobs based on deadlines
     * @param jobs a list of jobs to rearrange
     * @return the new ordered Job-List
     */
    @Override
    public List<Job> rank(Collection<Job> jobs) {
        ArrayList<Job> result = new ArrayList<>(jobs);
        result.sort(Comparator.comparingDouble(Job::getDeadline));
        return result;
    }
}
