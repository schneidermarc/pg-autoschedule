package autoschedule.control;

import autoschedule.Scheduler;
import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.control.database.TableRowResultsOptimization;
import autoschedule.model.MachineEnvironment;
import autoschedule.model.Schedule;
import autoschedule.utils.logging.Logger;
import autoschedule.utils.threading.DaemonThread;
import autoschedule.view.viewmodel.ViewModel;
import javafx.application.Platform;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.*;

@RequiredArgsConstructor
public class OptimizationRunner {
	@Getter
	private final SchedulingController schedulingController;
	private final ViewModel viewModel;

	/**
	 * Starts optimization without max schedules count
	 */
	public void startOptimization() {
		this.startOptimization(Integer.MAX_VALUE);
	}

	/**
	 * Starts optimization
	 *
	 * @param maxSchedulesToShow maximum schedules to show in table view
	 */
	public void startOptimization(int maxSchedulesToShow) {
		this.viewModel.getOptimizationRunningProperty().set(true);
		this.viewModel.getOptimizationProgressProperty().set(-1.0);
		this.viewModel.getOptimizationFinishedSchedules().removeAll();
		final List<Schedule> schedulesToRun = new ArrayList<>(viewModel.getSelectedSolutionsList());
		Thread thread = new DaemonThread(() -> {
			int index = 1;
			int amountOfSchedules = schedulesToRun.size();
			while (viewModel.getOptimizationRunningProperty().get() && schedulesToRun.size() > 0) {
				final double progress = (index) / (double)amountOfSchedules;
				final Schedule schedule = schedulesToRun.remove(0);
				index++;
				schedule.start();

				Platform.runLater(() -> {
					this.viewModel.getOptimizationProgressProperty().set(progress);

					this.viewModel.getSelectedSolutionsList().remove(schedule);

					this.viewModel.getOptimizationFinishedSchedules().add(new TableRowResultsOptimization(
							schedule.getRunId(),
							schedule.getAlgorithmHistoryStringWithIterations(),
							schedule.getTotalRuntime(),
							schedule.getObjectiveString(),
							new LinkedList<>(schedule.getEvaluatedObjectives().values()).getFirst(),
							schedule.getEvaluatedObjectives()));

					this.viewModel.getOptimizationFinishedSchedules().sort(Comparator.comparingDouble(TableRowResultsOptimization::getObjValueToCompare));

					if (this.viewModel.getOptimizationFinishedSchedules().size() > maxSchedulesToShow) {
						this.viewModel.getOptimizationFinishedSchedules().remove(maxSchedulesToShow);
					}

					Logger.message(schedule.getAlgorithmString() + " finished.");
				});
			}
			Platform.runLater(() -> this.viewModel.getOptimizationRunningProperty().set(false));
		});
		thread.start();
	}

	/**
	 * Runs CLS and generates schedule for each entry (cloned from template-schedule)
	 */
	public List<Schedule> generateSchedulesFromCLS( MachineEnvironment machineEnvironment, int iterCount, boolean iterCountLowerOrEqual, List<TreeNode> algorithms ) {
		//CLS call
		Map<String, List<Algorithm>> result = Scheduler.run(machineEnvironment, iterCount, iterCountLowerOrEqual, algorithms, viewModel.getClsResultProgress());

		List<Schedule> schedules = new ArrayList<>();
		for (Map.Entry<String, List<Algorithm>> algorithm : result.entrySet()) {
			Schedule newSchedule = schedulingController.getTemplateSchedule().copy();
			newSchedule.setAlgorithmsToRun(new ArrayList<>(algorithm.getValue()));
			newSchedule.generateAlgorithmStrings();
			// add schedule to schedules
			schedules.add(newSchedule);
		}

		return schedules;
	}
}
