package autoschedule.control.database;

import java.sql.Types;


public class NullValue {

    private int sqlType;

    public NullValue(Class<?> pClass){
        if(pClass.equals(String.class)){
            sqlType = Types.VARCHAR;
        }
        else if(pClass.equals(Integer.class)){
            sqlType = Types.INTEGER;
        }
        else if(pClass.equals(Double.class)){
            sqlType = Types.DOUBLE;
        }
        else if(pClass.equals(java.util.Date.class)){
            sqlType = Types.DATE;
        }
        else if(pClass.equals(java.sql.Date.class)){
            sqlType = Types.DATE;
        }
        else if(pClass.equals(Boolean.class)){
            sqlType = Types.BOOLEAN;
        }
        else if(pClass.equals(Long.class)){
            sqlType = Types.BIGINT;
        }

    }

    public int getSQLType(){
        return sqlType;
    }

}
