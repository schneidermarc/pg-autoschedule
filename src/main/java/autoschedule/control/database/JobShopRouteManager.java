package autoschedule.control.database;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

public class JobShopRouteManager extends RouteManager{

    private final Map<Map.Entry<Integer,String>,String> routes;

    public JobShopRouteManager(){
        routes = new HashMap<>();
    }

    public void addRoute(String article, String preArticle, int jobID){
        Map.Entry<Integer,String> key = new AbstractMap.SimpleEntry<>(jobID,article);
        routes.put(key,preArticle);
    }

    @Override
    public String getPreArticle(String article, int jobID) {
        Map.Entry<Integer,String> key = new AbstractMap.SimpleEntry<>(jobID,article);
        return routes.get(key);
    }
}
