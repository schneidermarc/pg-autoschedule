package autoschedule.control.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class OPT_ObjValuesObject {
    //region attributes
    @Getter @Setter private double runId = -1;
    @Getter @Setter private double cMax = -1;
    @Getter @Setter private double cBar = -1;
    @Getter @Setter private double lMax = -1;
    @Getter @Setter private double fMax = -1;
    @Getter @Setter private double uBar = -1;
    //endregion

    @Override
    public String toString() {
        return "OPT_ObjValuesObject{" +
                "runId=" + runId +
                ", CMax=" + cMax +
                ", CBar=" + cBar +
                ", LMax=" + lMax +
                ", FMax=" + fMax +
                ", UBar=" + uBar +
                '}';
    }
}
