package autoschedule.control.database;

import autoschedule.control.DatabaseConnector;

import javax.sql.rowset.CachedRowSet;
import java.util.concurrent.locks.ReentrantLock;

public class RunIdGenerator {

    private static int nextRunID = -1;
    private static final ReentrantLock runIdLock = new ReentrantLock();

    /**
     * Gibt die naechste zu vergebende RunId zur Ergebnisspeicherung zurueck.
     */
    public static int getNextRunID(){
        runIdLock.lock();
        try{
            if(nextRunID <= -1){
                nextRunID = getNextRunIdFromDatabase();
            }
            return nextRunID++;
        }
        finally{
            runIdLock.unlock();
        }
    }

    /**
     *  Laedt die nachste zu vergebende RunId aus der Datenbank.
     */
    private static int getNextRunIdFromDatabase(){
        DatabaseConnector dbConnector = dbConnectorFactory.createDatabaseConnector();
        String query = "SELECT MAX(run_id) AS id FROM(SELECT run_id FROM OPT_results UNION SELECT run_id FROM OPT_algorithm_data UNION SELECT run_id FROM OPT_constraints UNION SELECT run_id FROM OPT_obj_values UNION SELECT run_id FROM OPT_schedule)";
        CachedRowSet crs = dbConnector.readFromDatabase(query,dbConnector.getDefaultConnectionResultDB());
        try {
            if (crs.next()) {
                return crs.getInt("id")+1;
            }
            //Bisher keine Ergebnisse eingetragen
            return 1;
        }
        catch(Exception e){
            e.printStackTrace();
            return -2;
        }
//        finally {
//            dbConnector.closeConnection();
//        }
    }

}
