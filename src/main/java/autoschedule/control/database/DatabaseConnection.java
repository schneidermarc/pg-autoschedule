package autoschedule.control.database;

import lombok.Getter;

import java.io.File;
import java.sql.Connection;

public class DatabaseConnection {

    @Getter
    private final Connection connection;
    @Getter
    private final File databaseFile;

    public DatabaseConnection(Connection pConnection, File pDatabaseFile){
        connection = pConnection;
        databaseFile = pDatabaseFile;
    }

}
