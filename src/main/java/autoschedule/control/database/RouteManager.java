package autoschedule.control.database;

import java.util.ArrayList;
import java.util.List;

public abstract class RouteManager {
    public abstract String getPreArticle(String article, int jobID);

    public List<String> getRoute(String article, int jobID){
        List<String> route = new ArrayList<>();
        route.add(article);
        String preArticle = getPreArticle(article,jobID);
        while(preArticle != null){
            route.add(0,preArticle);
            article = preArticle;
            preArticle = getPreArticle(article,jobID);
        }
        return route;
    }
}
