package autoschedule.control.database;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ObjectiveValueWrapper {
    @Getter private final String objective;
    @Getter private final int value;
}
