package autoschedule.control.database;

import java.util.HashMap;
import java.util.Map;

public class DefaultRouteManager extends RouteManager{

    private final Map<String,String> routes;

    public DefaultRouteManager(){
        routes = new HashMap<>();
    }

    public void addRoute(String article, String preArticle){
        routes.put(article,preArticle);
    }

    @Override
    public String getPreArticle(String article, int jobID) {
        return routes.get(article);
    }
}
