package autoschedule.control.database;

import autoschedule.algorithms.objectives.SchedulingObjective;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@RequiredArgsConstructor
public class TableRowResultsOptimization {
    // Properties
    @Getter private final int runId;
    @Getter private final String heuristic;
    @Getter private final long time;
    @Getter private final String objValue;
    @Getter private final double objValueToCompare;
    @Getter private final Map<SchedulingObjective, Double> evaluatedObjectives;
}
