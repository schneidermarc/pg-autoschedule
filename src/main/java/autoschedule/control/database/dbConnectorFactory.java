package autoschedule.control.database;

import autoschedule.control.DatabaseConnector;

public class dbConnectorFactory {

    public static DatabaseConnector createDatabaseConnector(){
        DatabaseConnector dbConnector = new DatabaseConnector();
        dbConnector.establishDefaultConnections();
        return dbConnector;
    }

    public static DatabaseConnector createDatabaseConnector(String pDatabaseName){
        DatabaseConnector dbConnector = new DatabaseConnector();
        dbConnector.establishDefaultConnections();
        dbConnector.establishConnectionWithDatabase(dbConnector.loadDBFile(pDatabaseName));
        return dbConnector;
    }

}
