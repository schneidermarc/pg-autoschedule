package autoschedule.control.database;

import java.util.concurrent.locks.ReentrantLock;

public class ArticleIdGenerator {

    private static int nextID = 0;
    private static final ReentrantLock idLock = new ReentrantLock();

    /**
     * returns next available id
     */
    public static int getNextArticleID(){
        idLock.lock();
        try{
            return nextID++;
        }
        finally{
            idLock.unlock();
        }
    }

}
