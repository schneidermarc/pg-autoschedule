package autoschedule.control.database;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ExecutedAlgorithmData {
    @Getter private final String algorithm;
    @Getter private final int iterations;
    @Getter private final int runtime;
}
