package autoschedule.control.database;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

@SuppressWarnings("unused")
public class DemandData {
    private final SimpleIntegerProperty jobId;
    private final SimpleDoubleProperty weight;
    private final SimpleStringProperty article;
    private final SimpleIntegerProperty year;
    private final SimpleLongProperty week;
    private final SimpleIntegerProperty demand;
    private final SimpleDoubleProperty demandScrap;
    private final SimpleIntegerProperty avgCompleteProdTime;


    // Constructor
    public DemandData(int jobId, double weight, String article, int year, long week, int demand, double demandScrap){
        this.jobId = new SimpleIntegerProperty(jobId);
        this.weight = new SimpleDoubleProperty(weight);
        this.article = new SimpleStringProperty(article);
        this.year = new SimpleIntegerProperty(year);
        this.week = new SimpleLongProperty(week);
        this.demand = new SimpleIntegerProperty(demand);
        this.demandScrap = new SimpleDoubleProperty(demandScrap);
        this.avgCompleteProdTime = new SimpleIntegerProperty(0);
    }


    // Getter und Setter
    public int getJobId(){
        return this.jobId.get();
    }

    public double getWeight(){
        return this.weight.get();
    }

    public int getAvgCompleteProdTime(){
        return this.avgCompleteProdTime.get();
    }

    public String getArticle() {
        return article.get();
    }

    public int getYear(){
        return year.get();
    }

    public long getWeek() {
        return week.get();
    }

    public int getDemand() {
        return demand.get();
    }

    public Double getDemandScrap() {
        return demandScrap.get();
    }

    // Setter

    public void setArticle(String article) {
        this.article.set(article);
    }

    public void setWeek(long week) {
        this.week.set(week);
    }

    public void setDemand(int demand) {
        this.demand.set(demand);
    }

    public void setDemandScrap(double demandScrap) {
        this.demandScrap.set(demandScrap);
    }

    public void setAvgCompleteProdTime(int avgCompleteProdTime){
        this.avgCompleteProdTime.set(avgCompleteProdTime);
    }
}
