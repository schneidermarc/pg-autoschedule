package autoschedule.control.database;

import autoschedule.control.DatabaseConnector;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ScrapManager {

    private final Map<String,Double> scrapsPerArticle;
    private final Map<String,Double> scrapsPerStage;
    private final double MAX_SCRAPRATE;

    /**
     * Creates a ScrapManager for the given scrap data.
     * @param maxScraprate the max allowed scrap rate.
     */
    public ScrapManager(Map<String,Double> scrapsPerArticle, double maxScraprate){
        this.scrapsPerArticle = scrapsPerArticle;
        scrapsPerStage = generateScrapsPerStage();
        MAX_SCRAPRATE = maxScraprate;
    }

    /**
     * Returns the median scrap rate of the article over all machines of its stage.
     * If there are no scrap data recorded for the article, this method will return the median scrap rate over all articles on the articles stage.
     * If there are no scrap data recorded for the articles stage, this method will return the max allowed scrap rate.
     * @param article the article of which the scrap rate is needed.
     */
    public Double getScrapForArticle(String article){
        Double scrapArticle = scrapsPerArticle.get(article);
        return scrapArticle == null ? getScrapForStage(getStageByArticle(article)) : scrapArticle;
    }

    /**
     * returns the median scrap over all articles of this stage.
     */
    private Double getScrapForStage(String stage){
        Double scrapStage = scrapsPerStage.get(stage);
        return scrapStage == null ? MAX_SCRAPRATE : scrapStage;
    }

    /**
     * returns the stage of the given article aka the first letter of the articles name.
     */
    private String getStageByArticle(String article){
        return article.substring(0,1);
    }

    /**
     * returns a Map which maps stage names to the median scrap rate over all articles of the stage.
     */
    private Map<String,Double> generateScrapsPerStage(){
        Map<String,Double> output = new HashMap<>();
        Map<String, List<Double>> scrapsForStages = new HashMap<>();
        for(Map.Entry<String,Double> entry : scrapsPerArticle.entrySet()){
            String stage = getStageByArticle(entry.getKey());
            List<Double> listForStage = scrapsForStages.computeIfAbsent(stage, k -> new LinkedList<>());
            listForStage.add(entry.getValue());
        }
        for(Map.Entry<String,List<Double>> entry : scrapsForStages.entrySet()){
            String stage = entry.getKey();
            Double medianStageScrap = DatabaseConnector.medianDouble(entry.getValue());
            output.put(stage,medianStageScrap);
        }
        return output;
    }

}
