package autoschedule.control.database;

import autoschedule.utils.logging.Logger;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

public class StockManager {

    private final Map<ArticleTuple,Integer> stocks;

    /**
     * Creates a new StockManager with an empty stock.
     */
    public StockManager(){
        stocks = new HashMap<>();
    }

    /**
     * returns the currently available stock for this article/week/year.
     */
    public int getStock(int year, int week, String article){
        ArticleTuple tuple = new ArticleTuple(year, week, article);
        Integer stock = stocks.get(tuple);
        return stock == null ? 0 : stock;
    }

    /**
     * Adds the given stock to the available stock for this article/week/year.
     */
    public void addStock(int year, int week, String article, int stockToAdd){
        ArticleTuple tuple = new ArticleTuple(year, week, article);
        Integer stock = stocks.get(tuple);
        if(stock == null){
            stocks.put(tuple,stockToAdd);
        }
        else{
            stocks.replace(tuple,stock+stockToAdd);
        }
    }

    /**
     * Subtracts the given stock from the available stock for this article/week/year.
     * If the stockToSubstract is higher than the available stock, the complete available stock is deleted and a Logger warning will be displayed.
     */
    public void subtractStock(int year, int week, String article, int stockToSubtract){
        ArticleTuple tuple = new ArticleTuple(year, week, article);
        Integer stock = stocks.get(tuple);
        stock = stock == null ? 0 : stock;
        if(stock < stockToSubtract){
            stocks.remove(tuple);
            Logger.debug("Subtracted more stock than was available(stockToSubtract > stock). Resulting data might be inconsistent.");
        }
        else if(stock == stockToSubtract){
            stocks.remove(tuple);
        }
        else{
            stocks.replace(tuple,stock-stockToSubtract);
        }
    }

    @RequiredArgsConstructor
    private static class ArticleTuple{
        @Getter @NonNull private final Integer year;
        @Getter @NonNull private final Integer week;
        @Getter @NonNull private final String article;

        //Depends only on account number
        @Override
        public int hashCode() {
            return 31 * (year.hashCode() + week.hashCode() + article.hashCode());
        }

        //Compare only account numbers
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            ArticleTuple other = (ArticleTuple) obj;
            return year.equals(other.year) && week.equals(other.week) && article.equals(other.article);
        }
    }
}
