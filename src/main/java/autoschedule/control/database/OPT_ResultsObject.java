package autoschedule.control.database;

import autoschedule.model.MachineEnvironment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
public class OPT_ResultsObject {
    // Attribute
    @Getter @Setter private int runId;
    @Getter @Setter private String runName;
    @Getter @Setter private Date calcDate;
    @Getter @Setter private MachineEnvironment machineEnv;
    @Getter @Setter private String algString;
    @Getter @Setter private int week;
    @Getter @Setter private int year;
    @Getter @Setter private int duration;
    //@Getter @Setter private int steps;

    @Override
    public String toString() {
        return "OPT_ResultsObject{" +
                "runId=" + runId +
                ", runName='" + runName + '\'' +
                ", calcDate=" + calcDate +
                ", machineEnv='" + machineEnv.getName() + '\'' +
                ", algString='" + algString + '\'' +
                ", week=" + week +
                ", year=" + year +
                ", time=" + duration +
                //", steps=" + steps +
                '}';
    }
}
