package autoschedule.control;

import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.objectives.*;
import autoschedule.algorithms.utils.AlgorithmConfig;
import autoschedule.control.database.*;
import autoschedule.model.*;
import autoschedule.utils.logging.Logger;
import autoschedule.view.solutionviews.ScheduleData;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"SqlResolve", "ConstantConditions"})
public class DatabaseConnector {
	private static final double MAX_SCRAPRATE = 0.1D;
	private static final double DEFAULT_SCRAP_QUANTILE = 0.9D;

	public static final String RESOURCE_FOLDER = "src/main/resources/";
	public static final String DEFAULT_RESULT_DATABASE_PATH = "src/main/resources/results.db";
	public static final String DEFAULT_DATABASE = "db.db";

	// Attributes
	public static Connection connection = null, connectionResultDB = null;

	 /**
     * Holds database connections.
     * Maps file path to Connection
     */
    private static final Map<File,DatabaseConnection> connections = new HashMap<>();

    /**
     * Returns Connection for default DB
     */
    public synchronized Connection getDefaultConnection(){
        return establishConnectionWithDatabase(loadDBFile(DEFAULT_DATABASE));
    }

    /**
     * Returns Connection for default result DB
     */
    public synchronized Connection getDefaultConnectionResultDB(){
		return establishConnectionWithResultDatabase(new File(DEFAULT_RESULT_DATABASE_PATH));
    }

	/**
	 * Clears all from results connection
	 */
	@SuppressWarnings("SqlWithoutWhere")
	@SneakyThrows
	public void clearResultsDB() {
		Statement statement = connectionResultDB.createStatement();
		statement.executeUpdate("DELETE FROM OPT_algorithm_data");
		statement.executeUpdate("DELETE FROM OPT_constraints");
		statement.executeUpdate("DELETE FROM OPT_obj_values");
		statement.executeUpdate("DELETE FROM OPT_results");
		statement.executeUpdate("DELETE FROM OPT_schedule");
	}


	public synchronized void establishDefaultConnections(){
		establishConnectionWithDatabase(loadDBFile(DEFAULT_DATABASE));
		establishConnectionWithResultDatabase(new File(DEFAULT_RESULT_DATABASE_PATH));
	}

    /**
     * Returns database connection for given file path.
     * Establishes a new connection if necessary
     */
    public synchronized Connection establishConnectionWithDatabase(File pDatabase){
        DatabaseConnection databaseConnection = connections.get(pDatabase);
        if(databaseConnection == null){
            try {
                Class.forName("org.sqlite.JDBC");
                //File file = new File(pDatabasePath);
                String jdbcPath = "jdbc:sqlite:" + pDatabase.getAbsolutePath();
                Connection conn = DriverManager.getConnection(jdbcPath);
                databaseConnection = new DatabaseConnection(conn,pDatabase);
                connections.put(pDatabase,databaseConnection);
            }
            catch (Exception e) {
                Logger.message(e.getClass().getName() + ": " + e.getMessage());
            }
        }
        connection = databaseConnection.getConnection();
        return databaseConnection.getConnection();
    }


	public synchronized Connection establishConnectionWithResultDatabase(File pDatabase){
		DatabaseConnection databaseConnection = connections.get(pDatabase);
		if(databaseConnection == null){
			try {
				Class.forName("org.sqlite.JDBC");
				//File file = new File(pDatabasePath);
				String jdbcPath = "jdbc:sqlite:" + pDatabase.getAbsolutePath();
				Connection conn = DriverManager.getConnection(jdbcPath);
				databaseConnection = new DatabaseConnection(conn,pDatabase);
				connections.put(pDatabase,databaseConnection);
			}
			catch (Exception e) {
				Logger.message(e.getClass().getName() + ": " + e.getMessage());
			}
		}
		connectionResultDB = databaseConnection.getConnection();
		return databaseConnection.getConnection();
	}

	public synchronized File loadDBFile(String name) {
		String fileName = "Databases";
		ClassLoader classLoader = getClass().getClassLoader();

		File dir = new File(classLoader.getResource(fileName).getFile());
		final String[] extensions = new String[]{
				"db"
		};
		if(dir.isDirectory()) {
			for (final File f : dir.listFiles()) {
				for(String ext : extensions) {
					if(f.getName().equals(name)) {
						return f;
					}
				}
			}
		}
		return new File("J");
	}

    /**
     * Returns demand data
     * @param year Selected year
     * @param week Selected week
     * @param duration Selected duration
     * @param zeroDemands Whether to include articles with zero demand
     */
    @SneakyThrows
    public synchronized List<DemandData> getData(int year, int week, int duration, boolean zeroDemands){

		week = Math.max(week,0);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.WEEK_OF_YEAR, week);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.add(Calendar.WEEK_OF_YEAR, duration);
		int endYear = cal.get(Calendar.YEAR);
		int endWeek = cal.get(Calendar.WEEK_OF_YEAR)-1;
		int startYearWeek = Integer.parseInt(year + "" + (week < 10 ? "0" + week : week));
		int endYearWeek = Integer.parseInt(endYear + "" + (endWeek < 10 ? "0" + endWeek : endWeek));

    	List<DemandData> list = new ArrayList<>();

        String sql = "SELECT job_id, weight, article, week, year, demand, CAST(year || SUBSTR('00' || week, -2, 2) AS INTEGER) AS year_week FROM Demand "
				+ "WHERE year_week >= " + startYearWeek + " "
				+ "AND year_week <= " + endYearWeek + " ";
		if(!zeroDemands) {
			sql = sql + "AND demand!=0 ";
		}
		sql += "ORDER BY year,week,job_id ";
		CachedRowSet crs = readFromDatabase(sql,connection);
        while (crs.next()){
        	int jobId = crs.getInt("job_id");
        	BigDecimal weightData = crs.getBigDecimal("weight");
        	double weight = round(weightData.doubleValue(),2);
            String articleData = crs.getString("article");
            int weekData = crs.getInt("week");
            int yearData = crs.getInt("year");
            int demand = crs.getInt("demand");
            int demandScrap = getDemandWithScrap(demand,DEFAULT_SCRAP_QUANTILE,articleData);
            list.add(new DemandData(jobId,weight,articleData,yearData,weekData,demand, demandScrap));
        }
        return list;
    }

	@SuppressWarnings("SameParameterValue")
	private double round(double value, int decimalPoints) {
		double d = Math.pow(10, decimalPoints);
		return Math.round(value * d) / d;
	}

	/**
	 * Return available years from Demand table
	 */
	@SneakyThrows
	public synchronized List<Integer> getYearsFromDemand(){
		List<Integer> list = new ArrayList<>();
		CachedRowSet crs = readFromDatabase("SELECT year FROM Demand GROUP BY year",connection);
		while (crs.next()){
			list.add(crs.getInt("year"));
		}
		return list;
	}

	/**
	 * Return available years from Demand table
	 */
	@SneakyThrows
	public synchronized List<Integer> getWeeksFromDemand(int year){
		List<Integer> list = new ArrayList<>();
		String sql = "SELECT week FROM Demand WHERE year=" + year + " GROUP BY week";
		CachedRowSet crs = readFromDatabase(sql,connection);
		while (crs.next()){
			list.add(crs.getInt("week"));
		}
		return list;
	}

	/**
	 * Returns median over all processing times of given article on given machine.
	 */
	@SneakyThrows
	public synchronized double getProcessingMedian(String article, String machine){
		List<Integer> list = new LinkedList<>();
		CachedRowSet data = readFromDatabase("SELECT processing_time, occurrences FROM Processing_times WHERE article = '" + article + "' AND machine  = '" + machine + "'",connection);
		while (data.next()){
			int occurrences = data.getInt("occurrences");
			for (int i = 0; i < occurrences; i++){
				list.add(data.getInt(1));
			}
		}
		data.close();
		return median(list);
	}

	@SneakyThrows
	public synchronized Map<Map.Entry<String,String>,Double> getProcessingMedian(){
		Map<Map.Entry<String,String>,Double> output = new HashMap<>();
		List<Integer> list = new LinkedList<>();
		CachedRowSet data = readFromDatabase("SELECT article, machine, processing_time, occurrences FROM Processing_times ORDER BY article,machine",connection);
		if(data.next()){
			String currentArticle = data.getString("article");
			String currentMachine = data.getString("machine");
			AbstractMap.SimpleEntry<String,String> key = new AbstractMap.SimpleEntry<>(currentArticle,currentMachine);
			data.beforeFirst();

			while (data.next()){
				String article = data.getString("article");
				String machine = data.getString("machine");
				int occurrences = data.getInt("occurrences");
				int processingTime = data.getInt("processing_time");

				if(!(currentArticle.equals(article) && currentMachine.equals(machine)) ){
					output.put(key,median(list));
					currentArticle = article;
					currentMachine = machine;
					key = new AbstractMap.SimpleEntry<>(currentArticle,currentMachine);
					list.clear();
				}

				for (int i = 0; i < occurrences; i++){
					list.add(processingTime);
				}

			}
			//Last occurring article
			output.put(key,median(list));
		}
		data.close();
		return output;
	}

	@SneakyThrows
	public synchronized Map<String,Double> getProcessingMedianByArticle(){
		Map<String,Double> output = new HashMap<>();
		List<Integer> list = new LinkedList<>();
		CachedRowSet data = readFromDatabase("SELECT article, processing_time, occurrences FROM Processing_times ORDER BY article",connection);
		if(data.next()){
			String currentArticle = data.getString("article");
			data.beforeFirst();

			while (data.next()){
				String article = data.getString("article");
				int occurrences = data.getInt("occurrences");
				int processingTime = data.getInt("processing_time");

				if(!currentArticle.equals(article)){
					output.put(currentArticle,median(list));
					currentArticle = article;
					list.clear();
				}

				for (int i = 0; i < occurrences; i++){
					list.add(processingTime);
				}
			}
			//Last occurring article
			output.put(currentArticle,median(list));
		}
		data.close();
		return output;
	}

	/**
	 * Returns median over all processing times of all articles on given machine.
	 */
	@SneakyThrows
	public synchronized double getProcessingMedianByMachine(String machine){
		List<Integer> list = new LinkedList<>();
		CachedRowSet data = readFromDatabase("SELECT processing_time, occurrences FROM Processing_times WHERE machine  = '" + machine + "'",connection);
		while (data.next()){
			int occurrences = data.getInt("occurrences");
			for (int i = 0; i < occurrences; i++){
				list.add(data.getInt(1));
			}
		}
		data.close();
		return median(list);
	}

	@SneakyThrows
	public synchronized Map<Character,Double> getProcessingMedianByStage(){

		Map<Character,Double> output = new HashMap<>();
		List<Integer> list = new LinkedList<>();
		CachedRowSet data = readFromDatabase("SELECT article, processing_time, occurrences FROM Processing_times ORDER BY article",connection);
		if(data.next()){
			String currentArticle = data.getString("article");
			Character currentStage = currentArticle.charAt(0);
			data.beforeFirst();

			while (data.next()){
				String article = data.getString("article");
				Character stage = article.charAt(0);
				int occurrences = data.getInt("occurrences");
				int processingTime = data.getInt("processing_time");

				if(!currentStage.equals(stage)){
					output.put(currentStage,median(list));
					currentStage = stage;
					list.clear();
				}

				for (int i = 0; i < occurrences; i++){
					list.add(processingTime);
				}
			}
			//Last occurring stage
			output.put(currentStage,median(list));
		}
		data.close();
		return output;
	}


	/**
	 * Returns median Value in list
	 */
	public static synchronized double median(List<Integer> list) {
		if(list.isEmpty()){
			return 0.0;
		}
		list.sort(Integer::compareTo);
		int middle = list.size()/2;
		if (list.size()%2 == 1) {
			return list.get(middle);
		}
		else {
			return (list.get(middle-1) + list.get(middle)) / 2.0;
		}
	}

	/**
	 * Returns median Value in list
	 */
	public static synchronized double medianDouble(List<Double> list) {
		if(list.isEmpty()){
			return 0.0;
		}
		list.sort(Double::compareTo);
		int middle = list.size()/2;
		if (list.size()%2 == 1) {
			return list.get(middle);
		}
		else {
			return (list.get(middle-1) + list.get(middle)) / 2.0;
		}
	}

	/**
	 * Returns given quantile for given article
	 */
	@SneakyThrows
	public synchronized double getScrapQuantile(double p_quantile, String article){
		if(p_quantile <= 1){
			PreparedStatement stmt = connection.prepareStatement("SELECT SUM(occurrences) FROM Scrap_rates WHERE article like ?");
			stmt.setString(1, article);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				double totalScraps = rs.getDouble(1);

				PreparedStatement stmt1 = connection.prepareStatement("SELECT occurrences, scrap FROM Scrap_rates WHERE article like ? AND occurrences != 0");
				stmt1.setString(1, article);
				rs = stmt1.executeQuery();
				int pq = 0;
				while (rs.next()) {
					pq += rs.getInt(1);
					if (pq >= p_quantile * totalScraps) {
						double tmp = rs.getDouble(2);
						stmt.close();
						stmt1.close();
						return tmp;
					}
				}
				stmt1.close();
			}
			stmt.close();
		}
		return 0.0;
	}

	/**
	 * Returns a map for the scrap rate for given article given a quantile
	 */
	@SneakyThrows
	public synchronized ScrapManager getScrapManager(double p_quantile){
		Map<String,Double> scrapsPerArticleMap = new HashMap<>();
		if(p_quantile <= 1) {
			CachedRowSet crs = readFromDatabase("SELECT article, SUM(occurrences) AS occurrences, COUNT(*) AS entries FROM Scrap_rates WHERE occurrences != 0 GROUP BY article ORDER BY article", connection);
			CachedRowSet crsScrap = readFromDatabase("SELECT article, occurrences, scrap FROM Scrap_rates WHERE occurrences != 0 ORDER BY article", connection);
			while (crs.next()) {
				double totalScraps = crs.getDouble("occurrences");
				int entries = crs.getInt("entries");
				String article = crs.getString("article");

				int pq = 0;
				for (int i = 0; i < entries; i++) {
					if (crsScrap.next()) {
						pq += crsScrap.getInt("occurrences");
						if (pq >= p_quantile * totalScraps) {
							double tmp = crsScrap.getDouble("scrap");
							scrapsPerArticleMap.put(article, tmp);
						}
					}
				}
			}
		}
		return new ScrapManager(scrapsPerArticleMap,MAX_SCRAPRATE);
	}

	/**
	 * Calculates demand for given article with scrap
	 * @param demand Demand of article
	 * @param quantile Quantile to be used
	 * @param article Article
	 */
	public synchronized int getDemandWithScrap(int demand, double quantile, String article){
		return getDemandWithScrap(demand, quantile, MAX_SCRAPRATE, article);
	}

	/**
	 * Calculates demand for given article with scrap
	 * @param demand Demand of article
	 * @param quantile Quantile to be used
	 * @param maxScrap Max scrap rate
	 * @param article Article
	 */
	public synchronized int getDemandWithScrap(int demand, double quantile, double maxScrap, String article){
		return (int)Math.round(demand/(1-(Math.min(maxScrap, getScrapQuantile(quantile,article)/100))));
	}

	public int getDemandWithScrapFromScrapMap(int demand, String article, ScrapManager scrapMap){
		Double scrapRate = scrapMap.getScrapForArticle(article);
		return (int)Math.round(demand/(1-(Math.min(MAX_SCRAPRATE, scrapRate/100))));
	}

	/**
	 * Creates a StockManager from the given demand data.
	 */
	@SneakyThrows
	public synchronized StockManager getStocks(CachedRowSet demandData){
		StockManager stockManager = new StockManager();
		demandData.beforeFirst();
		while(demandData.next()){
			int year = demandData.getInt("year");
			int week = demandData.getInt("week");
			String article = demandData.getString("article");
			int stock = demandData.getInt("stock");
			stockManager.addStock(year,week,article,stock);
		}
		return stockManager;
	}

	/**
	 * Returns Map containing all qualified machines (value) for each article (key)
	 */
	@SneakyThrows
	public synchronized Map<String, Collection<Map.Entry<Machine, Integer>>> getMachineQualifications(Set<Constraint> pActiveConstraints){
		Map<String, Collection<Map.Entry<Machine, Integer>>> machineQualifications = createArticleMachineQualifications();
		Map<String,Integer[]> machineData = getMachineData(pActiveConstraints.contains(Constraint.MACHINE_ARRIVAL),false,false);
		Map<String,Machine> machinesByNames = createMachines(machineData);
		Map<String,Integer> machineMedianProcessingTimes = getMedianProcessingTimesByMachines();
		Map<Map.Entry<String,String>,Double> medianProcessingTimesByArticlesAndMachines = getProcessingMedian();
		CachedRowSet data = readFromDatabase("SELECT DISTINCT A.article, Machine_qualifications.machine as machine  " +
				"FROM " +
				"(SELECT DISTINCT article FROM demand " +
				"UNION " +
				"SELECT B.article FROM " +
				"(SELECT DISTINCT article FROM Route " +
				"UNION SELECT DISTINCT pre_article FROM Route)B " +
				"WHERE B.article NOT IN (SELECT DISTINCT article FROM demand))A " +
				"LEFT JOIN Machine_qualifications ON Machine_qualifications.article = A.article  " +
				"ORDER BY machine IS NULL",connection);
		boolean machineQualificationConstraint = pActiveConstraints.contains(Constraint.MJ);
		while(data.next()){
			String article =  data.getString("article");
			String machineName = data.getString("machine");
			if(machineName == null || !machineQualificationConstraint){    //Article has no qualifications
				for(Map.Entry<String,Integer> entry : machineMedianProcessingTimes.entrySet().stream().filter(entry -> entry.getKey().startsWith(article.substring(0,1))).collect(Collectors.toList())){
					machineName = entry.getKey();
					int processingTime = entry.getValue();
					Machine machine = machinesByNames.get(machineName);
					Collection<Map.Entry<Machine, Integer>> machinesAndProcessingTimes = machineQualifications.get(article);
					machinesAndProcessingTimes.add(new AbstractMap.SimpleEntry<>(machine, processingTime));
				}
			}
			else{
				Machine machine = machinesByNames.get(machineName);
				AbstractMap.SimpleEntry<String,String> key = new AbstractMap.SimpleEntry<>(article,machine.getName());
				Collection<Map.Entry<Machine, Integer>> machinesAndProcessingTimes = machineQualifications.get(article);
				machinesAndProcessingTimes.add(new AbstractMap.SimpleEntry<>(machine, (int)Math.round(medianProcessingTimesByArticlesAndMachines.get(key))));
			}
		}
		data.close();
		return machineQualifications;
	}

	/**
	 * Creates machines
	 */
	@SneakyThrows
	private synchronized Map<String,Machine> createMachines(Map<String,Integer[]> machineData){
		Map<String,Machine> machinesByNames = new HashMap<>();
		for(Map.Entry<String,Integer[]> entry : machineData.entrySet()){
			String machineName = entry.getKey();
			Integer[] valueEntry = entry.getValue();
			//int simultaneousWork = valueEntry[0];
			int arrival = valueEntry[1];
			//int bufferSize = valueEntry[2];
			Machine machine = new Machine(machineName);
			//machine.setSimultaneousAmount(simultaneousWork);
			machine.setArrival(arrival);
			//machine.setBufferSize(bufferSize);
			machinesByNames.put(machineName,machine);
		}
		return machinesByNames;
	}

    /**
     * Creates machine qualification Map
     */
    @SneakyThrows
    private synchronized Map<String, Collection<Map.Entry<Machine, Integer>>> createArticleMachineQualifications(){
        Map<String, Collection<Map.Entry<Machine, Integer>>> output = new HashMap<>();
        CachedRowSet data = readFromDatabase("SELECT DISTINCT article FROM demand " +
                                                    "UNION SELECT B.article " +
                                                    "FROM " +
                                                    "(SELECT DISTINCT article FROM Route UNION SELECT DISTINCT pre_article FROM Route)B " +
                                                    "WHERE B.article NOT IN (SELECT DISTINCT article FROM demand)",connection);
        while(data.next()){
            String article = data.getString("article");
            output.put(article, new ArrayList<>());
        }
        data.close();
        return output;
    }

	/**
	 * Returns Map, mapping articles (key) onto their predecessors (value)
	 */
	@SneakyThrows
	public synchronized RouteManager getArticleRoutes(MachineEnvironment pMachineEnvironment){
		if (pMachineEnvironment == MachineEnvironment.JOB_SHOP) {
			return getJobShopRouteManager();
		}
		return getDefaultRouteManager();
	}

	@SneakyThrows
	private synchronized RouteManager getJobShopRouteManager(){
		JobShopRouteManager routeManager = new JobShopRouteManager();
		CachedRowSet data = readFromDatabase("SELECT DISTINCT article, pre_article,job_id FROM Route",connection);
		while(data.next()){
			String article = data.getString("article");
			String preArticle = data.getString("pre_article");
			int jobID = data.getInt("job_id");
			routeManager.addRoute(article,preArticle,jobID);
		}
		data.close();
		return routeManager;
	}

	@SneakyThrows
	private synchronized RouteManager getDefaultRouteManager(){
		DefaultRouteManager routeManager = new DefaultRouteManager();
		CachedRowSet data = readFromDatabase("SELECT DISTINCT article, pre_article FROM Route",connection);
		while(data.next()){
			String article = data.getString("article");
			String preArticle = data.getString("pre_article");
			routeManager.addRoute(article,preArticle);
		}
		data.close();
		return routeManager;
	}

	/**
	 * Returns Map, mapping articles (key) onto their processing times (value)
	 */
	@SneakyThrows
	public synchronized Map<String,Integer> getMedianProcessingTimesByMachines(){
		Map<String,Integer> output = new HashMap<>();
		String query = "SELECT machine FROM Machines";
		CachedRowSet data = readFromDatabase(query,connection);
		while(data.next()){
			String machine = data.getString("machine");
			int median = (int)Math.round(getProcessingMedianByMachine(machine));
			output.put(machine,median);
		}
		return output;
	}

	/**
	 * Returns Map, mapping machines (key) onto their batch size (value1) and arrival times (in seconds) (value2)
	 * @param machineArrivalConstraint true if machine arrival constraint is active
	 * @param batchConstraint true if batch constraint is active (aka machines can potentially produce more than 1 article at a time)
	 */
	@SneakyThrows
	public synchronized Map<String,Integer[]> getMachineData(boolean machineArrivalConstraint, boolean batchConstraint, boolean blockConstraint){
		Map<String,Integer[]> output = new HashMap<>();
		String query = "SELECT machine, simultaneous_count, arrival, bufferSize FROM Machines";
		CachedRowSet data = readFromDatabase(query,connection);
		while(data.next()){
			String machine = data.getString("machine");
			int count = batchConstraint ? data.getInt("simultaneous_count") : 1;
			int arrival = data.getInt("arrival") * (machineArrivalConstraint ? 1 : 0);
			int bufferSize = data.getInt("bufferSize") * (blockConstraint ? 1 : 0);
			output.put(machine,new Integer[]{count,arrival,bufferSize});
		}
		return output;
	}

	/**
	 * Returns list of all jobs in selected scheduling duration
	 * @param pYear Selected year
	 * @param pStartWeek Selected week
	 * @param pSchedulingDuration Selected duration
	 * @param pMachineQualifications Machine qualifications
	 */
	@SneakyThrows
	public synchronized List<Job> getJobsFromDatabase(int pYear, int pStartWeek, int pSchedulingDuration, Map<String, Collection<Map.Entry<Machine, Integer>>> pMachineQualifications, Set<Constraint> pActiveConstraints, MachineEnvironment pMachineEnvironment){
		List<Job> jobs = new ArrayList<>();
		RouteManager articleRoutes = getArticleRoutes(pMachineEnvironment);
		CachedRowSet demand = getDemandFromDatabase(pYear, pStartWeek, pSchedulingDuration);
		ScrapManager scrapManager =  getScrapManager(DEFAULT_SCRAP_QUANTILE);
		StockManager stockManager = getStocks(demand);
		int jobCounter = 1;
		demand.beforeFirst();
		int startYear = 0;
		int startWeek = 0;
		if(demand.next()){
			startYear = demand.getInt("year");
			startWeek = demand.getInt("week");
		}
		demand.beforeFirst();
		while(demand.next()){
			List<Article> articleListForJob = new ArrayList<>();

			int jobID = demand.getInt("job_id");
			int year = demand.getInt("year");
			int week = demand.getInt("week");
			int yearWeek = demand.getInt("year_week");
			String article = demand.getString("article");
			String originalArticle = article;
			int correctDemand = (int) Math.ceil(demand.getDouble("correct_demand"));
			boolean jobArrivalConstraint = pActiveConstraints.contains(Constraint.JOB_ARRIVAL);
			int weekDiff = getWeekDiff(startYear,startWeek,year,week)*604800;
			int deadline = demand.getInt("deadline") + weekDiff;
			int arrival = jobArrivalConstraint ? demand.getInt("arrival") + weekDiff : 0;
			double weight = demand.getDouble("weight");
			//System.out.println("Job_"+jobID + "    yearWeek: " + yearWeek + "    article: " + article + "    weekDiff: " + weekDiff + "    arrival: " + arrival + "    arrivalDB: " + demand.getInt("arrival"));

			while(article != null){
				correctDemand = getDemandWithStockSubtracted(correctDemand,year,week,stockManager,article);
				if(correctDemand <= 0){
					break;
				}
				correctDemand = getDemandWithScrapFromScrapMap(correctDemand,article,scrapManager);
				Article generatedArticle = generateArticleFromData(article,pMachineQualifications.get(article));
				generatedArticle.setProductionAmount(correctDemand);
				generatedArticle.setArrival(arrival);
				articleListForJob.add(0,generatedArticle);
				article = articleRoutes.getPreArticle(article,jobID);
			}
			if(!articleListForJob.isEmpty()) {
				String jobName = "job" + jobCounter++ + "_" + yearWeek + "_" + originalArticle;
				Job job = new Job(jobID,jobName, articleListForJob, deadline, arrival, weight);
				jobs.add(job);
			}
		}
		return jobs;
	}

	private int getWeekDiff(int startYear, int startWeek, int endYear, int endWeek){
		if(startYear == endYear){
			return endWeek-startWeek;
		}
		else{
			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.YEAR,startYear);
			cal1.set(Calendar.WEEK_OF_YEAR,startWeek);
			cal1.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
			Calendar cal2 = Calendar.getInstance();
			cal2.set(Calendar.YEAR,endYear);
			cal2.set(Calendar.WEEK_OF_YEAR,endWeek);
			cal2.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);

			Instant instant1 = Instant.ofEpochMilli(cal1.getTimeInMillis());
			Instant instant2 = Instant.ofEpochMilli(cal2.getTimeInMillis());

			LocalDate startDate = LocalDate.ofInstant(instant1, ZoneId.systemDefault());
			LocalDate endDate = LocalDate.ofInstant(instant2, ZoneId.systemDefault());

			return (int)ChronoUnit.WEEKS.between(startDate, endDate);
		}
	}

	private synchronized int getDemandWithStockSubtracted(int demand, int year, int week, StockManager stockManager, String article){
		int stock = stockManager.getStock(year, week, article);
		if(demand <= stock){
			stockManager.subtractStock(year,week,article,demand);
			return 0;
		}
		else{
			stockManager.subtractStock(year,week,article,stock);
			return demand - stock;
		}
	}

	/**
	 * Generate articles
	 */
	private synchronized Article generateArticleFromData(String pArticle, Collection<Map.Entry<Machine, Integer>> machineQualifications){
		Article article = new Article(pArticle);
		if(machineQualifications != null) {
			for (Map.Entry<Machine, Integer> entry : machineQualifications) {
				Machine machine = entry.getKey();
				int processingTime = entry.getValue();
				article.addQualifiedMachine(machine, processingTime);
			}
		}
		return article;
	}

	/**
	 * Returns computed schedule by runId
	 */
	@SneakyThrows
	public synchronized CachedRowSet getComputedSchedule(int runId){
		String query = "SELECT calc_date, article, OPT_schedule.job_id, machine, start_time, end_time, year, week,  weight, arrival, production_amount, machine_arrival, deadline, OPT_schedule.run_id " +
				"FROM OPT_results " +
				"JOIN OPT_schedule ON OPT_results.run_id = OPT_schedule.run_id " +
				"LEFT JOIN OPT_job_data ON OPT_schedule.run_id = OPT_job_data.run_id AND OPT_schedule.job_id = OPT_job_data.job_id " +
				"WHERE OPT_results.run_id = " + runId + " " +
				" AND OPT_schedule.algorithm_id = (SELECT MAX(algorithm_id) FROM OPT_schedule WHERE run_id = " + runId + ")";
		return readFromDatabase(query, getDefaultConnectionResultDB());
	}

    /**
     * Returns computed schedule by runId and algorithmId
     */
    @SneakyThrows
    public CachedRowSet getComputedSchedule(int runId, int algorithmId){
    	if(algorithmId == 0)
    		return getComputedSchedule(runId);

        String query = "SELECT calc_date, article, job_id,  machine, start_time, end_time, year, week, OPT_schedule.run_id" +
                "FROM OPT_results, OPT_schedule " +
                "WHERE OPT_results.run_id = OPT_schedule.run_id and OPT_results.run_id = " + runId +
                " AND OPT_schedule.algorithm_id ="  + algorithmId;
        return readFromDatabase(query, getDefaultConnectionResultDB());
    }

	/**
	 * Get all qualification machine
	 * @return String of qualification machines
	 */
    @SneakyThrows
	public String getMachineQualifications(int runId, String articleName){
    	String query = "SELECT machine FROM OPT_machine_qualifications " +
						"WHERE run_id =" + runId + " and article LIKE '" + articleName + "'";
		ResultSet rs = readFromDatabase(query, getDefaultConnectionResultDB());
		String out = "";
		ArrayList<String> list = new ArrayList<>();
		while(rs.next()) list.add(rs.getString("machine"));
		// ---- Sort ----
		for (String s : list.stream().sorted((o1, o2) -> {
            if (o2 == null || o1 == null)
                return 0;

            int lengthFirstStr = o1.length();
            int lengthSecondStr = o2.length();

            int index1 = 0;
            int index2 = 0;

            while (index1 < lengthFirstStr && index2 < lengthSecondStr) {
                char ch1 = o1.charAt(index1);
                char ch2 = o2.charAt(index2);

                char[] space1 = new char[lengthFirstStr];
                char[] space2 = new char[lengthSecondStr];

                int loc1 = 0;
                int loc2 = 0;

                do {
                    space1[loc1++] = ch1;
                    index1++;

                    if (index1 < lengthFirstStr) {
                        ch1 = o1.charAt(index1);
                    } else {
                        break;
                    }
                } while (Character.isDigit(ch1) == Character.isDigit(space1[0]));

                do {
                    space2[loc2++] = ch2;
                    index2++;

                    if (index2 < lengthSecondStr) {
                        ch2 = o2.charAt(index2);
                    } else {
                        break;
                    }
                } while (Character.isDigit(ch2) == Character.isDigit(space2[0]));

                String str1 = new String(space1);
                String str2 = new String(space2);

                int result;

                if (Character.isDigit(space1[0]) && Character.isDigit(space2[0])) {
                    Integer firstNumberToCompare = Integer.parseInt(str1.trim());
                    Integer secondNumberToCompare = Integer.parseInt(str2.trim());
                    result = firstNumberToCompare.compareTo(secondNumberToCompare);
                } else {
                    result = str1.compareTo(str2);
                }

                if (result != 0) {
                    return result;
                }
            }
            return lengthFirstStr - lengthSecondStr;
            }).collect(Collectors.toList())){

            if (out.length() > 0)  out += ", ";
            out += s;
        }
		// ---- END Sort ----


    	return out;
	}


	/**
	 * returns demand from DB
	 * @param year Selected year
	 * @param week Selected week
	 * @param schedulingDuration Selected duration
	 */
	@SneakyThrows
	private synchronized CachedRowSet getDemandFromDatabase(int year, int week, int schedulingDuration){
		week = Math.max(week,0);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.WEEK_OF_YEAR, week);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.add(Calendar.WEEK_OF_YEAR, schedulingDuration);
		int endYear = cal.get(Calendar.YEAR);
		int endWeek = cal.get(Calendar.WEEK_OF_YEAR)-1;
		int startYearWeek = Integer.parseInt(year + "" + (week < 10 ? "0" + week : week));
		int endYearWeek = Integer.parseInt(endYear + "" + (endWeek < 10 ? "0" + endWeek : endWeek));

		PreparedStatement statement = null;
		ResultSet resultSet = null;
		//String query = "SELECT week, year, CAST(year || SUBSTR('00' || week, -2, 2) AS INTEGER) AS year_week, Demand.article, MAX((demand-stock),0) AS correct_demand, deadline, arrival, weight " +
		String query = "SELECT job_id, week, year, CAST(year || SUBSTR('00' || week, -2, 2) AS INTEGER) AS year_week, Demand.article, demand AS correct_demand, stock,  deadline, arrival, weight " +
				"FROM Demand " +
				"WHERE correct_demand > 0 " +
				"AND year_week >= ? " +
				"AND year_week <= ? " +
				"ORDER BY year,week,Demand.article";
		try{
			statement = connection.prepareStatement(query);
			statement.setInt(1,startYearWeek);
			statement.setInt(2,endYearWeek);
			resultSet = statement.executeQuery();
			CachedRowSet crs = RowSetProvider.newFactory().createCachedRowSet();
			crs.populate(resultSet);
			return crs;
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			if(resultSet != null){ resultSet.close(); }
			if(statement != null){ statement.close(); }
		}
	}

    /**
     * Returns opt results
     */
    public synchronized List<OPT_ResultsObject> getOPTResults(File pDatabaseName){
        List<OPT_ResultsObject> resultsList = new LinkedList<>();
        Connection currentDatabaseConnection = establishConnectionWithDatabase(pDatabaseName);
        String query = "SELECT run_id, run_name, calc_date, machine_env, alg_string, week, year, duration, steps FROM OPT_results";
        CachedRowSet data = readFromDatabase(query,currentDatabaseConnection);
        try{
            while(data.next()){
                OPT_ResultsObject resultsObject = new OPT_ResultsObject(
                        data.getInt("run_id"),
                        data.getString("run_name"),
                        new Date(1000L*data.getLong("calc_date")),
                        MachineEnvironment.getMachineEnvironmentByName(data.getString("machine_env")),
                        data.getString("alg_string"),
                        data.getInt("week"),
                        data.getInt("year"),
                        data.getInt("duration")
                        //data.getInt("steps")
                        );
                resultsList.add(resultsObject);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return resultsList;
    }

	public synchronized List<ExecutedAlgorithmData> getExecutedAlgorithmData(int runId){
		List<ExecutedAlgorithmData> output = new ArrayList<>();
		String query = "SELECT algorithm_id, iterations, runtime, alg_name FROM iterations_per_algorithm WHERE run_id = '" + runId + "'";
		CachedRowSet data = readFromDatabase(query,connectionResultDB);
		try{
			while(data.next()){
				int iterations = data.getInt("iterations");
				int runtime = data.getInt("runtime");
				String algName = data.getString("alg_name");
				output.add(new ExecutedAlgorithmData(algName,iterations,runtime));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return output;
	}

	public synchronized List<ObjectiveValueWrapper> getObjectiveValuesForRun (int runId){
		List<ObjectiveValueWrapper> output = new ArrayList<>();
		String query = "SELECT Cmax, Cbar, Lmax, Fmax, Ubar FROM OPT_obj_values WHERE run_id = '" + runId + "' AND algorithm_id = -1 AND iteration_id = -1";
		CachedRowSet data = readFromDatabase(query,connectionResultDB);
		try{
			while(data.next()){
				int cMax = Math.round(data.getFloat("Cmax"));
				int cBar = Math.round(data.getFloat("Cbar"));
				int lMax = Math.round(data.getFloat("Lmax"));
				int fMax = Math.round(data.getFloat("Fmax"));
				int uBar = Math.round(data.getFloat("Ubar"));
				output.add(new ObjectiveValueWrapper("Cmax",cMax));
				output.add(new ObjectiveValueWrapper("Cbar",cBar));
				output.add(new ObjectiveValueWrapper("Lmax",lMax));
				output.add(new ObjectiveValueWrapper("Fmax",fMax));
				output.add(new ObjectiveValueWrapper("Ubar",uBar));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return output;
	}

	/**
	 * Saves schedule in in 'connectionResultDB' (synchronized)
	 */
	@SneakyThrows
	public synchronized void saveCurrentScheduleStateInDatabase(Schedule pSchedule, Algorithm pAlgorithm, List<autoschedule.control.database.OPT_ObjValuesObject> pCachedObjectiveValues, int pRunId, int pAlgorithmId){

		if(pSchedule.getJobs().isEmpty()){
			throw new IllegalArgumentException("A schedule without jobs is not a valid input.");
		}
		List<String> updates = new ArrayList<>();
		List<Object> parameter = new ArrayList<>();

		//OPT_obj_values
		if(!pSchedule.getSchedulingObjectives().isEmpty()) {
			String query = "INSERT INTO OPT_obj_values (run_id, algorithm_id, iteration_id";
			String values = "VALUES(?,?,?";

			if (!pSchedule.getSchedulingObjectives().isEmpty()) {
				query += ", " + pSchedule.getSchedulingObjectives().stream().map(SchedulingObjective::getDatabaseColumnName).collect(Collectors.joining(","));
				values += ", " + pSchedule.getSchedulingObjectives().stream().map(obj -> "?").collect(Collectors.joining(","));
			}
			query += ") " + values + ")";
			parameter.add(pRunId);
			parameter.add(pAlgorithmId);
			parameter.add(-1);
			pSchedule.getSchedulingObjectives().forEach(obj -> parameter.add((int) Math.round(obj.evaluate(pSchedule))));
			updates.add(query);
		}

		if(!pCachedObjectiveValues.isEmpty()){
			String query = "INSERT INTO OPT_obj_values (run_id, algorithm_id, iteration_id, Cmax, Cbar, Lmax, Fmax, Ubar) VALUES ";
			List<String> valueList = new LinkedList<>();
			int iterationCounter = 1;
			for(autoschedule.control.database.OPT_ObjValuesObject objValuesObject : pCachedObjectiveValues){
				valueList.add("(?,?,?,?,?,?,?,?)");
				parameter.add(pRunId);
				parameter.add(pAlgorithmId);
				parameter.add(iterationCounter++);
				parameter.add(objValuesObject.getCMax());
				parameter.add(objValuesObject.getCBar());
				parameter.add(objValuesObject.getLMax());
				parameter.add(objValuesObject.getFMax());
				parameter.add(objValuesObject.getUBar());
			}
			query += String.join(",", valueList);
			updates.add(query);
		}

		//OPT_schedule
		String query = "INSERT INTO OPT_schedule(run_id, algorithm_id, job_id, article, machine, start_time, end_time, machine_arrival, production_amount) ";
		List<String> valueList = new ArrayList<>();
		for(Job job : pSchedule.getJobs()){
			for(Article article : job.getArticles()){
				valueList.add("(?,?,?,?,?,?,?,?,?)");
				parameter.add(pRunId);
				parameter.add(pAlgorithmId);
				parameter.add(job.getId());
				parameter.add(article.getArticleName());
				try{
					parameter.add(article.getScheduledMachine().getName());
				}
				catch (NullPointerException e){
					e.printStackTrace();
				}
				parameter.add(article.getScheduledTime());
				parameter.add(article.getFinishTime());
				parameter.add(article.getScheduledMachine().getArrival());
				parameter.add(article.getProductionAmount());
			}
		}
		if(!valueList.isEmpty()){
			query += "VALUES ";
			query += String.join(",", valueList);
			updates.add(query);
		}

		//OPT_algorithm_config
		AlgorithmConfig config = pAlgorithm.getAlgorithmConfig();
		if(config != null) {
			updates.add("INSERT INTO OPT_algorithm_data (run_id,algorithm_id,alg_config,runtime) VALUES(?,?,?,?)");
			parameter.add(pRunId);
			parameter.add(pAlgorithmId);
			parameter.add(config);
			parameter.add(pAlgorithm.getRuntimeMillis());
		}
		manipulate(updates,parameter,connectionResultDB);
	}

	/**
	 * Saves schedule metadata in 'connectionResultDB' (synchronized)
	 */
	public synchronized void saveScheduleMetadataAndResults(Schedule pSchedule, int pRunId){
		//OPT_Constraints
		if(pSchedule.getJobs().isEmpty()){
			throw new IllegalArgumentException("A schedule without jobs is not a valid input.");
		}
		List<String> updates = new ArrayList<>();
		List<Object> parameter = new ArrayList<>();

		updates.add("INSERT INTO OPT_constraints (run_id,prmu,skip,rj,rm,Mj) VALUES(?,?,?,?,?,?)");
		parameter.add(pRunId);
		parameter.add(pSchedule.getActiveConstraints().contains( Constraint.PRMU ));
		parameter.add(pSchedule.getActiveConstraints().contains( Constraint.SKIP ));
		parameter.add(pSchedule.getActiveConstraints().contains( Constraint.JOB_ARRIVAL ));
		parameter.add(pSchedule.getActiveConstraints().contains( Constraint.MACHINE_ARRIVAL ));
		parameter.add(pSchedule.getActiveConstraints().contains( Constraint.MJ ));

		//OPT_results
		updates.add("INSERT INTO OPT_results (run_id, run_name, calc_date, machine_env, alg_string, week, year, duration, steps) VALUES(?,?,?,?,?,?,?,?,?)");
		String run_name = pSchedule.getScheduleName();

		parameter.add(pRunId);
		parameter.add(run_name == null ? new NullValue(String.class) : run_name);
		parameter.add(System.currentTimeMillis()/1000L);	//Jetzt wird Calc_Date als die Zeit bestanden zu der das Schedule berechnet/gespeichert wurde
		parameter.add(pSchedule.getMachineEnvironment().getName());
		parameter.add(pSchedule.getAlgorithmHistoryString());
		parameter.add(pSchedule.getWeek());
		parameter.add(pSchedule.getYear());
		parameter.add(pSchedule.getSchedulingDuration());
		parameter.add(pSchedule.getAlgorithmsHistory().size());

		//OPT_obj_values
		OPT_ObjValuesObject endResult = new OPT_ObjValuesObject(
				pRunId,
				pSchedule.getEvaluatedObjectives().getOrDefault(new CMax(), -1D),
				pSchedule.getEvaluatedObjectives().getOrDefault(new CBar(), -1D),
				pSchedule.getEvaluatedObjectives().getOrDefault(new LMax(), -1D),
				pSchedule.getEvaluatedObjectives().getOrDefault(new FMax(), -1D),
				pSchedule.getEvaluatedObjectives().getOrDefault(new UBar(), -1D)
		);
		String query = "INSERT INTO OPT_obj_values (run_id, algorithm_id, iteration_id, Cmax, Cbar, Lmax, Fmax, Ubar) VALUES ";
		List<String> valueList = new LinkedList<>();
		valueList.add("(?,?,?,?,?,?,?,?)");
		parameter.add(pRunId);
		parameter.add(-1);
		parameter.add(-1);
		parameter.add(endResult.getCMax());
		parameter.add(endResult.getCBar());
		parameter.add(endResult.getLMax());
		parameter.add(endResult.getFMax());
		parameter.add(endResult.getUBar());
		query = query.concat(String.join(",", valueList));
		updates.add(query);


		//OPT_algorithm_data
		query = "INSERT INTO OPT_algorithm_data (run_id, algorithm_id, runtime, alg_name) VALUES ";
		valueList.clear();
		int algorithm_id = 1;
		for(Map.Entry<Algorithm,Integer> algEntry : pSchedule.getAlgorithmsHistory()){
			Algorithm alg = algEntry.getKey();
			long runtime = alg.getRuntimeMillis();
			String algName = alg.getSimpleString();
			valueList.add("(?,?,?,?)");
			parameter.add(pRunId);
			parameter.add(algorithm_id);
			parameter.add(runtime);
			parameter.add(algName);
			algorithm_id++;
		}
		query = query.concat(String.join(",", valueList));
		updates.add(query);


		valueList.clear();
		List<Job> jobs = pSchedule.getJobs();
		query = "INSERT INTO OPT_job_data (run_id, job_id, deadline, weight, arrival) VALUES ";
		for(Job job : jobs){
			valueList.add("(?,?,?,?,?)");
			parameter.add(pRunId);
			parameter.add(job.getId());
			parameter.add(job.getDeadline());
			parameter.add(job.getWeight());
			parameter.add(job.getArrival());
		}
		query = query.concat(String.join(",", valueList));
		updates.add(query);


		valueList.clear();
		List<Article> articles = pSchedule.getAllArticles();
		query = "INSERT INTO OPT_machine_qualifications (run_id,article,machine) VALUES ";
		List<Map.Entry<String,String>> qualifications = new LinkedList<>();
		for(Article article : articles){
			for(Machine machine : article.getQualifiedMachines()){
				String articleName = article.getArticleName();
				String machineName = machine.getName();
				Map.Entry<String,String> key = new AbstractMap.SimpleEntry<>(articleName,machineName);
				if(!qualifications.contains(key)){
					qualifications.add(key);
					valueList.add("(?,?,?)");
					parameter.add(pRunId);
					parameter.add(articleName);
					parameter.add(machineName);
				}
			}
		}
		query = query.concat(String.join(",", valueList));
		updates.add(query);

		manipulate(updates,parameter,connectionResultDB);
	}


	/**
	 * Updates database specified by 'conn' according to the given list of updates and parameters.
	 */
	public synchronized void manipulate(List<String> updates, List<Object> parameters, Connection conn){
		if(!updates.isEmpty()) {
			try{
				boolean commit = true;
				int currentParameter = 0;
				conn.setAutoCommit(false);

				for (String update : updates) {
					try (PreparedStatement statement = conn.prepareStatement(update)) {
						for (int i = 1; i <= statement.getParameterMetaData().getParameterCount(); i++) {
							setParameter(statement, i, parameters.get(currentParameter));
							currentParameter++;
						}
						statement.executeUpdate();
					} catch (Exception e) {
						e.printStackTrace();
						commit = false;
					}
				}

				if (commit) {
					conn.commit();
				} else {
					conn.rollback();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			finally{
				try {
					conn.setAutoCommit(true);
				}
				catch (Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Sets parameter 'statement' at index 'index' to value 'parameter'
	 */
	private static synchronized void setParameter(PreparedStatement statement, int index, Object parameter) throws SQLException, IOException {

		if(index < 0 || index > statement.getParameterMetaData().getParameterCount()){
			throw new IllegalArgumentException("Ein Parameter mit dem angegebenen Index existiert nicht!");
		}

		if(parameter == null){
			throw new IllegalArgumentException("'null' ist kein gueltiger Parameter Typ!");
		}

		Class<?> parameterClass = parameter.getClass();

		if(parameterClass.equals(autoschedule.control.database.NullValue.class)){
			autoschedule.control.database.NullValue nullValue = (autoschedule.control.database.NullValue)parameter;
			statement.setNull(index, nullValue.getSQLType());
		}
		else if(parameterClass.equals(String.class)){
			statement.setString(index, (String)parameter);
		}
		else if(parameterClass.equals(Integer.class)){
			statement.setInt(index, (Integer)parameter);
		}
		else if(parameterClass.equals(Double.class)){
			statement.setDouble(index, (Double)parameter);
		}
		else if(parameterClass.equals(java.util.Date.class)){
			java.sql.Date date = new java.sql.Date(((java.util.Date)parameter).getTime());
			statement.setDate(index, date);
		}
		else if(parameterClass.equals(java.sql.Date.class)){
			statement.setDate(index,(java.sql.Date)parameter);
		}
		else if(parameterClass.equals(Boolean.class)){
			statement.setBoolean(index, (Boolean)parameter);
		}
		else if(parameterClass.equals(Long.class)){
			statement.setLong(index, (Long)parameter);
		}
		else if(parameterClass.equals(AlgorithmConfig.class)){
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(parameter);
			byte[] configAsBytes = baos.toByteArray();
			ByteArrayInputStream bais = new ByteArrayInputStream(configAsBytes);
			statement.setBinaryStream(index,bais,configAsBytes.length);
		}
		else {
			throw new IllegalArgumentException("Parameter not supported: " + parameterClass);
		}

	}

	/**
	 * Queries DB and returns a CachedRowSet containing the result data.
	 */
	@SneakyThrows
	public synchronized CachedRowSet readFromDatabase(String query, Connection conn){
		Statement statement = conn.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		CachedRowSet crs = RowSetProvider.newFactory().createCachedRowSet();
		crs.populate(resultSet);
		return crs;
	}

	@SneakyThrows
	public List<OPT_ObjValuesObject> getSolutionProgress(int runId) {
		List<OPT_ObjValuesObject> list = new ArrayList<>();
		CachedRowSet cachedRowSet = readFromDatabase("SELECT Cmax, Cbar, Lmax, Fmax, Ubar FROM OPT_obj_values WHERE run_id=" + runId + " AND iteration_id > -1", connectionResultDB);

		while(cachedRowSet.next()) {
			list.add(new OPT_ObjValuesObject(runId,
					cachedRowSet.getDouble("Cmax"),
					cachedRowSet.getDouble("Cbar"),
					cachedRowSet.getDouble("Lmax"),
					cachedRowSet.getDouble("Fmax"),
					cachedRowSet.getDouble("Ubar")));
		}
		return list;
	}
}
