package autoschedule.control;

import autoschedule.model.Schedule;
import autoschedule.control.database.dbConnectorFactory;
import autoschedule.view.viewmodel.ViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Getter;
import lombok.Setter;

public class SchedulingController {
	@Getter
	private final ClassificationController classificationController;
	@Getter
	private final OptimizationRunner optimizationRunner;
	@Getter
	private final DatabaseConnector databaseConnector;
	@Getter
	private final ObservableList<Schedule> schedulesQueue;
	@Getter
	private final ViewModel viewModel;
	@Getter @Setter
	private Schedule templateSchedule;

	public SchedulingController(ViewModel viewModel) {
		this.classificationController = new ClassificationController();
		this.optimizationRunner = new OptimizationRunner(this, viewModel);
		this.databaseConnector = dbConnectorFactory.createDatabaseConnector();
		this.schedulesQueue = FXCollections.observableArrayList();
		this.viewModel = viewModel;
	}
}
