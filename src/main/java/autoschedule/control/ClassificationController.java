package autoschedule.control;

import autoschedule.model.*;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ClassificationController {

	/**
	 * Classifies given schedule as MachineEnvironment
	 * @param schedule - the given schedule for the computation
	 */
	public MachineEnvironment classify(Schedule schedule) {
		if(!validate(schedule)) {
			return MachineEnvironment.UNSUPPORTED;
		}

		if(isFlexible(schedule)) {
			if(isFlexibleFlowShop(schedule)) {
				if(isParallelMachines(schedule))
					return MachineEnvironment.PARALLEL_MACHINES;
				else
					return MachineEnvironment.FLEXIBLE_FLOW_SHOP;
			}else
				return MachineEnvironment.FLEXIBLE_JOB_SHOP;
		} else {
			if(isFlowShop(schedule)) {

				if (isSingleMachine(schedule))
					return MachineEnvironment.SINGLE_MACHINE;
				else
					return MachineEnvironment.FLOW_SHOP;
			} else
				return MachineEnvironment.JOB_SHOP;
		}
	}

	/**
	 * Checks for FlexibleFlowShop
	 * @param schedule - the given schedule for the computation
	 */
	private boolean isFlexibleFlowShop(Schedule schedule) {
		List<Article> allArticlesList = schedule.getJobs()
				.stream()
				.map(Job::getArticles)
				.flatMap(List::stream)
				.distinct()
				.collect(Collectors.toList());

		for(Article articleOne : allArticlesList) {
			for(Article articleTwo : allArticlesList) {
				if(articleOne.getStage() != articleTwo.getStage()) {
					for(Machine machine : articleTwo.getQualifiedMachines()) {
						if(articleOne.getQualifiedMachines().contains(machine)) {
							return false;
						}
					}
				}
			}
		}

		List<Integer> stageOrder = schedule.getJobs().get(0).getArticles()
				.stream()
				.map(Article::getStage)
				.collect(Collectors.toList());

		for(int i = 1; i < schedule.getJobs().size()-1; i++) {
			Job job = schedule.getJobs().get(i);
			for (int j = 0; j < job.getArticles().size() - 1; j++) {
				if (stageOrder.get(j) != job.getArticles().get(j).getStage()) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Checks for ParallelMachines
	 * @param schedule - the given schedule for the computation
	 */
	private boolean isParallelMachines(Schedule schedule) {
		return schedule.getJobs().stream().map(Job::getArticles).flatMap(Collection::stream).noneMatch(article -> article.getStage() > 1);
	}

	/**
	 * Checks for SingleMachine
	 * @param schedule - the given schedule for the computation
	 */
	private boolean isSingleMachine(Schedule schedule) {
		return schedule.getAllMachines().size() == 1;
	}

	/**
	 * Validates schedule
	 *
	 * min 1 Job
	 * no deadline < -1
	 * no job arrival < 0
	 * min 1 Article
	 * no null machine in article's qualification list
	 */
	private boolean validate(Schedule schedule) {
		if(schedule.getJobs() == null || schedule.getJobs().size() == 0)
			return false;

		for(Job job : schedule.getJobs()) {
			if(job == null || job.getName() == null || job.getDeadline() < -1 || job.getArrival() < 0) //release-date?
				return false;

			if(job.getArticles() == null || job.getArticles().size() <= 0)
				return false;

			for(Article article : job.getArticles()) {
				if(article == null || article.getArticleName() == null || article.getQualifiedMachines().size() == 0 /*|| article.getProcessingTime() <= 0*/)
					return false;

				for(Machine machine : article.getQualifiedMachines()) {
					if(machine == null || machine.getName() == null)
						return false;
				}
			}
		}
		return true;
	}

	/**
	 * Checks for flexibility
	 */
	private boolean isFlexible(Schedule schedule) {
		return schedule.getJobs().stream().anyMatch(job -> job.getArticles().stream().anyMatch(article -> article.getQualifiedMachines().size() > 1));
	}

	/**
	 * Checks for FlowShop
	 * @param schedule - for the computation
	 */
	private boolean isFlowShop(Schedule schedule) {
		for(Job job : schedule.getJobs()){
			//For every job, determine if all stages are visited in ascending order
			List<Integer> jobStageOrder = job.getStageOrder();
			boolean sorted = jobStageOrder.stream().sorted().collect(Collectors.toList()).equals(jobStageOrder);
			if(!sorted){
				return false;
			}
		}
		return true;
	}
}
