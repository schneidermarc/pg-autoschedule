package autoschedule.utils.parameteropt;

import autoschedule.utils.Tuple;
import autoschedule.utils.parameteropt.specificValues.Yielder;
import lombok.Getter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the grid, where parameters are arranged to be optimized
 */
public class OptimizationGrid {

	/**
	 * Contains all optimization configurations
	 */
	@Getter
	private final ArrayList<OptimizationConfiguration> gridlist = new ArrayList<>();

	/**
	 * Build an Optimization-Grid based on an Fields-Array. Some Fields are nested (GA-probabilities).
	 *
	 * @param fields Array of Fields, that was previously extracted from the Viewmodel
	 */
	public OptimizationGrid( List<Tuple<Field, Yielder<?,? extends Annotation>>> fields ) {
		int index = 0;

		Tuple<Field, Yielder<?,? extends Annotation>> tuple = fields.get( index );

		Object obj;
		while( (obj = tuple.getSecond().yieldFunction()) != null ) {
			List<Tuple<Field, Object>> allParameters = new ArrayList<>();
			allParameters.add( new Tuple<>( tuple.getFirst(), obj ) );
			rec( fields, index + 1, allParameters );
		}
	}

	private void rec( List<Tuple<Field, Yielder<?,? extends Annotation>>> fields, int index, List<Tuple<Field, Object>> allParameters ) {
		if( fields.size() == index ) {
			gridlist.add( new OptimizationConfiguration( new ArrayList<>( allParameters ) ) );
		} else {
			Tuple<Field, Yielder<?,? extends Annotation>> tuple  = fields.get( index );

			Object obj;
			while( (obj = tuple.getSecond().yieldFunction()) != null ) {
				Tuple<Field, Object> tpl = new Tuple<>( tuple.getFirst(), obj );
				allParameters.add( tpl );
				rec( fields, index + 1, allParameters );
				allParameters.remove( tpl );
			}
			tuple.getSecond().reset();
		}
	}

}
