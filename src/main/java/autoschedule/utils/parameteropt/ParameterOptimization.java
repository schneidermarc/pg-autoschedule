package autoschedule.utils.parameteropt;

import autoschedule.algorithms.Algorithm;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import autoschedule.utils.Tuple;
import autoschedule.utils.logging.Logger;
import autoschedule.utils.parameteropt.specificValues.Yielder;
import javafx.beans.property.SimpleObjectProperty;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author cstockhoff
 */
public class ParameterOptimization {

	/**
	 * Algorithm, which parameters will be optimized
	 */
	private final Algorithm algorithm;

	public ParameterOptimization( Algorithm algorithm ) {
		this.algorithm = algorithm;
	}

	/**
	 * Loops through every parameter, that was chosen to be optimized, and builds a optimization grid, hoping to find the best configuration
	 * Gets called right before executing the algorithm
	 * This method is implemented individually for every algorithm in sub-classes
	 */
	public List<OPT_ObjValuesObject> executeParameterOptimization( Schedule schedule ) {
		if( algorithm.getModel() == null ) {
			return algorithm.run( schedule );
		}

		List<Tuple<Field, Yielder<?, ? extends Annotation>>> allParameters = scanParametersToOptimize();

		//If we don't have any parameters, that need to be optimized
		if( allParameters.size() == 0 )
			return algorithm.run( schedule );

		Logger.system( "Execute a Parameter-Optimization for " + algorithm.getSimpleString() + "!" );
		OptimizationGrid grid = new OptimizationGrid( allParameters );

		List<OptimizationResult> results = new ArrayList<>();

		// run the algorithm with the different grid-parameters
		runWithDifferentParams( schedule, grid, results );

		// get the best result
		results.sort( Comparator.comparingDouble( a -> a.getObjective().evaluate( a.getSchedule() ) ) );
		OptimizationResult bestResult = results.size() > 1 ? results.get( 1 ) : results.get( 0 );

		JSONOptimizationConfig.saveConfiguration( bestResult.getConfig(), algorithm );

		return bestResult.getObjValues();
	}

	private void runWithDifferentParams( Schedule schedule, OptimizationGrid grid, List<OptimizationResult> results ) {
		// iterate over all parameter combinations and perform a execute(schedule);
		for( OptimizationConfiguration config : grid.getGridlist() ) {
			for( Tuple<Field, Object> fieldValues : config.getAllParameters() ) {
				try {
					fieldValues.getFirst().setAccessible( true );

					fieldValues.getFirst().set( algorithm.getModel(),
							autoCast(fieldValues.getSecond() )
					);
				} catch( IllegalAccessException e ) {
					Logger.error( e );
				}
			}

			// run with new parameters
			OptimizationResult result = new OptimizationResult( algorithm.run( schedule ), config, algorithm.getObjective(), schedule );

			results.add( result );
		}
	}

	private static <T> SimpleObjectProperty<T> autoCast(T value) {
		return new SimpleObjectProperty<>( value );
	}

	/**
	 * Scans the corresponding view model and finds the fitting viewModel
	 *
	 * @return An array of fields, which contains information about the name, and set-annotations
	 */
	protected List<Tuple<Field, Yielder<?, ? extends Annotation>>> scanParametersToOptimize() {
		List<Tuple<Field, Yielder<?, ? extends Annotation>>> fields = new ArrayList<>();
		Class<?> clazz = algorithm.getModel().getClass();

		// Reverse the order of the clazz hierarchy
		// Reason: Displaying fields of sub-classes are the first fields
		// and the specific fields are lower
		List<Class<?>> clazzes = new LinkedList<>();
		while( clazz != Object.class ) {
			clazzes.add( 0, clazz );
			// get next level class
			clazz = clazz.getSuperclass();
		}

		for( Class<?> c : clazzes ) {
			for( Field field : c.getDeclaredFields() ) {
				// only perform parameter-optimization on selected fields
				if( algorithm.getModel().getParameterOptimizationMap().get( field ) != null &&
						algorithm.getModel().getParameterOptimizationMap().get( field ).getValue() ) {

					for( Optimizer opt : Optimizer.values() ) {
						if( field.isAnnotationPresent( opt.clazz ) ) {
							fields.add( new Tuple<>( field,
									opt.yielder.clone( opt.yielder.cast(field.getAnnotation( opt.clazz ) ) ) ) );
						}
					}
				}
			}
		}

		return fields;
	}
}
