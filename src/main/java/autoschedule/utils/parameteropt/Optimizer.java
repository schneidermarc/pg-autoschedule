package autoschedule.utils.parameteropt;

import autoschedule.utils.parameteropt.specificValues.DoubleIntervalYielder;
import autoschedule.utils.parameteropt.specificValues.IntIntervalYielder;
import autoschedule.utils.parameteropt.specificValues.Yielder;

import java.lang.annotation.Annotation;

/**
 * @author cstockhoff
 */
public enum Optimizer {
	IntInterval( OptimizationAnnotations.IntIntervalParameter.class, new IntIntervalYielder() ),
	DoubleInterval( OptimizationAnnotations.DoubleIntervalParameter.class, new DoubleIntervalYielder() );

	public final Class<? extends Annotation> clazz;

	final Yielder<?, ? extends Annotation> yielder;

	Optimizer(Class<? extends Annotation> clazz, Yielder<?, ? extends Annotation> yielder ) {
		this.clazz = clazz;
		this.yielder = yielder;
	}
}
