package autoschedule.utils.parameteropt;

import autoschedule.utils.Tuple;
import lombok.Getter;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Class which contains a single Configuration for an Algorithm in form of
 * a list with fields (to be optimized) and their values
 */
public class OptimizationConfiguration {

	/**
	 * List of underlying fields with their given values
	 * For example:
	 * {(maxIterations, 5000), (tabuSize, 5}
	 */
	@Getter
	private final List<Tuple<Field, Object>> allParameters;

	public OptimizationConfiguration( List<Tuple<Field, Object>> allParameters) {
		this.allParameters = allParameters;
	}

}
