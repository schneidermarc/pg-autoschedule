package autoschedule.utils.parameteropt;

import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.control.database.OPT_ObjValuesObject;
import autoschedule.model.Schedule;
import autoschedule.utils.parameteropt.OptimizationConfiguration;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class OptimizationResult {

	@Getter
	private final List<OPT_ObjValuesObject> objValues;

	@Getter
	private final OptimizationConfiguration config;

	@Getter
	private final SchedulingObjective objective;

	@Getter
	private final Schedule schedule;

	public OptimizationResult( List<OPT_ObjValuesObject> objValues, OptimizationConfiguration config, SchedulingObjective objective, Schedule schedule ) {
		this.objValues = new ArrayList<>(objValues);
		this.config = config;
		this.objective = objective;
		this.schedule = schedule.copy();
	}
}
