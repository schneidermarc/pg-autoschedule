package autoschedule.utils.parameteropt;

import autoschedule.algorithms.Algorithm;
import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class JSONOptimizationConfig {

	/**
	 * Name of the Configuration-File containing all ascertained configurations
	 */
	private static final String CONFIG_FILENAME = "configurations.json";

	/**
	 * Class of the Algorithm
	 */
	public Class<? extends Algorithm> algorithmClazz;

	/**
	 * All Parameter-Values to be saved mapped by their field-Name
	 * For example for the LocalSearchViewModel-Property maxIterationsSteepestDescent:
	 * "maxIterationsSteepestDescent" : 5
	 */
	public final Map<String, Object> parameters;

	public JSONOptimizationConfig() {
		parameters = new HashMap<>();
	}

	/**
	 * Build the Name of the configuration based on an AlgorithmViewModel
	 */
	public static String generateConfigName( AlgorithmViewModel avModel ) {
		if( avModel == null || avModel.getSelectedObjectiveFunction().getValue() == null )
			return "";

		String[] strings = new String[ 4 ];
		strings[ 0 ] = avModel.getClass().getSimpleName();
		strings[ 1 ] = avModel.getViewModel().getSelectedMachineEnvironment().getValue().getName();
		strings[ 2 ] = avModel.getSelectedObjectiveFunction().getValue().getSimpleName();

		int jobAmount = avModel.getViewModel().getSchedulingController().getTemplateSchedule().getJobs().size();
		strings[ 3 ] = String.valueOf( jobAmount / 50 );

		return generateConfigName( strings );
	}

	/**
	 * Build the Name of the configuration based on an given Strings
	 */
	public static String generateConfigName( String... args ) {
		return String.join( "_", args ).replace( " ", "" );
	}

	/**
	 * Save the best configuration as a JSON-Object
	 */
	@SuppressWarnings("ResultOfMethodCallIgnored")
	public static void saveConfiguration(OptimizationConfiguration configuration, Algorithm algorithm ) {
		ObjectMapper mapper = new ObjectMapper();

		try {
			StringBuilder filePath = new StringBuilder( "src" );
			filePath.append( File.separatorChar )
					.append( "main" )
					.append( File.separatorChar )
					.append( "resources" )
					.append( File.separatorChar )
					.append( "parameters" );

			File dir = new File( filePath.toString() );
			if( !dir.exists() )
				dir.mkdirs();

			filePath.append( File.separatorChar )
					.append( CONFIG_FILENAME );

			Map<String, JSONOptimizationConfig> jsonConfigurations = new HashMap<>();

			File configFile = new File( filePath.toString() );
			if( configFile.exists() ) {
				jsonConfigurations = mapper.readValue( new File( filePath.toString() ), new TypeReference<>() {
				} );
			}

			JSONOptimizationConfig currentConfig = jsonConfigurations.get( JSONOptimizationConfig.generateConfigName( algorithm.getModel() ) );
			if( currentConfig == null ) {
				currentConfig = new JSONOptimizationConfig();
				currentConfig.algorithmClazz = algorithm.getClass();
				jsonConfigurations.put( JSONOptimizationConfig.generateConfigName( algorithm.getModel() ), currentConfig );

			}
			JSONOptimizationConfig finalCurrentConfig = currentConfig;
			configuration.getAllParameters().forEach( tpl -> finalCurrentConfig.parameters.put( tpl.getFirst().getName(), tpl.getSecond() ) );

			mapper.writerWithDefaultPrettyPrinter().writeValue( new File( filePath.toString() ), jsonConfigurations );

		} catch( IOException e ) {
			e.printStackTrace();
		}
	}

	/**
	 * Load the best configuration into a Algorithm
	 */
	@SuppressWarnings("UnusedReturnValue")
	public static boolean loadConfiguration( AlgorithmViewModel avModel ) {
		if( avModel == null )
			return false;

		ObjectMapper mapper = new ObjectMapper();

		try {
			StringBuilder filePath = new StringBuilder( "src" );
			filePath.append( File.separatorChar )
					.append( "main" )
					.append( File.separatorChar )
					.append( "resources" )
					.append( File.separatorChar )
					.append( "parameters" )
					.append( File.separatorChar )
					.append( CONFIG_FILENAME );

			File dir = new File( filePath.toString() );
			if( !dir.exists() )
				return false;

			Map<String, JSONOptimizationConfig> configurations = mapper.readValue( new File( filePath.toString() ), new TypeReference<>() {
			} );

			if( configurations == null || configurations.get( JSONOptimizationConfig.generateConfigName( avModel ) ) == null )
				return false;

			JSONOptimizationConfig obj = configurations.get( JSONOptimizationConfig.generateConfigName( avModel ) );

			if( obj != null ) {
				for( Map.Entry<String, Object> entry : obj.parameters.entrySet() ) {
					try {
						ReflectionUtils.setValueOfFieldByName( avModel, entry.getKey(), entry.getValue() );
					} catch( NoSuchFieldException e ) {
						e.printStackTrace();
					}
				}
			}
		} catch( IOException e ) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
}
