package autoschedule.utils.parameteropt.specificValues;

import autoschedule.utils.parameteropt.OptimizationAnnotations.IntIntervalParameter;

/**
 * @author cstockhoff
 */
public class IntIntervalYielder
		implements Yielder<Integer, IntIntervalParameter> {

	private boolean set = false;
	private int currentValue = 0;

	private IntIntervalParameter annotation;

	@Override
	public Integer yieldFunction() {
		if( set && currentValue > annotation.endValue() )
			return null;

		if( !set ) {
			currentValue = annotation.startValue();
			set = true;
		} else {
			currentValue += annotation.stepSize();
		}

		return currentValue;
	}

	@Override
	public Yielder<Integer, IntIntervalParameter> clone(IntIntervalParameter annotation) {
		IntIntervalYielder yielder = new IntIntervalYielder();
		yielder.annotation = annotation;
		return yielder;
	}

	@Override
	public void reset() {
		this.set = false;
	}
}
