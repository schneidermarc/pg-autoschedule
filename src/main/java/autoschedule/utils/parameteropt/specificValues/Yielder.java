package autoschedule.utils.parameteropt.specificValues;

import java.lang.annotation.Annotation;

/**
 * @author cstockhoff
 */
public interface Yielder<T, S extends Annotation> {

	T yieldFunction();

	Yielder<T,S> clone(S annotation);

	@SuppressWarnings({"unchecked", "TypeParameterHidesVisibleType"})
	default <S> S cast(Annotation annotation) {
		return (S) annotation;
	}

	void reset();
}
