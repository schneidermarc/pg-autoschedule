package autoschedule.utils.parameteropt.specificValues;

import autoschedule.utils.parameteropt.OptimizationAnnotations;
import autoschedule.utils.parameteropt.OptimizationAnnotations.DoubleIntervalParameter;

/**
 * @author cstockhoff
 */
public class DoubleIntervalYielder
		implements Yielder<Double, DoubleIntervalParameter> {
	private boolean set = false;
	private double currentValue = 0;

	private OptimizationAnnotations.DoubleIntervalParameter annotation;

	public Double yieldFunction() {
		if( set && currentValue > annotation.endValue() )
			return null;

		if( !set ) {
			currentValue = annotation.startValue();
			set = true;
		} else {
			currentValue += annotation.stepSize();
		}

		return currentValue;
	}

	@Override
	public Yielder<Double, DoubleIntervalParameter> clone(DoubleIntervalParameter annotation) {
		DoubleIntervalYielder yielder = new DoubleIntervalYielder();
		yielder.annotation = annotation;
		return yielder;
	}

	@Override
	public void reset() {
		this.set = false;
	}
}
