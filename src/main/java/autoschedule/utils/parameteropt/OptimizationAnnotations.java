package autoschedule.utils.parameteropt;

import java.lang.annotation.*;

import static autoschedule.utils.parameteropt.Optimizer.*;

public class OptimizationAnnotations {

	@Inherited
	@Target( ElementType.ANNOTATION_TYPE )
	@Retention( RetentionPolicy.RUNTIME )
	public @interface OptimizeAnnotation {
		Optimizer optimizer();
	}

	@Inherited
	@Target( ElementType.FIELD )
	@Retention( RetentionPolicy.RUNTIME )
	@OptimizeAnnotation( optimizer = IntInterval )
	public @interface IntIntervalParameter {
		int startValue();

		int endValue();

		int stepSize() default 1;
	}

	@Inherited
	@Target( ElementType.FIELD )
	@Retention( RetentionPolicy.RUNTIME )
	@OptimizeAnnotation( optimizer = DoubleInterval )
	public @interface DoubleIntervalParameter {
		double startValue();

		double endValue();

		double stepSize() default 1;
	}

}
