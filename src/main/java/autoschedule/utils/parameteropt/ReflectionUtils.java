package autoschedule.utils.parameteropt;

import autoschedule.view.viewmodel.algorithmModels.AlgorithmViewModel;
import javafx.beans.property.Property;

import java.lang.reflect.Field;

public class ReflectionUtils {

	@SuppressWarnings("unchecked")
	public static <T> void setValueOfFieldByName(AlgorithmViewModel viewModel, String fieldName, T value )
			throws NoSuchFieldException {
		Class<?> clazz = viewModel.getClass();

		while( clazz != Object.class ) {
			try {
				Field field = clazz.getDeclaredField( fieldName );
				boolean canAccess = field.canAccess( viewModel );
				if( !canAccess )
					field.setAccessible( true );

				if( field.get( viewModel ) instanceof Property ) {
					Property<T> prop = (Property<T>) field.get(viewModel);
					prop.setValue( value );
				} else{
					field.set( viewModel, value );
				}

				// reset accessibility
				field.setAccessible( canAccess );
				return;
			} catch( NoSuchFieldException | IllegalAccessException e ) {
				clazz = clazz.getSuperclass();
			}
		}
		throw new NoSuchFieldException( fieldName );
	}
}
