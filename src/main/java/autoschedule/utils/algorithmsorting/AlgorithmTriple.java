package autoschedule.utils.algorithmsorting;

import autoschedule.algorithms.iterative.IterativeAlgorithm;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Move;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Neighborhood;

import java.util.Objects;

public class AlgorithmTriple {

	private final Class<? extends IterativeAlgorithm> algorithm;
	private final Class<? extends Neighborhood> neighborhood;
	private final Class<? extends Move> move;

	public AlgorithmTriple(Class<? extends IterativeAlgorithm> algorithm, Class<? extends Neighborhood> neighborhood, Class<? extends Move> move) {
		this.algorithm = algorithm;
		this.neighborhood = neighborhood;
		this.move = move;
	}

	public AlgorithmTriple(IterativeAlgorithm algorithm) {
		this.algorithm = algorithm.getClass();
		this.neighborhood = algorithm.getNeighborhood().getClass();
		this.move = algorithm.getNeighborhood().getMove().getClass();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof AlgorithmTriple)) return false;
		AlgorithmTriple that = (AlgorithmTriple) o;
		return Objects.equals(algorithm, that.algorithm) && Objects.equals(neighborhood, that.neighborhood) && Objects.equals(move, that.move);
	}

	@Override
	public int hashCode() {
		return Objects.hash(algorithm, neighborhood, move);
	}
}
