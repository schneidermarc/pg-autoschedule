package autoschedule.utils.algorithmsorting;

import autoschedule.algorithms.priorityRules.*;

import java.util.HashMap;

public class ConstructivePointMap extends HashMap<Class<? extends PriorityRule>, Integer> {
	public ConstructivePointMap(int spt, int wspt, int ect, int fcfs, int random, int wdd, int edd, int lpt) {
		this.put(SPT.class, spt);
		this.put(WSPT.class, wspt);
		this.put(ECT.class, ect);
		this.put(FCFS.class, fcfs);
		this.put(Random.class, random);
		this.put(WDD.class, wdd);
		this.put(EDD.class, edd);
		this.put(LPT.class, lpt);
	}
}
