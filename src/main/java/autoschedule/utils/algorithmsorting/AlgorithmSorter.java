package autoschedule.utils.algorithmsorting;

import autoschedule.algorithms.constructive.ConstructiveAlgorithm;
import autoschedule.algorithms.constructive.GifflerThompson;
import autoschedule.algorithms.constructive.NEH;
import autoschedule.algorithms.constructive.PriorityRuleAlgorithm;
import autoschedule.algorithms.constructive.ga.GA;
import autoschedule.algorithms.iterative.IterativeAlgorithm;
import autoschedule.algorithms.priorityRules.EDD;
import autoschedule.algorithms.priorityRules.PriorityRule;
import autoschedule.model.MachineEnvironment;
import autoschedule.model.Schedule;
import javafx.beans.property.ObjectProperty;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.Map;

@RequiredArgsConstructor
@SuppressWarnings({"DuplicatedCode", "EnhancedSwitchMigration", "MismatchedQueryAndUpdateOfCollection"})
public class AlgorithmSorter implements Comparator<Schedule> {

	private final ObjectProperty<MachineEnvironment> selectedMachineEnvironment;

	/* Constructive */
	private final Map<Class<? extends PriorityRule>, Integer> constructiveValuesParallelMachines = new ConstructivePointMap(3,3,3,2,2,2,2,1);

	private final Map<Class<? extends PriorityRule>, Integer> constructiveValuesFlowShop = new ConstructivePointMap(3,3,3,2,2,2,2,1);

	private final Map<Class<? extends PriorityRule>, Integer> constructiveValuesFlexibleFlowShop = new ConstructivePointMap(3,3,3,2,2,2,2,1);

	private final Map<Class<? extends PriorityRule>, Integer> constructiveValuesJobShop = new ConstructivePointMap(1,1,1,2,2,2,2,3);

	/* Iterative */
	private final Map<AlgorithmTriple, Integer> iterativeValuesParallelMachines = new IterativePointMap(16,15,13,12,11,10,16,14,11,10);

	private final Map<AlgorithmTriple, Integer> iterativeValuesFlowShop = new IterativePointMap(16,15,13,12,11,10,16,14,11,10);

	private final Map<AlgorithmTriple, Integer> iterativeValuesFlexibleFlowShop = new IterativePointMap(16,15,13,12,11,10,16,14,11,10);

	@Override
	public int compare(Schedule schedule1, Schedule schedule2) {
		return Integer.compare(compareOne(schedule2), compareOne(schedule1));
	}

	private int compareOne(Schedule schedule) {
		switch (selectedMachineEnvironment.get()) {
			case SINGLE_MACHINE:
				return compareForSingleMachine(schedule);

			case PARALLEL_MACHINES:
				return compareForParallelMachines(schedule);

			case FLOW_SHOP:
				return compareForFlowShop(schedule);

			case FLEXIBLE_FLOW_SHOP:
				return compareForFlexibleFlowShop(schedule);

			case JOB_SHOP:
				return compareForJobShop(schedule);

			default: throw new IllegalStateException("Unsupported machine environment: " + selectedMachineEnvironment.get());
		}
	}

	private int compareForSingleMachine(Schedule schedule) {
		//Prioritize EDD rule
		if(schedule.getAlgorithmsToRun().stream().filter(a -> a instanceof PriorityRuleAlgorithm).map(a -> ((PriorityRuleAlgorithm)a).getPriorityRule()).anyMatch(p -> p instanceof EDD)
			|| schedule.getAlgorithmsToRun().stream().filter(a -> a instanceof GifflerThompson).map(a -> ((GifflerThompson)a).getPriorityRule()).anyMatch(p -> p instanceof EDD))
			return 1;

		return 0;
	}

	private int compareForParallelMachines(Schedule schedule) {
		int points = 0;
		ConstructiveAlgorithm constructiveAlgorithm = (ConstructiveAlgorithm)schedule.getAlgorithmsToRun().get(0);

		if(constructiveAlgorithm instanceof PriorityRuleAlgorithm)
			points += constructiveValuesParallelMachines.get(((PriorityRuleAlgorithm)constructiveAlgorithm).getPriorityRule().getClass());

		else if(constructiveAlgorithm instanceof GA)
			points += constructiveValuesParallelMachines.values().stream().mapToInt(i->i).max().orElse(0) + 2;

		//Iterative
		if(schedule.getAlgorithmsToRun().size() > 1)
			points += iterativeValuesParallelMachines.get(new AlgorithmTriple((IterativeAlgorithm) schedule.getAlgorithmsToRun().get(1)));

		return points;
	}

	private int compareForFlowShop(Schedule schedule) {
		int points = 0;
		ConstructiveAlgorithm constructiveAlgorithm = (ConstructiveAlgorithm)schedule.getAlgorithmsToRun().get(0);

		if(constructiveAlgorithm instanceof PriorityRuleAlgorithm)
			points += constructiveValuesFlowShop.get(((PriorityRuleAlgorithm)constructiveAlgorithm).getPriorityRule().getClass());

		else if(constructiveAlgorithm instanceof GifflerThompson)
			points += constructiveValuesFlowShop.get(((GifflerThompson)constructiveAlgorithm).getPriorityRule().getClass());

		else if(constructiveAlgorithm instanceof NEH)
			points += constructiveValuesFlowShop.values().stream().mapToInt(i->i).max().orElse(0) + 1;

		else if(constructiveAlgorithm instanceof GA)
			points += constructiveValuesFlowShop.values().stream().mapToInt(i->i).max().orElse(0) + 2;

		//Iterative
		if(schedule.getAlgorithmsToRun().size() > 1)
			points += iterativeValuesFlowShop.get(new AlgorithmTriple((IterativeAlgorithm) schedule.getAlgorithmsToRun().get(1)));

		return points;
	}

	private int compareForFlexibleFlowShop(Schedule schedule) {
		int points = 0;
		ConstructiveAlgorithm constructiveAlgorithm = (ConstructiveAlgorithm)schedule.getAlgorithmsToRun().get(0);

		if(constructiveAlgorithm instanceof PriorityRuleAlgorithm)
			points += constructiveValuesFlexibleFlowShop.get(((PriorityRuleAlgorithm)constructiveAlgorithm).getPriorityRule().getClass());

		else if(constructiveAlgorithm instanceof GA)
			points += constructiveValuesFlexibleFlowShop.values().stream().mapToInt(i->i).max().orElse(0) + 2;

		//Iterative
		if(schedule.getAlgorithmsToRun().size() > 1)
			points += iterativeValuesFlexibleFlowShop.get(new AlgorithmTriple((IterativeAlgorithm) schedule.getAlgorithmsToRun().get(1)));

		return points;
	}

	private int compareForJobShop(Schedule schedule) {
		if(schedule.getAlgorithmsToRun().get(0) instanceof GifflerThompson)
			return constructiveValuesJobShop.get(((GifflerThompson) schedule.getAlgorithmsToRun().get(0)).getPriorityRule().getClass());
		else
			return 0;
	}
}