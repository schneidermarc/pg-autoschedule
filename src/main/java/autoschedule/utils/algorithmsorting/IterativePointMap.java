package autoschedule.utils.algorithmsorting;

import autoschedule.algorithms.iterative.localsearch.LocalSearch;
import autoschedule.algorithms.iterative.localsearch.SimulatedAnnealing;
import autoschedule.algorithms.iterative.localsearch.TabuSearch;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.RandomDescent;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Shift;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.SteepestDescent;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Swap;

import java.util.HashMap;

public class IterativePointMap extends HashMap<AlgorithmTriple, Integer> {
	public IterativePointMap(int sa_rd_shift, int sa_rd_swap,
	                         int sa_sd_shift, int sa_sd_swap,
	                         int ts_sd_shift, int ts_sd_swap,
	                         int ls_rd_shift, int ls_rd_swap,
	                         int ls_sd_shift, int ls_sd_swap) {
		this.put(new AlgorithmTriple(SimulatedAnnealing.class, RandomDescent.class, Shift.class), sa_rd_shift);
		this.put(new AlgorithmTriple(SimulatedAnnealing.class, RandomDescent.class, Swap.class), sa_rd_swap);
		this.put(new AlgorithmTriple(SimulatedAnnealing.class, SteepestDescent.class, Shift.class), sa_sd_shift);
		this.put(new AlgorithmTriple(SimulatedAnnealing.class, SteepestDescent.class, Swap.class), sa_sd_swap);
		this.put(new AlgorithmTriple(TabuSearch.class, SteepestDescent.class, Shift.class), ts_sd_shift);
		this.put(new AlgorithmTriple(TabuSearch.class, SteepestDescent.class, Swap.class), ts_sd_swap);
		this.put(new AlgorithmTriple(LocalSearch.class, RandomDescent.class, Shift.class), ls_rd_shift);
		this.put(new AlgorithmTriple(LocalSearch.class, RandomDescent.class, Swap.class), ls_rd_swap);
		this.put(new AlgorithmTriple(LocalSearch.class, SteepestDescent.class, Shift.class), ls_sd_shift);
		this.put(new AlgorithmTriple(LocalSearch.class, SteepestDescent.class, Swap.class), ls_sd_swap);
	}
}
