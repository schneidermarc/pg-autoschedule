package autoschedule.utils;

import lombok.Getter;
import lombok.Setter;

public class Tuple<T1, T2> {
	
	@Getter @Setter
	private T1 first;
	
	@Getter @Setter
	private T2 second;
	
	public Tuple(T1 first, T2 second) {
		this.first = first;
		this.second = second;
	}
}
