package autoschedule.utils;

import autoschedule.utils.logging.Logger;

public class OSValidator {

	private static final String OS = System.getProperty("os.name").toLowerCase();

	public static void main(String[] args) {

		Logger.message(OS);

		if (isWindows()) {
			Logger.message("This is Windows");
		} else if (isMac()) {
			Logger.message("This is Mac");
		} else if (isUnix()) {
			Logger.message("This is Unix or Linux");
		} else if (isSolaris()) {
			Logger.message("This is Solaris");
		} else {
			Logger.message("Your OS is not support!!");
		}
	}

	public static boolean isWindows() {
		return (OS.contains("win"));
	}

	public static boolean isMac() {
		return (OS.contains("mac"));
	}

	public static boolean isUnix() {
		return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
	}

	public static boolean isSolaris() {
		return (OS.contains("sunos"));
	}
}
