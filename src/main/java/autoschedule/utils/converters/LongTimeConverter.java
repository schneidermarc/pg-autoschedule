package autoschedule.utils.converters;

import autoschedule.algorithms.objectives.SchedulingObjective;
import autoschedule.algorithms.objectives.UBar;
import autoschedule.control.database.TableRowResultsOptimization;
import javafx.beans.property.ObjectProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LongTimeConverter<T extends Number> implements Callback<TableColumn<TableRowResultsOptimization, T>, TableCell<TableRowResultsOptimization, T>> {

	private final boolean hasMillis;

	private final ObjectProperty<SchedulingObjective> schedulingObjectiveProperty;

	@Override
	public TableCell<TableRowResultsOptimization, T> call(TableColumn<TableRowResultsOptimization, T> tableRowResultsOptimizationLongTableColumn) {
		return new TableCell<>() {
			@Override
			protected void updateItem(T time, boolean empty) {
				super.updateItem(time, empty);

				if (time == null || empty) {
					setText(null);
					setStyle("");
				} else {

					//Catch UBar
					if(!hasMillis && schedulingObjectiveProperty.get() instanceof UBar) {
						long numJobs = Math.round(time.doubleValue());
						setText(numJobs + " Job" + ((numJobs != 1) ? "s" : ""));
					}
					else
						setText(format(time, hasMillis, true));
				}
			}
		};
	}

	public static String format(Number number, boolean hasMillis, boolean showMillis) {
		long time = Math.round(number.doubleValue()) * (hasMillis ? 1 : 1000);

		if(time == 0)
			return "0s";

		StringBuilder stringBuilder = new StringBuilder();
		if (time > 604800000){
			stringBuilder.append(time / (604800000)).append("w ");
		}
		if (time > 86400000){
			stringBuilder.append((time % 604800000) / 86400000 ).append("d ");
		}
		if (time > 3600000){
			stringBuilder.append((time % 86400000) / 3600000).append("h ");
		}
		if (time > 60000){
			stringBuilder.append((time % 3600000) / 60000).append("m ");
		}
		if (time > 1000){
			stringBuilder.append(((time % 60000) / 1000)).append("s ");
		}
		if (hasMillis && showMillis){
			stringBuilder.append((time % 1000)).append("ms ");
		}
		return stringBuilder.toString();
	}
}