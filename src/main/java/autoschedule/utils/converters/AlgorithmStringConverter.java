package autoschedule.utils.converters;

import autoschedule.model.Schedule;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class AlgorithmStringConverter implements Callback<ListView<Schedule>, ListCell<Schedule>>{

    @Override
    public ListCell<Schedule> call(ListView<Schedule> param) {
        return new ListCell<>() {
            @Override
            protected void updateItem(Schedule schedule, boolean empty) {
                super.updateItem(schedule, empty);
                if (schedule != null) {
                    setText(schedule.getScheduleName());
                } else {
                    setText("");
                }
            }
        };
    }
}