package autoschedule.utils.converters;

import javafx.util.StringConverter;

public class IntegerStringConverter extends StringConverter<Number> {
    @Override
    public String toString(Number number) {
        return number.toString();
    }

    @Override
    public Number fromString(String s) {
        if(s.length() != 0){
            return Integer.parseInt(s);
        }
        return 0;
    }
}