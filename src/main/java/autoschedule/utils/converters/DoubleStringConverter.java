package autoschedule.utils.converters;

import javafx.util.StringConverter;

public class DoubleStringConverter extends StringConverter<Number> {
    @Override
    public String toString(Number number) {
        return number.toString();
    }

    @Override
    public Number fromString(String s) {
        if(s.length() != 0){
            return Double.parseDouble(s);
        }
        return 0D;
    }
}