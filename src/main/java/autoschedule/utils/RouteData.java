package autoschedule.utils;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class RouteData {

    private final SimpleIntegerProperty step;
    private final SimpleStringProperty article;
    private final SimpleIntegerProperty processingTime;

    public RouteData(int step, String article, int processingTime){
        this.step = new SimpleIntegerProperty(step);
        this.article = new SimpleStringProperty(article);
        this.processingTime = new SimpleIntegerProperty(processingTime);
    }

    public int getStep(){
        return step.get();
    }

    public String getArticle(){
        return article.get();
    }

    public int getProcessingTime(){
        return processingTime.get();
    }

}
