package autoschedule.utils.threading;

public class DaemonThread extends Thread {
	public DaemonThread(Runnable target) {
		super(target);
		this.setDaemon(true);
	}
}
