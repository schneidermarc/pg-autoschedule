package autoschedule.utils;

public class MathUtils {
    public static int max (int... values){
        if(values.length == 0)
            throw new IllegalArgumentException();

        int max = values[0];

        for(int value : values){
            max = Math.max(max,value);
        }

        return max;

    }
}
