package autoschedule.utils;

import autoschedule.control.DatabaseConnector;
import autoschedule.model.*;
import autoschedule.control.database.dbConnectorFactory;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ScheduleFactory {

    @Getter @Setter private static int MAX_BREAKDOWN_PROBABILITY = 5;
    @Getter @Setter private static int PROBABILITY_OF_STAGE_SKIPPING = 10; //Angabe in % aka 'PROBABILITY_OF_STAGE_SKIPPING' = 10 -> 10% Wahrscheinlichkeit pro Stage pro Job
    /**
     * Specifies the probability with which each machine is not qualified for the production of each specific article. (Only relevant if 'pRestrictedMachineQualifications' is true)
     */
    @Getter @Setter private static double PROBABILITY_OF_MACHINE_NOT_QUALIFYING = .2;
    @Getter @Setter private static int MAXIMUM_PRODUCTION_AMOUNT = 60;
    @Getter @Setter private static int MINIMUM_PRODUCTION_AMOUNT = 50;
    @Getter @Setter private static int ARTICLES_GENERATED_PER_STAGE = 50;
    /**
     * Specifies how long the production of each generated article on each machine takes at least. (In seconds)
     */
    @Getter @Setter private static int MIN_PROCESSING_TIME_PER_ARTICLE = 10;
    /**
     * Specifies how long the production of each generated article on each machine takes at most. (In seconds)
     */
    @Getter @Setter private static int MAX_PROCESSING_TIME_PER_ARTICLE = 120;

    private static final Random randomNumberGen = new Random();

    /**
     * Creates an empty schedule without machines,jobs or objectives for a given MachineEnvironment.
     * Year, Week and SchedulingDuration should still be assigned.
     * @param pYear The Year for the generated Schedule. (Has no direct impact on the data-generating-process)
     * @param pWeek The Week for the generated Schedule. (Has no direct impact on the data-generating-process)
     * @param pSchedulingDuration The Duration for the generated Schedule. (Has no direct impact on the data-generating-process)
     */
    public static Schedule createEmptySchedule(int pYear, int pWeek, int pSchedulingDuration, MachineEnvironment pMachineEnvironment){
        return new Schedule(pYear, pWeek, pSchedulingDuration, new ArrayList<>(), pMachineEnvironment, new ArrayList<>());
    }

    /**
     * Creates a new Schedule-object for the given MachineEnvironment and fills it with data from the database.
     * @param pYear The year in which the Schedule should start.
     * @param pStartWeek The year in which the Schedule should start.
     * @param pSchedulingDuration Defines the time-interval in which jobs should be extracted from the database. pSchedulingDuration= '1' says, that only 1 week is loaded.
     * @param pMachineEnvironment Defines the MachineEnvironment for this Schedule.
     */
    public static Schedule getScheduleFromDatabase(int pYear, int pStartWeek, int pSchedulingDuration, MachineEnvironment pMachineEnvironment, Set<Constraint> pActiveConstraints){
        DatabaseConnector dbConnector = dbConnectorFactory.createDatabaseConnector();
        dbConnector.establishConnectionWithDatabase(dbConnector.loadDBFile(DatabaseConnector.DEFAULT_DATABASE));
        return getScheduleFromDatabase(pYear,pStartWeek,pSchedulingDuration,pMachineEnvironment,pActiveConstraints,dbConnector);
    }

    /**
     * Creates a new Schedule-object for the given MachineEnvironment and fills it with data from the database.
     * @param pYear The year in which the Schedule should start.
     * @param pStartWeek The year in which the Schedule should start.
     * @param pSchedulingDuration Defines the time-interval in which jobs should be extracted from the database. pSchedulingDuration= '1' says, that only 1 week is loaded.
     * @param pMachineEnvironment Defines the MachineEnvironment for this Schedule.
     * @param pDatabaseName Name of the database, from which to load from
     */
    public static Schedule getScheduleFromDatabase(int pYear, int pStartWeek, int pSchedulingDuration, MachineEnvironment pMachineEnvironment, Set<Constraint> pActiveConstraints, String pDatabaseName){
        DatabaseConnector dbConnector = dbConnectorFactory.createDatabaseConnector(pDatabaseName);
        return getScheduleFromDatabase(pYear,pStartWeek,pSchedulingDuration,pMachineEnvironment,pActiveConstraints,dbConnector);
    }

    /**
     * Creates a new Schedule-object for the given MachineEnvironment and fills it with data from the database.
     * @param pYear The year in which the Schedule should start.
     * @param pStartWeek The year in which the Schedule should start.
     * @param pSchedulingDuration Defines the time-interval in which jobs should be extracted from the database. pSchedulingDuration= '1' says, that only 1 week is loaded.
     * @param pMachineEnvironment Defines the MachineEnvironment for this Schedule.
     * @param dbConnector dbConnector which is used for the connection.
     */
    public static Schedule getScheduleFromDatabase(int pYear, int pStartWeek, int pSchedulingDuration, MachineEnvironment pMachineEnvironment, Set<Constraint> pActiveConstraints, DatabaseConnector dbConnector){
        Map<String, Collection<Map.Entry<Machine, Integer>>> machineQualifications = dbConnector.getMachineQualifications(pActiveConstraints);
        Collection<Machine> allMachines = getAllMachinesFromMachineQualifications(machineQualifications);
        List<Job> allJobs = dbConnector.getJobsFromDatabase(pYear, pStartWeek, pSchedulingDuration, machineQualifications, pActiveConstraints, pMachineEnvironment);
        Schedule schedule = new Schedule(pYear, pStartWeek, pSchedulingDuration, allMachines, pMachineEnvironment, new ArrayList<>());
        schedule.getJobs().addAll(allJobs);
        schedule.setActiveConstraints(pActiveConstraints);
        //dbConnector.closeConnection();
        return schedule;
    }

    /**
     * Erzeugt ein neues Schedule fuer eine gegebene MachineEnvironment und fuellt es mit Testdaten.
     * Creates a new Schedule-object for the given MachineEnvironment and fills it with data from the database.
     * @param pYear The year in which the Schedule should start.
     * @param pWeek The year in which the Schedule should start.
     * @param pSchedulingDuration Defines the time-interval in which jobs should be extracted from the database. pSchedulingDuration= '1' says, that only 1 week is loaded.
     * @param pNumberOfStages Defines the Number of Stages in this Schedule.
     * @param pMachinesPerStage Defines the number of machines on each stage.
     * @param pNumberOfJobs Defines the number of all jobs that are generated.
     * @param pSkippingStagesInJobs Defines the possibility for jobs to skip stages. The Probability of skipping a stage lies in 'PROBABILITY_OF_STAGE_SKIPPING'. It's possible for jobs to skip multiple stages.
     * @param pRestrictedMachineQualifications Indicates whether  Machine qualifications should be regarded in this Schedule. When 'false' every Article can be processed on every Machine on the stage; When 'true' there might be  bei restrictions.
     * @param pMachineEnvironment Defines the MachineEnvironment for this Schedule.
     * @return An new Schedule, that was filled by the parameters given.
     */
    public static Schedule createTestSchedule(int pYear, int pWeek, int pSchedulingDuration, int pNumberOfStages, int pMachinesPerStage, int pNumberOfJobs, boolean pSkippingStagesInJobs, boolean pRestrictedMachineQualifications, MachineEnvironment pMachineEnvironment){
        if(pNumberOfStages == 0 || pMachinesPerStage == 0){
            return createEmptySchedule(pYear,pWeek,pSchedulingDuration,pMachineEnvironment);
        }

        pNumberOfStages = Math.max(pNumberOfStages,1); //Mindestens eine Stage
        pNumberOfStages = Math.min(pNumberOfStages,26); //Maximal 26 Stages
        Collection<Machine> machineList = generateMachineList(pNumberOfStages,pMachinesPerStage);
        List<Collection<Machine>> machinesByStages = divideMachinesByStages(machineList, pNumberOfStages, pMachinesPerStage);
        List<Job> jobList = generateJobList(pNumberOfStages,pNumberOfJobs,pSkippingStagesInJobs, pRestrictedMachineQualifications, machinesByStages);

        Schedule schedule = new Schedule(pYear, pWeek, pSchedulingDuration, machineList, pMachineEnvironment, new ArrayList<>());
        schedule.getJobs().addAll(jobList);
        return schedule;
    }

    private static Collection<Machine> generateMachineList(int pNumberOfStages, int pMachinesPerStage){
        Collection<Machine> machineList = new ArrayList<>();
        int machineCounter = 0;
        for(int stage = 0; stage < pNumberOfStages; stage++){
            for(int j = 0; j < pMachinesPerStage; j++){
                machineList.add(new Machine(getStageLetterByStageNumber(stage) + "m" + machineCounter));
                machineCounter++;
            }
        }
        return machineList;
    }

    private static List<Job> generateJobList(int pNumberOfStages, int pNumberOfJobs, boolean pSkippingStagesInJobs, boolean pRestrictedMachineQualifications, List<Collection<Machine>> pMachinesByStages){
        List<Job> jobList = new ArrayList<>();
        List<List<Article>> articleListsForEachStage = generateArticleListsForEachStage(pNumberOfStages,ARTICLES_GENERATED_PER_STAGE, pRestrictedMachineQualifications, pMachinesByStages);
        boolean minOneStageSkipped = !pSkippingStagesInJobs; // Variable wird nur false(und damit relevant) wenn pSkippingStagesInJobs true ist
        for(int jobCounter = 0; jobCounter < pNumberOfJobs; jobCounter++){
            List<Article> articleList = new ArrayList<>();
            for(int stage = 0; stage < pNumberOfStages; stage++){
                if((pSkippingStagesInJobs && randomNumberGen.nextInt(100) < PROBABILITY_OF_STAGE_SKIPPING || !minOneStageSkipped)
                        && !(stage == pNumberOfStages-1 && articleList.isEmpty())){ //Verhindert, dass ein Job ohne mindestens einen Artikel existieren kann.
                    minOneStageSkipped = true;  //Mindestens eine Stage wird im gesamten Schedule geskippt, damit das 'true' setzten von 'pSkippingStagesInJobs' beim testen deterministische Ergebnisse liefert
                    continue;   //Aktuelle Stage in diesem Job ueberspringen.
                }
                List<Article> articlesForThisStage = articleListsForEachStage.get(stage);
                Article article = articlesForThisStage.get(randomNumberGen.nextInt(articlesForThisStage.size()));
                articleList.add(article.copy());
            }
            jobList.add(new Job("job" + jobCounter, articleList, -1, 0, 1));
        }
        return jobList;
    }

    private static List<List<Article>> generateArticleListsForEachStage(int pNumberOfStages, int pNumberOfArticlesPerStage, boolean pRestrictedMachineQualifications, List<Collection<Machine>> pMachinesByStages){
        List<List<Article>> articleListArray = new ArrayList<>();
        for(int stage = 0; stage < pNumberOfStages; stage++){
            articleListArray.add(generateArticleListForOneStage(stage,pNumberOfArticlesPerStage, pRestrictedMachineQualifications, pMachinesByStages.get(stage)));
        }
        return articleListArray;
    }

    private static List<Article> generateArticleListForOneStage(int pStage, int pNumberOfArticles, boolean pRestrictedMachineQualifications, Collection<Machine> pMachines){
        List<Article> articleList = new ArrayList<>(pNumberOfArticles);
        for(int articleCounter = 0; articleCounter < pNumberOfArticles; articleCounter++){
            String articleName = getStageLetterByStageNumber(pStage) + articleCounter;
            Article article = new Article(articleName);
            int productionAmount = (int)(Math.random() * (MAXIMUM_PRODUCTION_AMOUNT - MINIMUM_PRODUCTION_AMOUNT + 1) + MINIMUM_PRODUCTION_AMOUNT);
            article.setProductionAmount(productionAmount);

            int maximumNumberOfNonQualifyingMachines = pMachines.size()-1;
            int numberOfNonQualifyingMachines = 0;
            for(Machine currentMachine : pMachines) {
                if(pRestrictedMachineQualifications
                        && ThreadLocalRandom.current().nextInt(0,100) < PROBABILITY_OF_MACHINE_NOT_QUALIFYING
                        && numberOfNonQualifyingMachines < maximumNumberOfNonQualifyingMachines){
                    numberOfNonQualifyingMachines++;
                    continue;
                }
                article.addQualifiedMachine(currentMachine, generateProcessingTime());
            }
            articleList.add(article);
        }
        return articleList;
    }

    private static List<Collection<Machine>> divideMachinesByStages(Collection<Machine> pMachineList, int pNumberOfStages, int pMachinesPerStage){
        List<Collection<Machine>> machinesByStages = new ArrayList<>();
        for(int stage = 0; stage < pNumberOfStages; stage++){
            machinesByStages.add(new ArrayList<>(pMachinesPerStage));
        }
        int machineCounter = 0;
        for(Machine currentMachine : pMachineList){
            machinesByStages.get(machineCounter / pMachinesPerStage).add(currentMachine);
            machineCounter++;
        }
        return machinesByStages;
    }

    private static String getStageLetterByStageNumber(int i) {
        i++; //Um auszugleichen, dass Stages in dieser Klasse mit Stage 0 anfangen statt Stage 1
        return i > 0 && i < 27 ? String.valueOf((char)(i + 64)) : "[UNDEFINED STAGE]";
    }

    private static int generateProcessingTime(){
        return ThreadLocalRandom.current().nextInt(MIN_PROCESSING_TIME_PER_ARTICLE, MAX_PROCESSING_TIME_PER_ARTICLE+1);
    }

    private static Collection<Machine> getAllMachinesFromMachineQualifications(Map<String, Collection<Map.Entry<Machine, Integer>>> pMachineQualifications){
        return pMachineQualifications.values().stream().flatMap(Collection::stream).map(Map.Entry::getKey).filter(distinctByKey(Machine::getName)).collect(Collectors.toList());
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
