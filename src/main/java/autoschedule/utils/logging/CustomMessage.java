package autoschedule.utils.logging;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.PrintWriter;
import java.io.StringWriter;

@SuppressWarnings("unused")
public class CustomMessage {

    /**
    calculates the errorCode for the given errors
     */
    public static String errorCode(String[] errors) {
        int arrowUnicode = 2192;
        char arrowChar = (char) Integer.parseInt(String.valueOf(arrowUnicode), 16);
        final String arrowString = String.valueOf(arrowChar);

        StringBuilder errorString = new StringBuilder("Fix the following error" + (errors.length == 1 ? "" : "s") + " to continue: \n");

        for (String error : errors) {
            errorString.append("     ").append(arrowString).append(" ").append(error).append("\n");
        }

        return errorString.toString();
    }

    /**
     * Shows Warning Popup for the given errors
     * @param title Title of the Popup
     * @param errors Errors to be displayed
     */
    public static void showCustomErrors(String title, String[] errors) {
        String errorCode = errorCode(errors);
        showWarning(title, errorCode);
    }

    /**
     * Shows Information Popup
     * @param title Title of the Popup
     * @param msg Message of the Popup
     */
    public static void showInformation(String title, String msg){
        Alert alert = new Alert(AlertType.INFORMATION);
        configureAlert(alert, title, msg);
        alert.showAndWait();
    }

    /**
     * Shows Warning Popup
     * @param title Title of the Popup
     * @param msg Message of the Popup
     */
    public static void showWarning(String title, String msg){
        Alert alert = new Alert(AlertType.WARNING);
        configureAlert(alert, title, msg);
        alert.showAndWait();
    }

    /**
     * Shows Error Popup
     * @param title Title of the Popup
     * @param msg Message of the Popup
     */
    public static void showError(String title, String msg){
        Alert alert = new Alert(AlertType.ERROR);
        configureAlert(alert, title, msg);
        alert.showAndWait();
    }

    /**
     * Shows Exception Popup
     * @param title Title of the Popup
     * @param msg Message of the Popup
     */
    public static void showException(String title, String msg, Exception ex){
        Alert alert = new Alert(AlertType.ERROR);
        configureAlert(alert, title, msg);
        setExceptionAlert(alert, ex);
        alert.showAndWait();
    }
    /**
     * Shows Error Popup
     * @param title Title of the Popup
     * @param msg Message of the Popup
     */
    public static void showFatalError(String title, String msg){
        Alert alert = new Alert(AlertType.ERROR);
        configureAlert(alert, title, msg);
        alert.showAndWait();
        System.exit(0);
    }

    /**
     * Shows Exception Popup
     * @param title Title of the Popup
     * @param msg Message of the Popup
     */
    public static void showFatalException(String title, String msg, Exception excep){
        Alert alert = new Alert(AlertType.ERROR);
        configureAlert(alert, title, msg);
        setExceptionAlert(alert, excep);
        alert.showAndWait();
        System.exit(0);
    }

    /**
     * Helper-Method, that adds expandable content in form of an exception-stack-trace to the alert-dialog
     * @param alert The alert-dialog to be modified
     * @param excep The Exception to be added
     */
    private static void setExceptionAlert(Alert alert, Exception excep){

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        excep.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);
    }

    /**
     * Adds the Title and message to the alert. HeaderContent is null
     * @param alert The Alert to be added
     * @param title The Title to be set
     * @param msg The Message to be set
     */
    private static void configureAlert(Alert alert, String title, String msg){
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(msg);
    }
}
