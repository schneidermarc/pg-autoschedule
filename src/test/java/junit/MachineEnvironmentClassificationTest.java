package junit;

import autoschedule.control.ClassificationController;
import autoschedule.model.MachineEnvironment;
import autoschedule.model.Schedule;
import autoschedule.utils.ScheduleFactory;
import org.junit.Test;

import static org.junit.Assert.*;

public class MachineEnvironmentClassificationTest {

    @Test
    public void testUnsupported(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.createEmptySchedule(2020,1,10, MachineEnvironment.FLOW_SHOP);
        assertSame("Schedule ohne Jobs nicht als UNSUPPORTED erkannt", classificationController.classify(schedule), MachineEnvironment.UNSUPPORTED);
        schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,2,5,false,false,MachineEnvironment.FLOW_SHOP);
        schedule.getJobs().get(0).getArticles().clear();
        assertSame("Schedule mit einem leeren Job nicht als UNSUPPORTED erkannt", classificationController.classify(schedule), MachineEnvironment.UNSUPPORTED);
        schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,2,5,false,false,MachineEnvironment.FLOW_SHOP);
        schedule.getJobs().get(0).getArticles().get(0).getMachineQualification().clear();
        assertSame("Schedule mit einem Artikel, der keine qualifizierten Maschinen hat, nicht als UNSUPPORTED erkannt", classificationController.classify(schedule), MachineEnvironment.UNSUPPORTED);
    }

    @Test
    public void testSingleMachine(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.createTestSchedule(2020,1,10,1,1,5,false,false,MachineEnvironment.UNSUPPORTED);
        assertSame("Schedule mit nur einer Maschine nicht als SINGLE_MACHINE erkannt", classificationController.classify(schedule), MachineEnvironment.SINGLE_MACHINE);
        schedule = ScheduleFactory.createTestSchedule(2020,1,10,1,2,5,false,false,MachineEnvironment.UNSUPPORTED);
        assertNotSame("Schedule mit mehreren Maschinen auf einer Stage fehlerhafterweise als SINGLE_MACHINE erkannt", classificationController.classify(schedule), MachineEnvironment.SINGLE_MACHINE);
        schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,1,5,false,false,MachineEnvironment.UNSUPPORTED);
        assertNotSame("Schedule mit mehreren Stages fehlerhafterweise als SINGLE_MACHINE erkannt", classificationController.classify(schedule), MachineEnvironment.SINGLE_MACHINE);
    }

    @Test
    public void testParallelMachine(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.createTestSchedule(2020,1,10,1,2,5,false,false,MachineEnvironment.UNSUPPORTED);
        assertSame("Schedule mit mehreren Maschinen auf einer Stage nicht als PARALLEL_MACHINES erkannt", classificationController.classify(schedule), MachineEnvironment.PARALLEL_MACHINES);
        schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,1,5,false,false,MachineEnvironment.UNSUPPORTED);
        assertNotSame("Schedule mit mehreren Stages fehlerhafterweise als PARALLEL_MACHINES erkannt", classificationController.classify(schedule), MachineEnvironment.PARALLEL_MACHINES);
    }

    @Test
    public void testFlowShop(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,1,5,false,false,MachineEnvironment.UNSUPPORTED);
        assertSame("Schedule mit je einer Maschinen auf mehreren Stages nicht als FLOW_SHOP erkannt", classificationController.classify(schedule), MachineEnvironment.FLOW_SHOP);
        schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,2,5,false,false,MachineEnvironment.UNSUPPORTED);
        assertNotSame("Schedule mit je mehreren Maschinen auf mehreren Stages fehlerhafterweise als FLOW_SHOP erkannt", classificationController.classify(schedule), MachineEnvironment.FLOW_SHOP);
        schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,1,100,true,false,MachineEnvironment.UNSUPPORTED);
        assertNotSame("Schedule mit uebersprungenen Stages in einzelnen Jobs fehlerhafterweise als FLOW_SHOP erkannt", classificationController.classify(schedule), MachineEnvironment.FLOW_SHOP);
    }

    @Test
    public void testFlexibleFlowShop(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,2,5,false,false,MachineEnvironment.UNSUPPORTED);
        assertSame("Schedule mit je mehreren Maschinen auf mehreren Stages nicht als FLEXIBLE_FLOW_SHOP erkannt", classificationController.classify(schedule), MachineEnvironment.FLEXIBLE_FLOW_SHOP);
        schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,2,100,true,false,MachineEnvironment.UNSUPPORTED);
        assertNotSame("Schedule mit uebersprungenen Stages in einzelnen Jobs fehlerhafterweise als FLEXIBLE_FLOW_SHOP erkannt", classificationController.classify(schedule), MachineEnvironment.FLEXIBLE_FLOW_SHOP);
    }

    @Test
    public void testJobShop(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,1,5,true,false,MachineEnvironment.UNSUPPORTED);
        assertSame("Schedule mit je einer Maschinen auf mehreren Stages nicht als JOB_SHOP erkannt", classificationController.classify(schedule), MachineEnvironment.JOB_SHOP);
        schedule = ScheduleFactory.createTestSchedule(2020,1,10,2,2,5,true,false,MachineEnvironment.UNSUPPORTED);
        assertNotSame("Schedule mit je mehreren Maschinen auf mehreren Stages fehlerhafterweise als JOB_SHOP erkannt", classificationController.classify(schedule), MachineEnvironment.JOB_SHOP);
    }
}
