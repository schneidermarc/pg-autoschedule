package junit;

import junit.algorithms.AnnotationTest;
import junit.machineEnviroments.DatabaseTypeTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author cstockhoff
 */
@RunWith( Suite.class )
@Suite.SuiteClasses( { AnnotationTest.class, DatabaseConnectionTest.class, DatabaseTypeTest.class, MachineEnvironmentClassificationTest.class, CLSTest.class } )
public class TestSuite {
}
