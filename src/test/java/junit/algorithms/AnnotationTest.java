package junit.algorithms;

import autoschedule.algorithms.constructive.GifflerThompson;
import autoschedule.algorithms.constructive.NEH;
import autoschedule.algorithms.constructive.PriorityRuleAlgorithm;
import autoschedule.algorithms.constructive.ga.GA;
import autoschedule.algorithms.iterative.localsearch.LocalSearch;
import autoschedule.algorithms.iterative.localsearch.TabuSearch;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.RandomDescent;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Shift;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.SteepestDescent;
import autoschedule.algorithms.iterative.localsearch.neighborhoods.Swap;
import autoschedule.algorithms.objectives.*;
import autoschedule.algorithms.priorityRules.LPT;
import autoschedule.algorithms.priorityRules.Random;
import autoschedule.algorithms.priorityRules.SPT;
import autoschedule.algorithms.utils.AlgorithmAnnotations;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author cstockhoff
 */
public class AnnotationTest {

	/**
	 * Check if classes have required Annotations
	 */
	@Test
	public void testClazzesAnnotated() {
		// constructive algorithms
		assertTrue( PriorityRuleAlgorithm.class.isAnnotationPresent( AlgorithmAnnotations.Constructive.class ) );
		assertTrue( GifflerThompson.class.isAnnotationPresent( AlgorithmAnnotations.Constructive.class ) );
		assertTrue( NEH.class.isAnnotationPresent( AlgorithmAnnotations.Constructive.class ) );
		assertTrue( GA.class.isAnnotationPresent( AlgorithmAnnotations.Constructive.class ) );

		// iterative algorithms
		assertTrue( LocalSearch.class.isAnnotationPresent( AlgorithmAnnotations.Iterative.class ) );
		assertTrue( TabuSearch.class.isAnnotationPresent( AlgorithmAnnotations.Iterative.class ) );

		// priority rules
		assertTrue( SPT.class.isAnnotationPresent( AlgorithmAnnotations.Priority.class ) );
		assertTrue( LPT.class.isAnnotationPresent( AlgorithmAnnotations.Priority.class ) );
		assertTrue( Random.class.isAnnotationPresent( AlgorithmAnnotations.Priority.class ) );

		// objective functions
		assertTrue( CBar.class.isAnnotationPresent( AlgorithmAnnotations.ObjectiveFunction.class ) );
		assertTrue( CMax.class.isAnnotationPresent( AlgorithmAnnotations.ObjectiveFunction.class ) );
		assertTrue( FMax.class.isAnnotationPresent( AlgorithmAnnotations.ObjectiveFunction.class ) );
		assertTrue( LMax.class.isAnnotationPresent( AlgorithmAnnotations.ObjectiveFunction.class ) );
		assertTrue( UBar.class.isAnnotationPresent( AlgorithmAnnotations.ObjectiveFunction.class ) );

		// neighborhoods
		assertTrue( RandomDescent.class.isAnnotationPresent( AlgorithmAnnotations.Neighborhood.class ) );
		assertTrue( SteepestDescent.class.isAnnotationPresent( AlgorithmAnnotations.Neighborhood.class ) );

		// permutation strategies
		assertTrue( Shift.class.isAnnotationPresent( AlgorithmAnnotations.Move.class ) );
		assertTrue( Swap.class.isAnnotationPresent( AlgorithmAnnotations.Move.class ) );
	}

	/**
	 * Test if collected constructive algorithms contains specific elements
	 */
	@Test
	public void testConstructiveAlgorithms() {
		assertTrue( AlgorithmAnnotations.constructiveAlgorithms.contains( PriorityRuleAlgorithm.class ) );
		assertTrue( AlgorithmAnnotations.constructiveAlgorithms.contains( GifflerThompson.class ) );
		assertTrue( AlgorithmAnnotations.constructiveAlgorithms.contains( NEH.class ) );
		assertTrue( AlgorithmAnnotations.constructiveAlgorithms.contains( GA.class ) );
	}

	/**
	 * Test if collected constructive algorithms contains specific elements
	 */
	@Test
	public void testIterativeAlgorithms() {
		assertTrue( AlgorithmAnnotations.iterativeAlgorithms.contains( LocalSearch.class ) );
		assertTrue( AlgorithmAnnotations.iterativeAlgorithms.contains( TabuSearch.class ) );
	}

	/**
	 * Test if collected priority rules contains specific elements
	 */
	@Test
	public void testPriorityRules() {
		assertTrue( AlgorithmAnnotations.priorityRules.contains( SPT.class ) );
		assertTrue( AlgorithmAnnotations.priorityRules.contains( LPT.class ) );
		assertTrue( AlgorithmAnnotations.priorityRules.contains( Random.class ) );
	}

	/**
	 * Test if collected objective functions contains specific elements
	 */
	@Test
	public void testObjectiveFunctions() {
		assertTrue( AlgorithmAnnotations.objectiveFunctions.contains( CBar.class ) );
		assertTrue( AlgorithmAnnotations.objectiveFunctions.contains( CMax.class ) );
		assertTrue( AlgorithmAnnotations.objectiveFunctions.contains( FMax.class ) );
		assertTrue( AlgorithmAnnotations.objectiveFunctions.contains( LMax.class ) );
		assertTrue( AlgorithmAnnotations.objectiveFunctions.contains( UBar.class ) );
	}

	/**
	 * Test if collected neighborhood functions contains specific elements
	 */
	@Test
	public void testNeighborhoods() {
		assertTrue( AlgorithmAnnotations.neighborhoods.contains( SteepestDescent.class ) );
		assertTrue( AlgorithmAnnotations.neighborhoods.contains( RandomDescent.class ) );
	}

	/**
	 * Test if collected neighborhood functions contains specific elements
	 */
	@Test
	public void testPermutationStrategies() {
		assertTrue( AlgorithmAnnotations.moveStrategies.contains( Shift.class ) );
		assertTrue( AlgorithmAnnotations.moveStrategies.contains( Swap.class ) );
	}
}
