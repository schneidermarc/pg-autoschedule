package junit;

import autoschedule.control.DatabaseConnector;
import autoschedule.control.database.dbConnectorFactory;
import autoschedule.utils.logging.Logger;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class DatabaseConnectionTest {
    @Test
    public void conTest() throws SQLException {
        DatabaseConnector dc = dbConnectorFactory.createDatabaseConnector();

        Statement stmt = dc.getDefaultConnection().createStatement();
        String sql = "SELECT name FROM sqlite_master WHERE type='table'";
        ResultSet rs = stmt.executeQuery(sql);

        while ( rs.next() ) {

            String  name = rs.getString("name");


            Logger.message( "Table name: " + name );

        }
        rs.close();
        stmt.close();
        //dc.closeConnection();
    }

    @Test
    public void dbYearAndWeekTest() {
        DatabaseConnector dc = dbConnectorFactory.createDatabaseConnector();
        List<Integer> years = dc.getYearsFromDemand();
        for (int year :years){
            Logger.message(year);
        }
        List<Integer> weeks = dc.getWeeksFromDemand(years.get(2));
        for (int week :weeks){
            Logger.message(week);
        }
    }

    @Test
    public void dbProcessingMedian(){
        DatabaseConnector dc = dbConnectorFactory.createDatabaseConnector();
        Logger.message("Median von A968001-ZX: " + dc.getProcessingMedian("A968001-ZX","A9"));
    }
}
