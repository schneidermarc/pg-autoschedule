package junit;

import autoschedule.Scheduler;
import autoschedule.algorithms.Algorithm;
import autoschedule.algorithms.constructive.PriorityRuleAlgorithm;
import autoschedule.algorithms.priorityRules.Random;
import autoschedule.algorithms.priorityRules.SPT;
import autoschedule.algorithms.utils.TreeNode;
import autoschedule.control.SchedulingController;
import autoschedule.model.MachineEnvironment;
import autoschedule.model.Schedule;
import autoschedule.utils.ScheduleFactory;
import autoschedule.view.viewmodel.ViewModel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CLSTest {

	private SchedulingController schedulingController;

	@Before
	public void setUp() {
		int year = 0;
		int week = 0;
		int duration = 0;
		int numberOfStages = 0;
		int machinesPerStage = 0;
		int numberOfJobs = 0;
		boolean skippingJobs = false;
		boolean restrictedMachineQualifications = false;
		MachineEnvironment machineEnvironment = MachineEnvironment.FLOW_SHOP;

		// Generate template Schedule with default values
		Schedule templateSchedule = ScheduleFactory.createTestSchedule(year, week, duration, numberOfStages, machinesPerStage, numberOfJobs, skippingJobs, restrictedMachineQualifications, machineEnvironment);

		// Initialize SchedulingController and set templateSchedule
		this.schedulingController = new SchedulingController(new ViewModel());
		this.schedulingController.setTemplateSchedule(templateSchedule);
	}

	@Test
	public void testCLSNInvalidIterationCount() {
		List<TreeNode> roots = new ArrayList<>();
		TreeNode treeNode1 = new TreeNode( PriorityRuleAlgorithm.class, null );
		new TreeNode( Random.class, treeNode1 );
		new TreeNode( SPT.class, treeNode1 );
		Map<String, List<Algorithm>> result;

		// Start CLS and generate solutions
		/* @formatter:off */
		result = Scheduler.run(
				this.schedulingController.getTemplateSchedule().getMachineEnvironment(),
				-1,
				false,
				roots,
				null );
		/* @formatter:on */
		assertEquals(0, result.size());

		// Start CLS and generate solutions
		/* @formatter:off */
		result = Scheduler.run(
				this.schedulingController.getTemplateSchedule().getMachineEnvironment(),
				20,
				false,
				roots,
				null);
		/* @formatter:on */
		assertEquals(0, result.size());
	}

	@Test
	public void testCLSNullTree() {
		// Start CLS and generate solutions
		/* @formatter:off */
		Map<String, List<Algorithm>> result = Scheduler.run(
				this.schedulingController.getTemplateSchedule().getMachineEnvironment(),
				0,
				false,
				null,
				null );
		/* @formatter:on */

		assertEquals(0, result.size());
	}
}
