package junit.machineEnviroments;

import autoschedule.control.ClassificationController;
import autoschedule.model.MachineEnvironment;
import autoschedule.model.Schedule;
import autoschedule.utils.ScheduleFactory;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.Assert.assertSame;

public class DatabaseTypeTest {


    @Test
    public void testSingleMachine(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.getScheduleFromDatabase(2017, 16, 1, MachineEnvironment.SINGLE_MACHINE, new HashSet<>(), "dbSingleMachine.db");
        assertSame("Schedule wird nicht als Single Machine erkannt", MachineEnvironment.SINGLE_MACHINE,classificationController.classify(schedule));
    }

    @Test
    public void testParallelMachine(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.getScheduleFromDatabase(2017, 16, 1, MachineEnvironment.PARALLEL_MACHINES, new HashSet<>(), "dbParallelMachine.db");
        assertSame("Schedule wird nicht als Parallel Machine erkannt",MachineEnvironment.PARALLEL_MACHINES, classificationController.classify(schedule));
    }

    @Test
    public void testJobShop(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.getScheduleFromDatabase(2020, 1, 1, MachineEnvironment.JOB_SHOP, new HashSet<>(), "dbJobShopNeu.db");
        assertSame("Schedule wird nicht als Job Shop erkannt", MachineEnvironment.JOB_SHOP,classificationController.classify(schedule));
    }

    @Test
    public void testFlowShop(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.getScheduleFromDatabase(2017, 16, 1, MachineEnvironment.FLOW_SHOP, new HashSet<>(), "dbFlowShop.db");
        assertSame("Schedule wird nicht als Flow Shop erkannt", MachineEnvironment.FLOW_SHOP,classificationController.classify(schedule));
    }

    @Test
    public void testFlexibleFlowShop(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.getScheduleFromDatabase(2017, 16, 1, MachineEnvironment.FLEXIBLE_FLOW_SHOP, new HashSet<>(), "dbFlexibleFlowShop.db");
        assertSame("Schedule wird nicht als Flexible Flow Shop erkannt",MachineEnvironment.FLEXIBLE_FLOW_SHOP,classificationController.classify(schedule));
    }

    @Test
    public void testDefaultDb(){
        ClassificationController classificationController = new ClassificationController();
        Schedule schedule = ScheduleFactory.getScheduleFromDatabase(2017, 16, 1, MachineEnvironment.FLEXIBLE_FLOW_SHOP, new HashSet<>());
        assertSame("Schedule wird nicht als Flexible Flow Shop erkannt",MachineEnvironment.FLEXIBLE_FLOW_SHOP,classificationController.classify(schedule));
    }


}
